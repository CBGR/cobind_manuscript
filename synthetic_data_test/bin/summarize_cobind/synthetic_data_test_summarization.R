#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "dplyr",
                        "ggplot2",
                        "optparse",
                        "RColorBrewer",
                        "ggrepel")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only=TRUE, quietly = T))
}

#############
## Options ##
#############

options(stringsAsFactors = FALSE)
options(scipen=999)

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(
    c("-i", "--motif_folder"), 
    type = "character", 
    default = NULL,
    help = "Path to the folder with synthetic data test results for a motif. (Mandatory) ", 
    metavar = "character"),
  make_option(
    c("-s", "--range_number_of_sequences"), 
    type    = "character", 
    default = "800,1000,4000,8000,10000,40000,80000",
    help    = "Comma-separated list of number of sequences. Default: 800,1000,4000,8000,10000,40000,80000 ", 
    metavar = "character"),
  make_option(
    c("-p", "--range_of_proportions"), 
    type    = "character", 
    default = "0.005,0.01,0.03,0.05,0.1",
    help    = "Comma-separated list of proportions. Default: 0.005,0.01,0.03,0.05,0.1 ", 
    metavar = "character")

);

message("; Reading arguments from command line.")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

########################
## Set variable names ##
########################

MOTIF_FOLDER <- opt$motif_folder
SEQ_RANGE    <- opt$range_number_of_sequences
PROP_RANGE   <- opt$range_of_proportions

## Debug
# MOTIF_FOLDER <- "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/TBX21-MA0690.1"
# SEQ_RANGE <- "800,1000,4000,8000,10000,40000,80000"
# PROP_RANGE <- "0.005,0.01,0.03,0.05,0.1"

###########################
## Setting plotting theme ##
############################
message("; Setting ggplot theme.")

ggplot2::theme_set(
  theme_classic() +
  theme(
    plot.title = element_text(size = 16),
    axis.title = element_text(
      size = 16, 
      colour = "black"),
    axis.text = element_text(
      size = 16, 
      colour = "black"),
    plot.background = element_blank(),
    legend.position = "left",
    legend.box = "vertical",
    legend.direction = "vertical",
    legend.text = element_text(size = 14),
    legend.title = element_text(size = 14),
    legend.justification = "left",
    panel.border = element_rect(
      colour = "black", 
      fill = NA),
    plot.caption = element_text(
      size = 12,
      hjust = 0))
)

###############
## Functions ##
###############
  # dataframe_for_plot <- tomtom_results_df
  # seq_range <- paste0(sort(all_seq_range), " sequences")
  # prop_range <- prop_range
plot_test_summary <- function(
  dataframe_for_plot,
  seq_range,
  prop_range
) {
  synthetic_heatmap <- ggplot(
      dataframe_for_plot,
      aes(
        x = as.factor(nb_components),
        y = as.factor(proportion),
        colour = as.numeric(colour_value),
        size = as.numeric(nb_match_no_match_proportion))) +
    geom_point() +
    ggh4x::facet_nested_wrap(
      vars(
        factor(nb_seqs_label, levels = seq_range),
        factor(variable, levels = c("F1", "MCC"))),
      ncol = (round(length(seq_range) / 4, digits = 0) * 2),
      nest_line = element_line(linetype = 1),
      resect = unit(0.1, "mm"),
      strip = ggh4x::strip_nested(
        text_x = ggh4x::elem_list_text(size = c(13, rep(NA, 100))),
        clip = "off",
        by_layer_x = TRUE,
        background_x = element_rect(
          colour = "#ffffff00",
          fill = "#dfdede3d")),
      drop = FALSE) +
    scale_colour_gradient2(
      high = "#b2182b",
      mid = "#d1e5f0",
      low = "#053061",
      na.value = "#464444",
      limits = c(unique(ifelse(any(dataframe_for_plot$colour_value[!is.na(dataframe_for_plot$colour_value)] < 0), yes = -1, no = 0), 0), 1)) +
    scale_size_continuous(
          limits = c(0, 1),
          breaks = c(0, 0.5, 1)) +
    scale_y_discrete(limits = as.factor(prop_range)) +
    geom_text(
      aes(label = label_for_motif_nb),
      size = 7,
      nudge_y = 0.15,
      na.rm = TRUE,
      colour = "black") +
    labs(
      title = paste0(" Injected motif: ", tf_name),
      y = "Proportion of sequences with co-binding motif",
      x = "Number of components",
      colour = "MCC/F1 score",
      size = "Proportion of\nmatching motifs") +
    guides(
      shape = guide_legend(
        override.aes = list(
          size = 8),
        direction = "horizontal",
        title.position = "top",
        label.position = "bottom",
        label.hjust = 1,
        label.vjust = 0.5,
        order = 1)) +
    theme(
      strip.background = element_blank(),
      legend.title.align = 0.5,
      ggh4x.facet.nestline = element_line(colour = "black"),
      legend.position = c(0.9, 0),
      legend.justification = c("right", "bottom"),
      legend.box = "horizontal",
      axis.text.x = element_text(
        angle = 90,
        hjust = 1,
        vjust = 0.5))
  
  return(synthetic_heatmap)
}

###############################
## Creating output directory ##
###############################
message("; Creating output folder.")

OUTPUT_DIR <- file.path(MOTIF_FOLDER, "cobind_synthetic_run_summary")
dir.create(OUTPUT_DIR, showWarnings = F, recursive = T)

###########################################
## Reading tomtom motif comparison table ##
###########################################
message("; Reading motif comparison table.")

tomtom_results_df <- data.table::fread(
  file.path(MOTIF_FOLDER, "cobind_synthetic_run_summary", "all_discovered_vs_injected_tomtom.txt"),
  header = TRUE,
  sep = "\t",
  fill = TRUE)

tomtom_results_df <- tomtom_results_df %>%
  dplyr::filter(Target_ID != "")

colnames(tomtom_results_df) <- c(
  "query", "target", "offset",
  "p_value", "E_value", "q_value",
  "overlap", "query_consensus", "target_consensus", "orientation")

tomtom_results_df <- tomtom_results_df %>%
  select(target, p_value, E_value, q_value, overlap, orientation)

###################################
## Building a table with results ##
###################################
message("; Building summary table.")

tomtom_results_df <- tomtom_results_df %>%
  dplyr::mutate(name = target) %>%
  tidyr::separate(
    name,
    c("nb_seqs", "proportion", "flank_size", "motif", "flank", "nb_components", "component", "stats"),
    sep = "_") %>%
  tidyr::separate(
        stats,
        c("TP", "FN", "FP", "TN"),
        sep = "-") %>%
  dplyr::mutate(
        TP = as.numeric(gsub(TP, pattern = "TP:", replacement = "")),
        FN = as.numeric(gsub(FN, pattern = "FN:", replacement = "")),
        FP = as.numeric(gsub(FP, pattern = "FP:", replacement = "")),
        TN = as.numeric(gsub(TN, pattern = "TN:", replacement = ""))) %>%
  mutate(real_nb_sites = as.numeric(nb_seqs) * as.numeric(proportion)) %>%
  mutate(match_with_injected = ifelse(p_value < 0.05, yes = "match", no = "no-match")) %>%
  group_by(nb_seqs, proportion, flank_size, nb_components) %>%
  mutate(nb_motifs_per_experiment = length(target)) %>%
  ungroup() %>%
  group_by(nb_seqs, proportion, flank_size, nb_components, match_with_injected) %>%
  mutate(nb_of_match_no_match_per_experiment = length(target)) %>%
  mutate(nb_match_no_match_proportion = as.numeric(nb_of_match_no_match_per_experiment / nb_motifs_per_experiment)) %>%
  ungroup() %>%
  transform(
    nb_seqs = as.numeric(nb_seqs),
    nb_components = as.numeric(nb_components),
    proportion = as.numeric(proportion))

tomtom_results_df <- tomtom_results_df %>%
  dplyr::mutate(
    sensitivity = TP / (TP + FN),
    precision   = TP / (TP + FP),
    specificity = TN / (TN + FP),
    MCC         = (TP * TN - FP * FN) / sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN)),
    F1          = 2 * (precision * sensitivity) / (precision + sensitivity))

## TF name:
tf_name <- unique(tomtom_results_df$motif)

## Exporting full table:
message("; Exporting summary table.") 
summary_file_path <- file.path(OUTPUT_DIR, paste0("synthetic_data_test_summary_table_", tf_name, ".txt"))
fwrite(tomtom_results_df, file = summary_file_path, col.names = TRUE, sep = "\t")

## Filtering out no-match motifs:
tomtom_results_df <- tomtom_results_df %>%
  dplyr::filter(match_with_injected == "match") %>%
  dplyr::arrange(nb_seqs, proportion, flank_size, nb_components) %>%
  dplyr::group_by(nb_seqs, proportion, flank_size, nb_components) %>%
  dplyr::top_n(1, wt = F1) %>%
  dplyr::top_n(1, wt = MCC) %>%
  dplyr::sample_n(1) %>%
  dplyr::mutate(nb_of_motifs_after_selection = length(target)) %>%
  ungroup()

duplicate_motifs <- tomtom_results_df %>%
  filter(nb_of_motifs_after_selection != 1)
if (nrow(duplicate_motifs) > 0) {
  message("; There are duplicated motifs for a single experiment! Revise!")
}

##################################################
## Box plot with p-values from motif comparison ##
##################################################
message("; Drawing p-value boxplot for ", tf_name, " motif.")

synthetic_plot <- ggplot(
    data = tomtom_results_df, 
    aes(
      x = as.factor(nb_components),
      y = -log10(p_value))) +
  geom_boxplot() +
  geom_point(aes(colour = as.factor(proportion)), na.rm = TRUE) +
  facet_grid(. ~ nb_seqs) +
  coord_equal() +
  scale_color_brewer(palette = "RdPu") +
  geom_hline(
    aes(yintercept = -log10(0.05),
    linetype = "p-value = 0.05"),
    colour = "#49006a",
    size = 1) +
  scale_linetype_manual(
    name = "",
    values = c(1), 
    guide = guide_legend(override.aes = list(color = "#49006a"))) +
  labs(
    title = tf_name,
    x = "Number of components",
    y = "-log10(p-value)",
    color = "Motif proportion")

message("; Exporting summary plot.")                      
plot_file_path <- file.path(OUTPUT_DIR, paste0("motif_comparison_p_values.pdf"))
ggsave(
  filename = plot_file_path,
  plot = synthetic_plot,
  width = 31,
  height = 10,
  limitsize = FALSE)

plot_file_path <- file.path(OUTPUT_DIR, paste0("motif_comparison_p_values.png"))
ggsave(
  filename = plot_file_path,
  plot = synthetic_plot,
  width = 31,
  height = 10,
  limitsize = FALSE)

#############             
## Heatmap ##
#############

message("; Drawing summary heatmap.")

## Preparing the data frame:
tomtom_results_df <- tomtom_results_df %>%
  tidyr::pivot_longer(
    cols = c(F1, MCC),
    values_to = "colour_value",
    names_to = "variable")

## Range of number of sequences:
all_seq_range <- as.numeric(unlist(strsplit(SEQ_RANGE, split = ",")))
median_nb_seq <- median(all_seq_range)

## Proportion range:
prop_range <- as.numeric(unlist(strsplit(PROP_RANGE, split = ",")))

## Formatting to get labels:
tomtom_results_df <- tomtom_results_df %>%
  dplyr::mutate(nb_seqs_label = paste0(nb_seqs, " sequences")) %>%
  dplyr::mutate(nb_seqs = as.numeric(nb_seqs)) %>%
  dplyr::mutate(label_for_motif_nb = ifelse(nb_of_match_no_match_per_experiment > 1, yes = "*", no = NA))

# ## Two parts data:
# first_part_plot_df <- subset(tomtom_results_df, as.numeric(nb_seqs) <= median_nb_seq)
# first_part_seq_range   <- paste0(sort(unique(all_seq_range[all_seq_range <= median_nb_seq])), " sequences")

# second_part_plot_df <- subset(tomtom_results_df, as.numeric(nb_seqs) > median_nb_seq)
# second_part_seq_range   <- paste0(sort(unique(all_seq_range[all_seq_range > median_nb_seq])), " sequences")

## Plotting:

# synthetic_heatmap_part1 <- plot_test_summary(
#   first_part_plot_df,
#   first_part_seq_range,
#   prop_range
# )

# nb_of_facets <- length(first_part_seq_range) * 2

# message("; Exporting summary heatmap.")                      
# plot_file_path <- file.path(OUTPUT_DIR, paste0("synthetic_data_test_summary_plot_", tf_name, "_PART1.pdf"))
# ggsave(
#   filename = plot_file_path,
#   plot = synthetic_heatmap_part1,
#   width = (3 * nb_of_facets + 4) * 1.35,
#   height = 7,
#   limitsize = FALSE)

# plot_file_path <- file.path(OUTPUT_DIR, paste0("synthetic_data_test_summary_plot_", tf_name, "_PART1.png"))
# ggsave(
#   filename = plot_file_path,
#   plot = synthetic_heatmap_part1,
#   width = (3 * nb_of_facets + 4) * 1.35,
#   height = 7,
#   limitsize = FALSE)

# synthetic_heatmap_part2 <- plot_test_summary(
#   second_part_plot_df,
#   second_part_seq_range,
#   prop_range
# )

# nb_of_facets <- length(second_part_seq_range) * 2

# message("; Exporting summary heatmap.")                      
# plot_file_path <- file.path(OUTPUT_DIR, paste0("synthetic_data_test_summary_plot_", tf_name, "_PART2.pdf"))
# ggsave(
#   filename = plot_file_path,
#   plot = synthetic_heatmap_part2,
#   width = (3 * nb_of_facets + 4) * 1.35,
#   height = 7,
#   limitsize = FALSE)

# plot_file_path <- file.path(OUTPUT_DIR, paste0("synthetic_data_test_summary_plot_", tf_name, "_PART2.png"))
# ggsave(
#   filename = plot_file_path,
#   plot = synthetic_heatmap_part2,
#   width = (3 * nb_of_facets + 4) * 1.35,
#   height = 7,
#   limitsize = FALSE)

##### ALL ######
synthetic_heatmap <- plot_test_summary(
  tomtom_results_df,
  paste0(sort(all_seq_range), " sequences"),
  prop_range
)

message("; Exporting summary heatmap.")                      
plot_file_path <- file.path(OUTPUT_DIR, paste0("synthetic_data_test_summary_plot.pdf"))
ggsave(
  filename = plot_file_path,
  plot = synthetic_heatmap,
  width = (length(all_seq_range) / 7.5 * length(unique(tomtom_results_df$nb_components))),
  height = length(all_seq_range) * 2.5,
  limitsize = FALSE)

plot_file_path <- file.path(OUTPUT_DIR, paste0("synthetic_data_test_summary_plot.png"))
ggsave(
  filename = plot_file_path,
  plot = synthetic_heatmap,
  width = (length(all_seq_range) / 7.5 * length(unique(tomtom_results_df$nb_components))),
  height = length(all_seq_range) * 2.5,
  limitsize = FALSE)

# plot_file_path <- file.path(OUTPUT_DIR, paste0("synthetic_data_test_summary_plot_", tf_name, ".svg"))
# ggsave(
#   filename = plot_file_path,
#   plot = synthetic_heatmap,
#   width = (length(all_seq_range) / 7.5 * length(unique(tomtom_results_df$nb_components))),
#   height = length(all_seq_range) * 2.5,
#   device = 'svg',
#   limitsize = FALSE)

###################
## End of script ##
###################