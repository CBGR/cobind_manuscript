AC  injected_MEIS1-MA0498.2
XX
ID  injected_MEIS1-MA0498.2
XX
DE  MA0498.2 MEIS1; from JASPAR
P0       a     c     g     t
1    23727 18797  9714 24921
2     8690  5575  3174 77159
3        0  1249 77159     0
4    77159  2903   672     0
5        0 77159     0  1155
6    77159     0  9263  3861
7    17224 14864 29965 15106
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
