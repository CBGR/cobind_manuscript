# Analysis of single-molecule footprinting data #

These are instructions on how to run the analysis of single-molecule footprinting (SMF data). Simillarly to DHS analysis, we used regions with discovered co-binders withing SMF footprints. In this case we matched and analysed only datasets from mouse embryonic stem cells (mESCs), since SMF data comes from those cells. The data was downloaded from ArrayExpress with accession numbers E-MTAB-9033 and E-MTAB-9123. Processing of the raw SMF data was performed following the [protocol](https://www.nature.com/articles/s41596-021-00630-1).

This analysis includes several main steps:
1. Running COBIND analysis only on datasets from mESCs.
2. Processing SMF raw data and analysing regions with co-binders. All this analysis is done using a specific Snakefile `Snakefile_sm` and its associated configuration file `config_files/config_sm.yaml`. Download the raw SMF data and specify the path in the coniguration file.

## 1. Discovering co-binders in mESCs ##

We have selected those datasets that come from some type of mouse ESCs and placed them in a separate data directory, which should be dowloaded together with all other data from a ZENODO repository (doi: 10.5281/zenodo.7681483).

For this analysis we used the threshold that was produced with commands described in `random_analysis.md`. If you reproduced the file with thresholds, it should be in this path: `COBIND_results/random/for_threshold/thresholding_gini_results.txt` or your can use already provided output file `run_analysis/thresholding_gini_results.txt`.

```bash
## Path to a file with thresholds:
gini_thr_file="run_analysis/thresholding_gini_results.txt"

## Queries:
title_query="^title.*"
gini_query="^GINI_THRESHOLD:.*"
tf_groups_query="^TF_FAMILIES.*"
data_query="^BEDS_folder.*"

## Groups to itterate:
groups_array=( individual )

## Reading the variables and running analysis:
cat "$gini_thr_file" | sed 1d | grep "0.99" | grep "MOUSE" | while read species cutoff gini rdata ; do

  echo "; Analysis for: $species with $cutoff cutoff ( $gini )."

  if [ "$species" == "MOUSE" ] ; then
    assembly="mouse_mm10"
  else
    echo "Non valid species added to the list."
  fi ;

  echo "; Assembly: $assembly"

  ## Converting species name from lowercase to uppercase:
  species_low=$(echo "$species" | tr '[:upper:]' '[:lower:]')
  echo "; Species: "$species_low""

  ## Creating config:
  config="config_files/config_""$assembly"".yaml"
  echo "; Configuration file: "$config""

  ## Gini:
  gini_thr="GINI_THRESHOLD: ""$gini"""
  echo "; Gini: "$gini""
  sed -i 's,'"$gini_query"','"$gini_thr"',' "$config"

  ## Data:
  used_data="BEDS_folder: data/unibind_21/ub21_""$species_low""_esc"
  echo "; Used data: "$used_data""
  sed -i 's,'"$data_query"','"$used_data"',' "$config"

  ## Running for individual TFs and families:
  for group in "${groups_array[@]}" ; do

    echo "; Grouping: "$group""

    ## Title:
    title="title: COBIND_results/""$group""/""$species""_ESC_""$cutoff"""
    echo "; "$title""
    sed -i 's,'"$title_query"','"$title"',' "$config"

    ## Grouping file:
    tf_group="TF_FAMILIES: data/tfs_groups/tf_""$group""_""$species_low"".txt"
    echo "; "$tf_group""
    sed -i 's,'"$tf_groups_query"','"$tf_group"',' "$config"

    cat $config

    ## Launching snakemake command:
    echo "snakemake --config analysis_id="$assembly"  --cores 10 -k"
    snakemake --config analysis_id="$assembly"  --cores 10 -k

  done ;
done ;
```

## 2. Processing SMF data and analysing co-binder regions ##

For this analysis we source the [SingleMoleculeFootprinting](https://github.com/Krebslabrep/SingleMoleculeFootprinting) package functions (development version). These are included in this repository.

```bash
echo "; Running SMF analysis with COBIND results."

echo "snakemake -s Snakefile_sm --cores 10 -k"
snakemake -s Snakefile_sm --cores 10 -k
```
