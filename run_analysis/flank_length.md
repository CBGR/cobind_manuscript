# Discovering co-binder in difference size flanks #

These are commands that were used to run COBIND pipeline with iterations of different flank sizes. We analyzed GATA and SOX transcription factors in human and mouse genomes (hg38 and mm10, respectively). This code adjusts the `config.yaml` files for respective species and runs iterations of the pipeline.

We use flanks of sizes 30, 50, 100 and 150 bp. We use default Gini coefficient threshold (0.5).

To run this, you need to be in the `cobind_manunscript` directory and change the path to the output directory or put `.` to have results exported in the current directory.

## MOUSE ##

```bash
## OUTPUT_DIRECTORY is the only variable to change ##
OUTPUT_DIRECTORY="COBIND_results/flank_size"

gini_gata_thr=0.5

flank_array=( 30 50 100 150 )
title_query="^title.*"
flank_query="^TFBS_FLANK:.*"
gini_query="^GINI_THRESHOLD:.*"
data_query="^BEDS_folder:.*"
tf_groups_query="^TF_FAMILIES.*"

for flank_size in "${flank_array[@]}" ; do

  echo "; Analysis for: "$flank_size""

  ## Config:
  config_mouse="config_files/config_mouse_mm10_regular.yaml"
  echo "; Configuration file: "$config_mouse""

  ## Title:
  title="title: ""$OUTPUT_DIRECTORY""/""$flank_size""_flank_MOUSE_GATA_SOX_""$gini_gata_thr"""
  echo "; "$title""
  sed -i 's,'"$title_query"','"$title"',' "$config_mouse"

  ## Flank size:
  flank="TFBS_FLANK: ""$flank_size"""
  echo "; Flank size: "$flank_size""
  sed -i 's,'"$flank_query"','"$flank"',' "$config_mouse"

  ## Gini threshold:
  gini_thr="GINI_THRESHOLD: ""$gini_gata_thr"""
  echo "; Gini: "$gini_gata_thr""
  sed -i 's,'"$gini_query"','"$gini_thr"',' "$config_mouse"

  ## Data folder:
  data="BEDS_folder: data/unibind_21/ub21_mouse_gata_sox"
  echo "; "$data""
  sed -i 's,'"$data_query"','"$data"',' "$config_mouse"

  ## Grouping file:
  mouse_tf_group="TF_FAMILIES: data/tfs_groups/tf_individual_mouse.txt"
  echo "; "$mouse_tf_group""
  sed -i 's,'"$tf_groups_query"','"$mouse_tf_group"',' "$config_mouse"

  ## Launching snakemake command:
  echo "snakemake --config analysis_id=mouse_mm10_regular  --cores 50"
  snakemake --config analysis_id=mouse_mm10_regular  --cores 50

done ;
```

## HUMAN ##

```bash
OUTPUT_DIRECTORY="COBIND_results/flank_size"

gini_gata_thr=0.5

flank_array=( 30 50 100 150 )
title_query="^title.*"
flank_query="^TFBS_FLANK:.*"
gini_query="^GINI_THRESHOLD:.*"
data_query="^BEDS_folder:.*"
tf_groups_query="^TF_FAMILIES.*"

for flank_size in "${flank_array[@]}" ; do

  echo "; Analysis for: "$flank_size""

  ## Config:
  config_human="config_files/config_human_hg38_regular.yaml"
  echo "; Configuration file: "$config_human""

  ## Title:
  title="title: ""$OUTPUT_DIRECTORY""/""$flank_size""_flank_HUMAN_GATA_SOX_""$gini_gata_thr"""
  echo "; "$title""
  sed -i 's,'"$title_query"','"$title"',' "$config_human"

  ## Flank size:
  flank="TFBS_FLANK: ""$flank_size"""
  echo "; Flank size: "$flank_size""
  sed -i 's,'"$flank_query"','"$flank"',' "$config_human"

  ## Gini threshold:
  gini_thr="GINI_THRESHOLD: ""$gini_gata_thr"""
  gini_thr="GINI_THRESHOLD: 0.5"
  echo "; Gini: "$gini_gata_thr""
  sed -i 's,'"$gini_query"','"$gini_thr"',' "$config_human"

  ## Data folder:
  data="BEDS_folder: data/unibind_21/ub21_human_gata_sox"
  echo "; "$data""
  sed -i 's,'"$data_query"','"$data"',' "$config_human"

  ## Grouping file:
  human_tf_group="TF_FAMILIES: data/tfs_groups/tf_individual_human.txt"
  echo "; "$human_tf_group""
  sed -i 's,'"$tf_groups_query"','"$human_tf_group"',' "$config_human"

  ## Launching snakemake command:
  echo "snakemake --config analysis_id=human_hg38_regular  --cores 50"
  snakemake --config analysis_id=human_hg38_regular  --cores 50

done ;
```
