#-------------##
## Description ##
##-------------##

## Author: Ieva Rauluseviciute
## Affiliation: NCMM, UiO, Oslo, Norway
## This Snakemake pipeline is written for Dnase I hypersensitivity footprinting and COBIND combination analysis.

##------------------##
## Import libraries ##
##------------------##

import os
import itertools
from snakemake.utils import R
from snakemake.utils import min_version
import time
import numpy as np
import glob

##-------------------##
## Record start time ##
##-------------------##

start = time.time()

##-----------------------------##
## Choosing configuration file ##
##-----------------------------##

configfile: "config_files/config_dhs.yaml"

##-------------##
## Directories ##
##-------------##

MAIN_DIR_ABS      = os.getcwd()
DATA_DIR          = config["DATA_DIR"]
LEFTOVER_DATA_DIR = config["LEFTOVER_DATA_DIR"]
RESULTS_DIR       = config["title"]
COBIND_DIR        = config["COBIND_DIR"]

##-----------##
## Variables ##
##-----------##

## Variables from the config to write in the run_time text file:
config_title        = config["title"]
config_data         = config["DATA_DIR"]
config_cobind_title = config["COBIND_TITLE"]

##---------------##
## Info messages ##
##---------------##

## When pipeline is executed succesfully:
onsuccess:
    success_runtime = round((time.time() - start)/60,3)
    print("; Running time in minutes:\n %s" % success_runtime)
    f = open("{dir}/run_time.txt".format(dir = RESULTS_DIR), "w+")
    print("; Running time in minutes:\n %s" % success_runtime, file = f)
    print("\n; Analysis title:\n %s" % config_title, file = f)
    print("\n; Used data:\n %s" % config_data, file = f)
    print("\n; COBIND analysis title:\n  %s" % config_cobind_title, file = f)

    print("\n##---------------------------##\n; Workflow finished, no errors!\n##---------------------------##\n")

## When error occurs:
onerror:
    print("; Running time in minutes:\n %s\n" % round((time.time() - start)/60,1))
    print("\n##-----------------##\n; An error occurred!\n##-----------------##\n")

## When starting the run:
onstart:
    print("\n##-----------------------------------------##\n; Reading input and firing up the analysis...\n##-----------------------------------------##\n")
    print("\n; Analysis title:\n %s" % config_title)
    print("\n; Used data:\n %s" % config_data)
    print("\n; COBIND analysis title:\n  %s" % config_cobind_title)

##-------##
## Input ##
##-------##

## COBIND spacings tables to define the regions:
FAMILY,       = glob_wildcards(os.path.join(COBIND_DIR, "results", "clustering_results", "{family}", "spacings", "spacings_extended.txt"))

SPACINGS_TAB  = os.path.join(COBIND_DIR, "results", "clustering_results", "{family}", "spacings", "spacings_extended.txt")

##--------------##
## Output files ##
##--------------##

### 1. Ovelaps with footprints of different thresholds:
OVERLAP_NUMBERS_SUMMARY              = os.path.join(RESULTS_DIR, config_cobind_title, "overlap_summary", "{family}", "overlap_numbers_{fps}.tsv")

### 2. ###

## Conservation-like plots when directly analysing the regions in DHS data:
COMBINED_DHS_ANALYSIS_PLOTS          = os.path.join(RESULTS_DIR, config_cobind_title, "direct_regions_analysis", "{family}", "{family}_combined_dhs_plots.pdf")

COMBINED_DHS_ANALYSIS_STATISTICS_TAB = os.path.join(RESULTS_DIR, config_cobind_title, "direct_regions_analysis", "{family}", "{family}_wilcox_test_results.tsv")

## Collected plots from all TF cobinders:
COLLECTED_DHS_ANALYSIS_PLOTS         = os.path.join(RESULTS_DIR, config_cobind_title, "direct_regions_analysis", "collected_dhs_analysis_plots.pdf")

## Summary plot of DHS statistics analysis:
DHS_STATISTICS_ANALYSIS_SUMMARY_PLOT = os.path.join(RESULTS_DIR, config_cobind_title, "region_analysis_summary", "top_significant_cobinders_pvalues_in_different_DHS_datasets.pdf")

### 3. ###

COMBINED_DHS_ANALYSIS_PLOTS_NOT_MATCHED_CELL_TYPES          = os.path.join(RESULTS_DIR, config_cobind_title, "not_matched_direct_regions_analysis", "{family}", "{family}_combined_dhs_plots.pdf")

COMBINED_DHS_ANALYSIS_STATISTICS_TAB_NOT_MATCHED_CELL_TYPES = os.path.join(RESULTS_DIR, config_cobind_title, "not_matched_direct_regions_analysis", "{family}", "{family}_wilcox_test_results.tsv")

COLLECTED_DHS_ANALYSIS_PLOTS_NOT_MATCHED_CELL_TYPES         = os.path.join(RESULTS_DIR, config_cobind_title, "not_matched_direct_regions_analysis", "collected_dhs_analysis_plots.pdf")

DHS_STATISTICS_ANALYSIS_SUMMARY_PLOT_NOT_MATCHED_CELL_TYPES = os.path.join(RESULTS_DIR, config_cobind_title, "not_matched_region_analysis_summary", "top_significant_cobinders_pvalues_in_different_DHS_datasets.pdf")

##----------##
## Rule all ##
##----------##

rule all:
    """
    Defining a final output, which will request all the rules to be run.
    """
    input:
        COLLECTED_DHS_ANALYSIS_PLOTS, \
        DHS_STATISTICS_ANALYSIS_SUMMARY_PLOT
        # COLLECTED_DHS_ANALYSIS_PLOTS_NOT_MATCHED_CELL_TYPES, \
        # DHS_STATISTICS_ANALYSIS_SUMMARY_PLOT_NOT_MATCHED_CELL_TYPES

##-------##
## Rules ##
##-------##

rule overlapREGIONS:
    """
    1.0. Overlapping regions with and without cobinders with DHS footprints.
    """
    input:
        SPACINGS_TAB
    output:
        OVERLAP_NUMBERS_SUMMARY
    message:
        "; Overlapping regions with and without co-binders with DHS footprints."
    params:
        bin          = os.path.join(config["BIN"], "region_analysis"), \
        cobind_title = config["COBIND_TITLE"], \
        cobind_dir   = COBIND_DIR, \
        dhs_data_dir = DATA_DIR, \
        cobind_meta  = config["COBIND_META"] ,\
        dhs_meta     = config["DHS_META"], \
        output_dir   = RESULTS_DIR
    shell:
        """
        Rscript {params.bin}/overlap_regions.R \
        -c {params.cobind_title} \
        -i {params.cobind_dir} \
        -d {params.dhs_data_dir} \
        -m {params.cobind_meta} \
        -n {params.dhs_meta} \
        -o {params.output_dir} \
        -t {wildcards.fps} \
        -f {wildcards.family}
        """

rule analyseCOBINDregionsDHSdata:
    """
    2.1. Analysing COBIND regions for each cobinder configuration directly in the DHS data (bigwig files).
    """
    input:
        expand(OVERLAP_NUMBERS_SUMMARY, family = FAMILY, fps = config["FPS_LIST"])
    output:
        COMBINED_DHS_ANALYSIS_PLOTS
    message:
        "; Analysing COBIND regions in DHS data."
    params:
        bin             = os.path.join(config["BIN"], "region_analysis"), \
        cobind_title    = config["COBIND_TITLE"], \
        cobind_dir      = COBIND_DIR, \
        dhs_data_dir    = DATA_DIR, \
        cobind_meta     = config["COBIND_META"] ,\
        dhs_meta        = config["DHS_META"], \
        output_dir      = RESULTS_DIR, \
        analysis_window = config["ANALYSIS_WINDOW"], \
        chrom_sizes     = config["CHROM_SIZES"], \
        sampling_iterations = config["SAMPLE_ITERATIONS"]
    shell:
        """
        Rscript {params.bin}/analyse_regions_directly.R \
        -c {params.cobind_title} \
        -i {params.cobind_dir} \
        -d {params.dhs_data_dir} \
        -m {params.cobind_meta} \
        -n {params.dhs_meta} \
        -o {params.output_dir} \
        -f {wildcards.family} \
        -w {params.analysis_window} \
        -s {params.chrom_sizes} ;

        for iteration in {params.sampling_iterations} ;
        do
            Rscript {params.bin}/analyse_regions_directly.R \
            -c {params.cobind_title} \
            -i {params.cobind_dir} \
            -d {params.dhs_data_dir} \
            -m {params.cobind_meta} \
            -n {params.dhs_meta} \
            -o {params.output_dir} \
            -f {wildcards.family} \
            -w {params.analysis_window} \
            -s {params.chrom_sizes} \
            -t "$iteration" ;
        done ;
        """

rule computeDHSanalysisSTATISTICS:
    """
    2.2. Computing statistical significance of the DHS analysis.
    """
    input:
        COMBINED_DHS_ANALYSIS_PLOTS
    output:
        COMBINED_DHS_ANALYSIS_STATISTICS_TAB
    message:
        "; Computing statistical significance of the DHS analysis."
    params:
        bin             = os.path.join(config["BIN"], "region_analysis"), \
        cobind_title    = config["COBIND_TITLE"], \
        cobind_dir      = COBIND_DIR, \
        dhs_data_dir    = DATA_DIR, \
        cobind_meta     = config["COBIND_META"] ,\
        dhs_meta        = config["DHS_META"], \
        output_dir      = RESULTS_DIR, \
        sampling_iterations = config["SAMPLE_ITERATIONS"]
    shell:
        """
        Rscript {params.bin}/analyse_regions_directly_statistics.R \
        -c {params.cobind_title} \
        -i {params.cobind_dir} \
        -d {params.dhs_data_dir} \
        -m {params.cobind_meta} \
        -n {params.dhs_meta} \
        -o {params.output_dir} \
        -f {wildcards.family} ;

        for iteration in {params.sampling_iterations} ;
        do
            Rscript {params.bin}/analyse_regions_directly_statistics.R \
            -c {params.cobind_title} \
            -i {params.cobind_dir} \
            -d {params.dhs_data_dir} \
            -m {params.cobind_meta} \
            -n {params.dhs_meta} \
            -o {params.output_dir} \
            -f {wildcards.family} \
            -t "$iteration" ;
        done ;
        """

rule collectCOBINDinDHSanalysisPLOTS:
    """
    2.3. Collecting all plots into one PDF file.
    """
    input:
        expand(COMBINED_DHS_ANALYSIS_STATISTICS_TAB, family = FAMILY)
    output:
        COLLECTED_DHS_ANALYSIS_PLOTS
    message:
        "; Collecting all DHS analysis plots."
    params:
        dhs_analysis_dir = os.path.join(RESULTS_DIR, config_cobind_title, "direct_regions_analysis"), \
        sampling_iterations = config["SAMPLE_ITERATIONS"]
    shell:
        """
        pdfunite {params.dhs_analysis_dir}/*/*_combined_dhs_plots.pdf {output} ;

        for iteration in {params.sampling_iterations} ;
        do
            pdfunite {params.dhs_analysis_dir}_"$iteration"/*/*_combined_dhs_plots.pdf {output} ;
        done ;
        """

rule summariseDHSanalysisSTATISTICS:
    """
    2.4. Summarizing the p-values from DHS statistics analysis.
    """
    input:
        COLLECTED_DHS_ANALYSIS_PLOTS
    output:
        DHS_STATISTICS_ANALYSIS_SUMMARY_PLOT
    message:
        "; Summarizing p-values from DHS analysis."
    params:
        bin        = os.path.join(config["BIN"], "region_analysis"), \
        input_dir  = os.path.join(RESULTS_DIR, config_cobind_title, "direct_regions_analysis"), \
        output_dir = os.path.join(RESULTS_DIR, config_cobind_title, "region_analysis_summary"), \
        mixed_output_dir = os.path.join(RESULTS_DIR, config_cobind_title, "mixed_region_analysis_summary"), \
        cobind_dir = COBIND_DIR, \
        sampling_iterations = config["SAMPLE_ITERATIONS"], \
        ref_top_cobinders   = os.path.join(RESULTS_DIR, config_cobind_title, "region_analysis_summary", "top_significant_cobinders_dhs_analysis_results.tsv")
    shell:
        """
        echo "; Summarizing real region analysis"

        Rscript {params.bin}/summarize_cobinder_analysis.R \
        -i {params.input_dir} \
        -o {params.output_dir} \
        -c {params.cobind_dir} ;

        echo "; Summarizing randomized region analysis"

        for iteration in {params.sampling_iterations} ;
        do
            Rscript {params.bin}/summarize_cobinder_analysis.R \
            -i {params.input_dir}_"$iteration" \
            -o {params.output_dir}_"$iteration" \
            -c {params.cobind_dir} \
            -r {params.ref_top_cobinders} ;
        done ;

        Rscript {params.bin}/summarize_mixed_analysis.R \
        -i {params.output_dir} \
        -o {params.mixed_output_dir} ;
        """

# ############################################
# ## 3. Analysis in not matching cell-types ##
# ############################################

# rule analyseCOBINDregionsDHSdataUNMATCHED:
#     """
#     3.1. Analysing COBIND regions for each cobinder configuration directly in the DHS data that comes from not matching cell types (bigwig files).
#     """
#     input:
#         expand(OVERLAP_NUMBERS_SUMMARY, family = FAMILY, fps = config["FPS_LIST"])
#     output:
#         COMBINED_DHS_ANALYSIS_PLOTS_NOT_MATCHED_CELL_TYPES
#     message:
#         "; Analysing COBIND regions in DHS data."
#     params:
#         bin             = os.path.join(config["BIN"], "region_analysis"), \
#         cobind_title    = config["COBIND_TITLE"], \
#         cobind_dir      = COBIND_DIR, \
#         dhs_data_dir    = LEFTOVER_DATA_DIR, \
#         cobind_meta     = config["COBIND_META"] ,\
#         dhs_meta        = config["DHS_LEFTOVER_META"], \
#         output_dir      = RESULTS_DIR, \
#         analysis_window = config["ANALYSIS_WINDOW"], \
#         chrom_sizes     = config["CHROM_SIZES"]
#     shell:
#         """
#         Rscript {params.bin}/analyse_regions_directly_not_matching.R \
#         -c {params.cobind_title} \
#         -i {params.cobind_dir} \
#         -d {params.dhs_data_dir} \
#         -m {params.cobind_meta} \
#         -n {params.dhs_meta} \
#         -o {params.output_dir} \
#         -f {wildcards.family} \
#         -w {params.analysis_window} \
#         -s {params.chrom_sizes} ;
#         """

# rule computeDHSanalysisSTATISTICSunmatched:
#     """
#     3.2. Computing statistical significance of the DHS analysis.
#     """
#     input:
#         COMBINED_DHS_ANALYSIS_PLOTS_NOT_MATCHED_CELL_TYPES
#     output:
#         COMBINED_DHS_ANALYSIS_STATISTICS_TAB_NOT_MATCHED_CELL_TYPES
#     message:
#         "; Computing statistical significance of the DHS analysis."
#     params:
#         bin             = os.path.join(config["BIN"], "region_analysis"), \
#         cobind_title    = config["COBIND_TITLE"], \
#         cobind_dir      = COBIND_DIR, \
#         dhs_data_dir    = LEFTOVER_DATA_DIR, \
#         cobind_meta     = config["COBIND_META"] ,\
#         dhs_meta        = config["DHS_LEFTOVER_META"], \
#         output_dir      = RESULTS_DIR
#     shell:
#         """
#         Rscript {params.bin}/analyse_regions_directly_statistics_not_matching.R \
#         -c {params.cobind_title} \
#         -i {params.cobind_dir} \
#         -d {params.dhs_data_dir} \
#         -m {params.cobind_meta} \
#         -n {params.dhs_meta} \
#         -o {params.output_dir} \
#         -f {wildcards.family} ;
#         """

# rule collectCOBINDinDHSanalysisPLOTSunmatched:
#     """
#     3.3. Collecting all plots into one PDF file.
#     """
#     input:
#         expand(COMBINED_DHS_ANALYSIS_STATISTICS_TAB_NOT_MATCHED_CELL_TYPES, family = FAMILY)
#     output:
#         COLLECTED_DHS_ANALYSIS_PLOTS_NOT_MATCHED_CELL_TYPES
#     message:
#         "; Collecting all DHS analysis plots."
#     params:
#         dhs_analysis_dir = os.path.join(RESULTS_DIR, config_cobind_title, "not_matched_direct_regions_analysis")
#     shell:
#         """
#         pdfunite {params.dhs_analysis_dir}/*/*_combined_dhs_plots.pdf {output}
#         """

# rule summariseDHSanalysisSTATISTICSunmatched:
#     """
#     3.4. Summarizing the p-values from DHS statistics analysis.
#     """
#     input:
#         COLLECTED_DHS_ANALYSIS_PLOTS_NOT_MATCHED_CELL_TYPES
#     output:
#         DHS_STATISTICS_ANALYSIS_SUMMARY_PLOT_NOT_MATCHED_CELL_TYPES
#     message:
#         "; Summarizing p-values from DHS analysis."
#     params:
#         bin        = os.path.join(config["BIN"], "region_analysis"), \
#         input_dir  = os.path.join(RESULTS_DIR, config_cobind_title, "not_matched_direct_regions_analysis"), \
#         output_dir = os.path.join(RESULTS_DIR, config_cobind_title, "not_matched_region_analysis_summary")
#     shell:
#         """
#         Rscript {params.bin}/summarize_cobinder_analysis.R \
#         -i {params.input_dir} \
#         -o {params.output_dir}
#         """

##------------------##
## End of Snakefile ##
##------------------##