---
title: "IDENTIFICATION OF TRANSCRIPTION FACTOR CO-BINDING PATTERNS WITH NON-NEGATIVE MATRIX FACTORIZATION"
author:
  - name: Ieva Rauluseviciute
    institute: ncmm
  - name: Timothée Launay
    institute: ncmm
  - name: Guido Barzaghi
    institute: [embl, hu]
  - name: Sarvesh Nikumbh
    institute: [mrc, icl]
  - name: Boris Lenhard
    institute: [mrc, icl]
  - name: Arnaud Regis Krebs
    institute: embl
  - name: Jaime A. Castro-Mondragon
    institute: ncmm
  - Anthony Mathelier:
      email: anthony.mathelier@ncmm.uio.no
      institute: [ncmm, ouh, cb]
      correspondence: true
institute:
  - ncmm: Centre for Molecular Medicine Norway (NCMM), Nordic EMBL Partnership, University of Oslo, 0318 Oslo, Norway
  - embl: European Molecular Biology Laboratory (EMBL), Genome Biology Unit, Meyerhofstraße 1, 69117 Heidelberg, Germany
  - hu: Collaboration for Joint Ph.D. degree between EMBL and Heidelberg University, Heidelberg, Germany
  - mrc: MRC London Institute of Medical Sciences, Du Cane Road, London, W12 0NN, UK
  - icl: Institute of Clinical Sciences, Faculty of Medicine, Imperial College London, Hammersmith Hospital Campus, Du Cane Road, London, W12 0NN, UK
  - ouh: Department of Medical Genetics, Institute of Clinical Medicine, University of Oslo and Oslo University Hospital, Oslo, Norway
  - cb: Center for Bioinformatics, Department of Informatics, Faculty of Mathematics and Natural Sciences, University of Oslo, Oslo, Norway
output: 
  pdf_document:
    pandoc_args:
      - '--lua-filter=../scholarly-metadata.lua'
      - '--lua-filter=../author-info-blocks.lua'
    keep_md: true
header-includes:
  - \usepackage{caption}
  - \captionsetup[figure]{labelformat=empty}
bibliography: references.bib
csl: nucleic-acids-research.csl
---



\hfill\break

# ADDITIONAL FILE 2 #  


## MATERIALS & METHODS ##  

### Intron-exon border analysis ###

We downloaded intron coordinates for the human genome (*hg38* assembly) using the UCSC Table Browser [@karolchik2004ucsc]. We then extracted the first two nucleotides of the exons flanking the introns. Depending on the direction of transcription, we defined these regions as donors or acceptors. We created two BED-formatted files - one for the donor and one for the acceptor sites, which we used as independent input files for COBIND. The pipeline was applied using default parameters.

### COBIND analysis on randomly selected regions ###

We considered the 338 datasets for 44 TFs from the DHS analysis to perform control analyses with randomly selected regions. Specifically, we used the genomic mononucleotide distribution matched module of the Biasaway tool [@khan2021biasaway] for each dataset to randomly choose genomic regions of the same size and with the same %GC content. We ran COBIND on these control datasets to discover and summarize predicted co-binding patterns. This process was repeated five times per original dataset to ensure the representativity of the results (1,690 COBIND runs in total).

The co-binding patterns predicted by COBIND in the control datasets were compared to the ones predicted in the corresponding real dataset using Tomtom [@gupta2007quantifying].


## RESULTS ##  

### COBIND discovers expected splicing motifs ###

We ran COBIND on human donor and acceptor sites to see if COBIND discovers the expected splicing motifs [@li2017comparative]. The acceptor motif consensus in different species is GT, and the consensus motif for donor sites is CAG [@li2017comparative; @mishra2021intron].

We anchored the COBIND analyses at the two exonic nucleotides flanking the introns. Thus, we analyzed the flanking regions upstream and downstream from the exon-intron borders. One flank is in the intron and another is in the exon (Materials and Methods above). COBIND revealed two patterns in the donor sites, where both have the consensus donor GT motif in around 7% of the input donor regions (Figure S40A). This consensus motif downstream of the anchor indicates the intronic flank. Moreover, the recovered anchor motif for the GTA motif (Co-binding pattern 0 in Figure S40A) contained high IC for AG, meaning that 4.4% of all input donor sites include AG as the last two exon nucleotides. When analyzing the acceptor sites, we found one DNA pattern corresponding to the consensus acceptor motif CAG in 1.4 % of input sequences (Figure S40B).

These analyses indicate that COBIND recovers the expected splicing signal motifs.


![]( /storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/docs/manuscript/figures/S4-01-introns_exons/supp_introns_exons_v3.pdf ) 
 ***Figure S40.***   **COBIND recovers splicing signal motifs at the intron-exon boundaries for donor (A) and acceptor (B) sites.**

### Co-binding motifs in control regions ##

Across all 1,690 (338 datasets times 5 runs) COBIND runs with control regions, 45 co-binding motifs were reported, all consisting of repeats of As or Ts, except one motif rich in Gs (see examples in Figure S41). Out of these 45 patterns, only 7 matched the ones found in the corresponding real datasets (Tomtom p-value < 0.05; Figure S42), representing less than 0.5% of the total number of runs.

As highlighted in previous studies [@dozmorov2015detrimental; @chung2011discovering], discovering A/T-rich patterns with low complexity is an established issue for *de novo* motif discovery tools.


![]( /storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/docs/manuscript/figures/S2-41-random_motifs/random_motifs.pdf ) 
 ***Figure S41.***   **Examples of co-binding patterns found in random sequences matching the %GC composition of datasets for GATA2 (A) and HNF4G (B).** All patterns discovered in random regions are repeats of As or Ts and do not match the patterns found in the original datasets.



![]( /storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/docs/manuscript/figures/S2-42-comparison/comparison.pdf ) 
 ***Figure S42.***   **Motifs found in random region datasets compared to the ones found in real datasets.** Most patterns discovered in random regions did not match the ones found in the real datasets (Tomtom p-value > 0.05) (**A**). In only a few datasets, a similar pattern was found (Tomtom p-value < 0.05) (**B**), highlighting the limitation of A/T stretches discoveries.

\hfill\break

## REFERENCES ##  

