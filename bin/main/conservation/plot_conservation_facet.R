#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "doParallel",
                        "ggplot2",
                        "optparse",
                        "tidyverse")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only=TRUE, quietly = T))
}

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(c("-i", "--conservation_files"), type = "character", default = NULL,
              help = "Comma separated conservation tables (results of bwtool. (Mandatory) ", metavar = "character"),
  
  make_option(c("-c", "--score_files"), type = "character", default = NULL,
              help = "Comma separated score files that were used for bwtool. (Mandatory) ", metavar = "character"),
  
  make_option(c("-o", "--output_file"), type = "character", default = NULL,
              help = "Path to the output plot. (Mandatory)", metavar = "character")
  
);
message("; Reading arguments from command line")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

############
## Syntax ##
############

### Syntax:
## Rscript R-scripts/plot_conservation_facet.R
##    -c /storage/scratch/ieva/COBIND_results/individual_MOUSE_01/results/conservation_analysis/SOX2/full_no_cobinder/no_cobinder_full_regions_SOX2_conservation.tsv,
##       /storage/scratch/ieva/COBIND_results/individual_MOUSE_01/results/conservation_analysis/SOX2/full_with_cobinder/pooled_cobinder_full_regions_SOX2_conservation.tsv
##    -s /storage/mathelierarea/raw/UCSC/mm10/phastCons/mm10.60way.phastCons.bw,
##       /storage/mathelierarea/raw/UCSC/mm10/phastCons4way/mm10.phastCons4way.bw
##    -o /storage/scratch/ieva/COBIND_results/individual_MOUSE_01/results/conservation_analysis/SOX2/with_no_cobinder_conservation.pdf

########################
## Set variable names ##
########################

cons_file               <- opt$conservation_files
score_files             <- opt$score_files
output_file             <- opt$output_file

###########
## Debug ##
###########

# cons_file <- c("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/scratch/ieva/COBIND_results/individual_MOUSE_01/results/conservation_analysis/SOX2/full_no_cobinder/no_cobinder_full_regions_SOX2_conservation.tsv",
#                "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/scratch/ieva/COBIND_results/individual_MOUSE_01/results/conservation_analysis/SOX2/full_with_cobinder/pooled_cobinder_full_regions_SOX2_conservation.tsv")
# score_files <- c("/storage/mathelierarea/raw/UCSC/mm10/phastCons/mm10.60way.phastCons.bw",
#                  "/storage/mathelierarea/raw/UCSC/mm10/phastCons4way/mm10.phastCons4way.bw")
# output_file <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/scratch/ieva/COBIND_results/individual_MOUSE_01/results/conservation_analysis/SOX2/with_no_cobinder_conservation.pdf"

########################
## Parsing parameters ##
########################

cons_file <- unlist(strsplit(cons_file, ","))
score_files <- unlist(strsplit(score_files, ","))
score_files <- basename(score_files)

##############
## File map ##
##############

conservation_files <- as.data.frame(cons_file, stringsAsFactors = FALSE) %>%
  mutate(random_cons_file = gsub(cons_file, pattern = "_conservation.tsv", replacement = "_conservation_random.tsv"))

#################################
## Reading conservation tables ##
#################################

all_conservation_tab <- data.frame()

for (i in 1:nrow(conservation_files)) {
  
  entry <- conservation_files[i, ]
  print(entry)
  cons <- fread(entry$cons_file, header = FALSE)
  random <- fread(entry$random_cons_file, header = FALSE)
  
  cons <- cbind(cons, random[,2])
  colnames(cons) <- c("dist", score_files, "Random")
  
  df <- cons %>% 
    select(dist, all_of(score_files), Random) %>% 
    gather(key = "variable", value = "value", -dist) %>%
    mutate(target_file = gsub(basename(entry$cons_file), pattern = "_conservation.tsv", replacement = ""))
  
  all_conservation_tab <- rbind(all_conservation_tab, df)
  
}

###########################
## Plotting conservation ##
###########################
message("; Plotting conservation scores.")

cols <- c("#a6cee3", "#1f78b4", "#b2df8a",
          "#33a02c", "#fb9a99", "#e31a1c",
          "#fdbf6f", "#ff7f00", "#cab2d6",
          "#6a3d9a", "#ffff99", "#b15928")

colors = cols[1:(ncol(cons) - 1)]

## Exporting the plot:
conservation_plots <- ggplot(all_conservation_tab, aes(x = dist, y = value)) + 
  geom_line(aes(color = variable)) + 
  scale_color_manual(values = colors) + 
  facet_wrap(~ target_file, ncol = 2) +
  theme_bw() + 
  xlab("Distance from feature") + 
  ylab("Conservation score")

ggsave(filename = output_file,
       plot = conservation_plots,
       width = 12,
       height = 9,
       limitsize = FALSE)

###################
## End of script ##
###################
