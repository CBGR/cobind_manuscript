# COBIND analysis on synthetic data and comparison with other tools #

These are the instructions on how to run analysis of synthetic data and use it to compare COBIND with other tools, such as SpaMo, RSAT peak-motif and MEME. This analysis also include the benchmarking of the number of components used for co-binder discovery with NMF in COBIND.

The entire analysis is located in a directory `synthetic_data_test`, which had its own `Snakefile` and associated configuration file and scripts. So first thing that needs to be done is entering that directory.

Two types of analysis is conducted here:

1. Running COBIND on synthetic data with different numbers of components and summarizing the results. This analysis uses configuration file `config_components.yaml` and is shortly called `components`.
2. Benchmarking COBIND (with default parameters) against other motif discovery tools using synthetic data. This analysis uses configuration file `config_comparison.yaml` and is shortly called `comparison`.

```bash
## Switching to synthetic data test directory:
cd synthetic_data_test

## Two types of analysis will be performed in a loop:
analysis_array=( comparison components )

## Reading the variables and running analysis:
for analysis_type in "${analysis_array[@]}" ; do

  echo "; Launching $analysis_type analysis."

  ## Launching snakemake command:
  echo "snakemake --config analysis_id="$analysis_type"  --cores 10 -k"
  snakemake --config analysis_id="$analysis_type"  --cores 10 -k

done ;

## Moving back to main directory:
cd ..
```
