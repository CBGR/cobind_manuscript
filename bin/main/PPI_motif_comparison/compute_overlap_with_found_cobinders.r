##-------------------##
## Loading libraries ##
##-------------------##

required.libraries <- c(
    "data.table",
    "dplyr",
    "optparse",
    "ggplot2",
    "ggalluvial",
    "tidyr",
    "purrr")

for (lib in required.libraries) {
    suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

##---------##
## Options ##
##---------##

options(stringsAsFactors = FALSE)
options(scipen = 999)

##-------------------##
## Reading arguments ##
##-------------------##

option_list = list(

    make_option(
        c("-s", "--summary_table"), 
        type = "character", 
        default = NULL,
        help = "A path to a file from interspecies analysis with complete spacings summary. (Mandatory) ", 
        metavar = "character"),

    make_option(
        c("-o", "--output_dir"), 
        type = "character", 
        default = NULL,
        help = "A path to the base output directory. (Mandatory) ", 
        metavar = "character")
)

message("; Reading arguments from command line")
opt_parser = OptionParser(option_list = option_list)
opt        = parse_args(opt_parser)

##--------------------##
## Set variable names ##
##--------------------##

PATH_TO_SUMMARY <- opt$summary_table
MAIN_OUTPUT_DIR <- opt$output_dir

##--------##
## Syntax ##
##--------##

# Rscript bin/main/PPI_motif_comparison/compute_overlap_with_found_cobinders.r \
# -s COBIND_results/interspecies/individual_0.99/cobinders_clustering_results_summary/clusters_spacings_metadata_complete.tsv \
# -o COBIND_results/TFA_TFB_analysis

# PATH_TO_SUMMARY <- "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/interspecies/individual_0.99/cobinders_clustering_results_summary/clusters_spacings_metadata_complete.tsv"
# MAIN_OUTPUT_DIR <- "COBIND_results/TFA_TFB_analysis"

##----------------------##
## Creating directories ##
##----------------------##

dir.create(MAIN_OUTPUT_DIR, recursive = TRUE, showWarnings = FALSE)

##---------------------##
## Reading input table ##
##---------------------##

all_spacings_tab <- data.table::fread(
    PATH_TO_SUMMARY,
    header = TRUE,
    sep = "\t")

list_of_species <- unique(all_spacings_tab %>% dplyr::pull(species_name_short))

##-----------------------------------------##
## Iterating through species and cobinders ##
##-----------------------------------------##

match_summary_tab <- data.table()

for (species_id in list_of_species) {

    message("; Processing for ", species_id)

    ## Finding all original spacings tabs and reading:
    list_of_original_spacings_tabs <- list.files(
        file.path(
            "COBIND_results/individual",
            paste0(species_id, "_0.99"),
            "results/clustering_results"),
        pattern = "spacings_summary.tab",
        full.names = TRUE,
        recursive = TRUE)
    
    species_all_original_spacings <- purrr::map(
        list_of_original_spacings_tabs,
        function(spacings_tab) {
            spacings_info <- data.table::fread(
                    spacings_tab,
                    header = TRUE,
                    sep = "\t") %>%
                dplyr::mutate(original_subcluster = gsub(
                    Subcluster, 
                    pattern = "Co-binding ", 
                    replacement = "cobinder_")) %>%
                dplyr::select(Cobinder, original_subcluster)
            return(spacings_info)
        }
    )
    species_all_original_spacings <- do.call("rbind", species_all_original_spacings)

    ## Subsetting spacing table:
    species_spacings_tab <- all_spacings_tab %>%
        dplyr::filter(species_name_short == species_id)

    ## Adding original cobinder IDs:    
    species_spacings_tab <- dplyr::left_join(
            species_spacings_tab, species_all_original_spacings,
            by = c("Cobinder" = "Cobinder")) %>%
        dplyr::mutate(TF_cobinder = paste0(tf_family, "_", original_subcluster))

    ## List of unique co-binders:
    list_of_cobinders <- species_spacings_tab %>%
        dplyr::pull(TF_cobinder) %>%
        unique()
    nb_of_cobinders_in_species <- length(list_of_cobinders)
    
    ## Reading and processing confirmed pairs table:
    PATH_TO_CONFIRMED_PAIRS <- file.path(
        "COBIND_results/individual/",
        paste0(species_id, "_0.99"),
        "results/ppi_associations_results/physical_full/all_confirmed_pairs.tsv")
    confirmed_pairs_tab <- data.table::fread(
        PATH_TO_CONFIRMED_PAIRS,
        header = TRUE,
        sep = "\t")
    confirmed_pairs_tab <- confirmed_pairs_tab %>%
        dplyr::mutate(TF_cobinder = paste0(folder_name, "_", cobinder_id)) %>%
        dplyr::select(folder_name, anchor_tf, TF_cobinder, cobinder_name) %>%
        tidyr::separate_rows(cobinder_name, sep = ",") %>%
        dplyr::group_by(TF_cobinder) %>%
        dplyr::mutate(cobinder_name = paste0(unique(cobinder_name), collapse = ",")) %>%
        dplyr::ungroup() %>%
        unique()
    
    ## Adding confirmed pairs info to the main table:
    species_info_tab <- dplyr::left_join(
        species_spacings_tab, confirmed_pairs_tab,
        by = c("TF_cobinder" = "TF_cobinder"),
        relationship = "many-to-many"
        )

    ## iterating through co-binders:
    for (tf_cobinder_id in list_of_cobinders) {

        message("; Analysing for ", tf_cobinder_id)

        ## Getting relevant info:
        cobinder_info_tab <- species_info_tab %>%
            dplyr::filter(TF_cobinder == tf_cobinder_id)
        
        list_of_cell_lines <- cobinder_info_tab %>%
            dplyr::pull(cell_line) %>%
            unique()
        list_of_inferred_cobinders <- cobinder_info_tab %>%
            dplyr::pull(cobinder_name) %>%
            unique()
        list_of_inferred_cobinders <- unique(unlist(strsplit(list_of_inferred_cobinders, split = ",")))

        cobinder_info_tab <- cobinder_info_tab %>%
            dplyr::mutate(cell_line = paste0(cell_line, collapse = ",")) %>%
            dplyr::mutate(cobinder_name = paste0(list_of_inferred_cobinders, collapse = ",")) %>%
            dplyr::select(
                TF_cobinder,
                tf_family,
                TF,
                motif_name,
                anchor_tf,
                cobinder_name,
                species_name_short, 
                family, 
                class) %>%
            unique() %>%
            dplyr::mutate(nb_of_cobinders_in_species = nb_of_cobinders_in_species)
        
        if (all(is.na(list_of_inferred_cobinders))) {
            message("; There was no match with anything in PPI.")
            cobinder_info_tab <- cobinder_info_tab %>%
                dplyr::mutate(
                    matching_cobinder = "not_inferred",
                    matching_cell_type = "not_inferred",
                    reverse_match_cobinder = "not_inferred", 
                    reverse_match_cell_type = "not_inferred",
                    reverse_match = "not_possible"
                )
        } else {
            message("; Matches in PPI found. Analysing.")

            ## Matching co-binder name:
            cobinder_match <- rbind(
                species_info_tab[species_info_tab$anchor_tf %in% list_of_inferred_cobinders, ],
                species_info_tab[species_info_tab$motif_name %in% list_of_inferred_cobinders, ]
                ) %>%
                unique() %>%
                dplyr::filter(!is.na(cobinder_name)) %>%
                dplyr::filter(TF_cobinder != tf_cobinder_id) ## Just in case not to include datasets where the cobinder is the same as anchor

            ## Matching cell type:
            cobinder_match <- cobinder_match[cobinder_match$cell_line %in% list_of_cell_lines, ]
            
            list_of_matching_cell_lines <- cobinder_match %>%
                dplyr::pull(cell_line) %>%
                unique()
            list_of_matching_cobinders <- cobinder_match %>%
                dplyr::pull(TF_cobinder) %>%
                unique()

            ## Checking for reverse match:
            reverse_match_check <- cobinder_match %>%
                tidyr::separate_rows(cobinder_name, sep = ",") %>%
                unique()
            reverse_match_results_tab <- rbind(
                    reverse_match_check[reverse_match_check$cobinder_name %in% cobinder_info_tab$motif_name, ],
                    reverse_match_check[reverse_match_check$cobinder_name %in% cobinder_info_tab$anchor_tf, ]) %>%
                unique() %>%
                dplyr::filter(!is.na(anchor_tf))
            
            list_of_reverse_match_cell_lines <- reverse_match_results_tab %>%
                dplyr::pull(cell_line) %>%
                unique()
            list_of_reverse_match_cobinders <- reverse_match_results_tab %>%
                dplyr::pull(TF_cobinder) %>%
                unique()

            # list_of_cobinders_for_matching_cobinder <- cobinder_match %>%
            #     dplyr::pull(cobinder_name)
            # list_of_cobinders_for_matching_cobinder <- unique(unlist(strsplit(list_of_cobinders_for_matching_cobinder, split = ",")))

            # reverse_match_results_tab <- rbind(
            #         cobinder_info_tab[cobinder_info_tab$motif_name %in% list_of_cobinders_for_matching_cobinder, ],
            #         cobinder_info_tab[cobinder_info_tab$anchor_tf %in% list_of_cobinders_for_matching_cobinder, ]
            #         ) %>%
            #     unique() %>%
            #     dplyr::filter(!is.na(anchor_tf))

            ## Reporting:
            cobinder_info_tab <- cobinder_info_tab %>%
                dplyr::mutate(
                    matching_cobinder = ifelse(
                        length(list_of_matching_cobinders) > 0, 
                        yes = paste0(list_of_matching_cobinders, collapse = ","),
                        no = "no_match"),
                    matching_cell_type = ifelse(
                        length(list_of_matching_cell_lines) > 0, 
                        yes = paste0(list_of_matching_cell_lines, collapse = ","),
                        no = "no_match"),
                    reverse_match = ifelse(
                        nrow(reverse_match_results_tab) > 0, 
                        yes = 1, 
                        no = 0),
                    reverse_match_cobinder = ifelse(
                        length(list_of_reverse_match_cobinders) > 0, 
                        yes = paste(list_of_reverse_match_cobinders, collapse = ","),
                        no = "no_match"),
                    reverse_match_cell_type = ifelse(
                        length(list_of_reverse_match_cell_lines) > 0,
                        yes = paste0(list_of_reverse_match_cell_lines, collapse = ","),
                        no = "no_match")) %>%
                dplyr::group_by(species_name_short, TF_cobinder) %>%
                dplyr::mutate(anchor_tf = paste0(anchor_tf, collapse = ",")) %>%
                dplyr::ungroup() %>%
                unique()

        }

        match_summary_tab <- rbind(match_summary_tab, cobinder_info_tab)
    }
}

##--------------------------##
## Processing results table ##
##--------------------------##

message("; Processing results table")

match_summary_tab <- match_summary_tab %>%
    dplyr::mutate(
        reverse_match_result = ifelse(
            reverse_match == "not_possible",
            yes = 0, 
            no = reverse_match), 
        cobinder_match_result = ifelse(
            (matching_cobinder == "not_inferred" | matching_cobinder == "no_match"),
            yes = 0, 
            no = 1), 
        cell_type_match_result = ifelse(
            (matching_cell_type == "not_inferred" | matching_cell_type == "no_match"), 
            yes = 0, 
            no = 1)) %>%
    dplyr::mutate(
        match_only_with_cobinder = ifelse(
            (cobinder_match_result == 1 & cell_type_match_result == 0),
            yes = 1, 
            no = 0)) %>%
    dplyr::mutate(
        nb_of_all_total_matches = sum(as.numeric(reverse_match_result)),
        nb_of_all_cobinder_only_matches = sum(match_only_with_cobinder)) %>%
    dplyr::mutate(total_nb_of_unique_tfs = length(unique(motif_name))) %>%
    dplyr::group_by(reverse_match_result) %>%
    dplyr::mutate(nb_of_unique_tfs_rev_match = length(unique(motif_name))) %>%
    dplyr::ungroup() %>%
    dplyr::group_by(species_name_short) %>%
    dplyr::mutate(
        nb_of_species_total_matches = sum(as.numeric(reverse_match_result)),
        nb_of_species_cobinder_only_matches = sum(match_only_with_cobinder)) %>%
    dplyr::mutate(nb_of_one_way_match_cobinders = sum(cobinder_match_result)) %>%
    dplyr::ungroup() %>%
    dplyr::arrange(desc(reverse_match_result)) %>%
    dplyr::group_by(species_name_short, reverse_match_result) %>%
    dplyr::mutate(nb_of_unique_tfs_rev_match_per_species = length(unique(motif_name))) %>%
    dplyr::ungroup() %>%
    dplyr::group_by(species_name_short, cobinder_match_result) %>%
    dplyr::mutate(nb_tfs_with_one_way_match = length(unique(motif_name))) %>%
    dplyr::ungroup()

## Shortening the summary to only have per species summary:
per_species_summary_tab <- match_summary_tab %>%
    dplyr::select(
        species_name_short,
        nb_of_all_total_matches, 
        nb_of_all_cobinder_only_matches, 
        nb_of_species_total_matches, 
        nb_of_species_cobinder_only_matches, 
        reverse_match_result, 
        total_nb_of_unique_tfs,
        nb_of_unique_tfs_rev_match, 
        nb_of_unique_tfs_rev_match_per_species,
        cobinder_match_result,
        nb_of_one_way_match_cobinders,
        nb_tfs_with_one_way_match) %>%
    unique()

##-----------##
## Exporting ##
##-----------##

message("; Exporting results table to a file.")

data.table::fwrite(
    match_summary_tab,
    file = file.path(
        MAIN_OUTPUT_DIR,
        "all_matches_results_tab.tsv"),
    col.names = TRUE,
    sep = "\t"
)

data.table::fwrite(
    per_species_summary_tab,
    file = file.path(
        MAIN_OUTPUT_DIR,
        "short_per_species_results_tab.tsv"),
    col.names = TRUE,
    sep = "\t"
)

##------------------##
## Plotting matches ##
##------------------##

message("; Plotting the matches.")

message("; Setting ggplot parameters.")

## Theme:
ggplot2::theme_set(
    theme_bw() +
    theme(axis.text.x = element_text(size = 15),
            axis.title.y = element_text(size = 15),
            axis.text.y = element_text(size = 14),
            plot.title = element_text(size = 1),
            legend.position = "bottom")
    )

for (species_id in list_of_species) {

    species_match_summary_tab <- match_summary_tab %>%
        dplyr::filter(species_name_short == species_id) %>%
        dplyr::filter(reverse_match_result == 1)
    
    if (nrow(species_match_summary_tab) > 0) {

        message("; Plotting for ", species_id)

        species_match_summary_tab <- species_match_summary_tab %>%
            dplyr::select(
                TF_cobinder,
                reverse_match_cobinder,
                motif_name) %>%
            tidyr::separate_rows(reverse_match_cobinder, sep = ",") %>%
            unique() %>%
            dplyr::mutate(
                TF_cobinder = gsub(TF_cobinder, pattern = "_cobinder_.+", replacement = ""),
                reverse_match_cobinder = gsub(reverse_match_cobinder, pattern = "_cobinder_.+", replacement = "")) %>%
            unique() %>%
            dplyr::mutate(freq = 1)

        sankey_for_matches <- ggplot(
                species_match_summary_tab,
                aes(
                    y = freq,
                    axis1 = TF_cobinder,
                    axis2 = reverse_match_cobinder)) +
            ggalluvial::geom_alluvium(
                aes(fill = motif_name),
                width = 1/5,
                aes.bind = "alluvia") +
            ggalluvial::geom_stratum(width = 1/5) +
            geom_text(
                stat = "stratum",
                aes(label = after_stat(stratum)),
                size = 5,
                check_overlap = TRUE) +
            scale_x_continuous(
                breaks = 1:2,
                labels = c("TF A", "TF B"),
                expand = c(0.01, 0.01)) +
            scale_y_continuous(expand = expansion(mult = c(0.03, 0.01))) +
            labs(
                y = "TFs with co-biding patterns",
                fill = "TF A") +
            theme(
                axis.text.x = element_text(size = 15),
                axis.title.y = element_text(size = 15),
                axis.text.y = element_text(size = 14),
                plot.title = element_text(size = 1),
                panel.border = element_blank())
        
        ## Export plot:
        nb_of_cobinders <- length(species_match_summary_tab$TF_cobinder)
        nb_of_TFs <- length(unique(species_match_summary_tab$motif_name))
        ggsave(
            filename = file.path(MAIN_OUTPUT_DIR, paste0(species_id, "_sankey_for_matches.pdf")),
            plot = sankey_for_matches,
            width = 20,
            height = (nb_of_cobinders / 4) + (nb_of_TFs / 20),
            limitsize = FALSE)

    }
}

## End of the Script ##