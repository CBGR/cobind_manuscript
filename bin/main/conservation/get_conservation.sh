#!/bin/bash

#Get full path of the script - useful to make sure the script can be launched from anywhere
DIR=$( realpath "$( dirname "$0" )" )

usage() {
  echo "Usage: $0 -i INPUT BED -c INPUT CONSERVATION FILE(S) -w WINDOW -n CHROMS SIZES FILE -o OUTPUT DIR [-s STRANDEDNESS]" 1>&2
}
exit_abnormal() {
  usage
  exit 1
}

strandedness=0;

printf "Parsing arguments\n"

while getopts ":i:c:w:n:o:hs" options; do
  case "${options}" in
    i)
      bed_files=${OPTARG}
      bed_files=(${bed_files//","/" "}) #Separate the comma-separated input vector and put it into an array
      ;;
    c)
      conservation_files=${OPTARG}
      ;;
    w)
      number_nts=${OPTARG}
      ;;
    n)
      chroms_sizes=${OPTARG}
      ;;
    o)
      output_dir=${OPTARG}
      ;;
    s)
      strandedness=1;
      ;;
    h)
      echo "Usage: $0 -i INPUT BED -c INPUT CONSERVATION FILE(S) -w WINDOW -n CHROMS SIZES FILE -o OUTPUT DIR [-s STRANDEDNESS]"
      echo ""
      echo "Get and plot conservation scores in a defined window centered at coordinates from a BED file."
      echo ""
      echo "Arguments:"
      echo ""
      echo "    -i INPUT BED: comma-separated paths to the input BED file(s) with the coordinates of interest."
      echo "    -c CONSERVATION FILE(S): comma-separated paths to the bigWig files (.bw extension) with nucleotide conservation scores. Example: -c file1.bw,file2.bw,file3.bw"
      echo "    -w WINDOW: number of nucleotides to the right and left of the BED coordinates to extract the conservation scores."
      echo "    -n CHROMS SIZES: path to the chromosome sizes file."
      echo "    -o OUTPUT DIR: directory where the output will be saved."
      echo "    -s STRANDEDNESS: perform the analysis taking into account the strand of each coordinate."
      echo ""
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      exit_abnormal
      exit 1
      ;;
  esac
done

if [ ! -d $output_dir ]
then
  mkdir -p $output_dir
fi

# Function to compute stranded-less analysis
compute_strandedless () {

  bed=$1;

  printf "Computing conservation scores for: $bed\n"
  # Log type of analysis
  printf "$bed\tStrandedless analysis\n" >> $output_dir/log.txt

    # Run bwtool to get the conservation scores
    bwtool agg -h ${number_nts}:${number_nts} $bed $conservation_files $output_dir/$(basename -s ".bed" $bed)_conservation.tsv

    # Build random coordinates based on input bed file
    bedtools shuffle -chrom -i $bed -g $chroms_sizes | bedtools sort -i stdin > $output_dir/$(basename -s ".bed" $bed)_random.bed

    # Run bwtool to get the conservation scores of the random coordinates
    bwtool agg -h ${number_nts}:${number_nts} $output_dir/$(basename -s ".bed" $bed)_random.bed $conservation_files $output_dir/$(basename -s ".bed" $bed)_conservation_random.tsv

    # Plot the results
    # Rscript $DIR/plot_conservation.R -i $output_dir/$(basename -s ".bed" $bed)_conservation.tsv -c $conservation_files -o $output_dir/$(basename -s ".bed" $bed)_conservation.pdf

}

# Function to compute the stranded analysis
compute_stranded () {

  bed=$1;

  printf "Computing conservation scores for: $bed\n"

    tmp_pos=$(mktemp)
    tmp_neg=$(mktemp)

    # Separate sequences in positive and negative strands into two different files
    grep "+" $bed > $tmp_pos
    grep "-" $bed > $tmp_neg


    # Check if provided input has strand info OR all rows are in the same strand
    # Launch stranded-less analysis if TRUE
    if [ ! -s $tmp_pos ]
    then

      printf "All coordinates in $bed are on the same strand! Computing stranded-less analysis.\n"
      compute_strandedless $bed
      continue

    elif [ ! -s $tmp_neg ]
    then

      printf "All coordinates in $bed are on the same strand! Computing stranded-less analysis.\n"
      compute_strandedless $bed
      continue

    fi

    # Log type of analysis
    printf "$bed\tStranded analysis\n" >> $output_dir/log.txt

    tmp_pos_conservation=$(mktemp)
    tmp_neg_conservation=$(mktemp)

    # Run bwtool to get the conservation scores
    bwtool agg -h ${number_nts}:${number_nts} $tmp_pos $conservation_files $tmp_pos_conservation
    bwtool agg -h ${number_nts}:${number_nts} $tmp_neg $conservation_files $tmp_neg_conservation

    # Build random coordinates based on input bed file
    bedtools shuffle -chrom -i $bed -g $chroms_sizes | bedtools sort -i stdin > $output_dir/$(basename -s ".bed" $bed)_random.bed

    # Run bwtool to get the conservation scores of the random coordinates
    bwtool agg -h ${number_nts}:${number_nts} $output_dir/$(basename -s ".bed" $bed)_random.bed $conservation_files $output_dir/$(basename -s ".bed" $bed)_conservation_random.tsv

    # Reverse conservation vectors in negative strand and compute weighted average
    Rscript $DIR/combine_positive_negative_scores.R --posbed=$tmp_pos --poscons=$tmp_pos_conservation --negbed=$tmp_neg --negcons=$tmp_neg_conservation --output=$output_dir/$(basename -s ".bed" $bed)_conservation.tsv

    # Plot the results
    # Rscript $DIR/plot_conservation.R -i $output_dir/$(basename -s ".bed" $bed)_conservation.tsv -c $conservation_files -o $output_dir/$(basename -s ".bed" $bed)_conservation.pdf

}

for bed in ${bed_files[@]}
do

  if [ $strandedness -eq 1 ]
  then

    compute_stranded $bed

  else

    compute_strandedless $bed

  fi

done

# Plot all independent plots together
plot_files_conservation=$(find $output_dir -name "*_conservation.tsv")
plot_files_conservation=$(echo $plot_files_conservation | tr ' ' ',')

Rscript $DIR/plot_conservation_facet.R -i $plot_files_conservation -c $conservation_files -o $output_dir/all_combined_conservation.pdf
