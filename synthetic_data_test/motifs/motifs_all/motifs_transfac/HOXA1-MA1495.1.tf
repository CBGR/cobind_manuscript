AC  injected_HOXA1-MA1495.1
XX
ID  injected_HOXA1-MA1495.1
XX
DE  MA1495.1 HOXA1; from JASPAR
P0       a     c     g     t
1     1167  2244  2667  1328
2        0  1546     0  7406
3     7406  2083  1244     0
4     7406     0     0     0
5        0     0     0  7406
6        0  1310  2265  7406
7     7406     0  1576     0
8     1175  2811  2237  1182
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
