#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "tidyverse",
                        "optparse",
                        "stringr",
                        "GenomicRanges",
                        "purrr")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

#############
## Options ##
#############

## Turning off scientific notations:
options(scipen = 999)
## Disabling stringsasfactors:
options(stringsAsFactors = FALSE)

####################
## Read arguments ##
####################

option_list <- list(
  
  make_option(c("-i", "--spacing_file"),
              type = "character", default = NULL,
              help = "Path to the extended spacings tab. (Mandatory) ", metavar = "character"),
  
  make_option(c("-f", "--family_name"),
              type = "character", default = NULL,
              help = "Family or class name. (Mandatory) ", metavar = "character"),
  
  make_option(c("-o", "--output_folder"),
              type = "character", default = NULL,
              help = "Output diretory. (Mandatory) ", metavar = "character"),
  
  make_option(c("-c", "--cobind_analysis"),
              type = "character", default = NULL,
              help = "Title of the COBIND analysis. (Mandatory) ", metavar = "character")
  
  )

message("; Reading arguments from command line")
opt_parser <- OptionParser(option_list = option_list)
opt        <- parse_args(opt_parser)

############
## Syntax ##
############

### Syntax:
## Rscript bin/single_molecule/prep/prepare_cluster_regions.R
##        -i /storage/scratch/ieva/COBIND_results/individual/MOUSE_0.99/results/clustering_results/SRF/spacings/spacings_extended.txt
##        -f SRF
##        -o /storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind/SM_results/data
##        -c MOUSE_0.99

########################
## Set variable names ##
########################

extended.spacing.tab <- opt$spacing_file
family.name          <- opt$family_name
output.dir           <- opt$output_folder
cobind.title         <- opt$cobind_analysis

## Debug:
# setwd("/run/user/316574/gvfs/sftp:host=biotin4.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript")
# extended.spacing.tab <- "COBIND_results/individual/MOUSE_0.99/results/clustering_results/SRF/spacings/spacings_extended.txt"
# family.name          <- "CTCF"
# output.dir           <- "SMF_analysis/SM_results/data"
# cobind.title         <- "MOUSE_0.99"

####################
## Functions ##
###############

line2BED <- function(df.line = NULL) {
  
  anchor.cols   <- which(grepl(colnames(df.line), pattern = "anchor"))
  cobinder.cols <- which(grepl(colnames(df.line), pattern = "cobinder"))
  
  BED.df <- rbind(cbind(df.line[,..anchor.cols], "anchor"),
                  cbind(df.line[,..cobinder.cols], "cobinder"),
                  use.names = FALSE)
  colnames(BED.df) <- c("chr", "start", "end", "strand", "name")
  BED.df <- BED.df %>%
    mutate(start = start + 1)
  BED.GR <- GRanges(BED.df)
  
  return(BED.GR)
}

##########################
## Creating directories ##
##########################

message("; Creating output directory.")

family.out.dir <- file.path(output.dir, cobind.title, family.name)

## Cobinder-specific directory:
cobinder.output.dir <- file.path(output.dir, cobind.title, family.name, "cobinder_specific")
dir.create(cobinder.output.dir, showWarnings = F, recursive = T)

## Cobinder, location and spacing-specific directory:
case.output.dir <- file.path(output.dir, cobind.title, family.name, "case_specific")
dir.create(case.output.dir, showWarnings = F, recursive = T)

###################################
## Reading extended spacings tab ##
###################################

message("; Reading spacings table.")

spacings.file.tab <- fread(extended.spacing.tab)

###################################################
## Extracting paired anchor and cobinder regions ##
###################################################

message("; Extracting paired anchor and cobinder regions.")

## List of cobinders:
list.of.cobinders <- unique(spacings.file.tab$Subcluster)

## Number of regions summary:
regions_statistics <- data.frame()

## Itterating through different cobinders:
for (cobinder_id in list.of.cobinders) {
  
  ## Selecting only co-binder-specific spacings table entries:
  spacings.tab.cobinder.specific <- spacings.file.tab %>%
    filter(Subcluster == cobinder_id)
  
  ## Empty list to add regions from each dataset:
  anchor_cobinder_regions_as_column_pairs <- vector("list",  nrow(spacings.tab.cobinder.specific))
  
  ### Grouping by COBINDER, LOCATION and SPACER:
  spacings.tab.grouped       <- spacings.tab.cobinder.specific %>%
    dplyr::group_by(Subcluster, Location, Spacer_length)
  group_indices              <- group_rows(spacings.tab.grouped)
  
  ## Iterating in groups to extract group specific, cobinder specific and overall regions:
  for (i in seq_along(group_indices)) {
    
    group_entry          <- group_indices[[i]]
    group_subcluster     <- paste0("subcluster_", cobinder_id)
    group_location       <- spacings.tab.cobinder.specific[group_entry[1], ]$Location
    group_spacer_size    <- spacings.tab.cobinder.specific[group_entry[1], ]$Spacer_length
    
    ## Empty vector to add case-specific regions:
    CASE_anchor_cobinder_regions_as_column_pairs <- vector("list", length(group_entry))
    
    for (j in seq_along(group_entry)) {
      
      entry_idx         <- group_entry[j]
      entry             <- spacings.tab.cobinder.specific[entry_idx, ]
      
      entry_pfm_size             <- as.numeric(entry$pfm_matrix_length)
      entry_distance_to_cobinder <- entry$Distance_to_cobinder_start
      entry_location             <- entry$Location
      entry_flank                <- entry$Flank
      
      ########################
      ## 1. Reading indices ##
      ########################
      
      indices <- fread(file.path(gsub(entry$Path_to_indices,
                                      pattern = "/storage/scratch/ieva/",
                                      replacement = "")))
      # indices <- fread(file.path("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no", entry$Path_to_indices))
      indices <- indices$V1
      
      ##############################################################
      ## 2. Reading flank bed and centering around the co-binder: ##
      ##############################################################
      # flank_bed                  <- fread(file.path("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no",
      #                                               entry$Path_to_BED),
      #                                     header = FALSE,
      #                                     stringsAsFactors = FALSE) %>%
      #   dplyr::select(V1, V2, V3, V6)
      
      flank_bed                  <- fread(file.path(gsub(entry$Path_to_BED,
                                                         pattern = "/storage/scratch/ieva/",
                                                         replacement = "")),
                                          header = FALSE,
                                          stringsAsFactors = FALSE) %>%
                                    dplyr::select(V1, V2, V3, V6)
      
      centered_cobinder_bed <- flank_bed[indices + 1]
      
      ## Centering around the co-binder:
      centered_cobinder_bed <- centered_cobinder_bed %>%
        mutate(left_start  = ifelse(V6 == "+", 
                                    (V3 - entry_distance_to_cobinder - entry_pfm_size), 
                                    (V2 + entry_distance_to_cobinder))) %>%
        mutate(left_end    = ifelse(V6 == "+", 
                                    (V3 - entry_distance_to_cobinder),
                                    (V2 + entry_distance_to_cobinder + entry_pfm_size))) %>%
        mutate(right_start = ifelse(V6 == "+",
                                    (V2 + entry_distance_to_cobinder),
                                    (V3 - entry_distance_to_cobinder - entry_pfm_size))) %>%
        mutate(right_end   = ifelse(V6 == "+", 
                                    (V2 + entry_distance_to_cobinder + entry_pfm_size), 
                                    (V3 - entry_distance_to_cobinder)))
        # arrange(V1, V2, V3)
      
      if (entry_flank == "left") {
        
        centered_cobinder_bed <- centered_cobinder_bed %>%
          dplyr::select(V1, left_start, left_end, V6)
        
      } else {
        
        centered_cobinder_bed <- centered_cobinder_bed %>%
          dplyr::select(V1, right_start, right_end, V6)
        
      }
      
      ## Centered:
      colnames(centered_cobinder_bed) <- c("cobinder_chr",
                                           "cobinder_start",
                                           "cobinder_end", 
                                           "cobinder_strand")
      
      
      ################################################
      ## 3. Filtered and unfiltered anchor regions: ##
      ################################################
      # anchor_bed <- fread(file.path("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no",
      #                               entry$Path_to_anchor_BED), header = FALSE) %>%
      #   dplyr::select(V1, V2, V3, V6)
      
      anchor_bed <- fread(file.path(gsub(entry$Path_to_anchor_BED,
                                         pattern = "/storage/scratch/ieva/",
                                         replacement = "")), header = FALSE) %>%
        dplyr::select(V1, V2, V3, V6)
      colnames(anchor_bed) <- c("anchor_chr", 
                                "anchor_start",
                                "anchor_end",
                                "anchor_strand")
      
      filtered_anchor_bed                <- anchor_bed[indices + 1]

      ############################################
      ## 4. Binding anchor and cobinder regions ##
      ############################################
      
      anchor_cobinder_regions_from_dataset <- cbind(filtered_anchor_bed, centered_cobinder_bed)

      #######################################
      ## 5. Adding to lists of all regions ##
      #######################################
      
      CASE_anchor_cobinder_regions_as_column_pairs[[j]]    <- anchor_cobinder_regions_from_dataset
      anchor_cobinder_regions_as_column_pairs[[entry_idx]] <- anchor_cobinder_regions_from_dataset
      
    }
    
    ##################################################################
    ## Exporting location and spacer-sepcific anchor-cobinder pairs ##
    ##################################################################
    
    message("; Exporting location and spacer specific anchor-co-binder regions for ",
            paste0(group_subcluster, "_", group_location, "_", group_spacer_size))
    
    CASE_anchor_cobinder_regions_as_column_pairs <- do.call("rbind", CASE_anchor_cobinder_regions_as_column_pairs)
    
    ## Unique of all regions:
    CASE_anchor_cobinder_regions_as_column_pairs <- CASE_anchor_cobinder_regions_as_column_pairs %>%
      unique() %>%
      arrange(anchor_chr, anchor_start)
    
    ## Naming each TFBS:
    CASE_anchor_cobinder_regions_as_column_pairs$tfbs_id <- c(paste0("TFBS_cluster_",
                                                                     seq.int(nrow(CASE_anchor_cobinder_regions_as_column_pairs))))
    
    ##-------------------------------------------##
    ## Exporting as BED:
    CASE.anchor.cobinder.bed <- file.path(case.output.dir,
                                           paste0("cobinder_anchor_",
                                                  group_subcluster, "_",
                                                  group_location, "_",
                                                  group_spacer_size, ".bed"))
    fwrite(CASE_anchor_cobinder_regions_as_column_pairs,
           file = CASE.anchor.cobinder.bed,
           sep = "\t", col.names = TRUE)
    ##--------------------------------------------##
    
    ## Finding minimum window for each cluster:
    CASE_cluster_coordinates <- CASE_anchor_cobinder_regions_as_column_pairs %>%
      # rowwise %>%
      mutate(chr = anchor_chr,
             start = pmin(anchor_start, cobinder_start, anchor_end, cobinder_end),
             end   = pmax(anchor_start, cobinder_start, anchor_end, cobinder_end),
             strand = anchor_strand) %>%
      # ungroup() %>%
      select(chr, start, end, strand) %>%
      mutate(number_of_TF = 2)
    
    CASE_cluster_coordinates <- GRanges(CASE_cluster_coordinates)
    names(CASE_cluster_coordinates) <- CASE_anchor_cobinder_regions_as_column_pairs$tfbs_id
    
    ## Adapting to 1-based coordinates:
    CASE_cluster_coordinates <- narrow(CASE_cluster_coordinates, start = 2)
    # CASE_cluster_coordinates <- shift(CASE_cluster_coordinates, 1)
    
    ##--------------------------------------------##
    ## Adding the number of regions to a dataframe:
    nb_of_regions_per_case <- cbind(paste0("cobinder_anchor_",
                                           group_subcluster, "_",
                                           group_location, "_",
                                           group_spacer_size),
                                    nrow(CASE_anchor_cobinder_regions_as_column_pairs))
    colnames(nb_of_regions_per_case) <- c("type", "nb_regions")
    regions_statistics <- rbind(regions_statistics, nb_of_regions_per_case)
    ##--------------------------------------------##

    ## Splitting by TFBS ID:
    CASE_cluster_composition <- split(CASE_anchor_cobinder_regions_as_column_pairs,
                       f = CASE_anchor_cobinder_regions_as_column_pairs$tfbs_id)
    
    ## Splitting by the region type and creating a GRanges object:
    CASE_cluster_composition <- purrr::map(.x = CASE_cluster_composition,
                                          .f = ~line2BED(df.line = .x))
    
    ## GRanges object:
    CASE_anchor_cobinder_regions_as_column_pairs_GR <- list(ClusterCoordinates = CASE_cluster_coordinates,
                                                            ClusterComposition = GRangesList(CASE_cluster_composition))
    
    
    ## Exporting as RData:
    CASE.anchor.cobinder.path <- file.path(case.output.dir,
                                           paste0("cobinder_anchor_",
                                                  group_subcluster, "_",
                                                  group_location, "_",
                                                  group_spacer_size, ".RData"))
    save(CASE_anchor_cobinder_regions_as_column_pairs_GR,
         file = CASE.anchor.cobinder.path)
    
  }
    
  #######################################################
  ## Exporting cobinder-specific anchor-cobinder pairs ##
  #######################################################
  
  message("; Exporting anchor-co-binder regions for ", cobinder_id)
  
  anchor_cobinder_regions_as_column_pairs <- do.call("rbind", anchor_cobinder_regions_as_column_pairs)
  
  anchor_cobinder_regions_as_column_pairs <- anchor_cobinder_regions_as_column_pairs %>%
    unique() %>%
    arrange(anchor_chr, anchor_start)
  
  anchor_cobinder_regions_as_column_pairs$tfbs_id <- c(paste0("TFBS_",
                                                              seq.int(nrow(anchor_cobinder_regions_as_column_pairs))))
  
  ##-------------------------------------------------------------
  ## Exporting as BED:
  anchor.cobinder.bed <- file.path(cobinder.output.dir,
                                    paste0("cobinder_anchor_",
                                           cobinder_id, ".bed"))
  fwrite(anchor_cobinder_regions_as_column_pairs,
         file = anchor.cobinder.bed,
         sep = "\t",
         col.names = TRUE)
  ##-------------------------------------------------------------
  
  ## Finding minimum window for each cluster:
  cluster_coordinates <- anchor_cobinder_regions_as_column_pairs %>%
    # rowwise %>%
    mutate(chr = anchor_chr,
           start = pmin(anchor_start, cobinder_start, anchor_end, cobinder_end),
           end   = pmax(anchor_start, cobinder_start, anchor_end, cobinder_end),
           strand = anchor_strand) %>%
    # ungroup() %>%
    select(chr, start, end, strand) %>%
    mutate(number_of_TF = 2)
  
  cluster_coordinates <- GRanges(cluster_coordinates)
  names(cluster_coordinates) <- anchor_cobinder_regions_as_column_pairs$tfbs_id
  
  ## Adapting to 1-based coordinates:
  cluster_coordinates <- narrow(cluster_coordinates, start = 2)
  
  ##-------------------------------------------------
  ## Adding the number of regions to a dataframe:
  nb_of_regions_per_cobinder <- cbind(paste0("cobinder_anchor_",
                                             cobinder_id),
                                      nrow(anchor_cobinder_regions_as_column_pairs))
  colnames(nb_of_regions_per_cobinder) <- c("type", "nb_regions")
  regions_statistics <- rbind(regions_statistics, nb_of_regions_per_cobinder)
  ##----------------------------------------------------------------------
  
  ## Splitting by TFBS ID:
  cluster_composition <- split(anchor_cobinder_regions_as_column_pairs,
                               f = anchor_cobinder_regions_as_column_pairs$tfbs_id)
  
  ## Splitting by the region type and creating a GRanges object:
  cluster_composition <- purrr::map(.x = cluster_composition,
                                    .f = ~line2BED(df.line = .x))
  
  ## GRanges object:
  anchor_cobinder_regions_as_column_pairs_GR <- list(ClusterCoordinates = cluster_coordinates,
                                                     ClusterComposition = GRangesList(cluster_composition))
  
  ## Exporting as RData:
  anchor.cobinder.path <- file.path(cobinder.output.dir,
                                    paste0("cobinder_anchor_",
                                           cobinder_id, ".RData"))
  save(anchor_cobinder_regions_as_column_pairs_GR,
       file = anchor.cobinder.path)
  
}

##########################################
## Exporting numbers of regions summary ##
##########################################

fwrite(regions_statistics,
       file = file.path(family.out.dir, "nb_of_regions.txt"),
       sep = "\t", col.names = TRUE)

###################
## End of script ##
###################