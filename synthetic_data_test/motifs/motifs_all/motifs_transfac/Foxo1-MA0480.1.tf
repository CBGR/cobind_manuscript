AC  injected_Foxo1-MA0480.1
XX
ID  injected_Foxo1-MA0480.1
XX
DE  MA0480.1 Foxo1; from JASPAR
P0       a     c     g     t
1      468   277   478  1267
2      127  1279   752   332
3       87  1406   209   788
4        0     0     0  2490
5        0     0  2490     0
6        0     0     0  2490
7        0     0     0  2490
8        0     0   287  2203
9     1871   157     0   462
10       0  2007     0   483
11    1072   573    71   774
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
