#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "tidyverse",
                        "optparse")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only=TRUE, quietly = T))
}

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(c("-s", "--spamo_results"), type = "character", default = NULL,
              help = "A file with paths to the SpaMo results .tsv files. (Mandatory) ", metavar = "character"),
  
  make_option(c("-j", "--jaspar_motifs"), type = "character", default = NULL,
              help = "Path to the folder with individual jaspar motifs that has been used in SpaMo analysis. (Mandatory) ", metavar = "character"),
  
  make_option(c("-o", "--output_directory"), type = "character", default = NULL,
              help = "Output directory to output SpaMo discovered co-binders in meme format and with correct names. (Mandatory) ", metavar = "character"),
  
  make_option(c("-g", "--true_gap"), type = "numeric", default = NULL,
              help = "True gap size between secondary and primary motifs. (Mandatory) ", metavar = "numeric"),
  
  make_option(c("-l", "--motif_location"), type = "character", default = NULL,
              help = "Secondary motif location (left or right). (Mandatory) ", metavar = "character")

);

message("; Reading arguments from command line.")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

########################
## Set variable names ##
########################

SPAMO_RESULTS  <- opt$spamo_results
JASPAR_MOTIFS  <- opt$jaspar_motifs
OUTPUT_DIR     <- opt$output_directory
GAP_SIZE       <- opt$true_gap
MOTIF_LOCATION <- opt$motif_location

## Debug
# SPAMO_RESULTS  <- "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/Foxo1-MA0480.1/comparison_with_others/list_of_spamo_results_tabs.txt"
# JASPAR_MOTIFS  <- "/storage/mathelierarea/raw/JASPAR/2020/nonredundant/vertebrates/JASPAR2020_CORE_vertebrates_non-redundant_pfms_meme"
# OUTPUT_DIR     <- "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/Foxo1-MA0480.1/comparison_with_others"
# GAP_SIZE       <- 13
# MOTIF_LOCATION <- "left"

#############
## Options ##
#############

options(stringsAsFactors = FALSE)
options(scipen = 999)

###########################################
## Reading paths to SpaMo results tables ##
###########################################

message("; Reading paths to the SpaMo results.")
list_of_spamo_tabs <- data.table::fread(
  SPAMO_RESULTS, 
  header = FALSE)

list_of_spamo_tabs <- dplyr::pull(list_of_spamo_tabs, V1)

###############################
## Pooling discovered motifs ##
###############################
message("; Renaming and exporting discovered motifs.")

## Creating temporary directory to output the motifs:
TEMP_DIR <- file.path(OUTPUT_DIR, "temp_motif_folder")
dir.create(TEMP_DIR, showWarnings = F, recursive = T)

for (SPAMO_TAB in list_of_spamo_tabs) {
  
  ## Extracting experiment ID and motif ID:
  experiment_id <- gsub(SPAMO_TAB, pattern = ".+\\/results_(.+)\\/spamo_output.+", replacement = "\\1")
  experiment_id <- gsub(experiment_id, pattern = "_", replacement = "-")
  total_nb_seqs <- as.numeric(unlist(strsplit(experiment_id, "-"))[1])

  motif_id <- basename(dirname(dirname(dirname(SPAMO_TAB))))

  ## Reading the results:
  spamo_df <- data.table::fread(
    SPAMO_TAB, header = TRUE, sep = "\t", showProgress = FALSE, fill = TRUE)
  bad_idxs <- grep(spamo_df$prim_db, pattern = "#")
  spamo_df <- spamo_df[!bad_idxs, ]
  
  ## Filtering from NA values:
  spamo_df <- spamo_df %>%
    dplyr::filter(!is.na(`E-value`))

  ## Getting path to correct indices and reading the file:
  MOTIF_IDX <- file.path(dirname(dirname(SPAMO_TAB)), "data", paste0(motif_id, "_secondary_indices_with_motif.txt"))
  motif_indices <- data.table::fread(MOTIF_IDX, header = FALSE)
  motif_indices <- dplyr::pull(motif_indices, V1)
  nb_of_seqs_with_motif <- length(motif_indices)

  if (nrow(spamo_df) != 0) {
    
    ## Calculating the gap between motifs (to compare later with the true gap):
    if (MOTIF_LOCATION == "left") {
      spamo_df <- spamo_df %>%
        mutate(spacer = as.numeric(gap) - as.numeric(trim_left))
    } else {
      spamo_df <- spamo_df %>%
        mutate(spacer = as.numeric(gap) - as.numeric(trim_right))
    }
    
    spamo_df <- spamo_df %>%
      mutate(gap_hit    = ifelse(spacer == GAP_SIZE, yes = "gyes", no = "gno")) %>%
      mutate(orient_hit = ifelse(orient == 0,        yes = "oyes", no = "ono"))
    spamo_df <- spamo_df %>%
      dplyr::group_by(sec_id) %>%
      dplyr::top_n(n = -1, wt = `E-value`) %>%
      dplyr::sample_n(1) %>%
      ungroup()
    
    ## Reading each motif, changing the names and concatenating:
    for (i in 1:nrow(spamo_df)) {

      motif_hit <- spamo_df[i, ]
      
      ## Info:
      motif_id    <- motif_hit$sec_id
      motif_name  <- motif_hit$sec_alt
      e_value     <- round(motif_hit$`E-value`, digits = 2)
      gap_value   <- motif_hit$gap
      orient_value <- motif_hit$orient
      gap_hit     <- motif_hit$gap_hit
      orient_hit  <- motif_hit$orient_hit

      PATH_TO_INDIVIDUAL_HIT <- file.path(dirname(SPAMO_TAB), paste0("seqs_", motif_hit$prim_id, "_db1_", motif_id, ".tsv"))
      
      if ((as.numeric(e_value) <= 0.5) & file.exists(PATH_TO_INDIVIDUAL_HIT) & length(readLines(PATH_TO_INDIVIDUAL_HIT)) > 1) {

        ## Individual hit report:
        
        hit_df <- data.table::fread(
          PATH_TO_INDIVIDUAL_HIT,
          sep = "\t",
          header = FALSE,
          skip = 1)

        ## Finding how many indices match:
        # field	name	contents
        # 1	matches	Trimmed lowercase sequence centered on primary match with matches in uppercase.
        # 2	sec_pos	Position of the secondary match within the whole sequence.
        # 3	pri_match	Sequence fragment that the primary matched.
        # 4	pri_strand	Strand of the primary match (+/-).
        # 5	sec_match	Sequence fragment that the secondary matched.
        # 6	sec_strand	Strand of the secondary match (+/-).
        # 7	same_opp	The primary match on the same (s) or opposite (o) strand as the secondary.
        # 8	down_up	The secondary match is downstream (d) or upstream (u) of the primary.
        # 9	gap	The gap between the primary and secondary matches.
        # 10	seq_name	The name of the sequence.
        # 11	adj_p-value	The p-value of the bin containing the match, adjusted for the number of bins.

        if (nrow(hit_df) > 0) {

          hit_indices <- unique(hit_df$V10)
          nb_seqs <- length(hit_indices)

          TP <- as.numeric(sum(hit_indices %in% motif_indices))
          FN <- as.numeric(nb_of_seqs_with_motif - TP)
          FP <- as.numeric(nb_seqs - TP)
          TN <- as.numeric(total_nb_seqs - nb_of_seqs_with_motif - FP)

          ## Building a new motif name:
          motif_new_id <- paste0(
            "SPAMO", "_",
            experiment_id, "_",
            motif_id, "-", motif_name, "_",
            nb_seqs, "_",
            e_value, "-", gap_hit, "-", orient_hit, "_",
            "TP:", TP, "-FN:", FN, "-FP:", FP, "-TN:", TN)
          new_motif_name <- paste(
            "MOTIF",
            motif_new_id,
            sep = "\t")
          
          ## Reading motif:
          PATH_TO_MOTIF <- file.path(JASPAR_MOTIFS, paste0(motif_id, ".meme"))
          motif_matrix  <- readLines(PATH_TO_MOTIF)
          motif_matrix  <- gsub(motif_matrix, pattern = "^MOTIF.+", replacement = new_motif_name)
          
          ## Exporting renamed motif:
          RENAMED_MOTIF_PATH <- file.path(TEMP_DIR, paste0(motif_new_id, ".meme"))
          write_lines(motif_matrix, RENAMED_MOTIF_PATH)

        }

      } else {
        message("; Discovered SPAMO motif is not significant. Skipping")
      }
    }
  }
}

message("; Discovered motifs collected and exported.")
###################
## End of script ##
###################