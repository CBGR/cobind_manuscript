#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "optparse",
                        "tidyverse")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

#############
## Options ##
#############

options(stringsAsFactors = FALSE)

####################
## Read arguments ##
####################

option_list = list(
  
  optparse::make_option(c("-u", "--unibind_metadata"), 
                        type    = "character", 
                        default = NULL,
                        help    = "Path to the UniBind metadata table. (Mandatory) ",
                        metavar = "character"),
  
  optparse::make_option(c("-d", "--dhs_metadata"), 
                        type    = "character", 
                        default = NULL,
                        help    = "Path to the DHS metadata file (Supplementary table 1 from Viestra et al.). (Mandatory) ", 
                        metavar = "character"),
  
  optparse::make_option(c("-e", "--encode_metadata"), 
                        type    = "character", 
                        default = NULL,
                        help    = "ENCODE metadata - available ChIP-seq and DNase-seq data. (Mandatory) ", 
                        metavar = "character"),
  
  optparse::make_option(c("-o", "--output_folder"), 
                        type    = "character", 
                        default = NULL,
                        help    = "Output folder to output tables. (Mandatory)", 
                        metavar = "character")
  
);

message("; Reading arguments from command line")
opt_parser = optparse::OptionParser(option_list = option_list)
opt        = optparse::parse_args(opt_parser)

############
## Syntax ##
############

### Syntax:
# Rscript bin/dhs_footprinting/prep/overlap_dhs_unibind_metadata.R \
# -u data/ub_21/ub21_human_metadata.tsv \
# -d data/dhs_vierstra/dataset_info.csv \
# -e data/dhs_vierstra/ENCODE_dhs_chipseq_metadata.tsv \
# -o data/dhs_vierstra

########################
## Set variable names ##
########################

unibind.metadata.path <- opt$unibind_metadata
dhs.metadata.path     <- opt$dhs_metadata
encode.metadata.path  <- opt$encode_metadata
output.folder         <- opt$output_folder

## Debug
# unibind.metadata.path <- "data/unibind_21/ub21_human_metadata.tsv"
# dhs.metadata.path     <- "data/dhs_vierstra/dataset_info.csv"
# encode.metadata.path  <- "data/dhs_vierstra/ENCODE_dhs_chipseq_metadata.tsv"
# output.folder         <- "data/dhs_vierstra"

###############
## Functions ##
###############

outersect <- function(a,b) setdiff(union(a,b), intersect(a,b))

###############################
## Creating output directory ##
###############################

message("; Creating output directory.")
dir.create(output.folder, showWarnings = F, recursive = T)

##########################
## Reading input tables ##
##########################

message("; Reading metadata table.")

ub_meta_tab         <- data.table::fread(unibind.metadata.path, header = TRUE, sep = "\t")

dhs_meta_tab        <- data.table::fread(dhs.metadata.path, header = TRUE, sep = "\t")

encode_meta_tab     <- data.table::fread(encode.metadata.path, header = TRUE, sep = "\t")

encode_chip_tab     <- encode_meta_tab %>%
  dplyr::filter(`Assay name` == "ChIP-seq")

encode_dhs_tab      <- encode_meta_tab %>%
  dplyr::filter(`Assay name` == "DNase-seq")


nb_datasets_unibind <- nrow(ub_meta_tab)
nb_datasets_dhs     <- nrow(dhs_meta_tab)

total_nb_datasets_encode  <- nrow(encode_meta_tab)
nb_datasets_encode_chip   <- nrow(encode_chip_tab)
nb_datasets_encode_dhs    <- nrow(encode_dhs_tab)

#############
## UniBind ##
#############

message("; Annotating UniBind data with ENCODE information.")

ub_meta_tab <- ub_meta_tab %>%
  tidyr::separate_rows(identifier, sep = "\\::") %>%
  # tidyr::separate(identifier, sep = "\\:", into = c("source", "identifier")) %>%
  # dplyr::filter(source == "ENCODE") %>%
  unique()

nb_datasets_unibind_encode_only <- nrow(ub_meta_tab)

## Adding encode data:
ub_meta_annotated_with_encode <- dplyr::inner_join(
    ub_meta_tab, encode_meta_tab,
    by = c("identifier" = "Accession")) %>%
  dplyr::mutate(coded_file_name = paste0(dataset, ".bed")) %>%
  dplyr::select(dataset,
                coded_file_name,
                TF,
                motif_name,
                cell_line,
                biological_condition,
                original_dataset,
                identifier, `Assay name`,
                `Target of assay`,
                `Biosample summary`, `Biosample term name`,
                `Description`, `Biosample accession`)

colnames(ub_meta_annotated_with_encode) <- c(
  "dataset", "coded_file_name", "TF",
  "motif_name", "cell_line", "biological_condition",
  "original_dataset", "identifier", "encode_assay_name",
  "encode_target_of_assay", "encode_biosample_summary", 
  "encode_biosample_term_name", "encode_description", "biosample_accession")

## Number of datasets with annotations:
nb_datasets_unibind_annotated_with_encode <- nrow(ub_meta_annotated_with_encode)

##################
## Vierstra DHS ##
##################

message("; Annotating Vierstra DHS data with ENCODE information.")

dhs_meta_annotated_with_encode <- dplyr::inner_join(
    dhs_meta_tab, encode_meta_tab,
    by = c("DCC_experiment_id" = "Accession")) %>%
  dplyr::select(`Identifier`,
                `Description.x`,
                `DCC_experiment_id`,
                `DCC_library_id`,
                `DCC_biosample_id`,
                `DCC_file_id_FDR_1%`,
                `Assay name`,
                `Target of assay`,
                `Biosample summary`, `Biosample term name`,
                `Description.y`, `Biosample accession`)

colnames(dhs_meta_annotated_with_encode) <- c("dhs_dataset", "dhs_cell_line",
                                              "DCC_experiment_id", "DCC_library_id",
                                              "DCC_biosample_id", "DCC_file_id",
                                              "encode_assay_name",
                                              "encode_target_of_assay", "encode_biosample_summary", 
                                              "encode_biosample_term_name", "encode_description", "biosample_accession")

nb_datasets_dhs_annotated_with_encode <- nrow(dhs_meta_annotated_with_encode)

###################################
## Matching DHS and UniBind data ##
###################################

message("; Matching DHS datasets with UniBind data using identifiers.")

## A list of biosample terms that occur both in UniBind and Vierstra DHS data:

shared_biosamples_terms                               <- unique(intersect(ub_meta_annotated_with_encode$encode_biosample_term_name, dhs_meta_annotated_with_encode$encode_biosample_term_name))
nb_of_shared_biosample_terms                          <- length(shared_biosamples_terms)

## A list of leftover biosample terms:
leftover_biosamples_terms                             <- unique(outersect(ub_meta_annotated_with_encode$encode_biosample_term_name, dhs_meta_annotated_with_encode$encode_biosample_term_name))
nb_of_leftover_biosample_terms                        <- length(leftover_biosamples_terms)

## Subsetting UniBind data:
ub_meta_annotated_with_encode_from_shared_biosamples  <- ub_meta_annotated_with_encode[ub_meta_annotated_with_encode$encode_biosample_term_name %in% shared_biosamples_terms, ]
nb_of_ub_datasets_from_shared_biosamples              <- nrow(ub_meta_annotated_with_encode_from_shared_biosamples)

## Subsetting Vierstra's DHS data:
dhs_meta_annotated_with_encode_from_shared_biosamples <- dhs_meta_annotated_with_encode[dhs_meta_annotated_with_encode$encode_biosample_term_name %in% shared_biosamples_terms, ]
nb_of_dhs_datasets_from_shared_biosamples             <- nrow(dhs_meta_annotated_with_encode_from_shared_biosamples)

dhs_meta_annotated_with_encode_from_leftover_biosamples <- dhs_meta_annotated_with_encode[dhs_meta_annotated_with_encode$encode_biosample_term_name %in% leftover_biosamples_terms, ]
nb_of_dhs_datasets_from_leftover_biosamples             <- nrow(dhs_meta_annotated_with_encode_from_leftover_biosamples)

###################################################
## Exporting a list of datasets to run COBIND on ##
###################################################

message("; Exporting a list of datasets to run COBIND on.")

fwrite(ub_meta_annotated_with_encode_from_shared_biosamples,
       file      = file.path(output.folder, "UB21_datasets_with_shared_biosamples.tsv"),
       col.names = TRUE, sep = "\t")

######################################
## Exporting a list of DHS datasets ##
######################################

message("; Exporting a list of DHS datasets.")

fwrite(dhs_meta_annotated_with_encode_from_shared_biosamples,
       file      = file.path(output.folder, "DHS_datasets_with_shared_biosamples_before_COBIND.tsv"),
       col.names = TRUE, sep = "\t")

fwrite(dhs_meta_annotated_with_encode_from_leftover_biosamples,
       file      = file.path(output.folder, "DHS_datasets_with_leftover_biosamples_before_COBIND.tsv"),
       col.names = TRUE, sep = "\t")

#############################
## Summarizing the numbers ##
#############################

message("; Summarizing the numbers of datasets.")

nb_of_datasets_tab <- rbind(nb_datasets_unibind, nb_datasets_dhs,
                            total_nb_datasets_encode, nb_datasets_encode_chip, nb_datasets_encode_dhs,
                            nb_datasets_unibind_encode_only,
                            nb_datasets_unibind_annotated_with_encode,
                            nb_datasets_dhs_annotated_with_encode,
                            nb_of_shared_biosample_terms, nb_of_leftover_biosample_terms,
                            nb_of_ub_datasets_from_shared_biosamples,
                            nb_of_dhs_datasets_from_shared_biosamples, nb_of_dhs_datasets_from_leftover_biosamples)

nb_of_datasets_tab <- as.data.frame(nb_of_datasets_tab)

colnames(nb_of_datasets_tab) <- c("number")

nb_of_datasets_tab <- nb_of_datasets_tab %>%
  rownames_to_column(var = "number_of")

fwrite(nb_of_datasets_tab,
       file      = file.path(output.folder, "numbers_of_datasets_ub_dhs.txt"),
       col.names = TRUE, sep = "\t")

#######################
## End of the script ##
#######################
