import sys
from NMF_script import *

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

matplotlib.use('Agg')

## Generating random sequences with known motif.

#---------------------#
# Running the script.
#---------------------#

## Running the script with arguments.
if __name__=='__main__':
	arguments=sys.argv


	motif_path=arguments[1]
	n_sequences=int(arguments[2])
	n_nucleotides=int(arguments[3])
	motif_proportion=float(arguments[4])
	motif_position=int(arguments[5])

	output_folder=arguments[6]
	# score_folder=arguments[7]
	experiment_name=arguments[7]
	file_name=arguments[8]
	# n_components=int(arguments[9])
	# trim_threshold=float(arguments[10])

	motif=np.loadtxt(motif_path)
	random_sequences=make_random_fasta(n_sequences,n_nucleotides)
	motif_sequences, modified_indices = add_pwm(random_sequences,motif,motif_proportion,motif_position,n_sequences)

	os.makedirs('{}/{}'.format(output_folder,experiment_name),exist_ok = True)
	# apply_NMF_trim(output_folder,score_folder,experiment_name,motif_sequences,n_components,trim_threshold,full_output=False)
	SeqIO.write([SeqRecord(Seq(motif_sequences[i]),str(i)) for i in range(len(motif_sequences))], '{}/{}/{}.fa'.format(output_folder,experiment_name,file_name), "fasta")

	indices_output_file_path = '{}/{}/{}_modified_indices.txt'.format(output_folder,experiment_name,file_name)
	with open(indices_output_file_path, 'w') as file:
		for index in modified_indices:
			file.write(str(index) + '\n')


## End of script.
