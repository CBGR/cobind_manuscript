#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "ggplot2",
                        "optparse",
                        "tidyverse",
                        "purrr")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(c("-r", "--region_files"), 
              type    = "character", 
              default = NULL,
              help    = "Directory with BED files that should be compared. (Mandatory) ", 
              metavar = "character"),
  
  make_option(c("-s", "--score_files"), 
              type    = "character", 
              default = NULL,
              help    = "Comma separated score files that were used for bwtool. (Mandatory) ", 
              metavar = "character"),
  
  make_option(c("-n", "--family"), 
              type    = "character", 
              default = NULL,
              help    = "Family name. (Mandatory)", 
              metavar = "character"),
  
  make_option(c("-o", "--output_folder"), 
              type = "character", 
              default = NULL,
              help = "Path to the output folder. (Mandatory)", 
              metavar = "character"),
  
  make_option(c("-f", "--spacings_dir"), 
              type    = "character", 
              default = "full",
              help    = "A path to the spacings directory. (Mandatory)", 
              metavar = "character"),
  
  make_option(c("-b", "--cobinder_id"), 
              type    = "character", 
              default = NULL,
              help    = "Cobinder number (ID) to indicate which border should be drawn. (Mandatory)", 
              metavar = "character"),
  
  make_option(c("-l", "--cobinder_location"), 
              type    = "character", 
              default = NULL,
              help    = "Cobinder location (left or right) to indicate which border should be drawn. (Mandatory)", 
              metavar = "character"),
  
  make_option(c("-a", "--cobinder_spacer"), 
              type    = "character", 
              default = NULL,
              help    = "Cobinder spacer to indicate which border should be drawn. (Mandatory)", 
              metavar = "character")
  
);

message("; Reading arguments from command line.")
opt_parser = OptionParser(option_list = option_list)
opt        = parse_args(opt_parser)

############
## Syntax ##
############

### Syntax:
# Rscript bin/dhs_footprinting/region_analysis/compute_analysis_statistics.R \
# -r COBIND_results/individual/HUMAN_for_DHS_0.99/results/cobinder_regions/BED/full_regions \
# -s /storage/mathelierarea/raw/UCSC/hg38/phastCons100way/hg38.phastCons100way.bw \
# -n IRF-family \
# -o /storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind/embl_colab/grn_tfs_strict_kim1_gini04/results/conservation_analysis/IRF-family/conservation_statistics \
# -f /storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind/embl_colab/grn_tfs_strict_kim1_gini04/results/clustering_results/IRF-family/spacings
# -b
# -l
# -a

########################
## Set variable names ##
########################

bed_dir                 <- opt$region_files
family_name             <- opt$family
score_files             <- opt$score_files
output_folder           <- opt$output_folder
spacings_folder         <- opt$spacings_dir
cobinder_number         <- opt$cobinder_id
cobinder_loc            <- opt$cobinder_location
cobinder_spac           <- opt$cobinder_spacer

###########
## Debug ##
###########

## setwd("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind")
# bed_dir         <- "/storage/scratch/ieva/COBIND_results/individual/HUMAN_0.99/results/cobinder_regions/BED/full_regions"
# score_files     <- "/storage/mathelierarea/raw/UCSC/hg38/phastCons100way/hg38.phastCons100way.bw"
# spacings_folder <- "/storage/scratch/ieva/COBIND_results/individual/HUMAN_0.99/results/clustering_results/CTCF/spacings"
# output_folder   <- "/storage/scratch/ieva/COBIND_results/individual/HUMAN_0.99/results/conservation_analysis/CTCF/conservation_statistics"
# family_name <- "CTCF"

#############
## Options ##
#############

options(stringsAsFactors = FALSE)

#############################
## Create output directory ##
#############################

message("; Creating output directory.")
dir.create(output_folder, showWarnings = F, recursive = T)

###############
## Functions ##
###############

extract_cons_values <- function(value_vector, c_start, c_end) {
  
  extracted_values <- unlist(strsplit(value_vector, ","))
  extracted_values <- extracted_values[c_start:c_end]
  extracted_values <- paste(extracted_values, collapse = ",")
  return(extracted_values)
}

########################
## Parsing parameters ##
########################

score_file_name <- unlist(strsplit(score_files, ","))

####################################
## Finding the boarders of motifs ##
####################################

spacings.tab <- fread(file.path(spacings_folder, "spacings_extended.txt"))

spacings.tab <- spacings.tab %>%
  select(Subcluster,
         Spacer_length,
         Location,
         pfm_matrix_length,
         core_pfm_matrix_length) %>%
  unique() %>%
  filter(Subcluster    == cobinder_number,
         Location      == cobinder_loc,
         Spacer_length == cobinder_spac)

## Conditional coordinate building:
spacings.tab <- spacings.tab %>%
    mutate(x_core = (ceiling(core_pfm_matrix_length/2)) * -1) %>%
    mutate(xend_core = ceiling(core_pfm_matrix_length/2))
  
## Core location:
core_segment <- spacings.tab %>%
    mutate(motif = "Anchor") %>%
    mutate(x = min(x_core),
           xend = max(xend_core)) %>%
    select(Subcluster, motif, x, xend) %>%
    unique()
  
## Cobinder location:
cobinder_segment <- spacings.tab %>%
    mutate(motif = paste0("cobinder_subcluster_", Subcluster, "_", Location, "_", Spacer_length)) %>%
    mutate(x = ifelse(Location == "right", xend_core + Spacer_length, x_core - Spacer_length - pfm_matrix_length)) %>%
    mutate(xend = ifelse(Location == "right", xend_core + Spacer_length + pfm_matrix_length, x_core - Spacer_length)) %>%
    select(Subcluster, motif, x, xend)

##############################################
## Building paths to the cobinder BED files ##
##############################################
message("; Building paths to co-binder BED files.")

cobinder_segment <- cobinder_segment %>%
  mutate(bed_path_no_cobinder   = file.path(bed_dir,
                                          "no_cobinder_case_motif_specific",
                                          family_name,
                                          paste0("full_regions_no_",
                                                 motif, ".bed"))) %>%
  mutate(bed_path_with_cobinder = file.path(bed_dir,
                                          "with_cobinder_case_motif_specific",
                                          family_name,
                                          paste0("full_regions_with_",
                                                 motif, ".bed")))

######################################################
## Computing conservation for anchors and cobinders ##
######################################################

## Empty dataframe to add results of conservation statistics:
conservation_stats_results <- data.frame()

##===========================================================================================##
## First will loop through conservation scores, then cobinder and each version of a cobinder ##
##===========================================================================================##

list.of.cobinders <- unique(cobinder_segment$Subcluster)

for (cobinder_subcluster in list.of.cobinders) {
  
  message("; Analysing for cobinder ", cobinder_subcluster)
  
  ## Subsetting cobinder coordinates:
  cobinder_segment_subset <- cobinder_segment %>%
    filter(Subcluster == cobinder_subcluster)
  
  ## Subsetting core coordinates:
  core_segment_subset <- core_segment %>%
    filter(Subcluster == cobinder_subcluster)
  
  ## Loop:
  for (conservation_file in score_files) {
    
    conservation_file_name <- basename(conservation_file)
    message("; Analysing for: ", conservation_file_name)
    
    ## Empty dataframe to concat the conservation values for with and without cobinders:
    no_cobinder_anchor_conservation_df   <- vector("list",  nrow(cobinder_segment_subset))
    with_cobinder_anchor_conservation_df <- vector("list",  nrow(cobinder_segment_subset))
    
    for (i in 1:nrow(cobinder_segment_subset)) {
      
      entry      <- cobinder_segment_subset[i, ]
      motif_name <- entry$motif
      
      message("; Analysing for ", motif_name)
      
      # ## Cobinder ID:
      # cobinder_id <- entry$Subcluster
      
      bed_no_cobinder   <- entry$bed_path_no_cobinder
      bed_with_cobinder <- entry$bed_path_with_cobinder
  
      
      bed_name_no_cobinder   <- gsub(basename(bed_no_cobinder), pattern = ".bed", replacement = "")
      bed_name_with_cobinder <- gsub(basename(bed_with_cobinder), pattern = ".bed", replacement = "")
      
      ## Conservation computation:
      no.cobinder.cons.result.file   <- file.path(output_folder, 
                                                  paste0(bed_name_no_cobinder, "_conservation_results.bed"))
      with.cobinder.cons.result.file <- file.path(output_folder, 
                                                  paste0(bed_name_with_cobinder, "_conservation_results.bed"))
      
      ## BWtools:
      no.cobinder.bwtools.command <- paste0("bwtool extract bed ",
                                            bed_no_cobinder, " ",
                                            conservation_file, " ",
                                            no.cobinder.cons.result.file)
      with.cobinder.bwtools.command <- paste0("bwtool extract bed ",
                                              bed_with_cobinder, " ",
                                              conservation_file, " ",
                                              with.cobinder.cons.result.file)
      system(with.cobinder.bwtools.command)
      system(no.cobinder.bwtools.command)
      
      ## Reading the results:
      no_cobinder_conservation_results_bed   <- fread(no.cobinder.cons.result.file, sep = "\t", header = FALSE)
      with_cobinder_conservation_results_bed <- fread(with.cobinder.cons.result.file, sep = "\t", header = FALSE)
      
      colnames(no_cobinder_conservation_results_bed)   <- c("chr", "start", "end", "strand", "size", "conservation_values")
      colnames(with_cobinder_conservation_results_bed) <- c("chr", "start", "end", "strand", "size", "conservation_values")
      
      no_cobinder_conservation_results_bed   <- no_cobinder_conservation_results_bed %>%
        dplyr::select(conservation_values, strand, size)
      with_cobinder_conservation_results_bed <- with_cobinder_conservation_results_bed %>%
        dplyr::select(conservation_values, strand, size)
    
      
      ## Finding the indices of the anchor and adding to the main table:
      no_cobinder_conservation_results_bed   <- no_cobinder_conservation_results_bed %>%
        mutate(half_region       = as.numeric(size)/2) %>%
        mutate(anchor_start      = half_region + core_segment_subset$x,
               anchor_end        = half_region + core_segment_subset$xend)
      
      with_cobinder_conservation_results_bed <- with_cobinder_conservation_results_bed %>%
        mutate(half_region       = as.numeric(size)/2) %>%
        mutate(anchor_start      = half_region + core_segment_subset$x) %>%
        mutate(anchor_end        = half_region + core_segment_subset$xend)
      
      # print(no_cobinder_conservation_results_bed)
      # print(with_cobinder_conservation_results_bed)
      ### Adding conservation values to the main data frame to later compute anchor statistics:
      no_cobinder_anchor_conservation_df[[i]]   <- no_cobinder_conservation_results_bed
      with_cobinder_anchor_conservation_df[[i]] <- with_cobinder_conservation_results_bed
      
      ##-----------------------##
      ## COBINDER conservation ##
      ##-----------------------##
      
      ###### 1. NO COBINDER ######
      
      no_cobinder_conservation_results_bed <- no_cobinder_conservation_results_bed %>%
        mutate(cobinder_start = ifelse(strand == "+",
                                       half_region + entry$x,
                                       half_region + (-1 * entry$xend))) %>%
        mutate(cobinder_end   = ifelse(strand == "+",
                                       half_region + entry$xend,
                                       half_region + (-1 * entry$x))) %>%
        mutate(floor_start    = floor(cobinder_start),
               ceiling_end    = ceiling(cobinder_end)) %>%
        mutate(floor_start    = ifelse(floor_start < 0,    yes = 0,    no = floor_start),
               ceiling_end    = ifelse(ceiling_end > size, yes = size, no = ceiling_end))
      
      no_cobinder_conservation <- purrr::pmap(list(no_cobinder_conservation_results_bed$conservation_values,
                                                   no_cobinder_conservation_results_bed$floor_start,
                                                   no_cobinder_conservation_results_bed$ceiling_end),
                                              extract_cons_values)
      
      no_cobinder_conservation <- unlist(no_cobinder_conservation)
      no_cobinder_conservation <- unlist(sapply(no_cobinder_conservation, strsplit, split = ",", USE.NAMES = FALSE))
      no_cobinder_conservation <- suppressWarnings(as.numeric(no_cobinder_conservation))
      no_cobinder_conservation <- na.omit(as.vector(no_cobinder_conservation))
      
      
      no_cobinder_conservation <- as.data.frame(no_cobinder_conservation) %>%
        mutate(cobinder_conservation = as.numeric(no_cobinder_conservation)) %>%
        dplyr::select(cobinder_conservation) %>%
        mutate(bed_file_name = bed_name_no_cobinder) %>%
        mutate(motif_location_spacer = motif_name) %>%
        mutate(conservation_score = conservation_file_name)
      
      ###### 2. WITH COBINDER ######
      
      with_cobinder_conservation_results_bed <- with_cobinder_conservation_results_bed %>%
        mutate(cobinder_start = ifelse(strand == "+",
                                       half_region + entry$x,
                                       half_region + (-1 * entry$xend))) %>%
        mutate(cobinder_end   = ifelse(strand == "+",
                                       half_region + entry$xend,
                                       half_region + (-1 * entry$x))) %>%
        mutate(floor_start   = floor(cobinder_start),
               ceiling_end   = ceiling(cobinder_end)) %>%
        mutate(floor_start    = ifelse(floor_start < 0,    yes = 0,    no = floor_start),
               ceiling_end    = ifelse(ceiling_end > size, yes = size, no = ceiling_end)) 
      
      with_cobinder_conservation <- purrr::pmap(list(with_cobinder_conservation_results_bed$conservation_values,
                                                     with_cobinder_conservation_results_bed$floor_start,
                                                     with_cobinder_conservation_results_bed$ceiling_end),
                                                extract_cons_values)
      
      with_cobinder_conservation <- unlist(with_cobinder_conservation)
      with_cobinder_conservation <- unlist(sapply(with_cobinder_conservation, 
                                                  strsplit, split = ",", USE.NAMES = FALSE))
      with_cobinder_conservation <- suppressWarnings(as.numeric(with_cobinder_conservation))
      with_cobinder_conservation <- na.omit(as.vector(with_cobinder_conservation))
      
      
      with_cobinder_conservation <- as.data.frame(with_cobinder_conservation) %>%
        mutate(cobinder_conservation = as.numeric(with_cobinder_conservation)) %>%
        dplyr::select(cobinder_conservation) %>%
        mutate(bed_file_name = bed_name_with_cobinder) %>%
        mutate(motif_location_spacer = motif_name) %>%
        mutate(conservation_score = conservation_file_name)
      
      ## Combining conservation:
      # cobinder_conservation_df <- rbind(with_cobinder_conservation, no_cobinder_conservation)
      
      ##### Wilcox test #####
      cobinder_wil_test <- wilcox.test(with_cobinder_conservation$cobinder_conservation,
                                       no_cobinder_conservation$cobinder_conservation,
                                       paired = FALSE,
                                       alternative = "less")
      cobinder_wil_test_two_sided <- wilcox.test(with_cobinder_conservation$cobinder_conservation,
                                       no_cobinder_conservation$cobinder_conservation,
                                       paired = FALSE,
                                       alternative = "two.sided")
      
      cobinder_test_result <- cbind(cobinder_subcluster,
                                    motif_name,
                                    conservation_file_name,
                                    as.numeric(cobinder_wil_test$p.value),
                                    as.numeric(cobinder_wil_test_two_sided$p.value))
      
      colnames(cobinder_test_result) <- c("cobinder_id",
                                          "cobinder_location_spacer", 
                                          "conservation_score",
                                          "p_value",
                                          "two_sided_p_value")
      
      ## Adding resulting p-value to the main dataframe:
      conservation_stats_results <- rbind(conservation_stats_results, cobinder_test_result)
      
      ##-------------##
      ## Cleaning up ##
      ##-------------##
      # invisible(file.remove(no.cobinder.cons.result.file))
      # invisible(file.remove(with.cobinder.cons.result.file))
      
      if (file.exists(no.cobinder.cons.result.file)) {
        invisible(file.remove(no.cobinder.cons.result.file))
      }
      if (file.exists(with.cobinder.cons.result.file)) {
        invisible(file.remove(with.cobinder.cons.result.file))
      }
    }
    
    ##---------------------##
    ## ANCHOR conservation ##
    ##---------------------##
    
    ## Calling back all the regions for anchor conservation analysis:
    no_cobinder_anchor_conservation_df   <- do.call("rbind", no_cobinder_anchor_conservation_df) %>%
      dplyr::select(-size, -half_region) %>%
      unique() %>%
      mutate(ceiling_start = ceiling(anchor_start),
             floor_end     = floor(anchor_end))
    
    with_cobinder_anchor_conservation_df <-  do.call("rbind", with_cobinder_anchor_conservation_df) %>%
      dplyr::select(-size, -half_region) %>%
      unique() %>%
      mutate(ceiling_start = ceiling(anchor_start),
             floor_end     = floor(anchor_end))
    
    ##### 1. NO COBINDER #####
    
    ## Extracting conservation values for anchors:
    no_cobinder_anchor_conservation <- purrr::pmap(list(no_cobinder_anchor_conservation_df$conservation_values,
                                                        no_cobinder_anchor_conservation_df$ceiling_start, 
                                                        no_cobinder_anchor_conservation_df$floor_end), 
                                                   extract_cons_values)
    
    no_cobinder_anchor_conservation <- unlist(no_cobinder_anchor_conservation)
    no_cobinder_anchor_conservation <- unlist(sapply(no_cobinder_anchor_conservation, 
                                                     strsplit, 
                                                     split = ",",
                                                     USE.NAMES = FALSE))
    
    no_cobinder_anchor_conservation <- suppressWarnings(as.numeric(no_cobinder_anchor_conservation))
    no_cobinder_anchor_conservation <- na.omit(no_cobinder_anchor_conservation)
    
    no_cobinder_anchor_conservation <- as.data.frame(no_cobinder_anchor_conservation) %>%
      mutate(anchor_conservation = as.numeric(no_cobinder_anchor_conservation)) %>%
      dplyr::select(anchor_conservation) %>%
      mutate(bed_file_name = "anchor_no_cobinder") %>%
      mutate(motif_location_spacer = motif_name) %>%
      mutate(conservation_score = conservation_file_name)
    
    ##### 2. WITH COBINDER #####
    
    with_cobinder_anchor_conservation <- purrr::pmap(list(with_cobinder_anchor_conservation_df$conservation_values,
                                                          with_cobinder_anchor_conservation_df$ceiling_start, 
                                                          with_cobinder_anchor_conservation_df$floor_end), 
                                                     extract_cons_values)
    
    with_cobinder_anchor_conservation <- unlist(with_cobinder_anchor_conservation)
    with_cobinder_anchor_conservation <- unlist(sapply(with_cobinder_anchor_conservation, 
                                                     strsplit, 
                                                     split = ",",
                                                     USE.NAMES = FALSE))
    
    with_cobinder_anchor_conservation <- suppressWarnings(as.numeric(with_cobinder_anchor_conservation))
    with_cobinder_anchor_conservation <- na.omit(with_cobinder_anchor_conservation)
    
    with_cobinder_anchor_conservation <- as.data.frame(with_cobinder_anchor_conservation) %>%
      mutate(anchor_conservation = as.numeric(with_cobinder_anchor_conservation)) %>%
      dplyr::select(anchor_conservation) %>%
      mutate(bed_file_name = "anchor_with_cobinder") %>%
      mutate(motif_location_spacer = motif_name) %>%
      mutate(conservation_score = conservation_file_name)
    
    ##### Wilcox test #####
    anchor_wil_test <- wilcox.test(with_cobinder_anchor_conservation$anchor_conservation,
                                   no_cobinder_anchor_conservation$anchor_conservation,
                                   paired = FALSE,
                                   alternative = "less")
    
    anchor_wil_test_two_sided <- wilcox.test(with_cobinder_anchor_conservation$anchor_conservation,
                                   no_cobinder_anchor_conservation$anchor_conservation,
                                   paired = FALSE,
                                   alternative = "two.sided")
    
    anchor_test_result <- cbind(cobinder_subcluster,
                                # "anchor",
                                gsub(motif_name, pattern = "cobinder_", replacement = "anchor_"),
                                conservation_file_name,
                                as.numeric(anchor_wil_test$p.value),
                                as.numeric(anchor_wil_test_two_sided$p.value))
    
    colnames(anchor_test_result) <- c("cobinder_id",
                                        "cobinder_location_spacer", 
                                        "conservation_score",
                                        "p_value",
                                      "two_sided_p_value")
    
    ## Adding resulting p-value to the main dataframe:
    conservation_stats_results <- rbind(conservation_stats_results, anchor_test_result)

  }
}

#######################################
## Formatting the final output table ##
#######################################

message("; Exporting summarized results.")

conservation_stats_results <- conservation_stats_results %>%
  dplyr::mutate(p_value_formatted = formatC(as.numeric(p_value), format = "e", digits = 2),
                two_sided_p_value_formatted = formatC(as.numeric(two_sided_p_value), format = "e", digits = 2))


## Exporting 
wilcox.tab.path <- file.path(output_folder, "wilcox_test_no_vs_with_cobinder.txt")
fwrite(conservation_stats_results,
       file = wilcox.tab.path,
       sep = "\t",
       col.names = TRUE)

###################
## End of script ##
###################