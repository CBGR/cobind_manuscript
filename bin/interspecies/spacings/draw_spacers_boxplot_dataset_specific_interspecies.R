#####################
## Load R packages ##
#####################

required.libraries <- c("bedr",
                        "cowplot",
                        "data.table",
                        "dplyr",
                        "ggplot2",
                        "grid",
                        "optparse",
                        "patchwork",
                        "png",
                        "stringr")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only=TRUE, quietly = T))
}

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(c("-i", "--spacing_folder"), type = "character", default = NULL,
              help = "Path to the folder with the spacing information and the logos. (Mandatory) ", metavar = "character"),
  
  make_option(c("-t", "--analysis_titles"), type = "character", default = NULL,
              help = "Analysis titles matching all titles in the config.yaml. (Mandatory) ", metavar = "character"),
  
  make_option(c("-l", "--file_labels"), type = "character", default = NULL,
              help = "File labels that are used to distinguish BED files between different species (order should correspond to analysis titles order). (Mandatory) ", metavar = "character"),
  
  make_option(c("-f", "--family_name"), type = "character", default = NULL,
              help = "Individual TF, family or class name, based on which level clustering was done. (Mandatory) ", metavar = "character"),
  
  make_option(c("-o", "--output_directory"), type = "character", default = NULL,
              help = "Output directory to create directories for region output and plots. (Mandatory) ", metavar = "character")
  
);

message("; Reading arguments from command line")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

############
## Syntax ##
############

### Syntax:
## R-scripts/spacers_heatmaps_dataset_specific.R
##        -i interspecies_summary_AT_DR_RN_MM_HS/joined_clustering_results/CTCF
##        -t GINI_3to6_individual_ARABIDOPSIS_02,
##           GINI_3to6_individual_DRERIO_02,
##           GINI_3to6_individual_RAT_02,
##           GINI_3to6_individual_MOUSE_02,
##           GINI_3to6_individual_HUMAN_75CTCF
##        -l AT,DR,RN,MM,HS
##        -f CTCF
##        -o interspecies_summary_AT_DR_RN_MM_HS

########################
## Set variable names ##
########################
spacing.folder         <- opt$spacing_folder
analysis.title        <- opt$analysis_titles
file.labels            <- opt$file_labels
family.name            <- opt$family_name
analysis.output        <- opt$output_directory

## Debug
# setwd("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind")
# spacing.folder <- "test_interspecies/joined_clustering_results/CTCF/spacings"
# analysis.title <- c("RAT_test","DRERIO_test")
# file.labels <- c("RN","DR")
# family.name <- "CTCF"
# analysis.output <- "test_interspecies"

###########################
## Un-listing parameters ##
###########################

analysis.titles         <- unlist(strsplit(analysis.title, ","))
file.labels             <- unlist(strsplit(file.labels, ","))

file_mapping <- as.data.frame(cbind(analysis.titles, file.labels))
colnames(file_mapping) <- c("analysis_title", "file_label")

###############
## Functions ##
###############

### 1.
## Creating a fake entry in a table to have the same axis.
fake.entry <- function(l = 0, s = 0, nb.entries = nb.subclusters) {
  ## Left : l = 0
  ## Right: l = 1
  if (l == 0) {
    l <- "left"
  } else if (l == 1) {
    l <- "right"
  }
  return(
    data.frame(Dataset = "Fake",
               Core_logo = "Fake",
               Cobinder_logo = empty_logo,
               Subcluster = paste0("Subcluster_", 0:nb.entries),
               Location = l,
               Spacer = s,
               Fraction = max.fraction,
               Nb_datasets = 0)
  )
}

### 2.
## To insert the logos
annotation_custom2 <- 
  function (grob, xmin = -Inf, xmax = Inf, ymin = -Inf, ymax = Inf, data){
    
    layer(data = data, stat = StatIdentity, position = PositionIdentity,          
          geom = ggplot2:::GeomCustomAnn,
          inherit.aes = TRUE, params = list(grob = grob,
                                            xmin = xmin,
                                            xmax = xmax, 
                                            ymin = ymin,
                                            ymax = ymax))
  }

########################
## Read spacing table ##
########################

message(paste0("; Reading table with spacing information for ", family.name, "."))
spacing.tab <- fread(file.path(spacing.folder, "spacings.txt"), header = FALSE)

##############################
## Finding flank extensions ##
##############################
one.title <- file_mapping$analysis_title[1]

message("; Finding flank extension information.")
one.title.folder <- file.path(one.title, "results")

## Finding flank sizes from existing data:
BED_files <- list.files(file.path(one.title.folder, "datasets", (list.files(file.path(one.title.folder, "datasets"))[1]), "BED"))
BED_files <- grep(pattern = ".+subtd", BED_files, value = TRUE)

right_flank_extension <- grep(pattern = ".+\\.0l\\d+r", BED_files, value = TRUE)
left_flank_extension <- grep(pattern = ".+\\.\\d+l0r\\.", BED_files, value = TRUE)

right_flank_extension <- gsub(pattern = ".+\\.bed\\.oriented", replacement = "", right_flank_extension)
left_flank_extension <- gsub(pattern = ".+\\.bed\\.oriented", replacement = "", left_flank_extension)

#####################################
## Constructing new spacings table ##
#####################################

## Extracting relevant info to build tables for left and right flanks.
message("; Creating a table with relevant info.")

spacing.tab <- spacing.tab[,c(1, 2, 11, 14, 15, 16)] %>%
  dplyr::rename(Core = V1,
                Cobinder = V2,
                Flank = V11,
                Spacer = V14,
                Location = V15,
                Subcluster = V16) %>%
  mutate(Dataset = gsub(Core, pattern = "_\\D{4,5}_\\d+_components_.+", replacement = "")) %>%     ## Get the dataset ID
  mutate(Nb_components = stringr::str_extract(Core, pattern = "\\d+_components")) %>%
  mutate(Component = stringr::str_extract(Core, pattern = "Comp_\\d+")) %>%
  mutate(Component = gsub(Component, pattern = "Comp_", replacement = "")) %>%
  mutate(file_label = stringr::str_extract(Dataset, pattern = "^\\D{2}"))
# mutate(file_label = unlist(strsplit(Dataset, split = "_"))[1])

spacing.tab <- left_join(spacing.tab, file_mapping,
                         by = c("file_label" = "file_label")) %>%
  mutate(Path_to_indices = file.path(analysis_title, "results", "nmf_motifs", paste0(Dataset, "_", Flank), Nb_components, paste0("indices_", Component, ".txt")))

## Separating the table into two sides - left and right - according to NMF flank side.
spacing.tab.r <- spacing.tab %>% filter(Flank == "right")
spacing.tab.l <- spacing.tab %>% filter(Flank == "left")

## Constructing a table with file info for the right side.
spacing.tab.r <- spacing.tab.r %>%
  mutate(Path_to_BED = file.path(analysis_title, "results", "datasets", Dataset, "BED", paste0(Dataset, ".bed.oriented", right_flank_extension)))

## Constructing a table with file info for the left side.
spacing.tab.l <- spacing.tab.l %>%
  mutate(Path_to_BED = file.path(analysis_title, "results", "datasets", Dataset, "BED", paste0(Dataset, ".bed.oriented", left_flank_extension)))

## Combining left and right tables.
spacings.file.tab <- rbind(spacing.tab.r,
                           spacing.tab.l) %>%
  arrange(Subcluster, Location, Spacer, Dataset, Nb_components, Component)

###################################################
## Extracting sequences and counting the regions ##
###################################################
## Grouping the table and getting indices.

spacings.tab.grouped <- spacings.file.tab %>%
  group_by(Subcluster, Location, Spacer, Dataset)

group_indices <- group_rows(spacings.tab.grouped)

## Extracting regions and counting them for each group.
nb_regions_in_group <- data.frame()
all_groups_entry_info <- data.frame()

for (i in seq_along(group_indices)) {
  
  group_entry <- group_indices[[i]]
  
  pooled_filtered_group_bed <- data.frame()
  group_entry_info <- data.frame()
  
  for (j in seq_along(group_entry)) {
    
    entry_idx <- group_entry[j]
    entry <- spacings.file.tab[entry_idx, ]
    
    ## Creating a case code.
    case_code <- paste0(entry$Dataset,
                        "_flank_", entry$Flank, "_",
                        entry$Nb_components,
                        "_Comp_", entry$Component,
                        "_subcluster_", entry$Subcluster,
                        "_spacer_", entry$Spacer, "_", entry$Location)
    
    ## Reading indices.
    indices <- fread(file.path(entry$Path_to_indices))
    indices <- indices$V1
    
    ## Reading and filtering the bed based on indices for a specific case.
    entry_bed <- fread(file.path(entry$Path_to_BED))
    filtered_entry_bed <- entry_bed[indices + 1]
    
    ## Pooling all regions into one for one group:
    filtered_bed_modified <- filtered_entry_bed %>%
      select(V1, V2, V3) %>%
      mutate(V4 = case_code)
    
    pooled_filtered_group_bed <- rbind(pooled_filtered_group_bed, filtered_bed_modified)
    
    pooled_filtered_bed_length <- nrow(pooled_filtered_group_bed)
    dataset_original_length <- nrow(entry_bed)
    
    group_entry_info <- cbind(pooled_filtered_bed_length, dataset_original_length)
    
  }
  
  all_groups_entry_info <- rbind(all_groups_entry_info, group_entry_info)
  
  # Cleaning a filtered BED for each group
  merged_group_bed <- pooled_filtered_group_bed %>%
    arrange(V1, V2, V3) %>%
    group_by(V1, V2, V3) %>% 
    summarise(V4 = paste(V4, collapse = ","), .groups = 'drop') %>%
    ungroup()
  
  merged_group_bed_length <- nrow(merged_group_bed)
  
  ## Cleaning a BED of all group regions
  merged_entry_bed <- entry_bed %>%
    arrange(V1, V2, V3) %>%
    group_by(V1, V2, V3) %>% 
    summarise(V4 = paste(V4, collapse = ","), .groups = 'drop') %>%
    ungroup()
  
  all_group_length <- nrow(merged_entry_bed)
  
  nb_merged_regions <- cbind(all_group_length, merged_group_bed_length)
  nb_regions_in_group <- rbind(nb_regions_in_group, nb_merged_regions)
  
}

####################################################
## Modifying spacings tab with numbers of regions ##
####################################################
message("; Adding info about the numbers of regions.")

## Collecting all the info generated by the loop.
sequence_length_info <- cbind(all_groups_entry_info, nb_regions_in_group)

## Recreating the spacings tab to include sequence length info.
spacing.tab <- spacings.tab.grouped %>%
  ungroup() %>%
  mutate(Core_logo = file.path(spacing.folder, paste0("core_subcluster_", Subcluster, ".png")),  ## Logo file name
         Cobinder_logo = file.path(spacing.folder, paste0("cobinder_subcluster_", Subcluster, ".png")),
         Subcluster = paste0("Subcluster_", Subcluster)) %>%          
  select(Dataset, Core_logo, Cobinder_logo, Subcluster, Location, Spacer) %>%
  unique() %>%
  cbind(sequence_length_info) %>% 
  mutate(Fraction = round(merged_group_bed_length/all_group_length, digits = 3)) %>%
  select(Dataset, Core_logo, Cobinder_logo, Subcluster, Location, Spacer, Fraction) %>%
  group_by(Subcluster, Location, Spacer) %>%
  mutate(Nb_datasets = length(unique(Dataset))) %>%
  ungroup()

## Dividing the table into two, based on the location of the spacer.
spacing.tab.r <- spacing.tab %>% filter(Location == "right")
spacing.tab.l <- spacing.tab %>% filter(Location == "left")
spacing.tab.l$Spacer <- -spacing.tab.l$Spacer

## Creating a fake entries to have all the spacings and subclusters in the plot.
# max.spacer <- max(spacing.tab$Spacer)
# max.spacer <- 0
min.spacer <- 0

max.spacer.l <- ifelse(nrow(spacing.tab.l) == 0, 0, min(spacing.tab.l$Spacer))
max.spacer.r <- ifelse(nrow(spacing.tab.r) == 0, 0, max(spacing.tab.r$Spacer))

nb.subclusters <- length(unique(spacing.tab$Subcluster)) - 1
max.fraction <- max(spacing.tab$Fraction)

## Empty logo path
empty_logo <- file.path(analysis.output, "empty_logo.png")

## Building tables with fake entries to for cobinder logos insertion.
## LEFT
spacing.tab.l.fake <- rbind(fake.entry(l = 0, s = 0),
                            fake.entry(l = 0, s = max.spacer.l))

spacing.tab.l.fake$Subcluster <- unique(spacing.tab$Subcluster)

spacing.tab.l.fake <- rbind(spacing.tab.l,
                            spacing.tab.l.fake)

## RIGHT
spacing.tab.r.fake <- rbind(fake.entry(l = 1, s = 0),
                            fake.entry(l = 1, s = max.spacer.r))

spacing.tab.r.fake$Subcluster <- unique(spacing.tab$Subcluster)

spacing.tab.r.fake <- rbind(spacing.tab.r,
                            spacing.tab.r.fake)


#####################################
## Drawing boxplots for both sides ##
#####################################
text.size <- 15
max.nb.datasets <- max(spacing.tab$Nb_datasets)
max.nb.subclusters <- nb.subclusters + 1

legend.breaks <- c(0,
                   round(mean(c(0, max.nb.datasets))),
                   max.nb.datasets)

message("; Drawing upstream and downstream boxplots.")

## Boxplot for the left side
boxplot.left <- ggplot(data = spacing.tab.l, aes(x = Spacer, y = Fraction, group = Spacer, fill = Nb_datasets)) +
  geom_boxplot(na.rm = TRUE) +
  geom_dotplot(binaxis ='y', stackdir ='center', dotsize = 1, na.rm = TRUE, binwidth = 0.003) +
  facet_wrap(. ~ Subcluster, ncol = 1) +
  theme_bw() +
  scale_fill_distiller(palette = "RdPu", 
                       direction = +1,
                       limits = c(1, max.nb.datasets),
                       breaks = legend.breaks,
                       na.value = "white") +
  scale_y_continuous(limits = c(0, max.fraction)) +
  scale_x_continuous(breaks = seq(max.spacer.l-1, 0, by = 1), labels = seq(max.spacer.l-1, 0, by = 1)) +
  labs(x = "UPSTREAM spacer",
       y = "Fraction of the cobinder",
       fill = "Number of datasets") +
  geom_blank(data = spacing.tab.l.fake) +
  theme(legend.position="none",
        legend.title = element_text(size = text.size),
        axis.text.x = element_text(size = text.size),
        axis.text.y = element_text(size = text.size),
        axis.title.x = element_text(size = text.size, face = "bold"),
        axis.title.y = element_text(size = text.size),
        strip.text.x = element_text(size = text.size))

## Boxplot for the right side
boxplot.right <- ggplot(data = spacing.tab.r, aes(x = Spacer, y = Fraction, group = Spacer, fill = Nb_datasets)) +
  geom_boxplot(na.rm = TRUE) +
  geom_dotplot(binaxis = 'y', stackdir = 'center', dotsize = 1, na.rm = TRUE, binwidth = 0.003) +
  facet_wrap(. ~ Subcluster, ncol = 1)  +
  theme_bw() +
  scale_fill_distiller(palette = "RdPu", 
                       direction = +1,
                       limits = c(1, max.nb.datasets),
                       breaks = legend.breaks,
                       na.value = "white") +
  scale_y_continuous(limits = c(0, max.fraction), position = "right") +
  scale_x_continuous(breaks = seq(0, max.spacer.r+1, by = 1), labels = seq(0, max.spacer.r+1, by = 1)) +
  labs(x = "DOWNSTREAM spacer",
       y = "",
       fill = "Number of datasets") +
  geom_blank(data = spacing.tab.r.fake) +
  theme(legend.position="none",                                                  
        axis.text.x = element_text(size = text.size),
        axis.text.y = element_text(size = text.size),
        axis.title.x = element_text(size = text.size, face = "bold"),
        strip.text.x = element_text(size =text.size))

## Ordering a list just in case, so the logos are inserted in the correct order:
subclusters.order <- rbind(spacing.tab.l.fake, spacing.tab.r.fake) %>%
  filter(Core_logo != "Fake") %>%
  arrange(desc(Subcluster)) %>%
  select(Subcluster, Spacer)


## Subclusters names in order
subclusters.order <- unique(as.vector(subclusters.order$Subcluster))


## Empty dataframe to use as template ##
fake.tile.df <- data.frame(Subcluster = subclusters.order,
                           Nb_datasets = "A",
                           Spacer = 0,
                           Fraction = max.fraction)

#####################
## Core motif logo ##
#####################
message("; Inserting core logos.")

fake.tile.plot <- ggplot(data = fake.tile.df, aes(x = Spacer, y = Fraction, group = Spacer, fill = Nb_datasets)) +
  geom_boxplot(fill = "white", colour = "white") +
  facet_wrap(. ~ Subcluster, ncol = 1)  +
  theme_classic() +
  ylim(c(0, max.fraction)) +
  xlim(c(-1.1, 1.1)) +
  labs(x = "CORE motif",
       y = "") +
  theme(legend.position="none",
        axis.ticks = element_blank(),
        axis.line = element_blank(),
        axis.text.y = element_blank(),
        axis.text.x = element_text(size = text.size, colour = "white"),
        axis.title.x = element_text(size = text.size, face = "bold", colour = "black"),
        strip.text.x = element_text(size =text.size, colour = "white"),
        strip.background = element_blank())

spacing.plot.core.logos <- fake.tile.plot

## Inserting logos
for (i in 1:length(subclusters.order)) {                
  
  subcluster <- subclusters.order[i]
  
  logo <- as.vector(subset(spacing.tab, Subcluster == subcluster)[1,]$Core_logo)
  logo.png <- png::readPNG(logo)
  
  inserted_logo = annotation_custom2(rasterGrob(logo.png, interpolate=TRUE), 
                                     xmin = -1, 
                                     xmax = 1, 
                                     ymin = 0, 
                                     ymax = max.fraction, 
                                     data = fake.tile.df[i,])
  
  spacing.plot.core.logos <- spacing.plot.core.logos + inserted_logo
  
}

##########################
## Co-binder motif logo ##
##########################
message("; Inserting cobinder logos upstream.")

## Creating an empty logo file to insert as blanks:
png::writePNG(array(1, dim = c(700,1100,3)), empty_logo)

## Empty plot-space:
spacing.plot.cobinder.logos.l <- ggplot(data = fake.tile.df, aes(x = Spacer, y = Fraction, group = Spacer, fill = Nb_datasets)) +
  geom_boxplot(fill = "white", colour = "white") +
  facet_wrap(. ~ Subcluster, ncol = 1)  +
  theme_classic() +
  ylim(c(0, max.fraction)) +
  xlim(c(-1.1, 1.1)) +
  labs(x = "Upstream COBINDER motif",
       y = "") +
  theme(legend.position="none",
        axis.ticks = element_blank(),
        axis.line = element_blank(),
        axis.text.y = element_blank(),
        axis.title.y = element_blank(),
        axis.text.x = element_text(size = text.size, colour = "white"),
        axis.title.x = element_text(size = text.size, face = "bold", colour = "black"),
        strip.text.x = element_text(size =text.size, colour = "white"),
        strip.background = element_blank())


## Inserting logos for the left side:
for (i in 1:length(subclusters.order)) {
  
  subcluster <- subclusters.order[i]
  
  logo <- as.vector(subset(spacing.tab.l.fake, Subcluster == subcluster)[1,]$Cobinder_logo)
  # print(logo)
  logo.png <- png::readPNG(logo)
  # print(head(logo.png))
  
  inserted_logo = annotation_custom2(rasterGrob(logo.png, interpolate=TRUE), 
                                     xmin = -1, 
                                     xmax = 1, 
                                     ymin = 0, 
                                     ymax = max.fraction, 
                                     data = fake.tile.df[i,])
  
  spacing.plot.cobinder.logos.l <- spacing.plot.cobinder.logos.l + inserted_logo
}

## Empty plot-space for the right side:
spacing.plot.cobinder.logos.r <- ggplot(data = fake.tile.df, aes(x = Spacer, y = Fraction, group = Spacer, fill = Nb_datasets)) +
  geom_boxplot(fill = "white", colour = "white") +
  facet_wrap(. ~ Subcluster, ncol = 1)  +
  theme_classic() +
  ylim(c(0, max.fraction)) +
  xlim(c(-1.1, 1.1)) +
  labs(x = "Downstream COBINDER motif",
       y = "") +
  theme(legend.position="none",
        axis.ticks = element_blank(),
        axis.line = element_blank(),
        axis.text.y = element_blank(),
        axis.title.y = element_blank(),
        axis.text.x = element_text(size = text.size, colour = "white"),
        axis.title.x = element_text(size = text.size, face = "bold", colour = "black"),
        strip.text.x = element_text(size =text.size, colour = "white"),
        strip.background = element_blank())

message("; Inserting cobinder logos downstream.")
## Inserting logos for the right side:
for (i in 1:length(subclusters.order)) {                ## Insert logos, one by one
  
  subcluster <- subclusters.order[i]
  
  logo <- as.vector(subset(spacing.tab.r.fake, Subcluster == subcluster)[1,]$Cobinder_logo)
  # print(logo)
  logo.png <- readPNG(logo)
  # print(head(logo.png))
  
  inserted_logo = annotation_custom2(rasterGrob(logo.png, interpolate=TRUE), 
                                     xmin = -1, 
                                     xmax = 1, 
                                     ymin=0, 
                                     ymax = max.fraction, 
                                     data = fake.tile.df[i,])
  
  spacing.plot.cobinder.logos.r <- spacing.plot.cobinder.logos.r + inserted_logo
}

####################################
## Combine plots + Export Heatmap ##
####################################
message("; Putting plots and legend together.")

## Getting a legend from the left side to put under the plot
legend_box <- get_legend(boxplot.left + theme(legend.position = "bottom",
                                              legend.justification = "left"))

## Combining plots and logos
boxplot.plus.logos <- plot_grid(spacing.plot.cobinder.logos.l, boxplot.left, spacing.plot.core.logos,  boxplot.right, spacing.plot.cobinder.logos.r,
                                ncol = 5, nrow = 1, rel_heights = c(1, 1, 1, 1, 1),
                                align = 'v', axis = 'bt')

## Putting the plots and the legend together
boxplot.plus.logos <- plot_grid( boxplot.plus.logos, legend_box,
                                 ncol = 1, nrow = 2, rel_heights = c(.09, .01),
                                 align = 'h', axis = 'lr')


message(paste0("; Exporting PDF with the boxplot for ", family.name, " family."))
TF.cobinder.spacer.plot.pdf <-  file.path(spacing.folder, paste0("TF_cobinder_spacer_boxplot_", family.name, ".pdf"))

estimated.w <- 4 * (max.spacer.r - max.spacer.l)
ggsave(filename = TF.cobinder.spacer.plot.pdf,
       plot = boxplot.plus.logos,
       width = ifelse(estimated.w > 8, yes = estimated.w, no = 30),
       height = 15,
       limitsize = FALSE)

## End of script.