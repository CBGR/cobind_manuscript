# Notes on data preparation to reproduce analysis in the paper #

All data should be placed in the `data` directory. Download pre-processed data from ZENODO and remaining data following the suggestions bellow.

## Download data from ZENODO ##

UniBind 2021 data and motif collections used in the analysis should be downloaded from ZENODO (**10.5281/zenodo.7681483**). Data includes filtered TFBSs for analysed species and meatadata.

Place the contents of ZENODO archive `data.zip` in the `data` directory within pipeline's repository directory `cobind_manuscript`. The data structure should look like this:

```
cobind_manuscript
├── data
│   ├── jaspar_cisbp_motifs
│   │   ├── arabidopsis
│   │   ├── celegans
│   │   ├── combined_numbers_barplot.pdf
│   │   ├── combined_numbers_barplot.png
│   │   ├── drerio
│   │   ├── drosophila
│   │   ├── human
│   │   ├── motif_libraries_ppi_data_stats.tab
│   │   ├── motifs_composition_numbers_barplot.pdf
│   │   ├── motifs_ppi_numbers_barplot.pdf
│   │   ├── mouse
│   │   ├── multiple_species
│   │   └── rat
│   ├── STRING
│   │   ├── arabidopsis_thaliana
│   │   ├── caenorhabditis_elegans
│   │   ├── danio_rerio
│   │   ├── drosophila_melanogaster
│   │   ├── homo_sapiens
│   │   ├── mus_musculus
│   │   └── rattus_norvegicus
│   └── ub_21
│       ├── ub21_arabidopsis_metadata.tsv
│       ├── ub21_arabidopsis_renamed
│       ├── ub21_celegans_metadata.tsv
│       ├── ub21_celegans_renamed
│       ├── ub21_drerio_metadata.tsv
│       ├── ub21_drerio_renamed
│       ├── ub21_drosophila_metadata.tsv
│       ├── ub21_drosophila_renamed
│       ├── ub21_human_sampled50_CTCF_metadata.tsv
│       ├── ub21_human_sampled50_CTCF_renamed
│       ├── ub21_mouse_metadata.tsv
│       ├── ub21_mouse_renamed
│       └── ub21_mouse_renamed_gata_sox
```

## Data NOT INCLUDED in cobind_manuscript or ZENODO repositories ##

This data must be downloaded by you. Some data is quite obvious and you might already have it. Just be sure you synchronize the paths with the configuration files for the pipeline or place the files in `data` directory within pipeline.

### Download species genomes and chrom.sizes ###

Genomes and chrom.sizes files can be downloaded from [UCSC](https://hgdownload.soe.ucsc.edu/downloads.html).

Download these assemblies:

| Species name | Genome assembly |
| ------------ | --------------- |
| Arabidopsis thaliana | TAIR10 |
| Caenorhabditis elegans | WBcel235 |
| Danio rerio | GRCz11 |
| Drosophila melanogaster | dm6 |
| Homo sapiens | hg38 |
| Mus musculus | mm10 |
| Rattus norvegicus | Rnor 6.0 |

### Downloading and preparing plant conservation tracks ###

Conservation scores in bedgraph format were downloaded from [http://plantregmap.gao-lab.org/download.php#cis-elements](http://plantregmap.gao-lab.org/download.php#cis-elements).

```bash
data_output_dir="data"
data_raw_output_dir="$data_output_dir"/"raw"
data_ub21_dir="$data_output_dir"/"ub_21"

mkdir -p "$data_raw_output_dir"/UCSC/TAIR10/phastCons

## Reformating the bedgraph files:
awk -v OFS='\t' '{gsub("Chr", "chr", $1); print}' "$data_raw_output_dir"/UCSC/TAIR10/phastCons/Ath_PhastCons.bedGraph | awk -v OFS='\t' '{gsub("chrM", "chrMt", $1); print}' |  awk -v OFS='\t' '{gsub("chrC", "chrPt", $1); print}' | bedtools sort > "$data_raw_output_dir"/UCSC/TAIR10/phastCons/Ath_PhastCons-reformatted.bedGraph

## Converting to .bw:

bedGraphToBigWig "$data_raw_output_dir"/UCSC/TAIR10/phastCons/Ath_PhastCons-reformatted.bedGraph "$data_raw_output_dir"/UCSC/TAIR10/Sequence/WholeGenomeFasta/TAIR10.chrom.sizes "$data_raw_output_dir"/UCSC/TAIR10/phastCons/Ath_PhastCons.bw
```

### Downloading other species conservation tracks ###

Conservation tracks for other species were also downloaded from [UCSC](https://hgdownload.soe.ucsc.edu/downloads.html). **Note!** Make sure you match the genome assemblies.
