## Author: Ieva Rauluseviciute
## Affiliation: NCMM, UiO, Oslo, Norway

##------------------##
## Import libraries ##
##------------------##

import os
import itertools
from snakemake.utils import R
from snakemake.utils import min_version
import time
import numpy as np
import glob

##-------------------##
## Record start time ##
##-------------------##

start = time.time()

##-----------------------------##
## Choosing configuration file ##
##-----------------------------##

if config['analysis_id'] == "human_hg38":
    configfile: "config_files/config_human_hg38.yaml"

elif config['analysis_id'] == "mouse_mm10":
    configfile: "config_files/config_mouse_mm10.yaml"

elif config['analysis_id'] == "rat_Rnor_6.0":
    configfile: "config_files/config_rat_Rnor_6.0.yaml"

elif config['analysis_id'] == "arabidopsis_TAIR10":
    configfile: "config_files/config_arabidopsis_TAIR10.yaml"

elif config['analysis_id'] == "celegans_WBcel235":
    configfile: "config_files/config_celegans_WBcel235.yaml"

elif config['analysis_id'] == "drerio_GRCz11":
    configfile: "config_files/config_drerio_GRCz11.yaml"

elif config['analysis_id'] == "drosophila_dm6":
    configfile: "config_files/config_drosophila_dm6.yaml"

elif config['analysis_id'] == "human_hg38_subset":
    configfile: "config_files/config_human_hg38_subset.yaml"

elif config['analysis_id'] == "human_hg38_regular":
    configfile: "config_files/config_human_hg38_regular.yaml"

elif config['analysis_id'] == "human_hg38_grn":
    configfile: "config_files/config_human_hg38_grn.yaml"

elif config['analysis_id'] == "human_hg19_grn":
    configfile: "config_files/config_human_hg19_grn.yaml"

elif config['analysis_id'] == "mouse_mm10_smf":
    configfile: "config_files/config_mouse_mm10_smf.yaml"

elif config['analysis_id'] == "mouse_mm10_regular":
    configfile: "config_files/config_mouse_mm10_regular.yaml"

else :
    print("; ERROR: analysis name not found. Supported: human_hg38 | mouse_mm10 | rat_Rnor_6.0 | arabidopsis_TAIR10 | celegans_WBcel235 | drerio_GRCz11 | drosophila_dm6 | human_hg38_grn | human_hg19_grn | mouse_mm10_smf")

##-----------------------------------------------------------------##
## Select config file according to the given command line argument ##
##-----------------------------------------------------------------##
## snakemake --config analysis_id=human_hg38 --cores 10            ##
## snakemake --config analysis_id=mouse_mm10 --cores 10            ##
## snakemake --config analysis_id=rat_Rnor_6.0 --cores 10          ##
## snakemake --config analysis_id=arabidopsis_TAIR10 --cores 10    ##
## snakemake --config analysis_id=celegans_WBcel235 --cores 10     ##
## snakemake --config analysis_id=drerio_GRCz11 --cores 10         ##
## snakemake --config analysis_id=drosophila_dm6 --cores 10        ##
##-----------------------------------------------------------------##
## snakemake --config analysis_id=human_hg38_grn --cores 10        ##
## snakemake --config analysis_id=human_hg19_grn --cores 10        ##
## snakemake --config analysis_id=mouse_mm10_smf --cores 10        ##
##-----------------------------------------------------------------##

##-------------##
## Directories ##
##-------------##

MAIN_DIR_ABS = os.getcwd()
DATA_DIR     = config["BEDS_folder"]
RUN_TIME_DIR = config["title"]
RESULTS_DIR  = os.path.join(config["title"], "results")
REPORTS_DIR  = os.path.join(config["title"], "reports")

##-----------##
## Variables ##
##-----------##

## Variables from the config to write in the run_time text file:
config_title       = config["title"]
gini_threshold     = config["GINI_THRESHOLD"]
kim_park_threshold = config["KIM_PARK_THRESHOLD"]
config_data        = config["BEDS_folder"]
config_flank       = config["TFBS_FLANK"]
config_components  = config["NB_COMPONENTS"]
config_groups      = config["TF_FAMILIES"]

##---------------##
## Info messages ##
##---------------##

## When pipeline is executed succesfully:
onsuccess:
    success_runtime = round((time.time() - start)/60,3)
    print("; Running time in minutes:\n %s" % success_runtime)
    f = open("{dir}/run_time.txt".format(dir = RUN_TIME_DIR), "w+")
    print("; Running time in minutes:\n %s" % success_runtime, file = f)
    print("\n; Analysis title:\n %s" % config_title, file = f)
    print("\n; Used Gini threshold:\n %s" % gini_threshold, file = f)
    print("\n; Used Kim&Park threshold:\n %s" % kim_park_threshold, file = f)
    print("\n; Used data:\n %s" % config_data, file = f)
    print("\n; Flank length:\n %s" % config_flank, file = f)
    print("\n; Components:\n %s" % config_components, file = f)
    print("\n; Grouping file used:\n %s" % config_groups, file = f)

    print("\n##---------------------------##\n; Workflow finished, no errors!\n##---------------------------##\n")

## When error occurs:
onerror:
    print("; Running time in minutes:\n %s\n" % round((time.time() - start)/60,1))
    print("\n##-----------------##\n; An error occurred!\n##-----------------##\n")

## When starting the run:
onstart:
    print("\n##-----------------------------------------##\n; Reading input and firing up the analysis...\n##-----------------------------------------##\n")
    print("\n; Analysis title:\n %s" % config_title)
    print("\n; Used Gini threshold:\n %s" % gini_threshold)
    print("\n; Used Kim&Park threshold:\n %s" % kim_park_threshold)
    print("\n; Used data:\n %s" % config_data)
    print("\n; Flank length:\n %s" % config_flank)
    print("\n; Components:\n %s" % config_components)
    print("\n; Grouping file used:\n %s" % config_groups)

##--------------------------##
## Checking the input files ##
##--------------------------##

problematic_files = []
data_folder = Path(DATA_DIR).glob('**/*.bed')
for file in data_folder:
## Checking the length of the file names:
    basename = os.path.splitext(os.path.basename(file))[0]
    basename_length = len(basename)
    if basename_length > 65:
        file_and_name_length = [basename, basename_length]
        problematic_files.append(file_and_name_length)
        print("\n; ERROR! A filename ", basename, " is too long (longer than 65 characters).\n")
## Cheking if there are any dots in the file names:
    if '.' in basename:
        problematic_files.append(basename)
        print("\n; ERROR! A filename ", basename, " contains a dot.\n")
## Checking if regions in the bed are of the same length:
    with open(file) as bed_file:
        unique_region_sizes = set()
        for line in bed_file.readlines():
            columns = line.strip().split("\t")
            regions_size = int(columns[2]) - int(columns[1])
            unique_region_sizes.add(regions_size)
        nb_unique_sizes = len(unique_region_sizes)
        if regions_size < 2:
            print("\n; WARNING! Input regions are less than 2 bp long for ", file, " file.\n")
    if nb_unique_sizes > 1:
        problematic_files.append(basename)
        print("\n; ERROR! Regions in a file ", basename, " are not of the same length.\n")

if len(problematic_files) > 0:
    print("\n##-------------ERROR!------------##\n; Fix problems above and try again.\n##-------------------------------##\n")
    raise IOError("\n; Fix problems above and try again.\n")

##-------##
## Input ##
##-------##

# File names of input BED files (user defines the path to these files in the config.yaml file).
FILE, = glob_wildcards(os.path.join(DATA_DIR, "{file}.bed"))

##--------------##
## Output files ##
##--------------##

### Preprocessing:
FILTERED_BED                        = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed.oriented")
EXTENDED_BED_FULL                   = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed.oriented.full")
SUBTRACTED_BED_L                    = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed.oriented.{flank}l0r.subtd")
SUBTRACTED_BED_R                    = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed.oriented.0l{flank}r.subtd")

FASTA_L                             = os.path.join(RESULTS_DIR, "datasets", "{file}", "fasta", "{file}.bed.oriented.{flank}l0r.subtd.fa")
FASTA_R                             = os.path.join(RESULTS_DIR, "datasets", "{file}", "fasta", "{file}.bed.oriented.0l{flank}r.subtd.fa")
FASTA_CORE                          = os.path.join(RESULTS_DIR, "datasets", "{file}", "fasta", "{file}.bed.oriented.fa")
FASTA_FULL                          = os.path.join(RESULTS_DIR, "datasets", "{file}", "fasta", "{file}.bed.full.fa")

### NMF:
NMF_SUMMARY_L                       = os.path.join(RESULTS_DIR, "datasets", "{file}", "nmf_scores", "{file}_left_f{flank}_c{nb_components}.txt")
NMF_SUMMARY_R                       = os.path.join(RESULTS_DIR, "datasets", "{file}", "nmf_scores", "{file}_right_f{flank}_c{nb_components}.txt")

### Clustering:
CLUST_TABLE_LOG                     = os.path.join(RESULTS_DIR, "tf_families_tables", "tf_families_tables_log.txt")
CLUST_TABLE_CORE                    = os.path.join(RESULTS_DIR, "tf_families_tables", "{family}_core.tab")
CLUST_TABLE_COBINDER                = os.path.join(RESULTS_DIR, "tf_families_tables", "{family}_cobinder.tab")

CLUSTERING_RESULTS_CORE             = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "core", "{family}_core_tables", "clusters.tab")
CLUSTERING_RESULTS_COBINDER         = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "cobinder", "{family}_cobinder_tables", "clusters.tab")
ALIGNMENT_TABLE_CORE                = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "core", "{family}_core_tables", "alignment_table.tab")
ALIGNMENT_TABLE_COBINDER            = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "cobinder", "{family}_cobinder_tables", "alignment_table.tab")

## Spacings:
SPACINGS_SUMMARY                    = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings", "spacings.txt")
SPACING_HEATMAP                     = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings", "TF_cobinder_spacer_plot_{family}.pdf")
SPACING_BOXPLOT                     = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings", "TF_cobinder_spacer_boxplot_{family}.pdf")

## Regions:
FULL_COBINDER_REGION                = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "with_cobinder", "with_cobinder_full_regions_{family}.bed")
FULL_NO_COBINDER_REGION             = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "no_cobinder", "no_cobinder_full_regions_{family}.bed")

COBINDER_REGIONS_ANNOTATION         = os.path.join(RESULTS_DIR, "cobinder_regions_annotation", "centered", "{family}_chipseeker.pdf")

## Mixed regions:
MIXED_REGIONS_PATHS    = os.path.join(RESULTS_DIR, "cobinder_regions", "paths_to_mixed_regions", "paths_to_mixed_regions_{family}.txt")

## Comparison analysis:
PPI_ANALYSIS_STATS                  = os.path.join(RESULTS_DIR, "ppi_associations_results", "physical", "{family}", "motif_no_matches_stats.txt")
PPI_FULL_ANALYSIS_STATS             = os.path.join(RESULTS_DIR, "ppi_associations_results", "physical_full", "{family}", "motif_no_matches_stats.txt")

### GREAT analysis output:
GREAT_OUTPUT                        =  os.path.join(RESULTS_DIR, "GREAT_analysis", "{family}", "full_region", "with_cobinder", "gene_tss_distances_table.tsv")

## Conservation:
CONSERVATION_NO_COBINDER            = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "full", "no_cobinder_full_regions_{family}_conservation.tsv")
CONSERVATION_WITH_COBINDER          = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "full", "with_cobinder_full_regions_{family}_conservation.tsv")
CONSERVATION_PLOT                   = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "full", "all_combined_conservation.pdf")
FULL_CONSERVATION_PLOT_BOARDERS     = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "full", "all_combined_conservation_with_motif_borders.pdf")

MIXED_REGIONS_CONSERVATION_PLOTS    = os.path.join(RESULTS_DIR, "conservation_analysis_mixed", "collected_plots", "{family}_conservation_done.txt")

CONSERVATION_PLOT_CENTERED          = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "centered", "all_combined_conservation.pdf")
CENTERED_CONSERVATION_PLOT_BOARDERS = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "centered", "all_combined_conservation_with_motif_borders.pdf")

CONSERVATION_STATISTICS             = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "conservation_statistics", "wilcox_test_no_vs_with_cobinder.txt")

CONSERVATION_STATISTICS_MIXED       = os.path.join(RESULTS_DIR, "conservation_analysis_mixed", "collected_plots", "{family}_conservation_statistics_done.txt")

## Markdown report:
REPORT_HTML                         = os.path.join(REPORTS_DIR, "{family}", "COBIND_analysis_report_{family}.html")

## Final summary:
COBINDER_SUMMARY                    = os.path.join(RESULTS_DIR, "analysis_overall_summary", "cobinder_distribution_summary_table.txt")

##----------##
## Rule all ##
##----------##

rule all:
    """
    Defining a final output, which will request all the rules to be run.
    """
    input:
        COBINDER_SUMMARY

##-------##
## Rules ##
##-------##

#######################
## 1. Pre-processing ##
#######################

rule generateFLANKSandANCHORS:
    """
    1.1. Flanking regions are generated. Based on the input file, the anchor regions are also oriented.
    """
    input:
        os.path.join(DATA_DIR, "{file}.bed")
    output:
        SUBTRACTED_BED_L, \
        SUBTRACTED_BED_R
    message:
        "; Generating flanks for {wildcards.file}."
    params:
        bin         = os.path.join(config["BIN"], "preprocessing"), \
        output_dir  = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED"), \
        cpp_script  = os.path.join(config["BIN"], "preprocessing", "Distance_strings.cpp"), \
        chrom_sizes = config["CHROM_SIZES"], \
        genome      = config["GENOME_FA"], \
        flank_size  = config["TFBS_FLANK"], \

        bed_fa      = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed"), \
        bed_or      = os.path.join(RESULTS_DIR, "datasets", "{file}", "BED", "{file}.bed.or")
    shell:
        """
        n_columns=$(awk -F' ' '{{print NF; exit}}' {input}) ;

        if [ $n_columns == 6 ]
        then
            echo '; Six column BED file. Generating flanks.'

            Rscript {params.bin}/generate_flanks.R \
            -i {input} \
            -f {params.flank_size} \
            -o {params.output_dir} \
            -c {params.chrom_sizes} ;

        elif [ $n_columns == 3 ]
        then
            echo '; Three column BED file. Orienting and generating flanks.'

            bedtools getfasta -fi {params.genome} -bed {input} -tab -bedOut > {params.bed_fa} ;

            Rscript {params.bin}/orient_BED.R \
            -i {params.bed_fa} \
            -o {params.output_dir} \
            -c {params.cpp_script} ;

            Rscript {params.bin}/generate_flanks.R \
            -i {params.bed_or} \
            -f {params.flank_size} \
            -o {params.output_dir} \
            -c {params.chrom_sizes} ;

            rm {params.bed_fa}
            rm {params.bed_or}
        else
            echo '; Input BED does not have three or six columns. Check your input file {input}.'
        fi
        """

rule bed2fasta:
    """
    1.2. Flanking regions are converted to FASTA.
    """
    input:
        subtracted_left  = SUBTRACTED_BED_L, \
        subtracted_right = SUBTRACTED_BED_R
    output:
        fasta_left  = FASTA_L, \
        fasta_right = FASTA_R
    message:
        "; Converting flanks to fasta for {wildcards.file}."
    params:
        genome        = config["GENOME_FA"], \
        oriented_bed  = FILTERED_BED, \
        extended_full = EXTENDED_BED_FULL, \
        fasta_core    = FASTA_CORE, \
        fasta_full    = FASTA_FULL
    shell:
        """
        bedtools getfasta -fi {params.genome} -bed {input.subtracted_left} -s -fo {output.fasta_left} ;
        bedtools getfasta -fi {params.genome} -bed {input.subtracted_right} -s -fo {output.fasta_right} ;

        bedtools getfasta -fi {params.genome} -bed {params.oriented_bed} -s -fo {params.fasta_core} ;
        bedtools getfasta -fi {params.genome} -bed {params.extended_full} -s -fo {params.fasta_full} ;
        """

############
## 2. NMF ##
############

rule runNMF:
    """
    2.1. Running non-negative matrix factorization on flanking regions to discover co-binder motifs. If any significant motifs discovered, they are trimmed and anchor motifs recovered using a subset of sequences.
    """
    input:
        fasta_left  = FASTA_L, \
        fasta_right = FASTA_R
    output:
        NMF_SUMMARY_L, \
        NMF_SUMMARY_R
    message:
        "; Running NMF on flanking regions for {wildcards.file}."
    params:
        bin                = os.path.join(config["BIN"], "NMF"), \
        output_folder      = os.path.join(RESULTS_DIR, "nmf_motifs"), \
        score_folder       = os.path.join(RESULTS_DIR, "datasets", "{file}", "nmf_scores"), \
        nb_components      = config["NB_COMPONENTS"], \
        exp_name_left      = "{file}_left", \
        exp_name_right     = "{file}_right", \
        gini_threshold     = config["GINI_THRESHOLD"], \
        kim_park_threshold = config["KIM_PARK_THRESHOLD"], \
        fasta_core         = FASTA_CORE
    shell:
        """
        python {params.bin}/apply_NMF_trim.py \
            {input.fasta_left} \
            {params.output_folder} \
            {params.score_folder} \
            {params.exp_name_left} \
            {wildcards.nb_components} \
            {params.gini_threshold} \
            {params.kim_park_threshold} \
            {params.fasta_core} ;

        python {params.bin}/apply_NMF_trim.py \
            {input.fasta_right} \
            {params.output_folder} \
            {params.score_folder} \
            {params.exp_name_right} \
            {wildcards.nb_components} \
            {params.gini_threshold} \
            {params.kim_park_threshold} \
            {params.fasta_core};
        """

###################
## 3. Clustering ##
###################

checkpoint prepareCLUSTERINGtable:
    """
    3.1. Preparing tables with discovered co-binders and anchors files, which is then used as an input for clustering algorithm.
    """
    input:
        expand(NMF_SUMMARY_L, file = FILE, flank = config["TFBS_FLANK"], nb_components = config["NB_COMPONENTS"]), \
        expand(NMF_SUMMARY_R, file = FILE, flank = config["TFBS_FLANK"], nb_components = config["NB_COMPONENTS"])
    output:
        directory(os.path.join(RESULTS_DIR, "tf_families_tables"))
    message:
        "; Creating group motifs tables."
    params:
        bin           = os.path.join(config["BIN"], "clustering"), \
        output_folder = os.path.join(RESULTS_DIR, "tf_families_tables"), \
        tf_families   = config["TF_FAMILIES"], \
        nmf_motifs    = os.path.join(RESULTS_DIR, "nmf_motifs")
    shell:
        """
        python {params.bin}/make_clust_tables.py \
            {params.tf_families} \
            {params.nmf_motifs} \
            {params.output_folder} ;
        """

rule clusterMOTIFS:
    """
    3.2. Clustering co-binders and anchor motifs independently. If there is only one motif, the output is created directly.
    """
    input:
        table_core     = CLUST_TABLE_CORE, \
        table_cobinder = CLUST_TABLE_COBINDER
    output:
        CLUSTERING_RESULTS_CORE, \
        CLUSTERING_RESULTS_COBINDER, \
        ALIGNMENT_TABLE_CORE, \
        ALIGNMENT_TABLE_COBINDER
    message:
        "; Clustering core and cobinder motifs for {wildcards.family}."
    params:
        bin                    = os.path.join(config["BIN"], "clustering"), \
        output_folder_core     = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "core", "{family}_core"), \
        output_folder_cobinder = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "cobinder", "{family}_cobinder"), \
        cor                    = config["COR"], \
        ncor                   = config["NCOR"]
    shell:
        """
        export COBIND_CLUSTERING={params.bin}/RSAT-matrix-clustering ;
        
        n_lines=$(wc -l {input.table_cobinder} | awk '{{ print $1 }}')

        if [ $n_lines == 1 ]
        then
            echo '; Number of motifs is 1. Transfering one motif.'

            python {params.bin}/cluster_one_motif.py \
                {input.table_cobinder} {params.output_folder_cobinder} ;

            python {params.bin}/cluster_one_motif.py \
                {input.table_core} {params.output_folder_core}
        else
            echo '; Number of motifs is more than 1. Clustering.'

            perl {params.bin}/matrix-clustering.pl  -v 2 -matrix_file_table {input.table_core} -hclust_method average \
            -calc sum -title multicomponent_clustering \
            -label_motif name -metric_build_tree cor \
            -lth w 2 -lth cor 0.75 -lth Ncor 0.55 -label_in_tree name -quick -return json \
            -o {params.output_folder_core} ;

            perl {params.bin}/matrix-clustering.pl  -v 2 -matrix_file_table {input.table_cobinder} -hclust_method average \
            -calc sum -title multicomponent_clustering \
            -label_motif name -metric_build_tree cor \
            -lth w 3 -lth cor {params.cor} -lth Ncor {params.ncor} -quick -label_in_tree name -return json \
            -o {params.output_folder_cobinder}
        fi
        """

#################
## 4. Spacings ##
#################

rule computeSPACINGS:
    """
    4.1. Computing the spacings between co-binder and anchor motifs.
    """
    input:
        CLUSTERING_RESULTS_CORE, \
        CLUSTERING_RESULTS_COBINDER, \
        ALIGNMENT_TABLE_CORE, \
        ALIGNMENT_TABLE_COBINDER
    output:
        SPACINGS_SUMMARY
    message:
        "; Computing the spacers for {wildcards.family}."
    params:
        bin                        = os.path.join(config["BIN"], "spacings"), \
        clustering_bin             = os.path.join(config["BIN"], "clustering"), \
        core_clustering_folder     = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "core"), \
        cobinder_clustering_folder = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "cobinder"), \
        output_folder              = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings")
    shell:
        """
        export COBIND_CLUSTERING={params.clustering_bin}/RSAT-matrix-clustering ;

        python {params.bin}/compute_spacers.py \
        {params.core_clustering_folder} \
        {params.cobinder_clustering_folder} \
        {wildcards.family} \
        {params.output_folder}
        """

rule spacingPLOTSandREGIONS:
    """
    4.2. Drawing a spacings summary plot.
    """
    input:
        SPACINGS_SUMMARY
    output:
        SPACING_HEATMAP, \
        SPACING_BOXPLOT
    message:
        "; Summarizing spacers for {wildcards.family}."
    params:
        bin          = os.path.join(config["BIN"], "spacings"), \
        spacings_dir =  os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
        title        = config["title"]
    shell:
        """
        Rscript {params.bin}/draw_spacers_heatmap_extract_regions.R \
        -i {params.spacings_dir} \
        -t {params.title} \
        -f {wildcards.family} ;

        Rscript {params.bin}/draw_spacers_boxplot_dataset_specific.R \
        -i {params.spacings_dir} \
        -t {params.title} \
        -f {wildcards.family} ;
        """

        # Rscript {params.bin}/full_fasta_heatmap.R \
        # -i {params.spacings_dir} \
        # -t {params.title} \
        # -f {wildcards.family}

################
## 5. Regions ##
################

rule extractREGIONS:
    """
    5.1. Extracting genomic regions: anchors-only with and without co-binders, full regions (anchor + flanks) with and without co-binders.
    """
    input:
        SPACING_HEATMAP, \
        SPACING_BOXPLOT
    output:
        FULL_COBINDER_REGION, \
        FULL_NO_COBINDER_REGION
    message:
        "; Extracting regions with and without co-binders for {wildcards.family}."
    params:
        bin          = os.path.join(config["BIN"], "regions"), \
        spacings_dir =  os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
        title        = config["title"], \
        genome       = config["GENOME_FA"]
    shell:
        """
        Rscript {params.bin}/extract_centered_regions.R \
        -i {params.spacings_dir} \
        -t {params.title} \
        -f {wildcards.family} \
        -g {params.genome} ;
        """

rule annotateCOBINDERregions:
    """
    5.2. Annotating the regions by indicating their genomic locations.
    """
    input:
        FULL_COBINDER_REGION, \
        FULL_NO_COBINDER_REGION
    output:
        COBINDER_REGIONS_ANNOTATION
    message:
        "; Annotating co-binder regions for {wildcards.family}."
    params:
        bin = os.path.join(config["BIN"], "regions"), \
        cobinder_regions = os.path.join(RESULTS_DIR, "cobinder_regions", "BED"), \
        output_dir = os.path.join(RESULTS_DIR, "cobinder_regions_annotation"), \
        species_label = config["SPECIES_LABEL"]
    shell:
        """
        Rscript {params.bin}/annotate_cobinder_regions_chipseeker.R \
        -c {params.cobinder_regions} \
        -o {params.output_dir} \
        -s {params.species_label} \
        -f {wildcards.family}
        """

#############################
## 6. PPI motif comparison ##
#############################

rule motif2PPI:
    """
    6.1. Comparing discovered co-binders with collections of known motifs, then looking for protein-protein interactions (PPI).
    """
    input:
        COBINDER_REGIONS_ANNOTATION
    output:
        PPI_ANALYSIS_STATS, \
        PPI_FULL_ANALYSIS_STATS
    message:
        "; Inferring co-binding TFs with PPI data for {wildcards.family}."
    params:
        bin                    = os.path.join(config["BIN"], "PPI_motif_comparison"), \
        spacings_dir           = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
        full_specific_with_cobinder = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "with_cobinder_motif_specific", "{family}"), \
        output_dir_tomtom      = os.path.join(RESULTS_DIR, "ppi_associations_results", "tomtom", "{family}"), \
        
        output_dir_tomtom_full = os.path.join(RESULTS_DIR, "ppi_associations_results", "tomtom_full", "{family}"), \

        output_dir_full        = os.path.join(RESULTS_DIR, "ppi_associations_results", "physical_full", "{family}"), \
        output_dir_physical    = os.path.join(RESULTS_DIR, "ppi_associations_results", "physical", "{family}"), \

        tomtom_db              = config["TOMTOM_DB"], \
        tomtom_db_full         = config["TOMTOM_DB_FULL"], \
        motif_metadata         = config["MOTIF_METADATA"], \
        motif_metadata_full    = config["MOTIF_METADATA_FULL"], \

        ppi_table_physical     = config["PPI_TABLE_PHYSICAL"], \

        species_label          = config["SPECIES_LABEL"], \
        tf_families            = config["TF_FAMILIES"]
    shell:
        """
        mkdir -p {params.output_dir_physical} ;
        mkdir -p {params.output_dir_tomtom}/alignment_images ;

        mkdir -p {params.output_dir_full} ;
        mkdir -p {params.output_dir_tomtom_full}/alignment_images ;

        for file in {params.full_specific_with_cobinder}/full_regions_with_cobinder_*.bed ;
        do
            filename=$(basename -- "$file") ;
            cobinder_id="${{filename%.bed}}" ;
            cobinder_id="${{cobinder_id//"full_regions_with_cobinder_"/}}" ;

            motif_file={params.spacings_dir}/cobinder_subcluster_"$cobinder_id".txt ;
            cobinder_id=cobinder_"$cobinder_id" ;

            echo "; Converting motifs for: $cobinder_id" ;
            convert-matrix -from cluster-buster -to tf -i $motif_file -return counts -multiply 1000 -prefix $cobinder_id -o {params.output_dir_physical}/"$cobinder_id".tf ;
            transfac2meme {params.output_dir_physical}/"$cobinder_id".tf > {params.output_dir_physical}/"$cobinder_id".meme ;
            rm {params.output_dir_physical}/"$cobinder_id".tf ;

            echo "; Pairwise comparison for: $cobinder_id" ;

            tomtom -dist kullback -motif-pseudo 0.1 \
            -min-overlap 1 -thresh 1 -eps -no-ssc \
            {params.output_dir_physical}/"$cobinder_id".meme  \
            {params.tomtom_db} \
            -oc {params.output_dir_tomtom}/alignment_images ;

            tomtom -dist kullback -motif-pseudo 0.1 \
            -min-overlap 1 -thresh 1 -eps -no-ssc \
            {params.output_dir_physical}/"$cobinder_id".meme  \
            {params.tomtom_db_full} \
            -oc {params.output_dir_tomtom_full}/alignment_images ;

            mv {params.output_dir_tomtom}/alignment_images/tomtom.tsv {params.output_dir_physical}/"$cobinder_id"_comparisons.tsv ;

            mv {params.output_dir_tomtom_full}/alignment_images/tomtom.tsv {params.output_dir_full}/"$cobinder_id"_comparisons.tsv ;

            echo "; Associating motifs to PPI for: $cobinder_id" ;

            Rscript {params.bin}/motif_to_ppi.R \
            -t {params.output_dir_physical}/"$cobinder_id"_comparisons.tsv \
            -o {params.output_dir_physical} \
            -p {params.ppi_table_physical} \
            -m {params.motif_metadata} \
            -s {params.species_label} \
            -f {wildcards.family} \
            -g {params.tf_families};

            Rscript {params.bin}/motif_to_ppi.R \
            -t {params.output_dir_full}/"$cobinder_id"_comparisons.tsv \
            -o {params.output_dir_full} \
            -p {params.ppi_table_physical} \
            -m {params.motif_metadata_full} \
            -s {params.species_label} \
            -f {wildcards.family} \
            -g {params.tf_families};
        done ;

        echo "; Plotting for: $cobinder_id" ;

        Rscript {params.bin}/plot_motif_to_ppi.R \
        -t {params.output_dir_physical} ;

        Rscript {params.bin}/plot_motif_to_ppi.R \
        -t {params.output_dir_full} ;

        rm -r {params.output_dir_tomtom} ;
        rm -r {params.output_dir_tomtom_full} ;
        """

### This will run for human, mouse, celegans, drosophila and arabidopsis:
if (config['analysis_id'] == "human_hg38") | (config['analysis_id'] == "mouse_mm10") | (config['analysis_id'] == "human_hg38_grn") | (config['analysis_id'] == "human_hg19_grn") | (config['analysis_id'] == "mouse_mm10_smf") | (config['analysis_id'] == "arabidopsis_TAIR10") | (config['analysis_id'] == "celegans_WBcel235") | (config['analysis_id'] == "drosophila_dm6"):

    ##############################
    ## 7. Conservation analysis ##
    ##############################

    rule runFULLregionCONSERVATION:
        """
        7.1. Computing region conversation with and without discovered co-binders.
        """
        input:
            PPI_ANALYSIS_STATS, \
            PPI_FULL_ANALYSIS_STATS
        output:
            CONSERVATION_NO_COBINDER, \
            CONSERVATION_WITH_COBINDER, \
            CONSERVATION_PLOT, \
            FULL_CONSERVATION_PLOT_BOARDERS
        message:
            "; Running conservation analysis on full co-binder regions for {wildcards.family}."
        params:
            bin                          = os.path.join(config["BIN"], "conservation"), \
            spacings_dir           = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
            conservation_scores          = config["CONSERVATION_SCORES"], \
            chrom_sizes                  = config["CHROM_SIZES"], \
            conservation_window          = config["CONSERVATION_WINDOW"], \

            full_with_cobinder           = FULL_COBINDER_REGION, \
            full_no_cobinder             = FULL_NO_COBINDER_REGION, \
            output_dir                   = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "full"), \

            conservation_no_cobinder     = CONSERVATION_NO_COBINDER, \
            conservation_with_cobinder   = CONSERVATION_WITH_COBINDER, \
            full_plot_with_motif_borders = FULL_CONSERVATION_PLOT_BOARDERS, \

            full_specific_output_dir     = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "full_motif_specific"), \
            full_specific_with_cobinder  = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "with_cobinder_case_motif_specific", "{family}"), \
            full_specific_no_cobinder    = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "no_cobinder_case_motif_specific", "{family}")
        shell:
            """
            {params.bin}/get_conservation.sh \
                -i {params.full_with_cobinder},{params.full_no_cobinder} \
                -c {params.conservation_scores} \
                -w {params.conservation_window} \
                -n {params.chrom_sizes} \
                -o {params.output_dir} \
                -s ;

            Rscript {params.bin}/plot_conservation_facet_motif_borders.R \
                -c {params.conservation_no_cobinder},{params.conservation_with_cobinder} \
                -s {params.conservation_scores} \
                -f {params.spacings_dir} \
                -o {params.full_plot_with_motif_borders} \
                -t full ;

            for file in {params.full_specific_with_cobinder}/full_regions_with_cobinder_subcluster_*.bed ;
            do
                filename=$(basename -- "$file") ;
                cobinder_id_location_spacer="${{filename%.bed}}" ;
                cobinder_id_location_spacer="${{cobinder_id_location_spacer//"full_regions_with_cobinder_subcluster_"/}}" ;
                
                cobinder_id=$(echo "$cobinder_id_location_spacer" | rev | cut -d_ -f3- | rev)
                cobinder_location=$(echo "$cobinder_id_location_spacer" | awk -F_ '{{print $(NF-1)}}')
                cobinder_spacer=$(echo "$cobinder_id_location_spacer" | awk -F_ '{{print $NF}}')

                cobinder_output_folder={params.full_specific_output_dir}/cobinder_"$cobinder_id_location_spacer" ;
                mkdir -p $cobinder_output_folder ;
                echo "; Computing conservation for cobinder $cobinder_id" ;

                regions_with_cobinder={params.full_specific_with_cobinder}/full_regions_with_cobinder_subcluster_"$cobinder_id_location_spacer".bed ;
                regions_no_cobinder={params.full_specific_no_cobinder}/full_regions_no_cobinder_subcluster_"$cobinder_id_location_spacer".bed ;

                {params.bin}/get_conservation.sh \
                -i $regions_with_cobinder,$regions_no_cobinder \
                -c {params.conservation_scores} \
                -w {params.conservation_window} \
                -n {params.chrom_sizes} \
                -o $cobinder_output_folder \
                -s ;

                Rscript {params.bin}/plot_conservation_facet_motif_borders.R \
                -c "$cobinder_output_folder"/full_regions_no_cobinder_subcluster_"$cobinder_id_location_spacer"_conservation.tsv,"$cobinder_output_folder"/full_regions_with_cobinder_subcluster_"$cobinder_id_location_spacer"_conservation.tsv \
                -s {params.conservation_scores} \
                -f {params.spacings_dir} \
                -o "$cobinder_output_folder"/all_combined_conservation_with_motif_borders.pdf \
                -t full \
                -b "$cobinder_id" \
                -l "$cobinder_location" \
                -a "$cobinder_spacer" ;

                echo "; Plotting cobinder-specific region conservation against the regions with no cobinder at all."
                Rscript {params.bin}/plot_conservation_facet_motif_borders.R \
                -c {params.conservation_no_cobinder},"$cobinder_output_folder"/full_regions_with_cobinder_subcluster_"$cobinder_id_location_spacer"_conservation.tsv \
                -s {params.conservation_scores} \
                -f {params.spacings_dir} \
                -o "$cobinder_output_folder"/cobinder_vs_none_of_cobinders.pdf \
                -t full \
                -b "$cobinder_id" \
                -l "$cobinder_location" \
                -a "$cobinder_spacer" ;
            done ;

            rm {params.full_specific_output_dir}/*/log.txt ;
            rm {params.full_specific_output_dir}/*/*.bed ;
            rm {params.output_dir}/log.txt ;
            rm {params.output_dir}/*.bed
            """

    rule runCENTEREDregionCONSERVATION:
        """
        7.2. Computing region conversation with and without discovered co-binders when regions are centered around co-binder.
        """
        input:
            CONSERVATION_NO_COBINDER, \
            CONSERVATION_WITH_COBINDER, \
            CONSERVATION_PLOT, \
            FULL_CONSERVATION_PLOT_BOARDERS
        output:
            CONSERVATION_PLOT_CENTERED, \
            CENTERED_CONSERVATION_PLOT_BOARDERS
        message:
            "; Running conservation analysis on cobinder-centered regions for {wildcards.family}."
        params:
            bin                              = os.path.join(config["BIN"], "conservation"), \
            spacings_dir                     =  os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
            conservation_scores              = config["CONSERVATION_SCORES"], \
            chrom_sizes                      = config["CHROM_SIZES"], \
            conservation_window              = config["CONSERVATION_WINDOW"], \

            centered_folder                  = os.path.join(RESULTS_DIR, "cobinder_regions", "BED",  "centered_cobinder_regions", "{family}"), \
            centered_output_dir              = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "centered"), \
            centered_plot_with_motif_borders = CENTERED_CONSERVATION_PLOT_BOARDERS
        shell:
            """
            centered_regions=$(ls -d {params.centered_folder}/* | xargs echo | sed 's/ /,/g')

            {params.bin}/get_conservation.sh \
            -i $centered_regions \
            -c {params.conservation_scores} \
            -w {params.conservation_window} \
            -n {params.chrom_sizes} \
            -o {params.centered_output_dir} \
            -s ;

            centered_conservation=$(find {params.centered_output_dir} -name "*_conservation.tsv" | xargs echo | sed 's/ /,/g')

            Rscript {params.bin}/plot_conservation_facet_motif_borders.R \
            -c $centered_conservation \
            -s {params.conservation_scores} \
            -f {params.spacings_dir} \
            -o {params.centered_plot_with_motif_borders} \
            -t centered ;

            rm {params.centered_output_dir}/log.txt ;
            rm {params.centered_output_dir}/*.bed
            """

    rule computeCONSERVATIONstatistics:
        """
        7.3. Checking the statistical significance of conservation of regions with and without co-binders.
        """
        input:
            CONSERVATION_PLOT_CENTERED, \
            CENTERED_CONSERVATION_PLOT_BOARDERS
        output:
            CONSERVATION_STATISTICS
        message:
            "; Computing conservation statistics for {wildcards.family}."
        params:
            bin                        = os.path.join(config["BIN"], "conservation"), \
            cobinder_regions_dir       = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions"), \
            conservation_scores        = config["CONSERVATION_SCORES"], \
            spacings_dir               =  os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
            output_dir                 = os.path.join(RESULTS_DIR, "conservation_analysis", "{family}", "conservation_statistics")
        shell:
            """
            Rscript {params.bin}/compute_conservation_statistics.R \
            -r {params.cobinder_regions_dir} \
            -s {params.conservation_scores} \
            -n {wildcards.family} \
            -o {params.output_dir} \
            -f {params.spacings_dir} ;
            """

    rule mixREGIONidentities:
        """
        7.4. Mixing the regions with and without co-binders to later test the conservation statistical significance stability. This will be done multiple times.
        """
        input:
            CONSERVATION_STATISTICS
        output:
            MIXED_REGIONS_PATHS
        message:
            "; Mixing regions with and without co-binder for testing."
        params:
            bin                           = os.path.join(config["BIN"], "regions"), \
            sampling_iterations           = config["SAMPLE_ITERATIONS"], \
            family_name                   = "{family}", \
            full_no_cobinder              = FULL_NO_COBINDER_REGION, \
            specific_cobinder_regions_dir = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "with_cobinder_case_motif_specific", "{family}"), \
            specific_output_dir           = os.path.join(RESULTS_DIR, "cobinder_regions", "BED_mixed", "full_regions", "with_cobinder_case_motif_specific", "{family}"), \
            mixed_regions_base_dir        = os.path.join(RESULTS_DIR, "cobinder_regions")
        shell:
            """            
            original_output_dir={params.specific_output_dir} ;
            for iteration in {params.sampling_iterations} ;
            do
                output_dir=$(echo $original_output_dir | sed 's+'BED_mixed'+'BED_mixed_"$iteration"'+') ;
                Rscript {params.bin}/mix_regions_with_without_cobinder.R \
                    -i {params.specific_cobinder_regions_dir} \
                    -o "$output_dir" \
                    -n {params.full_no_cobinder} ;
            done ;

            find {params.mixed_regions_base_dir}/BED_mixed_*/*/*/{params.family_name} -name "*.bed" > {output} ;
            """
        
    rule runMIXEDregionsCONSERVATION:
        """
        7.5. Running conservation analysis on randomly sampled regions.
        """
        input:
            MIXED_REGIONS_PATHS
        output:
            MIXED_REGIONS_CONSERVATION_PLOTS
        message:
            "; Running conservation analysis on mixed regions."
        params:
            bin                          = os.path.join(config["BIN"], "conservation"), \
            spacings_dir                 = os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
            conservation_scores          = config["CONSERVATION_SCORES"], \
            chrom_sizes                  = config["CHROM_SIZES"], \
            conservation_window          = config["CONSERVATION_WINDOW"], \
            sampling_iterations          = config["SAMPLE_ITERATIONS"], \
            family_name                  = "{family}", \

            full_specific_output_dir     = os.path.join(RESULTS_DIR, "conservation_analysis_mixed"), \
            full_specific_region_dir     = os.path.join(RESULTS_DIR, "cobinder_regions", "BED_mixed")
        shell:
            """
            for iteration in {params.sampling_iterations}
            do
                echo "; Computing conservation for iteration number $iteration" ;
                full_specific_with_cobinder={params.full_specific_region_dir}_"$iteration"/full_regions/with_cobinder_case_motif_specific/{params.family_name} ;
                full_specific_no_cobinder={params.full_specific_region_dir}_"$iteration"/full_regions/no_cobinder_case_motif_specific/{params.family_name} ;

                for file in "$full_specific_with_cobinder"/full_regions_with_cobinder_subcluster_*.bed
                do
                    filename=$(basename -- "$file") ;
                    cobinder_id_location_spacer="${{filename%.bed}}" ;
                    cobinder_id_location_spacer="${{cobinder_id_location_spacer//"full_regions_with_cobinder_subcluster_"/}}" ;

                    cobinder_id=$(echo "$cobinder_id_location_spacer" | rev | cut -d_ -f3- | rev)
                    cobinder_location=$(echo "$cobinder_id_location_spacer" | awk -F_ '{{print $(NF-1)}}')
                    cobinder_spacer=$(echo "$cobinder_id_location_spacer" | awk -F_ '{{print $NF}}')

                    cobinder_output_folder={params.full_specific_output_dir}/"$iteration"/{params.family_name}/full_motif_specific/cobinder_"$cobinder_id_location_spacer" ;
                    mkdir -p $cobinder_output_folder ;
                    echo "; Computing conservation for cobinder $cobinder_id" ;

                    regions_with_cobinder="$full_specific_with_cobinder"/full_regions_with_cobinder_subcluster_"$cobinder_id_location_spacer".bed ;
                    regions_no_cobinder="$full_specific_no_cobinder"/full_regions_no_cobinder_subcluster_"$cobinder_id_location_spacer".bed ;

                    {params.bin}/get_conservation.sh \
                    -i $regions_with_cobinder,$regions_no_cobinder \
                    -c {params.conservation_scores} \
                    -w {params.conservation_window} \
                    -n {params.chrom_sizes} \
                    -o $cobinder_output_folder \
                    -s ;

                    Rscript {params.bin}/plot_conservation_facet_motif_borders.R \
                    -c "$cobinder_output_folder"/full_regions_no_cobinder_subcluster_"$cobinder_id_location_spacer"_conservation.tsv,"$cobinder_output_folder"/full_regions_with_cobinder_subcluster_"$cobinder_id_location_spacer"_conservation.tsv \
                    -s {params.conservation_scores} \
                    -f {params.spacings_dir} \
                    -o "$cobinder_output_folder"/cobinder_vs_none_of_cobinders_mixed.pdf \
                    -t full \
                    -b "$cobinder_id" \
                    -l "$cobinder_location" \
                    -a "$cobinder_spacer" ;
                done ;
            done ;

            touch {output} ;            
            """
    
    rule computedMIXEDregionCONSERVATIONstatistics:
        """
        7.6. Computing statistical differences in conservation analysis.
        """
        input:
            MIXED_REGIONS_CONSERVATION_PLOTS
        output:
            CONSERVATION_STATISTICS_MIXED
        message:
            "; Computing conservation analysis on mixed regions statistical differences."
        params:
            bin                  = os.path.join(config["BIN"], "conservation"), \
            sampling_iterations  = config["SAMPLE_ITERATIONS"], \
            family_name          = "{family}", \
            cobinder_regions_dir = os.path.join(RESULTS_DIR, "cobinder_regions"), \
            conservation_scores  = config["CONSERVATION_SCORES"], \
            spacings_dir         =  os.path.join(RESULTS_DIR, "clustering_results", "{family}", "spacings"), \
            output_dir           = os.path.join(RESULTS_DIR, "conservation_analysis_mixed")
        shell:
            """
            for iteration in {params.sampling_iterations}
            do
                echo "; Computing conservation statistics for iteration number $iteration" ;
                cobinder_regions_dir={params.cobinder_regions_dir}/BED_mixed_"$iteration"/full_regions ;
                output_dir={params.output_dir}/"$iteration"/{params.family_name}/conservation_statistics ;
                mkdir -p $output_dir ;

                Rscript {params.bin}/compute_conservation_statistics_for_mixed_regions.R \
                -r "$cobinder_regions_dir" \
                -s {params.conservation_scores} \
                -n {wildcards.family} \
                -o "$output_dir" \
                -f {params.spacings_dir} ;
            done ;

            echo "; Conservation statistics computed." > {output}
            """

    ### This will run only for human or mouse:
    if (config['analysis_id'] == "human_hg38") | (config['analysis_id'] == "mouse_mm10") | (config['analysis_id'] == "human_hg38_grn") | (config['analysis_id'] == "human_hg19_grn") | (config['analysis_id'] == "mouse_mm10_smf"):

        ######################
        # 7. GREAT analysis ##
        ######################

        rule runGREAT:
            """
            7.1. GREAT region enrichment is run on regions with and without co-binders.
            """
            input:
                CONSERVATION_STATISTICS_MIXED
            output:
                GREAT_OUTPUT
            message:
                "; Running GREAT analysis for regions with and without co-binder."
            params:
                GREAT                       = config["GREAT"], \
                genome_version              = config["GENOME_VERSION"], \
                full_with_cobinder          = FULL_COBINDER_REGION, \
                full_no_cobinder            = FULL_NO_COBINDER_REGION ,\
                output_full_with_cobinder   =  os.path.join(RESULTS_DIR, "GREAT_analysis", "{family}", "full_region", "with_cobinder"), \
                output_full_no_cobinder     =  os.path.join(RESULTS_DIR, "GREAT_analysis", "{family}", "full_region", "no_cobinder"), \
                output_cobinder_specific    =  os.path.join(RESULTS_DIR, "GREAT_analysis", "{family}", "cobinder_specific"), \
                full_specific_with_cobinder = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "with_cobinder_motif_specific", "{family}"), \
                full_specific_no_cobinder   = os.path.join(RESULTS_DIR, "cobinder_regions", "BED", "full_regions", "no_cobinder_motif_specific", "{family}")
            shell:
                """
                mkdir -p {params.output_full_with_cobinder} ;
                mkdir -p {params.output_full_no_cobinder} ;
        
                Rscript {params.GREAT} \
                    -p {params.full_with_cobinder} \
                    -o {params.output_full_with_cobinder} \
                    -g {params.genome_version} ;
        
                n_regions_no_cobinder=$(wc -l {params.full_no_cobinder} | awk '{{ print $1 }}') ;
                if (( $n_regions_no_cobinder <= 500000 )) ; then
                    Rscript {params.GREAT} \
                        -p {params.full_no_cobinder} \
                        -o {params.output_full_no_cobinder} \
                        -g {params.genome_version}
                fi ;

                for file in {params.full_specific_with_cobinder}/full_regions_with_cobinder_*.bed ; do
                    filename=$(basename -- "$file") ;
                    cobinder_id="${{filename%.bed}}" ;
                    cobinder_id="${{cobinder_id//"full_regions_with_cobinder_"/}}" ;
                    
                    with_output_folder={params.output_cobinder_specific}/cobinder_"$cobinder_id"/with_cobinder ;
                    mkdir -p $with_output_folder ;
                    
                    no_output_folder={params.output_cobinder_specific}/cobinder_"$cobinder_id"/no_cobinder ;
                    mkdir -p $no_output_folder ;
                    
                    echo "; Running GREAT analysis for cobinder $cobinder_id" ;
        
                    regions_with_cobinder={params.full_specific_with_cobinder}/full_regions_with_cobinder_"$cobinder_id".bed ;
                    regions_no_cobinder={params.full_specific_no_cobinder}/full_regions_no_cobinder_"$cobinder_id".bed ;
        
                    Rscript {params.GREAT} \
                        -p "$regions_with_cobinder" \
                        -o "$with_output_folder" \
                        -g {params.genome_version} ;
        
                    n_spec_regions_no_cobinder=$(wc -l "$regions_no_cobinder" | awk '{{ print $1 }}') ;
                    if (( $n_spec_regions_no_cobinder <= 500000 )) ; then
                        Rscript {params.GREAT} \
                            -p "$regions_no_cobinder" \
                            -o "$no_output_folder" \
                            -g {params.genome_version}
                    fi ;
                done ;
                """

    ### This will run for all other than human and mouse species:
    else:
        print("; GREAT and human, arabidopsis-specific PPI analysis skipped. Jumping to generating reports and summarizing.")

else:
    print("; GREAT and Jumping to generating reports and summarizing.")

###############
## 9. Report ##
###############

## Defining input:
def report_input(wildcards):
    if (config['analysis_id'] == "human_hg38") | (config['analysis_id'] == "mouse_mm10") | (config['analysis_id'] == "human_hg38_grn") | (config['analysis_id'] == "human_hg19_grn") | (config['analysis_id'] == "mouse_mm10_smf"):
        return GREAT_OUTPUT
    elif (config['analysis_id'] == "celegans_WBcel235") | (config['analysis_id'] == "drosophila_dm6") | (config['analysis_id'] == "arabidopsis_TAIR10"):
        return CONSERVATION_STATISTICS_MIXED
    else:
        return PPI_ANALYSIS_STATS

rule generateREPORT:
    """
    9.1. Summarizing the results into a HTML report.
    """
    input:
        report_input
    output:
        REPORT_HTML
    message:
        "; Generating analysis report for {wildcards.family}."
    params:
        bin      = os.path.join(config["BIN"], "report"), \
        title    = config["title"], \
        metadata = config["METADATA"]
    shell:
        """
        {{
            touch {output} ;

            echo "Rscript {params.bin}/generate_report.R \
            -f {wildcards.family} \
            -a {params.title} \
            -r {params.bin}/cobind_report_template.Rmd \
            -m {params.metadata}" > \
            {params.title}/reports/{wildcards.family}/command.txt ;

            Rscript {params.bin}/generate_report.R \
            -f {wildcards.family} \
            -a {params.title} \
            -r {params.bin}/cobind_report_template.Rmd \
            -m {params.metadata} ;

        }} || {{
            
            touch {output} ;

            echo "Rscript {params.bin}/generate_report.R \
            -f {wildcards.family} \
            -a {params.title} \
            -r {params.bin}/cobind_report_template.Rmd \
            -m {params.metadata}" > \
            {params.title}/reports/{wildcards.family}/command.txt
            
        }}
        """

# Function that defines new wildcard "family", so the pipeline uses the checkpoint to run uninterupted.
def aggregate_reports(wildcards):
    checkpoint_output = checkpoints.prepareCLUSTERINGtable.get(**wildcards).output[0]
    file_names = expand(REPORT_HTML,
                        family = glob_wildcards(os.path.join(checkpoint_output, "{family}_core.tab")).family)
    return file_names

#################
## 10. Summary ##
#################

rule distributionPLOTS:
    """
    10.1. Summarizing the numbers of discovered co-binders.
    """
    input:
        aggregate_reports
    output:
        COBINDER_SUMMARY
    message:
        "; Summarizing discovered co-binders."
    params:
        bin                    = os.path.join(config["BIN"], "summary"), \
        clustering_dir         =  os.path.join(RESULTS_DIR, "clustering_results"), \
        title                  = config["title"], \
        tf_families_tables_log = CLUST_TABLE_LOG, \
        sampling_iterations    = config["SAMPLE_ITERATIONS"], \

        output_dir_full        = os.path.join(RESULTS_DIR, "ppi_associations_results", "physical_full"), \
        output_dir_physical    = os.path.join(RESULTS_DIR, "ppi_associations_results", "physical"), \

        output_dir_tomtom      = os.path.join(RESULTS_DIR, "ppi_associations_results", "tomtom"), \
        output_dir_tomtom_full = os.path.join(RESULTS_DIR, "ppi_associations_results", "tomtom_full"), \

        conservation_dir       = os.path.join(RESULTS_DIR, "conservation_analysis"), \
        conservation_summary_out_dir = os.path.join(RESULTS_DIR, "analysis_overall_summary", "conservation_summary"), \
        conservation_mixed_summary_out_dir = os.path.join(RESULTS_DIR, "analysis_overall_summary", "mixed_conservation_summary"), \
        ref_top_cobinders      = os.path.join(RESULTS_DIR, "analysis_overall_summary", "conservation_summary/top_conserved_cobinders_conservation_results.tsv")
    shell:
        '''
        n_lines=$(wc -l {params.tf_families_tables_log} | awk '{{ print $1 }}') ;

        if [ $n_lines == 0 ]
        then
            echo '; No co-binders were found during analysis.'
            echo 'No co-binders were found during analysis.' > {output} ;
        else
            echo '; Summarizing co-binder analysis.'
            rm -rf {params.output_dir_tomtom} ;
            rm -rf {params.output_dir_tomtom_full} ;

            Rscript {params.bin}/cobinder_overall_distribution.R \
            -i {params.clustering_dir} \
            -t {params.title} ;

            echo "; Summarizing PPI analysis."
            Rscript {params.bin}/summarize_ppi_tomtom_analysis.R \
            -i {params.output_dir_full} \
            -t {params.title} ;

            Rscript {params.bin}/summarize_ppi_tomtom_analysis.R \
            -i {params.output_dir_physical} \
            -t {params.title} ;

            if [[ -d {params.conservation_dir} ]]
            then
                echo "; Summarizing conservation analysis."
                Rscript {params.bin}/summarize_cobinder_conservation.R \
                -i {params.conservation_dir} \
                -o {params.conservation_summary_out_dir} ;

                echo "; Summarizing mixed regions runs."
                for iteration in {params.sampling_iterations}
                do
                    conservation_dir_mixed={params.conservation_dir}_mixed/"$iteration" ;
                    Rscript {params.bin}/summarize_cobinder_conservation.R \
                    -i "$conservation_dir_mixed" \
                    -o {params.conservation_summary_out_dir}_"$iteration" \
                    -r {params.ref_top_cobinders};
                done ;

                Rscript {params.bin}/summarize_mixed_regions_conservation.R \
                -i {params.conservation_summary_out_dir} \
                -o {params.conservation_mixed_summary_out_dir} ;
            fi ;   
        fi
        '''

##------------------##
## End of Snakefile ##
##------------------##