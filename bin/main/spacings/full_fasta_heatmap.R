#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "dplyr",
                        "optparse",
                        "seqinr",
                        "ComplexHeatmap",
                        "circlize",
                        "nationalparkcolors")


for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only=TRUE, quietly = T))
}

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(c("-i", "--spacing_folder"), type = "character", default = NULL,
              help = "Path to the folder with the spacing information and the logos. (Mandatory) ", metavar = "character"),
  
  make_option(c("-t", "--analysis_title"), type = "character", default = NULL,
              help = "Analysis title matching the title in the config.yaml. (Mandatory) ", metavar = "character"),
  
  make_option(c("-f", "--family_name"), type = "character", default = NULL,
              help = "TF, family or class name. (Mandatory) ", metavar = "character")
  
);
message("; Reading arguments from command line")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

############
## Syntax ##
############

### Syntax:
## Rscript R-scripts/full_fasta_heatmap.R 
##        -i 0919_GINI_filtering_048_2to5/results/clustering_results/GATA1/spacings
##        -t 0919_GINI_filtering_048_2to5
##        -f GATA1

# Rscript R-scripts/full_fasta_heatmap.R -i 0921_GINI_filtering_04803937_2to5/results/clustering_results/GATA1/spacings -t 0921_GINI_filtering_04803937_2to5 -f GATA1

########################
## Set variable names ##
########################
spacing.folder         <- opt$spacing_folder
analysis.title         <- opt$analysis_title
family.name            <- opt$family_name

# Debug

# setwd("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind")
# spacing.folder <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/scratch/ieva/COBIND_results/individual_HUMAN_02/results/clustering_results/GATA1/spacings"
# family.name <- "GATA1"
# analysis.title <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/scratch/ieva/COBIND_results/individual_HUMAN_02"

###############
## Functions ##
###############

## Converting nucleotides to numbers:
# l <- fasta_seq_filtered[1,]

fasta2ohe <- function(l){
  
  ohe <- sapply(l, function(nt){
    
    if (nt == "a"){
      nt <- 1
    } else if (nt == "c"){
      nt <- 2
    } else if (nt == "g"){
      nt <- 3
    } else if (nt == "t"){
      nt <- 4
    }
  })
  as.vector(unlist(ohe))
  
}

## Making a combined table of all datasets:
# spacing_tab_info <- spacing.tab.r
nucleotide_table_with_annotation <- function(spacing_tab_info) {
  
  nc_anno_table <- data.frame()
  
  for (i in 1:nrow(spacing_tab_info)) {

    entry <- spacing_tab_info[i, ]
    # print(i)
    ## Reading indices
    indices <- fread(file.path(entry$Path_to_indices))
    indices <- indices$V1
    indices <- indices +1

    ## How many sequences were used to build a logo in a first place
    nb_motif_sequences <- length(indices)
    
    ## Reading fasta file:
    fasta_seq <- read.fasta(file.path(entry$Path_to_full_FASTA))
    fasta_seq <- do.call(rbind, fasta_seq)
    
    fasta_seq_filtered <- fasta_seq[indices, ]
    fasta_seq_filtered <- tolower(fasta_seq_filtered)
    head(fasta_seq_filtered)
    # fasta_seq_filtered <- as.data.frame(fasta_seq_filtered)
    
    # Converting nucleotides to numbers
    ohe.df <- apply(fasta_seq_filtered, 1, fasta2ohe)
    # head(ohe.df)
    ohe.df <- t(ohe.df)
    # head(ohe.df)
    
    ## New dataframe for annotation:
    anno.df <- as.data.frame(ohe.df) %>%
                mutate(Dataset = entry$Dataset) %>%
                mutate(Nb_components = entry$Nb_components) %>%
                mutate(Component = entry$Component) %>%
                mutate(Nb_sequences = nb_motif_sequences) %>%
                mutate(Flank_location = entry$Flank) %>%
                mutate(Path_to_indices = entry$Path_to_indices) %>%
                mutate(Subcluster = entry$Subcluster)
    
    nc_anno_table <- rbind(nc_anno_table, anno.df)
    
  }
  return(nc_anno_table)
}

## Separating the heatmaps, based on the flank size:
separate_flanks <- function(i){
  
  if (i <= flank.size) {
    "Left_flank"
    
  } else if (i > (ncol(ohe.df.sequences) - flank.size)) {
    "Right_flank"
  } else {
    "Anchor"
  }
}

##############################
## Finding flank extensions ##
##############################

message("; Finding flank extension information.")
working.folder <- file.path(analysis.title, "results")

## Finding flank sizes from existing data:
FASTA_files <- list.files(file.path(working.folder, "datasets", (list.files(file.path(working.folder, "datasets"))[1]), "fasta"))
FASTA_files <- grep(pattern = ".+subtd", FASTA_files, value = TRUE)

right_flank_extension <- grep(pattern = ".+\\.0l\\d+r", FASTA_files, value = TRUE)
left_flank_extension <- grep(pattern = ".+\\.\\d+l0r\\.", FASTA_files, value = TRUE)

right_flank_extension <- gsub(pattern = ".+\\.bed\\.oriented", replacement = "", right_flank_extension)
left_flank_extension <- gsub(pattern = ".+\\.bed\\.oriented", replacement = "", left_flank_extension)

flank.size <-  as.numeric(gsub(pattern = "(\\.0l)(\\d+)(r.subtd.fa)", replacement = "\\2", right_flank_extension))

########################
## Read spacing table ##
########################

message("; Reading table with spacing information.")
spacing.tab <- fread(file.path(spacing.folder, "spacings.txt"), header = FALSE)

#####################################
## Constructing new spacings table ##
#####################################

## Extracting relevant info to build tables for left and right flanks.
message("; Creating a table with relevant info.")

spacing.tab <- spacing.tab[,c(1, 2, 11, 14, 15, 16)] %>%
  dplyr::rename(Core = V1,
                Cobinder = V2,
                Flank = V11,
                Spacer_length = V14,
                Location = V15,
                Subcluster = V16) %>%
  mutate(Dataset = gsub(Core, pattern = "_\\D{4,5}_\\d+_components_.+", replacement = "")) %>%
  mutate(Nb_components = stringr::str_extract(Core, pattern = "\\d+_components")) %>%
  mutate(Component = stringr::str_extract(Core, pattern = "Comp_\\d+")) %>%
  mutate(Component = gsub(Component, pattern = "Comp_", replacement = "")) %>%
  mutate(Path_to_indices = file.path(working.folder, "nmf_motifs", paste0(Dataset, "_", Flank), Nb_components, paste0("indices_", Component, ".txt"))) %>%
  mutate(Path_to_full_FASTA = file.path(working.folder, "full_fasta", paste0(Dataset, ".bed.full.fa"))) %>%
  arrange(Dataset, Nb_components, Component) %>%
  dplyr::select(Subcluster, Dataset, Flank, Path_to_full_FASTA, Nb_components, Component, Path_to_indices, Cobinder, Spacer_length, Location)

## Separating the table into two sides - left and right - according to NMF flank side.
spacing.tab.r <- spacing.tab %>% filter(Flank == "right")
spacing.tab.l <- spacing.tab %>% filter(Flank == "left")

#####################
## OHE table RIGHT ##
#####################
message("; Gathering sequences for the right side.")

if (nrow(spacing.tab.r) > 0) {
  
  anno.df.right <- nucleotide_table_with_annotation(spacing.tab.r)
  
  ## Masking the left nucleotides to grey
  anno.df.right[,1:flank.size] <- 0
  
} else {
  
  message("; No-cobinder on the right.")
  anno.df.right <- data.frame()
}


####################
## OHE table LEFT ##
####################
message("; Gathering sequences for the left side.")

if (nrow(spacing.tab.l) > 0) {
  
  anno.df.left <- nucleotide_table_with_annotation(spacing.tab.l)
  
  ## Masking the right side, where there is no cobinder
  anno.df.left[, (ncol(anno.df.left) - flank.size - 6) : (ncol(anno.df.left) - 7)] <- 0

} else {
  
  message("; No-cobinder on the left")
  anno.df.left <- data.frame()
  
}

##########################
## Combining both sides ##
##########################

anno.df.full <- rbind(anno.df.left, anno.df.right)

anno.df.full <- anno.df.full %>%
      arrange(Subcluster, Dataset, Nb_components, Component, Flank_location)


#########################
## Drawing the heatmap ##
#########################

ohe.df.sequences <- anno.df.full %>%
          select(-Subcluster,
                 -Dataset, 
                 -Nb_components, 
                 -Component, -Nb_sequences, 
                 -Flank_location, 
                 -Path_to_indices)

## Colours of the main heatmap
col_fun = colorRamp2(c(0:4), c("lightgrey", "darkgreen", "blue", "orange", "red"))


## Separating core and both flanks
col.index <- 1:ncol(ohe.df.sequences)

col.ann <- sapply(col.index, separate_flanks)
col.levels <- c("Left_flank", "Anchor", "Right_flank")

split.col.df <- data.frame(Coln = 1:ncol(ohe.df.sequences), Pos = col.ann)

## Spliting rows
split.rows.by <- anno.df.full$Subcluster
row.levels <- unique(split.rows.by)

### Heatmap

ht = Heatmap(as.matrix(ohe.df.sequences),
             
             ## Color
             col = col_fun,
             
             ## Clustering
             cluster_columns = F,
             cluster_rows = F,
             show_row_dend = F,
             show_row_names = F,
             
             show_column_names = F,
             
             ## Cell spacing
             rect_gp = gpar(col = "white", lty = 0.1, lwd = 0.1, row = "white"),
             
             show_heatmap_legend = F,
             
             ## Split section
             row_split    = factor(split.rows.by, levels = row.levels),
             column_split = factor(split.col.df$Pos, levels = col.levels),
             
             # row_gap = unit(3, "mm"),
             column_gap = unit(3, "mm"),
             
             use_raster = TRUE,
             raster_device = "tiff")

## Calculating unique categories
nb_unique_sublusters <- length(unique(as.character(anno.df.full$Subcluster)))
nb_unique_datasets <- length(unique(anno.df.full$Dataset))
nb_unique_nb_components <- length(unique(anno.df.full$Nb_components))
nb_unique_components <- length(unique(anno.df.full$Component))

## Brewing colours for those categories
# col_subclusters <- colorRampPalette(brewer.pal(10, "Spectral"), space="Lab")(nb_unique_sublusters)
# col_datasets <- colorRampPalette(brewer.pal(10, "Spectral"), space="Lab")(nb_unique_datasets)
# col_nb_components <- colorRampPalette(brewer.pal(10, "Spectral"), space="Lab")(nb_unique_nb_components)
# col_components <- colorRampPalette(brewer.pal(10, "Spectral"), space="Lab")(nb_unique_components)

# col_subclusters <- colorRampPalette(nord("aurora", 5), space="Lab")(nb_unique_sublusters)
# col_datasets <- colorRampPalette(nord("afternoon_prarie", 9), space="Lab")(nb_unique_datasets)
# col_nb_components <- colorRampPalette(nord("lumina", 5), space="Lab")(nb_unique_nb_components)
# col_components <- colorRampPalette(nord("lumina", 5), space="Lab")(nb_unique_components)

col_subclusters <- colorRampPalette(park_palette("DeathValley", 6), space = "Lab")(nb_unique_sublusters)
col_datasets <- colorRampPalette(park_palette("SmokyMountains", 6), space = "Lab")(nb_unique_datasets)
col_nb_components <- colorRampPalette(park_palette("Badlands", 4), space = "Lab")(nb_unique_nb_components)
col_components <- colorRampPalette(park_palette("BlueRidgePkwy", 6), space = "Lab")(nb_unique_components)

## Drawing annotation heatmaps
ht_anno = Heatmap(as.character(anno.df.full$Subcluster),
                  col = col_subclusters,
                  name = "Subcluster",
                  row_split    = split.rows.by,
                  show_column_names = F) +
          Heatmap(anno.df.full$Dataset,
                  col = col_datasets,
                  name = "Dataset",
                  row_split    = split.rows.by,
                  show_column_names = F) +
          Heatmap(anno.df.full$Nb_components,
                  col = col_nb_components,
                  name = "Number of components",
                  row_split    = split.rows.by,
                  show_column_names = F) +
          Heatmap(anno.df.full$Component, 
                  name = "Component",
                  col = col_components,
                  row_split    = split.rows.by,
                  show_column_names = F)

ht_combined = ht_anno + ht

### colorRampPalette(brewer.pal(10, "Set1"), space="Lab")(100)

################################################
## Exporting the heatmap and annotation table ##
################################################

anno.df.full.export <- anno.df.full %>%
                        select(Subcluster,
                               Dataset,
                               Nb_components,
                               Component, 
                               Nb_sequences, 
                               Flank_location, 
                               Path_to_indices) %>%
                        unique()

# anno.df.full.export <- spacing.tab

anno.tab.path <- file.path(spacing.folder, paste0("heatmap_annotation_", family.name, ".txt"))
fwrite(anno.df.full.export, file = anno.tab.path, col.names = TRUE, sep = "\t")


heatmap.path <- file.path(spacing.folder, paste0("heatmap_combined_from_full_fasta_", family.name, ".pdf"))

pdf(file = heatmap.path, width = unit(20, "cm"), height = unit(20, "cm"))
ht_combined <- draw(ht_combined)
dev.off()

#############################################
## Same heatmap with unmasked empty flanks ##
#############################################

#####################
## OHE table RIGHT ##
#####################
message("; Gathering sequences for the right side.")

if (nrow(spacing.tab.r) > 0) {
  
  anno.df.right <- nucleotide_table_with_annotation(spacing.tab.r)
  
  ## Masking the left nucleotides to grey
  # anno.df.right[,1:flank.size] <- 0
  
} else {
  
  message("; No-cobinder on the right.")
  anno.df.right <- data.frame()
}


####################
## OHE table LEFT ##
####################
message("; Gathering sequences for the left side.")

if (nrow(spacing.tab.l) > 0) {
  
  anno.df.left <- nucleotide_table_with_annotation(spacing.tab.l)
  
  ## Masking the right side, where there is no cobinder
  # anno.df.left[, (ncol(anno.df.left) - flank.size - 6) : (ncol(anno.df.left) - 7)] <- 0
  
} else {
  
  message("; No-cobinder on the left")
  anno.df.left <- data.frame()
  
}

##########################
## Combining both sides ##
##########################

anno.df.full <- rbind(anno.df.left, anno.df.right)

anno.df.full <- anno.df.full %>%
  arrange(Subcluster, Dataset, Nb_components, Component, Flank_location)


#########################
## Drawing the heatmap ##
#########################

ohe.df.sequences <- anno.df.full %>%
  select(-Subcluster,
         -Dataset, 
         -Nb_components, 
         -Component, -Nb_sequences, 
         -Flank_location, 
         -Path_to_indices)

## Colours of the main heatmap
col_fun = colorRamp2(c(0:4), c("lightgrey", "darkgreen", "blue", "orange", "red"))


## Separating core and both flanks
col.index <- 1:ncol(ohe.df.sequences)

col.ann <- sapply(col.index, separate_flanks)
col.levels <- c("Left_flank", "Anchor", "Right_flank")

split.col.df <- data.frame(Coln = 1:ncol(ohe.df.sequences), Pos = col.ann)

## Spliting rows
split.rows.by <- anno.df.full$Subcluster
row.levels <- unique(split.rows.by)

### Heatmap

ht = Heatmap(as.matrix(ohe.df.sequences),
             
             ## Color
             col = col_fun,
             
             ## Clustering
             cluster_columns = F,
             cluster_rows = F,
             show_row_dend = F,
             show_row_names = F,
             
             show_column_names = F,
             
             ## Cell spacing
             rect_gp = gpar(col = "white", lty = 0.1, lwd = 0.1, row = "white"),
             
             show_heatmap_legend = F,
             
             ## Split section
             row_split    = factor(split.rows.by, levels = row.levels),
             column_split = factor(split.col.df$Pos, levels = col.levels),
             
             # row_gap = unit(3, "mm"),
             column_gap = unit(3, "mm"),
             
             use_raster = TRUE,
             raster_device = "tiff")

## Calculating unique categories
nb_unique_sublusters <- length(unique(as.character(anno.df.full$Subcluster)))
nb_unique_datasets <- length(unique(anno.df.full$Dataset))
nb_unique_nb_components <- length(unique(anno.df.full$Nb_components))
nb_unique_components <- length(unique(anno.df.full$Component))

col_subclusters <- colorRampPalette(park_palette("DeathValley", 6), space = "Lab")(nb_unique_sublusters)
col_datasets <- colorRampPalette(park_palette("SmokyMountains", 6), space = "Lab")(nb_unique_datasets)
col_nb_components <- colorRampPalette(park_palette("Badlands", 4), space = "Lab")(nb_unique_nb_components)
col_components <- colorRampPalette(park_palette("BlueRidgePkwy", 6), space = "Lab")(nb_unique_components)

## Drawing annotation heatmaps
ht_anno = Heatmap(as.character(anno.df.full$Subcluster),
                  col = col_subclusters,
                  name = "Subcluster",
                  row_split    = split.rows.by,
                  show_column_names = F) +
  Heatmap(anno.df.full$Dataset,
          col = col_datasets,
          name = "Dataset",
          row_split    = split.rows.by,
          show_column_names = F) +
  Heatmap(anno.df.full$Nb_components,
          col = col_nb_components,
          name = "Number of components",
          row_split    = split.rows.by,
          show_column_names = F) +
  Heatmap(anno.df.full$Component, 
          name = "Component",
          col = col_components,
          row_split    = split.rows.by,
          show_column_names = F)

ht_combined = ht_anno + ht

###########################
## Exporting the heatmap ##
###########################

heatmap.path <- file.path(spacing.folder, paste0("heatmap_combined_from_full_fasta_unmasked_", family.name, ".pdf"))

pdf(file = heatmap.path, width = unit(20, "cm"), height = unit(20, "cm"))
ht_combined <- draw(ht_combined)
dev.off()

###################
## End of script ##
###################
# 
# 
# 
# # spacing_tab_info <- spacing.tab.r
# path.to.full.fasta <- "individual_MOUSE_01/results/full_fasta/MM_D1461_C115_J29_T218_SOX9.bed.full.fa"
# path.to.indices <- "individual_MOUSE_01/results/nmf_motifs/MM_D1461_C115_J29_T218_SOX9_right/4_components/indices_1.txt"
# path.to.right.fasta <- "individual_MOUSE_01/results/datasets/MM_D1461_C115_J29_T218_SOX9/fasta/MM_D1461_C115_J29_T218_SOX9.bed.oriented.0l30r.subtd.fa"
# # individual_MOUSE_01/results/datasets/MM_D1461_C115_J29_T218_SOX9/fasta/MM_D1461_C115_J29_T218_SOX9.bed.oriented.0l30r.subtd.fa
# 
#     indices <- fread(file.path(path.to.indices))
#     indices <- indices$V1
#     indices <- indices +1
#     ## How many sequences were used to build a logo in a first place
#     nb_motif_sequences <- length(indices)
#     print(nb_motif_sequences)
#     
#     ## Reading fasta file:
#     fasta_seq <- read.fasta(file.path(path.to.right.fasta))
#     fasta_seq <- do.call(rbind, fasta_seq)
#     
#     fasta_seq_filtered <- fasta_seq[indices, ]
#     fasta_seq_filtered <- tolower(fasta_seq_filtered)
#     # print(nrow(fasta_seq_filtered))
#     # print(str(fasta_seq_filtered))
#     
#     # Converting nucleotides to numbers
#     ohe.df <- apply(fasta_seq_filtered, 1, fasta2ohe)
#     # print(str(ohe.df))
#     ohe.df <- t(ohe.df)
#     # print(str(ohe.df))
#     
#     ## Colours of the main heatmap
#     col_fun = colorRamp2(c(0:4), c("lightgrey", "darkgreen", "blue", "orange", "red"))
#     
#     ## Separating core and both flanks
#     col.index <- 1:ncol(ohe.df.sequences)
#     
#     col.ann <- sapply(col.index, separate_flanks)
#     col.levels <- c("Left_flank", "Anchor", "Right_flank")
#     
#     split.col.df <- data.frame(Coln = 1:ncol(ohe.df.sequences), Pos = col.ann)
#     
#     ## Spliting rows
#     split.rows.by <- anno.df.full$Subcluster
#     row.levels <- unique(split.rows.by)
#     
#     ### Heatmap
#     
#     ht = Heatmap(as.matrix(ohe.df),
#                  
#                  ## Color
#                  col = col_fun,
#                  
#                  ## Clustering
#                  cluster_columns = F,
#                  cluster_rows = F,
#                  show_row_dend = F,
#                  show_row_names = F,
#                  
#                  show_column_names = F,
#                  
#                  ## Cell spacing
#                  rect_gp = gpar(col = "white", lty = 0.1, lwd = 0.1, row = "white"),
#                  
#                  show_heatmap_legend = F,
#                  
#                  ## Split section
#                  # row_split    = factor(split.rows.by, levels = row.levels),
#                  # column_split = factor(split.col.df$Pos, levels = col.levels),
#                  
#                  # row_gap = unit(3, "mm"),
#                  column_gap = unit(3, "mm"),
#                  
#                  use_raster = TRUE,
#                  raster_device = "tiff")
#     
# 
# 
# 
#     >chr11:72226583-72226652(-)
#     TGTGAATGAGCGAGTCGGCAGACAATGGCCTCTTTGTTCAATGTCTTCTGAATGGACACAATTCAAGCT
# 
#     "t"  "g"  "t"  "g"  "a"  "a"  "t"  "g"  "a"  "g"   "c"   "g"   "a"   "g"   "t"   "c"   "g"   "g"   "c"   "a"   "g" 
#     chr11:72226622-72226652(-)   "a"   "c"   "a"   "a"   "t"   "g"   "g"   "c"   "c"
