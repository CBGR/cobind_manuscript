#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "dplyr",
                        "purrr",
                        "ggplot2",
                        "optparse",
                        "stringr")

for (lib in required.libraries) {
    suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = TRUE))
}

####################
## Read arguments ##
####################

option_list = list(

    make_option(
        c("-i", "--input_directory"), 
        type = "character", 
        default = NULL,
        help = "Path to the name of directory, where to look for mixed regions analysis summaries. (Mandatory) ", 
        metavar = "character"),

    make_option(
        c("-o", "--output_directory"), 
        type = "character", 
        default = NULL,
        help = "Output direcory to output summarized results. (Mandatory) ", 
        metavar = "character")

);

message("; Reading arguments from command line")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

############
## Syntax ##
############

## Rscript bin/dhs_footprinting/region_analysis/summarize_mixed_analysis.R \
##  -i DHS_data_analysis/HUMAN_for_DHS_0.99/region_analysis_summary \
##  -o DHS_data_analysis/HUMAN_for_DHS_0.99/mixed_region_analysis_summary 

########################
## Set variable names ##
########################

INPUT_DIR  <- opt$input_directory
OUTPUT_DIR <- opt$output_directory

## Debug:
# INPUT_DIR <- "DHS_data_analysis/HUMAN_for_DHS_0.99/region_analysis_summary"
# OUTPUT_DIR <- "DHS_data_analysis/HUMAN_for_DHS_0.99/mixed_region_analysis_summary"

#############
## Options ##
#############

options(stringsAsFactors = FALSE)

###############################
## Creating output directory ##
###############################

message("; Creating output directory.")
dir.create(OUTPUT_DIR, showWarnings = F, recursive = T)

############################
## Setting plotting theme ##
############################

message("; Setting ggplot theme.")

ggplot2::theme_set(
    theme_bw() +
        theme(
            plot.title = element_text(size = 16),
            axis.title = element_text(size = 16, colour = "black"),
            axis.text = element_text(size = 14, colour = "black")
        )
)

###########################
## Finding all summaries ##
###########################

message("; Collecting summaries from mixed region analysis")

## Mixed regions summaries:
MIXED_REGIONS_SUMMARIES_FILES <- file.path(OUTPUT_DIR, "list_of_analysis_summaries.txt")

find_summaries_files_cmd <- paste0(
    " find ", INPUT_DIR, "_*",
    " -name *.tsv > ",
    MIXED_REGIONS_SUMMARIES_FILES)

system(find_summaries_files_cmd)

list_of_summary_files <- data.table::fread(
    MIXED_REGIONS_SUMMARIES_FILES,
    header = FALSE, 
    sep = "\t")

list_of_summary_files <- list_of_summary_files %>%
    dplyr::pull(V1)

## Normal summaries:
ALL_PAIRS_SUMMARY_FILE <- file.path(INPUT_DIR, "combined_dhs_analysis_results_all_pairs.tsv")
ALL_PAIRS_WITHOUT_CTCF_SUMMARY_FILE <- file.path(INPUT_DIR, "combined_dhs_analysis_results_all_pairs_without_CTCF.tsv")

TOP_COBINDERS_SUMMARY_FILE <- file.path(INPUT_DIR, "top_significant_cobinders_dhs_analysis_results.tsv")
TOP_COBINDERS_WITHOUT_CTCF_SUMMARY_FILE <- file.path(INPUT_DIR, "top_significant_cobinders_dhs_analysis_results_excluding_CTCF.tsv")

###################################################
## Extracting only those files that will be used ##
###################################################

list_of_all_pairs_summary_files <- list_of_summary_files[basename(list_of_summary_files) == "combined_dhs_analysis_results_all_pairs.tsv"]
list_of_all_pairs_without_CTCF_summary_files <- list_of_summary_files[basename(list_of_summary_files) == "combined_dhs_analysis_results_all_pairs_without_CTCF.tsv"]

list_of_top_cobinders_summary_files <- list_of_summary_files[basename(list_of_summary_files) == "top_significant_cobinders_dhs_analysis_results.tsv"]
list_of_top_cobinders_without_CTCF_summary_files <- list_of_summary_files[basename(list_of_summary_files) == "top_significant_cobinders_dhs_analysis_results_excluding_CTCF.tsv"]

#################
## Summarizing ##
#################

# list_of_files = list_of_top_cobinders_summary_files
# real_regions_file = TOP_COBINDERS_SUMMARY_FILE
# title = "top_cobinders"
# output_dir = OUTPUT_DIR
summarize_and_plot_mixed_region_analysis <- function(
    list_of_files,
    real_regions_file,
    title,
    output_dir) {

    ## Reading summary for real regions:
    all_pairs_summary_tab <- data.table::fread(
            real_regions_file,
            header = TRUE,
            sep = "\t") %>%
        dplyr::mutate(
            iteration_id = "0",
            results_type = "Observed")

    ## Reading all summaries for mixed regions:
    all_pairs_mixed_summary_tab <- list_of_files %>%
        purrr::map(
            function(x)
            data.table::fread(file.path(x), header = TRUE, sep = "\t") %>%
            dplyr::mutate(
                iteration_id = gsub(
                    basename(dirname(x)),
                    pattern = "region_analysis_summary_",
                    replacement = ""),
                results_type = "Expected")) %>%
        purrr::reduce(rbind) %>%
        unique()

    ## Recomputing proportions when combining all iterations:
    all_pairs_summary_tab <- rbind(
            all_pairs_summary_tab,
            all_pairs_mixed_summary_tab) %>%
        dplyr::group_by(
            results_type,
            region_type,
            cobinder_cell_type) %>%
        dplyr::mutate(
            sum_nb_of_both_significant = sum(as.numeric(nb_of_both_anchor_cobinder_significant)),
            sum_of_nb_significant = sum(as.numeric(nb_significant_points)),
            sum_of_total_nb_points = sum(as.numeric(total_nb_points))) %>%
        dplyr::mutate(
            overall_proportion_of_significant = sum_of_nb_significant/sum_of_total_nb_points,
            overall_proportion_of_singificant_both = sum_nb_of_both_significant/sum_of_total_nb_points) %>%
        dplyr::ungroup()

    ## Exporting the table:
    PVALUES_SUMMARY_PATH <- file.path(output_dir, paste0(title, "_pvalues_dhs_analysis.tsv"))
    data.table::fwrite(
        all_pairs_summary_tab,
        file = PVALUES_SUMMARY_PATH,
        col.names = TRUE, sep = "\t")

    ## Labels for plots:
    labels_for_plot <- all_pairs_summary_tab %>%
        dplyr::select(
            results_type, 
            region_type, 
            overall_proportion_of_significant, 
            overall_proportion_of_singificant_both) %>%
        unique() %>%
        dplyr::mutate(
            label = paste0(
                results_type,
                " p-value < 0.05\nfor ", 
                round(overall_proportion_of_significant*100, digits = 0),
                "%\n",
                "p-value < 0.05 (anchor+co-binding together)\nfor ",
                round(overall_proportion_of_singificant_both*100, digits = 0),
                "%"))

    ## Params for plot:
    if (title == "all_pairs" | title == "top_cobinders") {
        x_axis_label <- "Proportion of co-binding patterns and anchors\nin different DHS datasets"
    } else if (title == "all_pairs_without_CTCF" | title == "top_cobinders_without_CTCF") {
        x_axis_label <- "Proportion of co-binding patterns and anchors\nin different DHS datasets (excluding CTCF)"
    } else {
        x_axis_label <- "Proportion of co-binding patterns and anchors\nin different DHS datasets"
    }

    ## Plotting:
    pvalues_distribution <- ggplot() +
        geom_histogram(
            data = subset(all_pairs_summary_tab, results_type == "Observed"),
            aes(
                x = p_value,
                y = stat(density) * 0.03,
                fill = "Observed"),
            binwidth = 0.03,
            size = 0.5,
            color = "#ae017e") +
        geom_histogram(
            data = subset(all_pairs_summary_tab, results_type == "Expected"),
            aes(
                x = p_value,
                y = - (stat(density) * 0.03),
                fill = "Expected"),
            binwidth = 0.03, 
            size = 0.5, 
            color = "#ae017e") +
        facet_wrap(. ~ region_type, ncol = 2) +
        labs(
            x = "DHS p-value",
            y = x_axis_label) +
        scale_fill_manual(
            values = c("#fde0dd", "#dd3497"),
            labels = c("Expected", "Observed"),
            name   = "Results") +
        geom_vline(
            xintercept = 0.05, 
            col        = "#7a0177",
            size       = 1) +
        theme(legend.position = "bottom")

    upper_y_lim <- max(ggplot_build(pvalues_distribution)$layout$panel_scales_y[[1]]$range$range)
    lower_y_lim <- min(ggplot_build(pvalues_distribution)$layout$panel_scales_y[[1]]$range$range)

    upper_y_lim <- ifelse(
        round(upper_y_lim, digits = 1) > upper_y_lim,
        yes = round(upper_y_lim, digits = 1),
        no  = round(upper_y_lim, digits = 1) + 0.1)
    lower_y_lim <- ifelse(
        round(lower_y_lim, digits = 1) < lower_y_lim,
        yes = round(lower_y_lim, digits = 1),
        no  = round(lower_y_lim, digits = 1) - 0.1)

    pvalues_distribution <- pvalues_distribution +
        scale_y_continuous(
            labels = abs,
            limits = c(
                lower_y_lim, upper_y_lim),
            breaks = seq(lower_y_lim, upper_y_lim, 0.2)) +
        geom_text(
            data = subset(labels_for_plot, results_type == "Observed"),
            aes(
            x = 0.15,
            y = upper_y_lim - (upper_y_lim / 5),
            label = label),
            hjust = 0, 
            vjust = 0) +
        geom_text(
            data = subset(labels_for_plot, results_type == "Expected"),
            aes(
            x = 0.15,
            y = lower_y_lim - (lower_y_lim / 7),
            label = label),
            hjust = 0, 
            vjust = 0)

    PVALUES_HIST_PATH <- file.path(output_dir, paste0(title, "_pvalues_dhs_analysis.pdf"))
    ggsave(
        filename  = PVALUES_HIST_PATH,
        plot      = pvalues_distribution,
        width     = 12,
        height    = 8,
        limitsize = FALSE)
    }

## Applying function to different lists:

summarize_and_plot_mixed_region_analysis(
    list_of_files = list_of_all_pairs_summary_files,
    real_regions_file = ALL_PAIRS_SUMMARY_FILE,
    title = "all_pairs",
    output_dir = OUTPUT_DIR) 

summarize_and_plot_mixed_region_analysis(
    list_of_files = list_of_all_pairs_without_CTCF_summary_files,
    real_regions_file = ALL_PAIRS_WITHOUT_CTCF_SUMMARY_FILE,
    title = "all_pairs_without_CTCF",
    output_dir = OUTPUT_DIR) 

summarize_and_plot_mixed_region_analysis(
    list_of_files = list_of_top_cobinders_summary_files,
    real_regions_file = TOP_COBINDERS_SUMMARY_FILE,
    title = "top_cobinders",
    output_dir = OUTPUT_DIR) 

summarize_and_plot_mixed_region_analysis(
    list_of_files = list_of_top_cobinders_without_CTCF_summary_files,
    real_regions_file = TOP_COBINDERS_WITHOUT_CTCF_SUMMARY_FILE,
    title = "top_cobinders_without_CTCF",
    output_dir = OUTPUT_DIR) 

###################
## End of Script ##
###################