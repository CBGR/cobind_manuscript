##-------------------##
## Loading libraries ##
##-------------------##

required.libraries <- c(
    "data.table",
    "dplyr",
    "optparse",
    "ggplot2",
    "tidyr",
    "purrr")

for (lib in required.libraries) {
    suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

##---------##
## Options ##
##---------##

options(stringsAsFactors = FALSE)
options(scipen = 999)

##-------------------##
## Reading arguments ##
##-------------------##

option_list = list(

    make_option(
        c("-a", "--annotations"), 
        type = "character", 
        default = NULL,
        help = "A path to a file from interspecies analysis with family annotations. (Mandatory) ", 
        metavar = "character"),

    make_option(
        c("-s", "--shared_cobinders"), 
        type = "character", 
        default = NULL,
        help = "A path to a file from interspecies analysis with shared co-binders summary. (Mandatory) ", 
        metavar = "character"),

    make_option(
        c("-o", "--output_dir"), 
        type = "character", 
        default = NULL,
        help = "A path to the base output directory. (Mandatory) ", 
        metavar = "character")
)

message("; Reading arguments from command line")
opt_parser = OptionParser(option_list = option_list)
opt        = parse_args(opt_parser)

##--------------------##
## Set variable names ##
##--------------------##

PATH_TO_ANNO                     <- opt$annotations
PATH_TO_SHARED_COBINDERS_SUMMARY <- opt$shared_cobinders
MAIN_OUTPUT_DIR                  <- opt$output_dir

##--------##
## Syntax ##
##--------##

# Rscript bin/main/regions/jaccard_analysis.r \
# -a /storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/interspecies/families_0.99/cobinders_clustering_results_summary/clusters_spacings_metadata_complete.tsv \
# -s /storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/interspecies/families_0.99/cobinders_clustering_results_summary/tfs_sharing_cobinders_per_family_summary_table.txt \
# -o /storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/jaccard_analysis

## Debug:
# PATH_TO_ANNO <- "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/interspecies/families_0.99/cobinders_clustering_results_summary/clusters_spacings_metadata_complete.tsv"
# PATH_TO_SHARED_COBINDERS_SUMMARY <- "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/interspecies/families_0.99/cobinders_clustering_results_summary/tfs_sharing_cobinders_per_family_summary_table.txt"
# MAIN_OUTPUT_DIR <- "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/jaccard_analysis"

############################
## Setting plotting theme ##
############################

message("; Setting ggplot theme.")

ggplot2::theme_set(
    theme_bw() +
        theme(
            plot.title = element_text(size = 16),
            axis.title = element_text(size = 16, colour = "black"),
            axis.text = element_text(size = 14, colour = "black")
        )
)

##-----------##
## Functions ##
##-----------##

# species_family_name <- list_of_species_tf_families[1]
# summary_tab <- analysis_tab
# tmp_regions_output_dir <- TMP_REGIONS_DIR
# output_dir <- FAMILIES_ANALYSIS_OUT_DIR
compute_jaccard_for_family <- function(
    species_family_name,
    summary_tab,
    tmp_regions_output_dir,
    output_dir,
    type = "all"
) {

    message("; Computing for species-family: ", species_family_name)

    ## Getting TF names:
    summary_tab_for_family <- summary_tab %>%
        dplyr::filter(species_family_id == species_family_name)
    list_of_tfs_in_family <- unique(summary_tab_for_family$motif_name)

    ## Getting other params:
    list_of_representing_species <- paste0(
        unique(summary_tab_for_family$species_name_short), 
        collapse = ",")
    list_of_tf_motifs <- paste0(
        unique(summary_tab_for_family$motif_name),
        collapse = ",")
    list_of_TFs <- paste0(
        unique(summary_tab_for_family$TF),
        collapse = ",")
    list_of_cobinder_clusters <- paste0(
        unique(summary_tab_for_family$cobinders_cluster_id),
        collapse = ",")
    nb_of_unique_TF_motifs <- unique(summary_tab_for_family$total_nb_unique_tfs)
    nb_of_TFs_sharing_cobinders <- unique(summary_tab_for_family$number_of_tfs_sharing_cobinder_in_family)
    nb_of_representing_species <- unique(summary_tab_for_family$number_of_species_representing_sharing_cobinder_in_family)

    ## Creating a summary for family:
    family_summary_entry <- data.table(
        species_family_name,
        list_of_tf_motifs,
        list_of_representing_species,
        list_of_TFs,
        list_of_cobinder_clusters,
        nb_of_unique_TF_motifs,
        nb_of_TFs_sharing_cobinders,
        nb_of_representing_species,
        jaccard_done = "no")

    ## Computing Jaccard:
    if (length(list_of_tfs_in_family) > 1) {

        message("; There are more than two TFs in a family, computing Jaccard index.")
        ## Merging the regions with and without:
        purrr::pwalk(
            list(
                unique(summary_tab_for_family$path_to_regions_with_cobinders),
                unique(summary_tab_for_family$path_to_regions_without_cobinders),
                unique(summary_tab_for_family$path_to_merged)
            ),
            function(
                regions_with,
                regions_without,
                output_file
            ) {

                if (type == "with") {
                    original_file <- regions_with
                    output_file <- file.path(
                        paste0(dirname(output_file), "_with"),
                        basename(output_file))
                } else if (type == "without") {
                    original_file <- regions_without
                    output_file <- file.path(
                        paste0(dirname(output_file), "_without"),
                        basename(output_file))
                }

                dir.create(dirname(output_file), recursive = TRUE, showWarnings = FALSE)

                if (type == "all") {
                    
                    merge_cmd <- paste(
                        "cat",
                        regions_with, regions_without,
                        "|", "sort -k1,1 -k2,2n ", "|", "uniq", ">",
                        output_file,
                        sep = " ")
                    message(merge_cmd)
                    # system(merge_cmd)
                } else {
                    cp_cmd <- paste(
                        "cat",
                        original_file,
                        "|", "sort -k1,1 -k2,2n ", "|", "uniq", ">",
                        output_file,
                        sep = " ")
                    message(cp_cmd)
                    # system(cp_cmd)
                }
            }
        )

        if (type == "all") {
            list_of_files_for_intervene <- unique(summary_tab_for_family$path_to_merged)
        } else if (type == "with") {
            list_of_files_for_intervene <- unique(file.path(
                paste0(dirname(summary_tab_for_family$path_to_merged), "_with"),
                basename(summary_tab_for_family$path_to_merged)))
        } else if (type == "without") {
            list_of_files_for_intervene <- unique(file.path(
                paste0(dirname(summary_tab_for_family$path_to_merged), "_without"),
                basename(summary_tab_for_family$path_to_merged)))
        } else {
            stop("; Incorrect analysis type. Stopping.")
        }

        ## Running Intervene Jaccard analysis:
        INTERVENE_OUT_DIR <- file.path(output_dir, species_family_name)

        nb_of_files <- length(list_of_files_for_intervene)
        fig_height <- nb_of_files * 0.25
        fig_len <- fig_height * 2
        
        intervene_cmd <- paste(
            "intervene pairwise -i ",
            paste0(list_of_files_for_intervene, collapse = " "),
            "--type genomic --compute jaccard --htype tribar",
            "--title", species_family_name,
            "--output",
            INTERVENE_OUT_DIR,
            sep = " ")
        message(intervene_cmd)
        # system(intervene_cmd)

        ## Adding tag that intervene was performed:
        family_summary_entry$jaccard_done <- "yes"

    } else {
        message("; Only one TF in the family, skipping.")
    }

    return(family_summary_entry)
}

##----------------------##
## Creating directories ##
##----------------------##

TMP_REGIONS_DIR <- file.path(MAIN_OUTPUT_DIR, "tmp_regions")

dir.create(TMP_REGIONS_DIR, recursive = TRUE, showWarnings = FALSE)

##------------------------------##
## Reading and processing input ##
##------------------------------##

## Annotation:
anno_summary_tab <- data.table::fread(
    PATH_TO_ANNO,
    header = TRUE,
    sep = "\t")

anno_summary_tab <- anno_summary_tab %>%
    dplyr::select(
        tf_family,
        TF,
        motif_name,
        species_name_short,
        cobinders_cluster_id) %>%
    unique() %>%
    dplyr::mutate(
        species_family_id = paste(species_name_short, tf_family, sep = "-"),
        path_to_regions_with_cobinders = file.path(
            "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/individual",
            paste0(species_name_short, "_0.99"),
            "results", "cobinder_regions", "BED", "full_regions", "with_cobinder",
            paste0("with_cobinder_full_regions_", motif_name, ".bed")),
        path_to_regions_without_cobinders = file.path(
            "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/individual",
            paste0(species_name_short, "_0.99"),
            "results", "cobinder_regions", "BED", "full_regions", "no_cobinder",
            paste0("no_cobinder_full_regions_", motif_name, ".bed")),
        path_to_merged = file.path(
            TMP_REGIONS_DIR,
            species_name_short,
            paste0(species_name_short, "_", motif_name, ".bed"))
    )

## Shared co-binders summary:
shared_cobinders_summary_tab <- data.table::fread(
    PATH_TO_SHARED_COBINDERS_SUMMARY,
    header = TRUE,
    sep = "\t") 

shared_cobinders_summary_tab <- shared_cobinders_summary_tab %>%
    dplyr::select(
        tf_family,
        total_nb_unique_tfs,
        number_of_tfs_sharing_cobinder_in_family,
        number_of_species_representing_sharing_cobinder_in_family)

## Combining two tables:
analysis_tab <- dplyr::left_join(
    anno_summary_tab, shared_cobinders_summary_tab,
    by = c("tf_family" = "tf_family"))

## Getting a list of unique family names:
list_of_species_tf_families <- unique(anno_summary_tab$species_family_id)

## Species-based table:
anno_summary_species_tab <- anno_summary_tab %>%
    dplyr::mutate(species_family_id = species_name_short)
list_of_species_names <- unique(anno_summary_species_tab$species_family_id)

##----------------------##
## Running the analysis ##
##----------------------##

categories_wilcox_test_results_tab <- data.frame()
all_data_combined_tab <- data.frame()

for (analysis_type in c("all", "with", "without")) {

    ########################
    ## 1. For each family ##
    ########################

    ## For each family and species:
    FAMILIES_ANALYSIS_OUT_DIR <- file.path(MAIN_OUTPUT_DIR, analysis_type, "species_families_analysis")
    dir.create(FAMILIES_ANALYSIS_OUT_DIR, recursive = TRUE, showWarnings = FALSE)

    ## Running the analysis:
    summary_after_families_jaccard_analysis <- purrr::pmap(
    list(
        list_of_species_tf_families,
        rep(list(analysis_tab), times = length(list_of_species_tf_families)),
        rep(TMP_REGIONS_DIR, times = length(list_of_species_tf_families)),
        rep(FAMILIES_ANALYSIS_OUT_DIR, times = length(list_of_species_tf_families)),
        rep(analysis_type, times = length(list_of_species_tf_families))
        ),
        compute_jaccard_for_family
    )

    summary_after_families_jaccard_analysis <- do.call("rbind", summary_after_families_jaccard_analysis)

    ## Exporting the summary table:
    data.table::fwrite(
        summary_after_families_jaccard_analysis,
        file = file.path(
            FAMILIES_ANALYSIS_OUT_DIR,
            "after_jaccard_analysis_family_summary.tsv"),
        col.names = TRUE,
        sep = "\t"
    )

    #########################
    ## 2. For each species ##
    #########################

    ## For each species:
    ## For each family and species:
    SPECIES_ANALYSIS_OUT_DIR <- file.path(MAIN_OUTPUT_DIR, analysis_type, "species_analysis")
    dir.create(SPECIES_ANALYSIS_OUT_DIR, recursive = TRUE, showWarnings = FALSE)

    ## Running the analysis:
    summary_after_jaccard_analysis <- purrr::pmap(
        list(
            list_of_species_names,
            rep(list(anno_summary_species_tab), times = length(list_of_species_names)),
            rep(TMP_REGIONS_DIR, times = length(list_of_species_names)),
            rep(SPECIES_ANALYSIS_OUT_DIR, times = length(list_of_species_names)),
            rep(analysis_type, times = length(list_of_species_names))
            ),
        compute_jaccard_for_family
    )

    summary_after_jaccard_analysis <- do.call("rbind", summary_after_jaccard_analysis)

    ## Exporting the summary table:
    data.table::fwrite(
        summary_after_jaccard_analysis,
        file = file.path(
            SPECIES_ANALYSIS_OUT_DIR,
            "after_jaccard_analysis_family_summary.tsv"),
        col.names = TRUE,
        sep = "\t"
    )

    ##-------------------------------------##
    ## Density within families and outside ##
    ##-------------------------------------##

    ## Iterating through the species to make density plots:
    for (species_name in list_of_species_names) {

        message("; Processing for ", species_name)

        ## Reading the Jaccard matrix:
        INTERVENE_OUT_FILE_PATH <- file.path(MAIN_OUTPUT_DIR, analysis_type, "species_analysis", species_name, "Intervene_pairwise_jaccard_matrix.txt")

        if (file.exists(INTERVENE_OUT_FILE_PATH)) {

            jaccard_results <- as.matrix(read.table(INTERVENE_OUT_FILE_PATH))
            jaccard_results <- reshape2:::melt.matrix(jaccard_results, )
            colnames(jaccard_results) <- c("TF1", "TF2", "value")

            ## Filtering out the same TF comparisons:
            jaccard_results <- jaccard_results %>%
                dplyr::mutate(
                    TF1 = as.character(TF1),
                    TF2 = gsub(
                        as.character(TF2),
                        pattern = "\\.",
                        replacement = "-"),
                    value = as.numeric(value)) %>%
                dplyr::filter(TF1 != TF2) %>%
                dplyr::mutate(TF1 = gsub(
                    TF1,
                    pattern = paste0(species_name, "_"),
                    replacement = "")) %>%
                dplyr::mutate(TF2 = gsub(
                    TF2,
                    pattern = paste0(species_name, "_"),
                    replacement = ""))

            ## Adding family annotations:
            anno_to_add <- anno_summary_tab %>%
                dplyr::select(
                    tf_family,
                    motif_name,
                    species_name_short) %>%
                dplyr::filter(species_name_short == species_name) %>%
                unique() %>%
                dplyr::select(tf_family, motif_name)

            jaccard_results <- dplyr::left_join(
                jaccard_results, anno_to_add,
                by = c("TF1" = "motif_name"))
            colnames(jaccard_results) <- c("TF1", "TF2", "value", "family_1")

            jaccard_results <- dplyr::left_join(
                jaccard_results, anno_to_add,
                by = c("TF2" = "motif_name"))
            colnames(jaccard_results) <- c("TF1", "TF2", "value", "family_1", "family_2")

            ## Categorizing:
            jaccard_results <- jaccard_results %>%
                dplyr::mutate(category = ifelse(
                    family_1 == family_2, 
                    yes = "within_family", 
                    no = "across_families"))

            ## Computing significance:
            within_families_values <- jaccard_results %>%
                dplyr::filter(category == "within_family") %>%
                dplyr::pull(value)
            across_families_values <- jaccard_results %>%
                dplyr::filter(category == "across_families") %>%
                dplyr::pull(value)

            across_vs_within_stats_res <- stats::wilcox.test(
                within_families_values,
                across_families_values,
                exact = FALSE,
                # paired = FALSE,
                alternative = "greater")
            formatted_pvalue <- formatC(
                as.numeric(across_vs_within_stats_res$p.value),
                format = "e", 
                digits = 2)

            stats_test_results <- cbind(
                species_name,
                analysis_type,
                as.numeric(across_vs_within_stats_res$p.value))
            
            colnames(stats_test_results) <- c(
                "species_name",
                "analysis_type",
                "p_value")
            stats_test_results <- as.data.frame(stats_test_results) %>%
                dplyr::mutate(p_value_formatted = formatted_pvalue)

            ## Adding to main results:
            all_data_combined_tab <- rbind(
                all_data_combined_tab,
                jaccard_results %>%
                    dplyr::mutate(
                        species_name = stats_test_results$species_name,
                        analysis_type = stats_test_results$analysis_type,
                        p_value = stats_test_results$p_value,
                        p_value_formatted = stats_test_results$p_value_formatted)
                )

            ## Plotting:
            x_limit <- round(max(jaccard_results$value)[1], digits = 1) + 0.1
            jaccard_summary_plot <- ggplot(
                    jaccard_results,
                    aes(
                        x = value,  
                        fill = category)) +
                geom_density(
                    color = "#7a0177",
                    alpha = 0.7) +
                scale_x_continuous(
                    limits = c(0, x_limit)) +
                scale_fill_manual(
                    values = c("#fde0dd", "#dd3497"),
                    labels = c("Across families", "Within families"),
                    name   = "Group") +
                labs(
                    x = "Jaccard statistic",
                    y = "Density") +
                theme(legend.position = "bottom")

            upper_y_lim <- max(ggplot_build(jaccard_summary_plot)$layout$panel_scales_y[[1]]$range$range)

            ## Saving values to add labels later:
            stats_test_results <- stats_test_results %>%
                dplyr::mutate(
                    x_coord_label = (x_limit - 0.1),
                    y_coord_label = (upper_y_lim / 2))
            
            categories_wilcox_test_results_tab <- rbind(categories_wilcox_test_results_tab, stats_test_results)
    
            jaccard_summary_plot <- jaccard_summary_plot +
                geom_text(
                    aes(
                        x = (x_limit - 0.1),
                        y = upper_y_lim / 2),
                    label = paste0(
                        "p-value = ",
                        formatted_pvalue))

            JACCARD_SUMMARY_PLOT_PATH <- file.path(
                MAIN_OUTPUT_DIR, analysis_type, "species_analysis", 
                paste0(toupper(analysis_type), "_", species_name, "_jaccard_density_within_across_families.pdf"))
            ggsave(
                filename  = JACCARD_SUMMARY_PLOT_PATH,
                plot      = jaccard_summary_plot,
                width     = 6,
                height    = 4,
                limitsize = FALSE)

            JACCARD_SUMMARY_PLOT_PATH <- file.path(
                MAIN_OUTPUT_DIR, analysis_type, "species_analysis", 
                paste0(toupper(analysis_type), "_", species_name, "_jaccard_density_within_across_families.png"))
            ggsave(
                filename  = JACCARD_SUMMARY_PLOT_PATH,
                plot      = jaccard_summary_plot,
                width     = 6,
                height    = 4,
                limitsize = FALSE)

        }
    }
}

categories_wilcox_test_results_tab <- categories_wilcox_test_results_tab %>%
    dplyr::mutate(plot_label = paste0("p-value: ", p_value_formatted))

## Processing the test results:
data.table::fwrite(
    categories_wilcox_test_results_tab,
    file = file.path(MAIN_OUTPUT_DIR, "statistics_for_each_species_region_type.tsv"),
    col.names = TRUE,
    sep = "\t")

categories_wilcox_test_results_tab <- categories_wilcox_test_results_tab %>%
    dplyr::mutate(category = "within_family") %>%
    dplyr::filter(as.numeric(p_value) <= 0.05)

## Plotting all results into a facet plot:
all_jaccard_summary_plot <- ggplot(
        as.data.frame(all_data_combined_tab),
        aes(
            x = value,  
            fill = category)) +
    geom_density(
        color = "#7a0177",
        alpha = 0.7) +
    ggplot2::facet_grid(
        species_name ~ analysis_type,
        scales = "free") +
    scale_fill_manual(
        values = c("#fde0dd", "#dd3497"),
        labels = c("Across families", "Within families"),
        name   = "Group") +
    labs(
        x = "Jaccard statistic",
        y = "Density") +
    theme(legend.position = "bottom") +
    geom_text(
        data = categories_wilcox_test_results_tab,
        aes(
            x = Inf,
            y = Inf,
            label = plot_label),
        hjust = 1.1,
        vjust = 5)

JACCARD_SUMMARY_PLOT_PATH <- file.path(
    MAIN_OUTPUT_DIR,
    "collected_jaccard_density_within_across_families.pdf")
ggsave(
    filename  = JACCARD_SUMMARY_PLOT_PATH,
    plot      = all_jaccard_summary_plot,
    width     = 11,
    height    = 14,
    limitsize = FALSE)

JACCARD_SUMMARY_PLOT_PATH <- file.path(
    MAIN_OUTPUT_DIR,
    "collected_jaccard_density_within_across_families.png")
ggsave(
    filename  = JACCARD_SUMMARY_PLOT_PATH,
    plot      = all_jaccard_summary_plot,
    width     = 11,
    height    = 14,
    limitsize = FALSE)

##------------------------------------------------##
## Computing differences between with and without ##
##------------------------------------------------##

with_without_data_for_test <- all_data_combined_tab %>%
    dplyr::filter(analysis_type != "all") %>%
    dplyr::select(value, category, species_name, analysis_type) %>%
    tidyr::pivot_wider(
        names_from = "analysis_type",
    values_from = "value")

# category_name <- with_without_data_for_test$category[1]
# species_id <- with_without_data_for_test$species_name[1]
# values_for_with <- with_without_data_for_test$with[1]
# values_for_without <- with_without_data_for_test$without[1]
with_without_stats_test_results <- purrr::pmap(
    list(
        with_without_data_for_test$category,
        with_without_data_for_test$species_name,
        with_without_data_for_test$with,
        with_without_data_for_test$without
    ),
    function(
        category_name,
        species_id,
        values_for_with,
        values_for_without
    ) {

        test_res <- stats::wilcox.test(
            unlist(values_for_with),
            unlist(values_for_without),
            exact = FALSE,
            alternative = "greater")
        res_to_report <- cbind(
            category_name,
            species_id,
            as.numeric(test_res$p.value),
            formatC(
                as.numeric(test_res$p.value),
                format = "e", 
                digits = 2))
        res_to_report <- as.data.frame(res_to_report)
        colnames(res_to_report) <- c("category", "species_name", "p_value", "p_value_formatted")
        return(res_to_report)
    }
)
with_without_stats_test_results <- do.call("rbind", with_without_stats_test_results)

data.table::fwrite(
    with_without_stats_test_results,
    file = file.path(MAIN_OUTPUT_DIR, "statistics_for_each_species_category_with_vs_without_cobinders.tsv"),
    col.names = TRUE,
    sep = "\t")


##----------##
## Cleaning ##
##----------##

unlink(TMP_REGIONS_DIR, recursive = TRUE)

## End of Script ##
