#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "dplyr",
                        "optparse",
                        "stringr",
                        "tidyr")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only=TRUE, quietly = T))
}

stringsAsFactors = FALSE

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(c("-m", "--motif_file"), type = "character", default = NULL,
              help = "A folder to the file with paths to all dicovered motifs. (Mandatory) ", metavar = "character"),

  make_option(c("-o", "--output_folder"), type = "character", default = NULL,
              help = "Output folder to output a table for clustering. (Mandatory)", metavar = "character"),
  
  make_option(c("-t", "--script_type"), type = "logical", default = FALSE,
              help = "Subset motifs or not. Default: FALSE", metavar = "logical"),
  
  make_option(c("-c", "--cobind_motifs"), type = "character", default = NULL,
              help = "A path to the clustering table of cobind discovered motifs. (Mandatory)", metavar = "character")

  
);
message("; Reading arguments from command line.")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

############
## Syntax ##
############

### Syntax:
## R-scripts/make_peak_motif_clustering_tables.R
##        -m individual_MOUSE_02/results/peak_motifs_no_cobinder_clustering/CTCF/no_cobinder_discovered_motifs.txt
##        -o individual_MOUSE_02/results/peak_motifs_no_cobinder_clustering/CTCF/clustering_tables
##        -t FALSE

########################
## Set variable names ##
########################

discovered.motifs     <- opt$motif_file
output.folder         <- opt$output_folder
script.type           <- opt$script_type
cobind.motifs         <- opt$cobind_motifs

# ## Debug
# setwd("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/scratch/ieva/COBIND_results/")
# discovered.motifs <- "individual_HUMAN_HEK293_01/results/peak_motifs_all_datasets_clustering/PRDM4/all_discovered_motifs.txt"
# output.folder  <- "individual_HUMAN_HEK293_01/results/peak_motifs_all_datasets_clustering/PRDM4/clustering_tables_with_cobind_motifs"
# cobind.motifs <- "individual_HUMAN_HEK293_01/results/tf_families_tables/PRDM4_cobinder.tab"
# script.type <- TRUE

###############################
## Creating output directory ##
###############################
message("; Creating output directory.")

## Main output folder:
dir.create(output.folder, showWarnings = F, recursive = T)

###############################
## Reading cobind motifs tab ##
###############################
cobind_clustering_table <- fread(cobind.motifs, header = FALSE)
colnames(cobind_clustering_table) <- c("path", "name", "type")

###############################
## Creating clustering table ##
###############################

discovered_motifs <- fread(discovered.motifs, header = FALSE)

clustering_table <- discovered_motifs %>%
  mutate(dataset = basename(V1)) %>%
  mutate(dataset = gsub(dataset, pattern = "_motifs_discovered.tf", replacement = "")) %>%
  mutate(type = "transfac")

colnames(clustering_table) <- c("path", "name", "type")

if (script.type == TRUE) {
  
  nb_files <- nrow(clustering_table)
  nb_iterations <- ceiling(nb_files/10)
  idx_start <- 0
  idx_end <- 10
  
  for (i in 1:nb_iterations) {
    
    message("; Subset number: ", i)
    message("; Motif file from ", idx_start, " to ", idx_end)
    
    clustering_table_subset <- clustering_table[idx_start:idx_end, ]
    clustering_table_subset <- rbind(clustering_table_subset, cobind_clustering_table)
    clustering_table_subset <- na.omit(clustering_table_subset)
    
    path.to.tab <- file.path(output.folder, paste0("all_discovered_motifs_", i, ".txt"))
    fwrite(clustering_table_subset,
           file = path.to.tab,
           col.names = FALSE,
           sep = "\t")
    
    idx_start <- idx_start + 10
    idx_end <- ifelse(i == nb_iterations - 1, nb_files, idx_end + 10)
    
  }
  
} else {
  
  clustering_table <- rbind(clustering_table, cobind_clustering_table)
  path.to.tab <- file.path(output.folder, "discovered_motifs_clustering.tab")
  fwrite(clustering_table,
         file = path.to.tab,
         col.names = FALSE,
         sep = "\t")
  
}


# print(numbers_of_motifs)



#######################
## End of the script ##
#######################