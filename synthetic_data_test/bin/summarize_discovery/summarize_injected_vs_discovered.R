#####################
## Load R packages ##
#####################

required.libraries <- c(
       "data.table",
       "optparse",
       "tidyverse",
       "ggh4x")

for (lib in required.libraries) {
       suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

#############
## Options ##
#############

options(stringsAsFactors = FALSE)
options(scipen = 999)

####################
## Read arguments ##
####################

option_list = list(

       make_option(
              c("-t", "--tomtom_table"), 
              type    = "character", 
              default = NULL,
              help    = "A table with motif comparisons. (Mandatory) ", 
              metavar = "character"),
       make_option(
              c("-o", "--output_folder"), 
              type    = "character", 
              default = NULL,
              help    = "Path to output folder. (Mandatory) ", 
              metavar = "character"),
       make_option(
              c("-s", "--range_number_of_sequences"), 
              type    = "character", 
              default = "800,1000,4000,8000,10000,40000,80000",
              help    = "Comma-separated list of number of sequences. Default: 800,1000,4000,8000,10000,40000,80000 ", 
              metavar = "character"),
       make_option(
              c("-p", "--range_of_proportions"), 
              type    = "character", 
              default = "0.005,0.01,0.03,0.05,0.1",
              help    = "Comma-separated list of proportions. Default: 0.005,0.01,0.03,0.05,0.1 ", 
              metavar = "character"),
       make_option(
              c("-m", "--list_of_methods"), 
              type    = "character", 
              default = "COBIND,MCOT,RSAT-dyads,RSAT-oligos,RSAT-positions,SPAMO,STREME",
              help    = "Comma-separated list of applied methods. Default: COBIND,MCOT,RSAT-dyads,RSAT-oligos,RSAT-positions,SPAMO,STREME ", 
              metavar = "character")
);

message("; Reading arguments from command line.")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

########################
## Set variable names ##
########################

TOMTOM_FILE   <- opt$tomtom_table
OUTPUT_DIR    <- opt$output_folder
SEQ_RANGE     <- opt$range_number_of_sequences
PROP_RANGE    <- opt$range_of_proportions
METHODS_RANGE <- opt$list_of_methods

## Debug:
# TOMTOM_FILE <- "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/CTCF-MA0139.1/comparison_with_others/injected_vs_all_discovered.txt"
# OUTPUT_DIR  <- "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/CTCF-MA0139.1/comparison_with_others"
# SEQ_RANGE <- "800,1000,4000,8000,10000,40000,80000"
# PROP_RANGE <- "0.005,0.01,0.03,0.05,0.1"
# METHODS_RANGE <- "COBIND,MCOT,RSAT-dyads,RSAT-oligos,RSAT-positions,SPAMO,STREME"

###############################
## Creating output directory ##
###############################

dir.create(OUTPUT_DIR, recursive = TRUE, showWarnings = FALSE)

##########################
## Reading TOMTOM table ##
##########################

message("; Reading comparisons between injected and discovered motifs.")

tomtom_tab <- data.table::fread(
       TOMTOM_FILE,
       header = TRUE,
       sep = "\t",
       fill = TRUE)

## Removing empty lines from the end of the table:
tomtom_tab <- tomtom_tab %>%
       dplyr::filter(Target_ID != "")

## Processing the table:
colnames(tomtom_tab) <- c(
       "query", "motif_name", "offset",
       "p_value", "e_value", "q_value",
       "overlap", "query_consensus", "target_consensus",
       "orientation")

tomtom_tab <- tomtom_tab %>%
       dplyr::mutate(name = motif_name) %>%
       tidyr::separate(
              name,
              c("type", "experiment_id", "motif", "nb_sites", "other", "stats"),
              sep = "_") %>%
       dplyr::mutate(experiment = experiment_id) %>%
       tidyr::separate(experiment,
              c("nb_seqs", "proportion", "flank_size"),
              sep = "-") %>%
       tidyr::separate(
              stats,
              c("TP", "FN", "FP", "TN"),
              sep = "-") %>%
       dplyr::mutate(
              TP = as.numeric(gsub(TP, pattern = "TP:", replacement = "")),
              FN = as.numeric(gsub(FN, pattern = "FN:", replacement = "")),
              FP = as.numeric(gsub(FP, pattern = "FP:", replacement = "")),
              TN = as.numeric(gsub(TN, pattern = "TN:", replacement = ""))) %>%
       mutate(real_nb_sites = as.numeric(nb_seqs) * as.numeric(proportion)) %>%
       mutate(match_with_injected = ifelse(p_value < 0.05, yes = "match", no = "no-match")) %>%
       group_by(type, experiment_id) %>%
       mutate(nb_motifs_per_experiment = length(motif_name)) %>%
       ungroup() %>%
       group_by(experiment_id, type, match_with_injected) %>%
       mutate(nb_of_match_no_match_per_experiment = length(motif_name)) %>%
       mutate(nb_match_no_match_proportion = as.numeric(nb_of_match_no_match_per_experiment / nb_motifs_per_experiment)) %>%
       ungroup() 

###################################################
## Keeping only the methods in the provided list ##
###################################################

methods_range <- unlist(strsplit(METHODS_RANGE, split = ","))

tomtom_tab <- tomtom_tab[tomtom_tab$type %in% methods_range, ]

##########################
## Computing statistics ##
##########################

tomtom_tab <- tomtom_tab %>%
       dplyr::mutate(
              sensitivity = TP / (TP + FN),
              precision   = TP / (TP + FP),
              specificity = TN / (TN + FP),
              MCC         = (TP * TN - FP * FN) / sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN)),
              F1          = 2 * (precision * sensitivity) / (precision + sensitivity))

## Selecting one motif per method, experiment (relevant only for matches):
tomtom_tab <- tomtom_tab %>%
       dplyr::arrange(
              type, 
              experiment_id,
              match_with_injected) %>%
       dplyr::group_by(
              type, 
              experiment_id,
              match_with_injected) %>%
       dplyr::top_n(1, wt = F1) %>%
       dplyr::top_n(1, wt = MCC) %>%
       dplyr::sample_n(1) %>%
       dplyr::mutate(nb_of_motifs_after_selection = length(experiment_id)) %>%
       ungroup()

duplicate_motifs <- tomtom_tab %>%
       filter(nb_of_motifs_after_selection != 1)
if (nrow(duplicate_motifs) > 0) {
       message("; There are duplicated motifs for a single experiment! Revise!")
}

#########################
## Injected motif name ##
#########################

message("; Finding injected motifs name.")

injected_motif_name <- tomtom_tab$query[1]
injected_motif_name <- gsub(injected_motif_name, pattern = "injected_", replacement = "")

#################################
## Exporting the summary table ##
#################################

message("; Exporting summary table.")

SUMMARY_TAB_PATH <- file.path(OUTPUT_DIR, paste0("cobind_vs_others_stats.tsv"))
fwrite(tomtom_tab,
       file = SUMMARY_TAB_PATH,
       sep = "\t",
       col.names = TRUE)

############################
## Setting plotting theme ##
############################

message("; Setting ggplot theme.")

ggplot2::theme_set(
       theme_classic() +
       theme(
              plot.title = element_text(size = 16),
              axis.title = element_text(
                     size = 16, 
                     colour = "black"),
              axis.text = element_text(
                     size = 16, 
                     colour = "black"),
              plot.background = element_blank(),
              axis.text.x = element_text(
                     angle = 90,
                     hjust = 1,
                     vjust = 0.5),
              legend.position = "left",
              legend.box = "vertical",
              legend.direction = "vertical",
              legend.text = element_text(size = 14),
              legend.title = element_text(size = 14),
              legend.justification = "left",
              panel.border = element_rect(
                     colour = "black", 
                     fill = NA),
              plot.caption = element_text(
                     size = 12,
                     hjust = 0))
)

###############
## Functions ##
###############
       # dataframe_to_plot <- results_for_plot
       # max_nb_of_non_matching_motifs <- max_nb_of_non_matching_motifs
       # seq_range <- paste0(sort(all_seq_range), " sequences")
       # prop_range <- prop_range
       # methods_range <- methods_range
plot_tool_comparison <- function(
       dataframe_to_plot,
       max_nb_of_non_matching_motifs,
       seq_range,
       prop_range,
       methods_range) {

       comparison_plot <- ggplot(
                     dataframe_to_plot,
                     aes(
                            x = as.factor(variable),
                            y = as.factor(proportion),
                            colour = as.numeric(colour_value),
                            shape = as.factor(shape_data),
                            size = as.numeric(nb_no_matches_variable))) +
              geom_point() +
              ggh4x::facet_nested_wrap(
                     vars(
                            factor(nb_seqs_label, levels = seq_range),
                            factor(type, levels = methods_range)),
                     ncol = length(methods_range) * 2,
                     nest_line = element_line(linetype = 1),
                     resect = unit(0.1, "mm"),
                     strip = ggh4x::strip_nested(
                            text_x = elem_list_text(
                                   size = c(13, rep(NA, 100)),
                                   colour = "black"),
                            clip = "off",
                            by_layer_x = TRUE,
                            background_x = element_rect(
                                   colour = "#ffffff00",
                                   fill = "#dfdede3d")),
                     drop = FALSE) +
              scale_colour_gradient2(
                     high = "#b2182b",
                     mid = "#d1e5f0",
                     low = "#053061",
                     na.value = "#464444",
                     limits = c(unique(ifelse(any(dataframe_to_plot$colour_value[!is.na(dataframe_to_plot$colour_value)] < 0), yes = -1, no = 0), 0), 1)) +
              scale_shape_manual(
                     values = c(
                            "Number of motifs" = 15,
                            "MCC/F1" = 19),
                     breaks = c("MCC/F1"),
                     labels = c(""),
                     name = "MCC/F1\nmetric") +
              scale_size_continuous(
                     limits = c(1, max_nb_of_non_matching_motifs),
                     breaks = c(1, (round(median(1:max_nb_of_non_matching_motifs), digits = 0)), max_nb_of_non_matching_motifs)) +
              scale_x_discrete(
                     limits = c("Incorrect", "F1", "MCC"),
                     labels = scales::wrap_format(9)) +
              scale_y_discrete(limits = as.factor(prop_range)) +
              labs(
                     title = paste0(" Injected motif: ", injected_motif_name),
                     y = "Proportion of sequences with co-binding motif",
                     x = "",
                     colour = "MCC/\nF1 score",
                     size = "Number of\nincorrect motifs") +
              guides(
                     shape = guide_legend(
                            override.aes = list(
                                   size = 8),
                            direction = "horizontal",
                            title.position = "top",
                            label.position = "bottom",
                            label.hjust = 1,
                            label.vjust = 0.5,
                            order = 1),
                     size  = guide_legend(
                            override.aes = list(shape = 15),
                            order = 2)) +
              theme(
                     strip.background = element_blank(),
                     legend.title.align = 0.5,
                     ggh4x.facet.nestline = element_line(colour = "black"),
                     legend.position = c(0.95, 0),
                     legend.justification = c("right", "bottom"),
                     legend.box = "horizontal")

       return(comparison_plot)
       }

#################################
## Preparing data for plotting ##
#################################

message("; Preparing data for plotting.")
max_nb_of_non_matching_motifs <- tomtom_tab %>%
       dplyr::group_by(match_with_injected, nb_seqs, proportion, type) %>%
       dplyr::mutate(max_nb_of_match_no_match = max(nb_of_match_no_match_per_experiment)) %>%
       ungroup() %>%
       dplyr::filter(match_with_injected == "no-match") %>%
       dplyr::select(max_nb_of_match_no_match)
max_nb_of_non_matching_motifs <- max(max_nb_of_non_matching_motifs$max_nb_of_match_no_match)

results_for_plot <- tomtom_tab %>%
       dplyr::mutate(
              F1_value  = ifelse(match_with_injected == "match", yes = F1, no = NA),
              MCC_value = ifelse(match_with_injected == "match", yes = MCC, no = NA),
              nb_no_matches_value = NA) %>%
       dplyr::mutate(nb_no_matches_variable = ifelse(match_with_injected == "no-match", yes = nb_of_match_no_match_per_experiment, no = max_nb_of_non_matching_motifs)) %>%
       tidyr::pivot_longer(
              cols = c(F1_value, MCC_value, nb_no_matches_value),
              values_to = "colour_value",
              names_to = "variable") %>%
       dplyr::mutate(
              colour_value = as.numeric(colour_value),
              nb_no_matches_variable = as.numeric(nb_no_matches_variable)) %>%
       dplyr::mutate(
              shape_data = ifelse(variable == "nb_no_matches_value", yes = "Number of motifs", no = "MCC/F1")) %>%
       dplyr::filter(variable != "nb_no_matches_value" | match_with_injected != "match") %>%
       dplyr::filter(match_with_injected != "no-match" | variable != "F1_value") %>%
       dplyr::filter(match_with_injected != "no-match" | variable != "MCC_value")

## Variable aestetics, so they are nice in the plot:
results_for_plot <- results_for_plot %>%
       dplyr::mutate(variable = gsub(variable, pattern = "_value", replacement = "")) %>%
       dplyr::mutate(variable = gsub(variable, pattern = "nb_no_matches", replacement = "Incorrect")) %>%
       dplyr::mutate(nb_seqs_label = paste0(nb_seqs, " sequences")) %>%
       dplyr::mutate(nb_seqs = as.numeric(nb_seqs))

##############
## Plotting ##
##############

message("; Plotting comparison results.")

## Params for the plot:
prop_range <- as.numeric(unlist(strsplit(PROP_RANGE, split = ",")))

## Calculating the split:
all_seq_range <- as.numeric(unlist(strsplit(SEQ_RANGE, split = ",")))
median_nb_seq <- median(all_seq_range)

# ## Two parts data:
# first_part_plot_df <- subset(results_for_plot, as.numeric(nb_seqs) <= median_nb_seq)
# first_part_seq_range   <- paste0(sort(unique(all_seq_range[all_seq_range <= median_nb_seq])), " sequences")

# second_part_plot_df <- subset(results_for_plot, as.numeric(nb_seqs) > median_nb_seq)
# second_part_seq_range   <- paste0(sort(unique(all_seq_range[all_seq_range > median_nb_seq])), " sequences")

# ##### First part plot #####
# cobind_vs_others_stats_plot_part1 <- plot_tool_comparison(
#        first_part_plot_df,
#        max_nb_of_non_matching_motifs,
#        first_part_seq_range,
#        prop_range,
#        methods_range)

# nb_of_facets <- length(first_part_seq_range) * length(methods_range)

# plot_file_path <- file.path(OUTPUT_DIR, paste0("injected_vs_all_discovered_PART1.pdf"))
# ggsave(filename = plot_file_path,
#        plot = cobind_vs_others_stats_plot_part1,
#        width = (nb_of_facets + 2) * 1.35,
#        height = 9,
#        limitsize = FALSE)
# plot_file_path <- file.path(OUTPUT_DIR, paste0("injected_vs_all_discovered_PART1.png"))
# ggsave(filename = plot_file_path,
#        plot = cobind_vs_others_stats_plot_part1,
#        width = (nb_of_facets + 2) * 1.35,
#        height = 9,
#        limitsize = FALSE)

# ##### Second part plot #####
# cobind_vs_others_stats_plot_part2 <- plot_tool_comparison(
#        second_part_plot_df,
#        max_nb_of_non_matching_motifs,
#        second_part_seq_range,
#        prop_range,
#        methods_range)

# nb_of_facets <- length(second_part_seq_range) * length(methods_range)

# plot_file_path <- file.path(OUTPUT_DIR, paste0("injected_vs_all_discovered_PART2.pdf"))
# ggsave(filename = plot_file_path,
#        plot = cobind_vs_others_stats_plot_part2,
#        width = (nb_of_facets + 2) * 1.35,
#        height = 9,
#        limitsize = FALSE)
# plot_file_path <- file.path(OUTPUT_DIR, paste0("injected_vs_all_discovered_PART2.png"))
# ggsave(filename = plot_file_path,
#        plot = cobind_vs_others_stats_plot_part2,
#        width = (nb_of_facets + 2) * 1.35,
#        height = 9,
#        limitsize = FALSE)

##### Full plot #####
cobind_vs_others_stats_plot <- plot_tool_comparison(
       results_for_plot,
       max_nb_of_non_matching_motifs,
       paste0(sort(all_seq_range), " sequences"),
       prop_range,
       methods_range)

plot_file_path <- file.path(OUTPUT_DIR, paste0("injected_vs_all_discovered.pdf"))
ggsave(filename = plot_file_path,
       plot = cobind_vs_others_stats_plot,
       width = length(methods_range) * 2.1,
       height = length(all_seq_range) * 2,
       limitsize = FALSE)
plot_file_path <- file.path(OUTPUT_DIR, paste0("injected_vs_all_discovered.png"))
ggsave(filename = plot_file_path,
       plot = cobind_vs_others_stats_plot,
       width = length(methods_range) * 2.1,
       height = length(all_seq_range) * 2,
       device = 'png', dpi = 1000,
       type = "cairo",
       bg = "white",
       limitsize = FALSE)
# plot_file_path <- file.path(OUTPUT_DIR, paste0("injected_vs_all_discovered.svg"))
# ggsave(filename = plot_file_path,
#        plot = cobind_vs_others_stats_plot,
#        width = length(methods_range) * 2.1,
#        height = length(all_seq_range) * 2,
#        device = 'svg', dpi = 1000,
#        limitsize = FALSE)

#######################
## End of the script ##
#######################
