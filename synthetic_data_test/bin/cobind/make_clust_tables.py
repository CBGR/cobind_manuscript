import os
import sys
import glob

### Prepares the clustering tables with found motif information.
## A table for core and cobinder each and for each TF family/class.
## A table is an input for the clustering program (RSAT) to cluster motifs according to the provided TF family/TF class.

## Syntax : python make_clust_tables <tf_families> <motif_folder> <output_folder>

## In <output_folder> for each familiy, if motifs were found, a table is created for the clustering of the core motifs and one for the cobinder motifs

def clust_tables(tf_families,motif_folder,output_folder):
	with open(tf_families) as families_file:
		families=families_file.readlines()

	if not os.path.exists(output_folder):
		os.makedirs(output_folder)

	with open('{}/tf_families_tables_log.txt'.format(output_folder),'w+') as log: # The summary of TF families/classes. First column is the family/class, second - the number of motifs found in that family/class.
		for i in range(len(families)):
			name=families[i].rstrip().split("	")[0].replace(' ','_')
			list=families[i].rstrip().split("	")[1].split(',')

			count=0
			for tf in list: # Counting the number of motifs for a TF family/class.
				count += len(glob.glob("{}/{}_*/*_components/cobinder_pfm_*.txt".format(motif_folder,tf)))

			if count>=1: # If there is at least 1 motif, the clustering tables for cobinders and cores are prepared and outputed.
				with open('{}/{}_cobinder.tab'.format(output_folder,name),'w+') as table:
					for tf in list:
						for motif_path in glob.glob("{}/{}_*/*_components/cobinder_pfm_*.txt".format(motif_folder,tf)):
							table.write("{}\t{}\tcluster-buster\n".format(motif_path,'_'.join(motif_path.split('/')[-3:]).replace('.txt','')))

				with open('{}/{}_core.tab'.format(output_folder,name),'w+') as table:
					for tf in list:
						for motif_path in glob.glob("{}/{}_*/*_components/core_pfm_*.txt".format(motif_folder,tf)):
							table.write("{}\t{}\tcluster-buster\n".format(motif_path,'_'.join(motif_path.split('/')[-3:]).replace('.txt','')))

				log.write('{}	{}\n'.format(name,count))

if __name__=='__main__':
	arguments=sys.argv

	tf_families=arguments[1]
	motif_folder=arguments[2]
	output_folder=arguments[3]

	clust_tables(tf_families,motif_folder,output_folder)
