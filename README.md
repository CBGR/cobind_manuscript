<!-- ![COBIND logo](docs/repo_documentation/COBIND_logo.png){height = 50 width = 100} -->

# COBIND - a pipeline to discover co-binding motifs *de novo* #

2024 August 2

## About ##

**COBIND** pipeline identifies the co-binder/co-factor DNA binding pattersn. The method uses coordinates of transcription factor binding sites (TFBSs) as an input and returns information about co-binders and their binding fashion — spacings between the two binding sites.

This repository and its contents were used to produce the results for the COBIND manuscript. The results are available to view as HTML reports [**here**](https://cbgr.bitbucket.io/COBIND_results_page/). We made available COBIND as a tool, which can be found [**here**](https://bitbucket.org/CBGR/cobind_tool/src/master/).

![COBIND overview](docs/repo_documentation/main_method_scheme.png){width = 300 height = 200}

**Figure 1.** Overview of the COBIND pipeline.

The main method recruited in this pipeline is **non-negative matrix factorization (NMF)**. The algorithm takes one-hot encoded genomic sequences of the same length and factorizes them into two types of matrices - *amplitude* and *pattern*. Depending on a number of components that was given as a hyperparamater, we can build sequence motifs out of the two resulting matrices.

![NMF for motif discovery](docs/repo_documentation/nmf_explained.svg){width = 300 height = 200}

**Figure 2.** Non-negative matrix factorization (NMF) application on genomic sequences to discover transcription factor binding motifs.

## Running COBIND on UniBind 2021 data ##

In the paper we present results of analysis of UniBind 2021 data. These analysis can be reproduced following instructions and commands stored as `.md` files in `run_analysis` directory.

Some of the required data is already available in this repository, but pre-processed TFBS data from UniBind, motif collections and PPIs are all available in ZENODO repository (doi: 10.5281/zenodo.7681483) and must be downloaded. Place downloaded data in this repository's `data` directory.

The required dependencies are described bellow. Note that we recommend using Python 3.6 Ghostscript 9.25 versions.

### Analysis in different parts ###

0. `run_analysis/data_preparation.md` - We provide the instructions of the data processing and how to get the data that is not included in this repository or in ZENODO.
1. `run_analysis/flank_length.md` - Running COBIND on mouse and human SOX and GATA TFBSs data using different flank lenghts to assess how the discovery changes with increased size of the flanking regions.
2. `run_analysis/synthetic_data_test.md` - Running COBIND on synthetic data to assess the impact of number of components for NMF motif discovery and to compare COBIND with other tools.
3. `run_analysis/random_analysis.md` - Running COBIND on randomized and real UniBind 2021 data without motif filtering to determine species-specific Gini thresholds.
4. `run_analysis/unibind_analysis.md` - *Running COBIND on UniBind 2021 data to discover co-binding DNA patterns*. This is the main analysis. Species-specific and interspecies summaries for individual TFs and TF families.
5. `run_analysis/run_dhs_analysis.md` - Discovered co-binder analysis within DNase I hypersensitivity footprints (matching human cell types).
6. `run_analysis/run_smf_analysis.md` - Discovered co-binder analysis within single-molecule footprinting (SMF) data (mouse embryonic stem cells).

You can find more details about how data was prepared and how these analysis are set up bellow.

## Installation ##

Make sure you have installed:

* Python (version = 3.6.2);
* R (version = 3.6.1);
* Ghostscript (version = 9.25);
* Snakemake (version >= 5.26.1) (for installation follow the instructions [*here*](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html));
* Bedtools (version >= 2.29.2) (for installation follow the instructions [*here*](https://bedtools.readthedocs.io/en/latest/content/installation.html)).

### Download/Clone the repository ###

Clone the git repo from BitBucket:

```bash
git clone https://ievara@bitbucket.org/CBGR/cobind_manuscript.git

cd cobind_manuscript
```

### Install libraries and packages required by R ##

* libcurl
* openssl
* libxml2
* pandoc

#### 1. Install **libcurl** library (required by R [*cowplot*](https://cran.r-project.org/web/packages/cowplot/vignettes/introduction.html)) ####

```bash
## Debian, Ubuntu
libcurl4-openssl-dev

## Fedora, CentOS, RHEL
libcurl-devel
```

#### 2. Install **openssl** library (required by R [*cowplot*](https://cran.r-project.org/web/packages/cowplot/vignettes/introduction.html)) ####

```bash
## Debian, Ubuntu
libssl-dev

## Fedora, CentOS, RHEL
openssl-devel

## MAC OSX
openssl@1.1
```

#### 3. Install **libxml2** library ####

```bash
## Debian, Ubuntu
libxml2-dev

## Fedora, CentOS, RHEL
libxml2-devel
```

#### 4. Install **pandoc** ####

Follow the instructions [here](https://pandoc.org/installing.html).

#### 5. Install the required **R packages** (internal to R) ####

```bash
Rscript R-scripts/install_R_packages_cobind.R
```

### Installation of CRAN and **Bioconductor** packages ###

The list of all R packages required is listed within the *R-scripts/install_R_packages_cobind.R*.

### Install packages required by **Python** ###

* biopython (version >= 1.78);
* scipy (version >= 1.5.2);
* seqlogo (version >= 5.29.8);
* scikit-learn (version >= 0.23.2).

#### 1. Install **Biopython** ####

```bash
pip install biopython
```

#### 2. Install **scipy** ####

```bash
## Debian, Ubuntu
sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose

## Fedora, CentOS, RHEL
sudo dnf install numpy scipy python-matplotlib ipython python-pandas sympy python-nose atlas-devel

## MAC OSX
sudo port install py35-numpy py35-scipy py35-matplotlib py35-ipython +notebook py35-pandas py35-sympy py35-nose

# or alternatively
brew install numpy scipy ipython jupyter
```

#### 3. Install **seqlogo** ####

```bash
pip install seqlogo
```

#### 4. Install **scikit-learn** ####

```bash
pip install scikit-learn
```

### Install RSAT - Regulatory Sequence Analysis tools ###

RSAT is required to perform motif clustering step in the pipeline.

To install follow the instructions [here](http://pedagogix-tagc.univ-mrs.fr/rsat/). Start by requesting the [download](http://pedagogix-tagc.univ-mrs.fr/rsat/download-request_form.cgi).

As an alternative, you can download RSAT as a repository and install it by following instructions [here](https://rsa-tools.github.io/installing-RSAT/unix-install-rsat/installing_RSAT_procedure.html).

## Input files ##

**COBIND** requires the following files as input input: (1) a collection of UniBind TFBSs in BED format, (2) human genome in FASTA format, (3) human genome chromosomes size file and (4) a table with information about TF families or TF classes or individual TFs.

  1. TFBSs in BED format *(mandatory)*;
  2. Genome in FASTA format *(mandatory)*;
  3. Chromosomes sizes file *(mandatory)*;
  4. A .txt file with input BED file groups (for example, files grouped by a TF family) *(mandatory)*.

**NOTE!** Make sure that TFBSs, human genome and chrom sizes files were generated using the same human genome assembly version (e.g. hg38).

### TFBSs in BED format **(mandatory input)** ###

Information with transcription factor binding sites (TFBSs) information for associated TFs in different conditions in BED-like format. Input file names must have a ".bed" extension:

```bash
{file_name}.bed
```

It is important to synchronize the file names with your input family/class table (more info on this bellow).

**NOTE!** NO dots " . " (except in the extension ".bed") are tolerated in the file name.

**NOTE!** File name length should be limited to and not exceed 65 characters (without extension).

**NOTE!** Transcription factor (TF) motif name here should indicate what motif is found in the regions in the bed file. Between different files with the same TF motif name, the length of the regions must be the same. TF motif name differentiated by underscores is mandatory in the file name and must match the names in the TF family/classes table described below.

If any of these problems will be found, the pipeline will produce errors and indicate, which files needs to be revisited before launching the pipeline again.

A. Required columns (columns should NOT be named):

* 1st - Chromosome
* 2nd - Start
* 3rd - End
  
B. Optional columns:

* 4th - TF_motif
* 5th - Motif score
* 6th - Strand

#### Examples of input region files ####

```bash
## 6-column BED file:
chr1 960585 960596 FOXP2_GAGTGAGCGAC 89    +
chr1 1064322 1064333 FOXP2_AGCGCTGACGC 92      -
chr1 1116374 1116385 FOXP2_GGCGTTGCCGG 91.3 -
chr1 1232053 1232064 FOXP2_GCGGAAGCGGT 90.4 +
chr1 1353914 1353925 FOXP2_GTTGTTTTTGT 92      -
```

### A .txt file with input BED file groups **(Mandatory input)** ###

Information about what TFs are included in each of the TF family/class. The file should contain two tab-separated columns: one representing TF family/class or another preferred file grouping, while the second column should list the original file names without the extension. **NOTE!** This is why it is important that you synchronize the file names and this .txt table. File names must be separated by comma.

#### Example of a grouping file ####

Example table:

```bash
ATF-4-related factors	HS_D487_C241_J197_T6_ATF4,HS_D1092_C173_J197_T6_ATF4,HS_D1093_C173_J197_T6_ATF4,HS_D1094_C173_J197_T6_ATF4
B-ATF-related factors	HS_D1523_C148_J199_T9_BATF3
```

Corresponding files, for this example table would be:

```bash
HS_D487_C241_J197_T6_ATF4.bed
HS_D1092_C173_J197_T6_ATF4.bed
HS_D1093_C173_J197_T6_ATF4.bed
HS_D1094_C173_J197_T6_ATF4.bed
HS_D1523_C148_J199_T9_BATF3.bed
...
```

## Output ##

The pipeline outputs many files, but the most informative summaries of results can be found in the following files.

### Spacing summary heatmap & boxplot ###

#### A. Spacing heatmap plot will be found in a path like this (example output) ####

```bash
docs/manuscript/example_output/results/clustering_results/CTCF/spacings/TF_cobinder_spacer_plot_CTCF.pdf
```

In the spacing summary heatmap in the middle of the plots anchor motifs are represented, while on the right there is a co-binder candidate motif. The squares left and/or right indicate, where the co-binder motif was found relative to the displayed anchor motif (upstream or downstream) and the number of spaces in between the two motifs. The intensity of color reflects the number of datasets this specific combination of anchor and co-binder occurs (including the position and spacer). The proportion inside each square represent the proportion of sequences the co-binder was found in the number of those datasets.

![Spacing plot](docs/manuscript/example_output/results/clustering_results/CTCF/spacings/TF_cobinder_spacer_plot_CTCF.png){width = 150 height = 100}

**Figure 3.** Example of a spacing heatmap plot.

### Experiment summaries ###

#### A. Numbers of candidate co-binder motifs and the fractions of sequences are summarized in the density plot and histogram for all datasets (example output) ####

```bash
docs/manuscript/example_output/results/analysis_overall_summary/aggregated_density_plot_cobinder_specific.pdf

docs/manuscript/example_output/results/analysis_overall_summary/aggregated_histogram_plot_cobinder_specific.pdf
```

#### B. For individual TFs/families/classes separate histograms are also exported as a facet plot (example output) ####

```bash
docs/manuscript/example_output/results/analysis_overall_summary/histogram_plot_cobinder_specific.pdf
```

#### C. Finally, the table with discovered candidate co-binders, the numbers of datasets and the sequence proportion is exported here (example output) ####

```bash
docs/manuscript/example_output/results/analysis_overall_summary/cobinder_distribution_summary_table.txt
```

**NOTE!** For each dataset, disregarding whether any co-binders were found, NMF summary will be exported. Note that, if values for Gini coefficient, median, etc are "-2", it means that after filtering, the cluster, which leads to formation of a motif, did not contain 42 or more sequences, so there were not enough sequences to form a good candidate motif.

The path to such summaries is:

```bash
docs/manuscript/example_output/results/datasets/example_CTCF/nmf_scores/example_CTCF_left_f30_c3.txt
```

## Quick start ##

We provide a collection of configuration files in a folder ```config_files```. These files are experiment and species specific. Analysis with a specific configuration file can be run by specifying the analysis ID.

```bash
## Running an analysis for human data (assuming you are in the cobind_manuscript directory):
snakemake --cores 1 --config analysis_id=human_hg38
```

Options for analysis_id and their corresponding configuration files:

```bash
analysis_id=human_hg38         : config_files/config_human_hg38.yaml
analysis_id=mouse_mm10         : config_files/config_mouse_mm10.yaml
analysis_id=rat_Rnor_6.0       : config_files/config_rat_Rnor_6.0.yaml
analysis_id=arabidopsis_TAIR10 : config_files/config_arabidopsis_TAIR10.yaml
analysis_id=celegans_WBcel235  : config_files/config_celegans_WBcel235.yaml
analysis_id=drerio_GRCz11      : config_files/config_drerio_GRCz11.yaml
analysis_id=drosophila_dm6     : config_files/config_drosophila_dm6.yaml
```

**NOTE!** The user must provide the human genome files (*chrom_sizes* and *genome.fa*). Make sure the assemblies are matching your input data.

Check the [*Snakemake* documentation](https://snakemake.readthedocs.io/en/stable/index.html) for any question related to **Snakemake**.

## Running **INTERSPECIES** pipeline to summarize discovered motifs in different runs/species ##

This pipeline serves as a tool to summarize the discovered motifs over different species. For example, we have a common TF on mouse and human and discovered a number of co-binder in datasets in both species. We can of course look into them separately and visually decide how similar these co-binder motifs are, but running this pipeline is creating the spacings heatmaps over all co-binders.

The first part of the pipeline will sumarize the discovered motifs across the species by indicating how many datasets of all input ones has a co-binder discovered. In the second part, the pipeline will cluster the discovered motifs together, compute the spacers and do the plotting. Finally, in the third part of the pipeline the resulted co-binders will be clustered again to create familial binding profiles over all the data.

This pipeline can be set-up using its own configuration file ```config_files/config_interspecies.yaml```, where you will have to indicate the analysis titles of the main runs that should be summarized together with the metadata of the original input data (see the *Supplementary scripts* for more information of how the metadata was generated).

To run the pipeline (assuming you are in ```cobind_manuscript``` directory):
```bash
snakemake -s Snakefile_interspecies --cores 1
```

## Running the **RANDOM** pipeline to determine Gini coeficient thresholds ##

This procedure contains of two steps:

1. getting co-binder motifs from real and randomized sequences;
2. determining the Gini threshold to select the best candidate co-binders.

This repository contains a separate snakefile that takes BED files with transcription factor binding sites (TFBSs), also randomizes those sequences and runs non-negative matrix factorization algorithm on both groups. Randomization is done using Biasaway tool (<https://biasaway.uio.no/>). The input of this pipeline is the same as in the main COBIND pipeline: (1) a collection of UniBind TFBSs in BED format, (2) human genome in FASTA format, (3) human genome chromosomes size file and (4) a table with information about TF families or TF classes or individual TFs.

**NOTE!** To run this specific **RANDOM** pipeline snakefile must be specified. Example of the command (assuming you are in the cobind_manuscript directory):

```bash
## Running RANDOM pipeline wih human data:
snakemake -s Snakefile_random_experiment --cores 1 --config analysis_id=human_hg38
```

There are different configuration files for each species, which will be selected by specifying analysis_id. Options for analysis_id and their corresponding configuration files:

```bash
analysis_id=human_hg38         : config_files/config_human_hg38_random.yaml
analysis_id=mouse_mm10         : config_files/config_mouse_mm10_random.yaml
analysis_id=rat_Rnor_6.0       : config_files/config_rat_Rnor_6.0_random.yaml
analysis_id=arabidopsis_TAIR10 : config_files/config_arabidopsis_TAIR10_random.yaml
analysis_id=celegans_WBcel235  : config_files/config_celegans_WBcel235_random.yaml
analysis_id=drerio_GRCz11      : config_files/config_drerio_GRCz11_random.yaml
analysis_id=drosophila_dm6     : config_files/config_drosophila_dm6_random.yaml
```

The pipeline returns all possible co-binding motifs for two groups: real and random (generated from actual TFBSs and from the same number of randomized TFBSs sequences, respectively).

### Different ways to determine Gini thresholds ###

We have developed two ways how Gini thresholds can be chosen for the motif filtering. Both of them are using the results of the **RANDOM** pipeline (basically all possible motifs generated by the NMF). In the paper we are using approach **"A"** and apply species-specific Gini thresholds based on FDRs.

#### A. False discovery rate (FDR) approach ####

The first approach looks into the Gini values distribution for motifs generated from randomized sequences and takes Gini values at 95% and 99% quantiles. These specific values are then transfered to the distribution of Gini coefficients for the motifs discovered in the real data. They are considered Gini thresholds with 5% and 1% false discovery rate (FDR). We are running our analysis with both of these thresholds.

With this approach we are determining the Gini thresholds in species-specific manner.

#### B. A common Gini threshold from mixture model ####

The second approach looks into the all motifs discovered in the real sequences in different species, pools all Gini values and plots their distribution. The mixture model with 2 components is then applied and a threshold determined. Mixture model is "cut" into two, cutting point being the threshold. The cut-off is based on a probability P that a point with threshold Gini value belongs to a distribution with lower mean (left distribution) (the approach was inspired by [this conversation in the forum](https://stats.stackexchange.com/questions/57993/how-to-explain-how-i-divided-a-bimodal-distribution-based-on-kernel-density-esti/78397#78397)). See in the *Supplementary scripts* how to run this particular script.

## Running the **SYNTHETIC DATA** pipeline to explore number of NMF components and compare with other tools ##

The synthetic data tests are performed on synthetic data. Synthetic flank sequences were generated by inserting known motifs of different transcription factors. In ```motifs``` folder we stored 11 motifs taken from JASPAR database: ATF4, CEBPB, CTCF, Foxo1, HOXA1, HOXB13, MEIS1, PAX5, POU2F1, SOX2 and TBX21. The total number of sequences and the proportion of them to which the known motif should be inserted can be indicated in the configuration file. In addition, the lenght of the flank and the position of the inserted motif can also be selected.

To run the tests with synthetically generated data, you have to navigate into a dedicated folder. Two types of tests are available and can be selected by using different configuration files.

* **COMPONENTS** is exploring the number of components *k*, which is a hyperparameter

```bash
analysis_id=components          : config_files/config_components.yaml
analysis_id=commparison         : config_files/config_comparison.yaml
```

```bash
cd synthetic_data_test

## Components analysis:
snakemake --config analysis_id=components  --cores 1

## Comparison with other tools:
snakemake --config analysis_id=comparison  --cores 1
```

### The output ###

The pipeline outputs several summary tables and plots, all being inserted-motif specific.

A) From the exploration of components:

* ```{title}/{motif}/cobind_synthetic_run_summary/synthetic_data_test_summary_heatmap_{motif}.pdf```

   The plot indicates how well the original motif was recovered using COBIND. Two asterixes mean that a correct original motif with that particular number of components in a specific number and proportion of sequences was found only one, which is a desired output. The colours indicate the observed/expected ratio of sequences that were used to build a discovered motif. In a perfect case that would be 1. We chose to indicate an interval [0.5-1.5] that accomodates a desirable sequence recovery ratio.

B) From comparison with other tools:

* ```{title}/{motif}/comparison_with_others/injected_vs_all_discovered.pdf```

   The plot summarizes the motif discovery using different tools. The colouring of each point follow the same logis explained above, while the size of the point indicates the proportion of all discovered motifs, since here we really care how many false positive each tools is discovering together with the expected motif.

* ```{title}/{motif}/comparison_with_others/max_runtime_summary_lineplot_nb_seqs.pdf``` and ```{title}/{motif}/comparison_with_others/sum_runtime_summary_lineplot_nb_seqs.pdf```

   The plots are summarizing the time that it takes for each tools to discover motifs for a single synthetic dataset consisting of different number of sequences. Since COBIND is ran with a range of components *k*, such analysis can be paralelized, so the runtime is lower if we parallelize (`max`), but if we run without parallelization, runtime for each component must be summed (`sum`).

## Running the **Single Molecule Footprinting** pipeline ##

This pipeline includes the processing and analysis of Single Molecule Footprinting (SMF) data. Analysis of the data is following the recommendation in the from the [protocol paper](https://www.nature.com/articles/s41596-021-00630-1) by Barzaghi and Kleinendorst et al., 2021 and using R [Bioconductor package](https://bioconductor.org/packages/release/bioc/html/SingleMoleculeFootprinting.html) SingleMoleculeFootprinting (development version) by Barzaghi et al., 2022. 12 SMF triple-methyltransferases knockout in mouse embryonic stem cells (ESCs) datasets were downloaded from ArrayExpress under accession numbers E-MTAB-9033 and E-MTAB-9123.

Predicted anchor-co-binder pair regions are then analysed in the SMF data providing the SingleMoleculeFootprinting package functions with already defined cluster regions (in this case we do not need the package to build clusters of binding sites, we provide anchor-co-binder regions). Because of this, we needed to modify one function of the package to take already existing cluster regions (modified function provided in ```bin/single_molecule/analyse_sm/sort_tf_reads_by_cobind_clusters.R```).

Configuration file for this pipeline can be found here: ```config_files/config_sm.yaml```.

```bash
## Running SMF pipeline:
snakemake -s Snakefile_sm --cores 1
```

## Running the **DHS analysis** pipeline ##

This pipeline includes the processing and analysis of DNase I hypersensitivity (DHS) data. The DHS footprints were downloaded in .```bw``` format and were published by [Vierstra et al., 2020](https://www.nature.com/articles/s41586-020-2528-x) and made available [here](https://resources.altius.org/~jvierstra/projects/footprinting.2020). DHS data from various human cell types was downloaded in a specific manner. A fraction of UniBind 2021 data used in the original COBIND analysis comes from ENCODE, so we were able to match the cell types of those datasets with DHS data that also originally come from ENCODE. We split the download of the full set of data into two parts: 1) data that have a match with data were at least one co-binder was found and 2) data that does not have any matches in the TFBSs data used for co-binder discovery. We later focused on the data from matching cell types.

Predicted anchor-co-binder pair regions in DHS data were analysed in almost the same fashion as in the evolutionary conservation of regions in the original ```Snakemake``` pipeline. An adjustment had to be made when computing the statistical significance of the DHS footprint, since here the deeper the footprint, the better, so Wilcoxon one sided test alternative was set to ```less```.

Configuration file for this pipeline can be found here: ```config_files/config_dhs.yaml```.

```bash
## Running DHS pipeline:
snakemake -s Snakefile_dhs --cores 1
```

## Supplementary scripts ##

### Running the script to determine the threshold ###

The R script named "mixture_model.R" summarizes all motifs from different runs (for example, runs from different species). It plots distributions of Gini coeficient and applies a mixture model with two distributions. It returns distribution count plot and distribution density plot. Density plot includes four cut-offs, based in different probabilities P that a point with threshold Gini value belongs to a distribution with lower mean (left distribution).

Current probabilities that are used to compute threshold:
P[belonging to lower mean distribution] = 0.50;
P[belonging to lower mean distribution] = 0.25;
P[belonging to lower mean distribution] = 0.05;
P[belonging to lower mean distribution] = 0.01.

A table with threshold values at each P is also outputed by the script.

**Required packages** for this script to run with R (version >= 3.6.1):

* data.table (CRAN);
* DescTools (CRAN);
* dplyr (CRAN);
* ggplot2 (CRAN);
* mixtools (CRAN);
* optparse (CRAN);
* rootSolve (CRAN;)
* tidyr (CRAN).

**Parameters:**

```unix
-t : (--analysis_title)             Analysis titles (folder names with the output from "Random" COBIND pipeline run) separated by commas. (Mandatory)
```

**Example** (assuming that folders are in the current directory):

```unix
Rscript bin/main/gini_analysis/mixture_model.R
   -t run_1_RAT,run_2_HUMAN,run_3_MOUSE
```

### Producing required TF families table on your own ###

We provide a script that takes metadata file of an input UniBind data and groups the datasets based on the TFs, TF families and classes. Separate tables are generated for each species, but also for all datasets over different tables.

**Required packages** for this script to run with R (version >= 3.6.1):

* data.table (CRAN);
* dplyr (CRAN);
* optparse (CRAN);
* tidyr (CRAN).

**Parameters** (if multiple taxons are processed, multiple table paths and taxon names must be separated by commas and in the same order):

```unix
-u : (--unibind_metadata)  A list of comma-separated metadata file(s) for UniBind data. (Mandatory)
-o : (--output_folder)     Output folder to output the family table(s). (Mandatory)
-s : (--species)           A list of comma-separated species names that corresponds with list of metadata. (Mandatory)
```

Input unibind metadata must have at least four columns *motif_name*, *class*, *family*, *dataset*. Dataset column should match the names of the BED files intended to use as input for the pipeline. For example: dataset **GATA1_experiment1** would correspond to the file **GATA1_experiment1.bed**.

**Example** (assuming you are in the cobind_manuscript directory):

```bash
Rscript bin/data_preparation/create_tf_grouping_tab.R
   -u data/unibind_21/ub21_arabidopsis_metadata.tsv,
      data/unibind_21/ub21_celegans_metadata.tsv,
      data/unibind_21/ub21_drosophila_metadata.tsv,
      data/unibind_21/ub21_drerio_metadata.tsv,
      data/unibind_21/ub21_human_metadata.tsv,
      data/unibind_21/ub21_mouse_metadata.tsv,
      data/unibind_21/ub21_rat_metadata.tsv
   -o data/tfs_groups
   -s arabidopsis,celegans,drosophila,drerio,human,mouse,rat
```

**Output:**

This script returns three tables in *.txt* format for each species:

* *tf_individual_{speices}.txt*
* *tf_families_{species}.txt*
* *tf_classes_{species}.txt*

The script also outputs joined tables for all species that were put as parameters:

* *all_species_tf_individual.txt*
* *all_species_tf_families.txt*
* *all_species_tf_classes.txt*

**Example of *tf_families_{species}.txt* table:**

```unix
ARID-related factors                    ARID3A,ARID3B,experiment1_ARID5A,experiment2_ARID5A                                                                                          
ATF-4-related factors                   ATF4                                                                                                          
B-ATF-related factors                   BATF,BATF3                                                                                                    
B-ATF-related factors::Jun-related fac… BATF-JUN                                                                                                      
BED zinc finger factors                 ZBED1 
...
```

### Coding sample names, creating a metadata table with data from UniBind 2021 and JASPAR databases ###

This script aims to rename (code) transcription factor binding sites (TFBSs) files from UniBind 2021, keeping only the TF motif name, while dataset, cell line and biological condition data is coded to make file names short in order to run COBIND more efficiently. It basically assigns the same number to the same name in a particular field. When downloading bulk data from UniBind 2021, metadata file (which is required by this script) is downloaded together with BED files.

Since it is the best that TFBS BED file name reflects the name of the motif rather than a ChIP'ed TF, motif name is used in the new BED file name. UniBind 2021 metadata includes Jaspar profiles names, reflecting Jaspar ID in the original file name. In addition, this script includes family and class name in the newly generated metadata table. For this we use information from JASPAR database. JASPAR tables for each taxon can be retrieved by following the section *"Retrieve motif information from JASPAR 2020"* [from this repository](https://github.com/jaimicore/R_utilities).

New version of this script allows to sample CTCF datasets. The argument "-c" requires a number of how many CTCF datasets should be included in the resulting set of datasets and in the new metadata table. Datasets are sampled randomly without replacement. This feature was included for the computational efficiency of the COBIND pipeline. The default is "-1", which means that no sampling is being done.

**Format of the original file names in UniBind 2021:**

```unix
{identifier}.{title}.{biological_condition}.{tf_name}.{jaspar_id}.damo.bed
```

* identifier           : dataset ID, indicating the publicly available dataset from which this dataset comes from;
* title                : cell-line/tissue;
* biological_condition : biological condition of the cell-line/tissue;
* tf_name              : name of the TF that has been ChIP'ed;
* jaspar_id            : name of the motif that has been used to scan the peaks.

*NOTE!* For the same ChIP'ed TF different motif variants can be applied, which is why we focus and group files based on motif names rather than ChIP'ed TF names.

**Example of the original file from UniBind 2021:**

```unix
ENCSR000AHD.MCF7_Invasive_ductal_breast_carcinoma.CTCF.MA0139.1.damo.bed
```

**Required packages** for this script to run with R (version >= 3.6.1):

* data.table (CRAN);
* dplyr (CRAN);
* optparse (CRAN);
* tidyr (CRAN).

**Parameters:**

```unix
-d : (--tfbs_data_folder)  Path to the folder with the original TFBS data (BED files). (Mandatory)
-m : (--metadata_file)     Path to the original UniBind metadata file, downloaded together with BED files. (Mandatory)
-o : (--output_folder)     Path to the folder, where renamed files should be exported. (Mandatory)
-t : (--output_table)      Folder, where the metadata table with coded files will be exported. (Mandatory)
-f : (--jaspar_table)      Path to the Jaspar motif info file. (Mandatory)
-c : (--ctcf_sampling)     Number of lines to sample CTCF (0 to max number of CTCF datasets). Default is no sampling (-1).
-l : (--label)             Species or some other label to add to metadata table. Default is (LB).
```

**Example** (assuming you are in the cobind_manuscript directory):

```unix
Rscript bin/data_preparation/make_tfbs_metadata.R \
   -d unibind_2021_mouse \
   -m unibind_2021_mouse.tsv \
   -o ub21_mouse \
   -t unibind2021_mouse_metadata.tsv \
   -f Jaspar_2020_info_Vertebrates_table.tab
```

*Output:**

The script renames the files to this format.

```unix
{label}_{file_id}.{cell_id}.{jaspar_id}.{TF_id}.{motif_name}.damo.bed
```

* label      : label from parameters - useful to code-in the species name;
* file_id    : code that reflects the whole original file name;
* cell_id    : coded cell-line/tissue name;
* jaspar_id  : coded JASPAR matirx id;
* TF_id      : coded ChIP'ed TF name;
* motif_name : motif, that corresponds TFBS, name.

**Output example:**

```unix
MM_D5_C125_J52_T28_CTCF.bed
```

### Preparing the joined CIS-BP and JASPAR motif collections to run PPI and motif comparison analysis ###

For the inference of what transcription factor (TF) could be binding the motif, discovered by COBIND, we can use collections of known motifs to find the similarities with our motifs. There are many resources available that store collections of known TF binding motifs. [JASPAR](https://jaspar.genereg.net/) is a manually curated database that contains CORE collections for different taxonomic groups, in which all the motifs have orthogonal support from the literature. JASPAR also stores UNVALIDATED collection, where the motif are not yet supported by the literature. Another popular resource containing vast collections of motifs is [CIS-BP](http://cisbp.ccbr.utoronto.ca/). It has a large number of non-curated motif, which can be direct or inferred from DNA binding domain similarities. We chose to combine the collections from both of these resources and make large species-specific motif collections and compare COBIND discovered co-binders with motifs in them.

On top of that, since the anchor and the discovered motif are in close proximity to each other, the two proteins binding those motifs are highly interacting. Such interactions are also stored in different databases. One such database is [STRING](https://string-db.org/). Physical PPIs (version 11.0) were downloaded for all analyzed species.

**Required packages** for this script to run with R (version >= 3.6.1):

* data.table (CRAN);
* dplyr (CRAN);
* optparse (CRAN);
* tidyr (CRAN).

**Parameters:**

```unix
-c : (--cisbp_motifs)     A path to folder with CIS-BP pwms. (Mandatory)
-m : (--cisbp_metadata)   A table with CIS-BP motif metadata. (Mandatory)
-j : (--jaspar_motifs)    A path to folder with JASPAR motifs. (Mandatory)
-n : (--jaspar_metadata)  A table with JASPAR motifs metadata. (Mandatory)
-o : (--output_folder)    An output folder. (Mandatory)
```

**Example:** (assuming you are in the cobind_manuscript directory):

```unix
Rscript bin/data_preparation/prepare_cisbp_jaspar_motifs_metadata.R
    -c CIS-BP/arabidopsis_thaliana/pwms_all_motifs
    -m CIS-BP/arabidopsis_thaliana/TF_Information_all_motifs.txt
    -j JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs
    -n JASPAR/2022/core_unvalidated_nonredundant/Plants/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_plants.tab
    -o data/jaspar_cisbp_motifs/arabidopsis
```

**Output:**

This script returns three files: motif collections in MEME and TRANSFAC formats together with a text file with the metadata. In addition, the folder with individual converted CIS-BP motifs in TRASFAC format will be created next to the original CIS-BP data folder.

* *{output_folder}/cisbp_jaspar_motif_collection.meme*
* *{output_folder}/cisbp_jaspar_motif_collection.tf*
* *{output_folder}/cisbp_jaspar_motif_metadata.tab*

**Examples of how each input file should be formatted (column names are important!):**

* *CIS-BP/arabidopsis_thaliana/pwms_all_motifs* folder should contain motif files (with *.txt* extension) in CIS-BP format.

```unix
cat CIS-BP/arabidopsis_thaliana/pwms_all_motifs/M06730_2.00.txt
Pos A         C         G         T
1   0.169154 0.636816 0.099502 0.094527
2   0.542289 0.00995   0.288557 0.159204
3   0.0     0.711443 0.034826 0.253731
4   0.189055 0.0199   0.761194 0.029851
5   0.139303 0.159204 0.039801 0.661692
6   0.059701 0.064677 0.577114 0.298507
7   0.298507 0.074627 0.144279 0.482587
8   0.273632 0.134328 0.154229 0.437811
9   0.303483 0.303483 0.074627 0.318408
10   0.054726  0.945274 0.0       0.0
11   0.960199 0.0       0.0199   0.0199
12   0.0       0.99005   0.0       0.00995
13   0.044776 0.0       0.955224 0.0
14   0.034826 0.014925 0.0       0.950249
15   0.0       0.0       0.9801   0.0199
16   0.144279 0.169154 0.393035 0.293532
17   0.19403   0.298507 0.169154 0.338308
```

* *CIS-BP/arabidopsis_thaliana/TF_Information_all_motifs.txt* should have at least these columns:
  TF_ID, Motif_ID, TF_Name, DBDs, Family_Name, TF_Species, TF_Status, Motif_Type, PMID

The original file you can download from CIS-BP webpage usually contains much more information:

```unix
head -n5 CIS-BP/arabidopsis_thaliana/TF_Information_all_motifs.txt

TF_ID Family_ID TSource_ID Motif_ID MSource_ID DBID TF_Name TF_Species TF_Status Family_Name DBDs DBD_Count Cutoff DBID Motif_Type MSource_Identifier MSource_Type MSource_Author MSource_Year PMID MSource_Version SR_Model SR_NoThresholdTfSource_Name TfSource_URL TfSource_Year TfSource_Month TfSource_Day
T000400_2.00 F009_2.00 TS12_2.00 M06560_2.00 MS46_2.00 AT1G01250 ERF023 Arabidopsis_thaliana D AP2 AP2 1 0.6765891324647211 AT1G01250_col_a Dap-seq OMalley2016 Dap-seq OMalley 2016 27203113 NULL SimilarityRegression False Ensembl http://www.ensembl.org/ 2018 Dec 8
T000400_2.00 F009_2.00 TS12_2.00 M06561_2.00 MS46_2.00 AT1G01250 ERF023 Arabidopsis_thaliana D AP2 AP2 1 0.6765891324647211 AT1G01250_colamp_a Dap-seq OMalley2016 Dap-seq OMalley 2016 27203113 NULL SimilarityRegression False Ensembl http://www.ensembl.org/ 2018 Dec 8
T000401_2.00 F009_2.00 TS12_2.00 M06562_2.00 MS46_2.00 AT1G03800 ERF10 Arabidopsis_thaliana D AP2 AP2 1 0.6765891324647211 ERF10_col_a Dap-seq OMalley2016 Dap-seq OMalley 2016 27203113 NULL SimilarityRegression False Ensembl http://www.ensembl.org/ 2018 Dec 8
T000401_2.00 F009_2.00 TS12_2.00 M06563_2.00 MS46_2.00 AT1G03800 ERF10 Arabidopsis_thaliana D AP2 AP2 1 0.6765891324647211 ERF10_colamp_a Dap-seq OMalley2016 Dap-seq OMalley 2016 27203113 NULL SimilarityRegression False Ensembl http://www.ensembl.org/ 2018 Dec 8
```

* *JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs*
JASPAR website provides all collections for download in different formats. This script requires to have all motifs in one folder as individual motifs in JASPAR format. An example of the format:

```unix
cat JASPAR/2022/core_unvalidated_nonredundant/all_taxons/jaspar_all_motifs/MA1140.2.jaspar
>MA1140.2 JUNB
A  [    50    305      1      0    305      0      0      1     17    305      0     40 ]
C  [    46      7      1      0      1    305      1      3    305      2    114    187 ]
G  [   162     15      4    305      1      0    305      1      3      1     10     43 ]
T  [    47      6    305     13      0      2      0    305      1      1    191     35 ]
```

* *JASPAR/2022/core_unvalidated_nonredundant/Plants/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_plants.tab*
JASPAR metadata can be obtained through API and must contain all of the following collumns:

```unix
head -n5 JASPAR/2022/core_unvalidated_nonredundant/Plants/JASPAR_2022_metadata_nonredundant_CORE_UNVALIDATED_plants.tab 

base_id version name uniprot_ids tax_id class family collection type source pubmed_ids comment tax_group
MA0570 2 ABF1 Q9M7Q5 3702 Basic leucine zipper factors (bZIP) Group A CORE ChIP-seq 30445619 10636868  plants
MA0941 1 ABF2 Q9M7Q4 3702 Basic leucine zipper factors (bZIP) Group A CORE PBM 25215497 11005831  plants
MA0930 2 ABF3 Q9M7Q3 3702 Basic leucine zipper factors (bZIP) Group A CORE  ReMap 11005831::11005831  plants
MA1659 1 ABF4 Q9M7Q2 3702 Basic leucine zipper factors (bZIP) Group A CORE DAP-seq 27203113 25220462  plants
```

## Contact ##

In case you have any questions related to this pipeline, please contact the following persons:

|  Name                                                     | E-mail                          |
| ----------------------------------------------------------|---------------------------------|
| [Ieva Rauluseviciute](https://bitbucket.org/ievara/)      | ieva.rauluseviciute@ncmm.uio.no |
| [Anthony Mathelier](https://mathelierlab.com/)            | anthony.mathelier@ncmm.uio.no   |
