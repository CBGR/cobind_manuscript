#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "tidyverse",
                        "patchwork",
                        "optparse",
                        "BSgenome.Mmusculus.UCSC.mm10",
                        "GenomicRanges",
                        "Matrix",
                        "plyranges")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

#############
## Options ##
#############

## Disabling strings as factors:
options(stringsAsFactors = FALSE)

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(c("-i", "--input"), type = "character", default = NULL,
              help = "Methylation summary table. (Mandatory) ", metavar = "character"),
  
  make_option(c("-o", "--output_dir"), type = "character", default = NULL,
              help = "Output directory. (Mandatory)", metavar = "character"),
  
  make_option(c("-t", "--region_type"), type = "character", default = "cobinder_specific",
              help = "Region type. [Default: \"%default\" . Options: cobinder_specific, case_specific]", metavar = "character"), 
  
  make_option(c("-c", "--cores"), type = "numeric", default = 5,
              help = "Number of cores that will be used to perform the analysis [Default: \"%default\"]", metavar = "numeric"),
  
  make_option(c("-m", "--cobind_analysis_title"), type = "character", default = "MOUSE_0.99",
              help = "The basename of the COBIND title, which regions are analysed. [Default: \"%default\"]", metavar = "character"),
  
  make_option(c("-s", "--smf_functions"), type = "character", default = "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/bin/single_molecule/SingleMoleculeFootprinting/R",
              help = "A path to smf package functions.", metavar = "character"),
  
  make_option(c("-p", "--pointer_file"), type = "character", default = NULL,
              help = "A pointer file with information on .bam files. (Mandatory) ", metavar = "character")
  
);

message("; Reading arguments from command line")
opt_parser = OptionParser(option_list = option_list)
opt        = parse_args(opt_parser)

########################
## Set variable names ##
########################

INPUT         <- opt$input
OUTPUT_DIR    <- opt$output_dir
REGION_TYPE   <- opt$region_type
NB_CORES      <- opt$cores
COBIND_TITLE  <- opt$cobind_analysis_title
SMF_FUNCTIONS <- opt$smf_functions
POINTER_FILE  <- opt$pointer_file

############
## Syntax ##
############

# Rscript bin/single_molecule/analyse_sm/plot_methylation_loci.R \
# -i SMF_analysis/SM_results/footprinting_summary/MOUSE_ESC_0.99/with_cobinder/case_specific/methylation_states_joined_table.tsv \
# -o SMF_analysis/SM_results/footprinting_summary \
# -t case_specific \
# -c 1 \
# -m MOUSE_ESC_0.99 \
# -p SMF_analysis/SM_results/data/combined_pointer_file.txt

###########
## Debug ##
###########

# INPUT        <- "SMF_analysis/SM_results/footprinting_summary/MOUSE_ESC_0.99/with_cobinder/case_specific/methylation_states_joined_table.tsv"
# OUTPUT_DIR   <- "SMF_analysis/SM_results/footprinting_summary"
# REGION_TYPE  <- "case_specific"
# NB_CORES     <- 1
# COBIND_TITLE  <- "MOUSE_ESC_0.99"
# SMF_FUNCTIONS <- "/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/bin/single_molecule/SingleMoleculeFootprinting/R"
# POINTER_FILE  <- "SMF_analysis/SM_results/data/combined_pointer_file.txt"

###############################
## Creating output directory ##
###############################

message("; Creating output directory.")

OUTPUT_DIR <- file.path(OUTPUT_DIR, COBIND_TITLE)
dir.create(OUTPUT_DIR, showWarnings = F, recursive = T)

###############
## Functions ##
###############

## Sourcing package functions:
source(file.path(SMF_FUNCTIONS, "GenomeWide_wrappers_to_SortingFunctions.R"))
source(file.path(SMF_FUNCTIONS, "plot_single_molecule_examples.r"))
source(file.path(SMF_FUNCTIONS, "utilities.r"))
source(file.path(SMF_FUNCTIONS, "context_methylation_functions.r"))
source(file.path(SMF_FUNCTIONS, "single_molecule_manipulation_functions.r"))

###############################
## Reading methylation table ##
###############################

methylation_joined_tab <- data.table::fread(INPUT, header = TRUE, sep = "\t")

## List of unique family names:
list_of_families <- unique(methylation_joined_tab$family_name)

###################################################################
## Itterating through families and plotting the loci methylation ##
###################################################################

for (family in list_of_families) {
  
  message("; Family: ", family)
  
  met_entry <- methylation_joined_tab %>%
    dplyr::filter(family_name == family)
  
  list_of_cobinders <- unique(met_entry$cobinder_id)
  
  for (cobinder in list_of_cobinders) {
    
    message("; Cobinder: ", cobinder)
    
    met_cobinder_entry <- met_entry %>%
      dplyr::filter(cobinder_id == cobinder)
    
    list_of_TFBS_clusters <- unique(met_cobinder_entry$TFBS_cluster)
    
    ## A path to the RData with methylation results:
    PATH_TO_METHYLATION_RDATA <- file.path(
      gsub(OUTPUT_DIR, pattern = "_summary\\/.+", replacement = ""),
      COBIND_TITLE,
      family,
      "with_cobinder",
      REGION_TYPE,
      paste0("cobinder_", cobinder, ".RData"))
    
    if (file.exists(PATH_TO_METHYLATION_RDATA)) {
      
      cobinder_methylation_res <- get(load(PATH_TO_METHYLATION_RDATA))
      
      ## Itterating through TFBSs:
      for (tfbs_cluster in list_of_TFBS_clusters) {
        
        message("; TFBS: ", tfbs_cluster)
        met_tfbs_entry <- met_cobinder_entry %>%
          dplyr::filter(TFBS_cluster == tfbs_cluster)
        
        list_of_samples <- unique(met_tfbs_entry$Sample)

        ## Region of interest:
        tfbs_granges_coordinates <- cobinder_methylation_res[[1]]$ClusterCoordinates[
          names(cobinder_methylation_res[[1]]$ClusterCoordinates) %in% tfbs_cluster]
        tfbs_granges_coordinates_with_flank <- GenomicRanges::flank(
          tfbs_granges_coordinates,
          150,
          both = TRUE)
        
        ## Anchor and cobinder coordinates:
        anchor_cobinder_anno_coordinates <- cobinder_methylation_res[[1]]$ClusterComposition[[tfbs_cluster]]
        names(anchor_cobinder_anno_coordinates) <- paste0(
          tfbs_cluster, 
          "_", 
          anchor_cobinder_anno_coordinates$name)
        anchor_cobinder_anno_coordinates$TF <- gsub(
          gsub(
            anchor_cobinder_anno_coordinates$name,
            pattern = "anchor", 
            replacement = "A"),
          pattern = "cobinder", 
          replacement = "C")
        
        ## Calling methylation:
        region_methylation_context <- CallContextMethylation(
          sampleSheet = POINTER_FILE,
          sample = list_of_samples, 
          genome = BSgenome.Mmusculus.UCSC.mm10, 
          RegionOfInterest = tfbs_granges_coordinates_with_flank,
          ConvRate.thr = NULL)
        
        ## Plotting:
        if (length(region_methylation_context[[1]]) != 0) {
          
          ## Recomputing coverage and methylation rate:
          message("; Merging methylation rate and coverage.")
          genomic_context_df <- S4Vectors::elementMetadata(
            region_methylation_context[[1]])[
              grepl(
                "GenomicContext", 
                names(elementMetadata(region_methylation_context[[1]])))]
          coverage_df <- S4Vectors::elementMetadata(
            region_methylation_context[[1]])[
              grepl(
                "_Coverage", 
                names(elementMetadata(region_methylation_context[[1]])))]
          methrate_df <- S4Vectors::elementMetadata(
            region_methylation_context[[1]])[
              grepl(
                "_MethRate", 
                names(elementMetadata(region_methylation_context[[1]])))]
          
          pooled_DE__Coverage <- rowSums(as.data.frame(coverage_df), na.rm = TRUE)
          pooled_DE__MethRate <- rowMeans(as.data.frame(methrate_df), na.rm = TRUE)
          
          values(region_methylation_context[[1]]) <- S4Vectors::DataFrame(
            pooled_DE__Coverage = pooled_DE__Coverage,
            pooled_DE__MethRate = pooled_DE__MethRate,
            GenomicContext = genomic_context_df$GenomicContext)
          
          ## Concatenating the methylation array:
          all_positions <- sort(unique(unlist(lapply(region_methylation_context[[2]], colnames))))
          
          region_methylation_context[[2]] <- list(
            do.call(
              rbind,
              c(
                lapply(
                  region_methylation_context[[2]],
                  function(x) {
                    ## Filling matrix:
                    filled_matrix <- data.frame(
                      c(
                        as.data.frame(as.matrix(x)),
                        sapply(
                          setdiff(
                            all_positions,
                            names(as.data.frame(as.matrix(x)))),
                          function(y) "0")))
                    ## Renaming columns:
                    colnames(filled_matrix) <- gsub(
                      colnames(filled_matrix),
                      pattern = "X",
                      replacement = "")
                    ## Formatting matrix:
                    filled_matrix <- as.matrix(filled_matrix[all_positions])
                    filled_matrix <- matrix(
                      as.numeric(filled_matrix),
                      nrow = length(rownames(x)))
                    rownames(filled_matrix) <- rownames(x)
                    colnames(filled_matrix) <- all_positions

                    filled_matrix <- as(filled_matrix, "sparseMatrix")

                    return(filled_matrix)
                  }
                ))))
          
          # region_methylation_context[[2]] <- list(do.call("rbind", region_methylation_context[[2]]))
          names(region_methylation_context[[2]]) <- c("pooled_DE_")
          
          ## Sorting reads:
          SortedReads_TFCluster <- SortReadsByTFCluster(
            MethSM = region_methylation_context[[2]], 
            TFBS_cluster = anchor_cobinder_anno_coordinates)
          
          single_site_plot <- PlotSingleSiteSMF(
            Methylation      = region_methylation_context,
            RegionOfInterest = tfbs_granges_coordinates_with_flank,
            SortedReads      = SortedReads_TFCluster,
            TFBSs            = anchor_cobinder_anno_coordinates)
          
          single_site_plot <- single_site_plot +
            ggtitle(
              paste0(
                family, 
                " - cobinder_", cobinder, 
                "_", tfbs_cluster,
                "\nSamples - ",
                paste0(list_of_samples, collapse = ",")))
          
          PATH_TO_OUTPUT_PLOT <- file.path(
            OUTPUT_DIR,
            "with_cobinder",
            REGION_TYPE,
            family, 
            paste0(
              "cobinder_", cobinder,
              "_", tfbs_cluster,
              "_single_site_plot.pdf"))
          ggsave(
            filename  = PATH_TO_OUTPUT_PLOT,
            plot      = single_site_plot,
            width     = 11,
            height    = 9,
            limitsize = FALSE)
        }
      }
      
    } else {
      
      message("; Methylation R data does not exist. Skipping.")
    }

  }
}

###################
## End of script ##
###################