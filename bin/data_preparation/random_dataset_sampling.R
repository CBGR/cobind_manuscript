#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "dplyr",
                        "tidyr",
                        "optparse")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(c("-d", "--tfbs_data_folder"), type = "character", default = NULL,
              help = "Path to the folder with the TFBS data (BED files). (Mandatory) ", metavar = "character"),
  
  make_option(c("-o", "--output_folder"), type = "character", default = NULL,
              help = "Folder, where copied files should be exported. (Mandatory) ", metavar = "character"),
  
  make_option(c("-s", "--sample_sizes"), type = "character", default = NULL,
              help = "A list of sample sizes indicated what number of files should be randomly sampled). (Mandatory)", metavar = "character"),
  
  make_option(c("-i", "--sampling_iterations"), type = "numeric", default = NULL,
              help = "A number of how many times the datasets should be sampled. (Mandatory)", metavar = "numeric")
  
);

message("; Reading arguments from command line.")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

########################
## Set variable names ##
########################

tfbs.data.folder         <- opt$tfbs_data_folder
output.folder            <- opt$output_folder
sampling.intervals       <- opt$sample_sizes
sampling.iterations      <- opt$sampling_iterations

## Syntax
# Rscript bin/data_preparation/random_dataset_sampling.R
#   -d /storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind/data/ub_21/ub21_mouse_renamed
#   -o /storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind/data/ub_21/ub21_mouse_sampled
#   -s 50,100,500,1000,2000,3000,4000
#   -i 5


## Debug
# setwd("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind/")
# tfbs.data.folder     <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind/data/ub_21/ub21_mouse_renamed"
# output.folder        <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind/data/ub_21/ub21_mouse_sampled"
# sampling.intervals   <- c("50","100","500","1000","2000","3000","4000")
# sampling.iterations  <- 3

###############################
## Separating the parameters ##
###############################

sampling_intervals <- unlist(strsplit(sampling.intervals, ","))
sampling_intervals <- as.numeric(sampling_intervals)
print(sampling_intervals)

###############################
## Creating output directory ##
###############################

message("; Creating output directory.")
dir.create(output.folder, showWarnings = F, recursive = T)

###########################
## Listing all the files ##
###########################

bed_files <- list.files(file.path(tfbs.data.folder))

nb_bed_files <- length(bed_files)
message("; There are ", nb_bed_files, " BED files.")

bed_files_df <- as.data.frame(bed_files) %>%
  dplyr::mutate(original_path = file.path(tfbs.data.folder, bed_files))

########################################################################################
## Random sampling the listed numbers of files and copying them into separate folders ##
########################################################################################

message("; Randomly sampling the files.")

for (sample_id in sampling_intervals) {
  
  for (i in 1:sampling.iterations) {
    
    sample_id < nb_bed_files
    if (sample_id < nb_bed_files) {
      
      message("; Sampling ", sample_id, " datasets")
      
      ## Directory to output sampled datasets:
      sample.out.dir <- file.path(output.folder, sample_id, i)
      dir.create(sample.out.dir, showWarnings = F, recursive = T)
      
      sampled_datasets <- bed_files_df %>%
        dplyr::sample_n(as.numeric(sample_id)) %>%
        dplyr::mutate(new_path = file.path(sample.out.dir, bed_files))
      
      apply(sampled_datasets, 1, function(l) {
        
        cp.cmd <- paste(" cp -r ", l[2], " ", l[3])
        system(cp.cmd)
      })
    } else {
      message("; There is not enough samples to sample from. Skipping this sampling size.")
    }
  }
}

#######################
## End of the script ##
#######################