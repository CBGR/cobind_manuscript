#####################
## Load R packages ##
#####################

required.libraries <- c("data.table",
                        "tidyverse",
                        "optparse",
                        "this.path")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only = TRUE, quietly = T))
}

#############
## Options ##
#############

## Disabling strings as factors:
options(stringsAsFactors = FALSE)

script.dir <- this.path::this.dir()

####################
## Read arguments ##
####################

option_list = list(
  
  optparse::make_option(c("-c", "--cobind_title"), 
                        type = "character", 
                        default = NULL,
                        help = "A title of COBIND analysis. (Mandatory) ", 
                        metavar = "character"),
  
  optparse::make_option(c("-i", "--cobind_input_directory"), 
                        type = "character", 
                        default = NULL,
                        help = "A path to the directory with COBIND results. (Mandatory) ", 
                        metavar = "character"),
  
  optparse::make_option(c("-d", "--dhs_data_directory"), 
                        type = "character", 
                        default = NULL,
                        help = "A path to directory with DHS data. (Mandatory) ", 
                        metavar = "character"),
  
  optparse::make_option(c("-m", "--cobind_meta"), 
                        type = "character", 
                        default = NULL,
                        help = "A path to the metadata table of UniBind data with ENCODE biosample IDs. (Mandatory) ", 
                        metavar = "character"),
  
  optparse::make_option(c("-n", "--dhs_meta"), 
                        type = "character", 
                        default = NULL,
                        help = "A path to the metadata table of DHS data with ENCODE biosample IDs. (Mandatory) ", 
                        metavar = "character"),
  
  optparse::make_option(c("-o", "--output"), 
                        type = "character", 
                        default = NULL,
                        help = "Output directory. (Mandatory)", 
                        metavar = "character"),
  
  optparse::make_option(c("-f", "--family_name"), 
                        type = "character", 
                        default = NULL,
                        help = "Family name for which to do the overlap. (Mandatory)", 
                        metavar = "character"),
  
  optparse::make_option(c("-w", "--analysis_window"), 
                        type = "numeric", 
                        default = 50,
                        help = "Size of a window to extract the DHS values and plot. (Default: %default)", 
                        metavar = "numeric"),
  
  optparse::make_option(c("-s", "--chrom_sizes"), 
                        type = "character", 
                        default = NULL,
                        help = "A path to a file with chrom sizes. (Mandatory)", 
                        metavar = "character")
  
);

message("; Reading arguments from command line")
opt_parser = optparse::OptionParser(option_list = option_list)
opt        = optparse::parse_args(opt_parser)

########################
## Set variable names ##
########################

cobind.title     <- opt$cobind_title
cobind.input.dir <- opt$cobind_input_directory
dhs.data.dir     <- opt$dhs_data_directory
cobind.meta.path <- opt$cobind_meta
dhs.meta.path    <- opt$dhs_meta
output.dir       <- opt$output
family.name      <- opt$family_name
analysis.window  <- opt$analysis_window
chrom.sizes      <- opt$chrom_sizes

############
## Syntax ##
############

# Rscript bin/dhs_footprinting/region_analysis/analyse_regions_directly.R \
# -c HUMAN_for_DHS_0.99 \
# -i COBIND_results/individual/HUMAN_for_DHS_0.99 \
# -d /storage/mathelierarea/raw/DHS_footprinting_vierstra \
# -m data/dhs_vierstra/UB21_datasets_with_shared_biosamples.tsv \
# -n data/dhs_vierstra/DHS_datasets_with_shared_biosamples_after_COBIND.tsv \
# -o DHS_data_analysis \
# -f CTCF \
# -w 50 \
# -s /storage/mathelierarea/raw/UCSC/hg38/Sequence/WholeGenomeFasta/hg38.chrom.sizes

###########
## Debug ##
###########

# setwd("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind")
# cobind.title     <- "HUMAN_for_DHS_0.99"
# cobind.input.dir <- "COBIND_results/individual/HUMAN_for_DHS_0.99"
# dhs.data.dir     <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/raw/DHS_footprinting_vierstra"
# cobind.meta.path <- "data/dhs_vierstra/UB21_datasets_with_shared_biosamples.tsv"
# dhs.meta.path    <- "data/dhs_vierstra/DHS_datasets_with_leftover_biosamples_before_COBIND.tsv"
# output.dir       <- "DHS_data_analysis"
# family.name      <- "CTCF"
# analysis.window  <- 50
# chrom.sizes      <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/raw/UCSC/hg38/Sequence/WholeGenomeFasta/hg38.chrom.sizes"

##################################
## Defining the path to scripts ##
##################################

get.conservation        <- file.path(script.dir, "get_conservation.sh")
plot.with.motif.borders <- file.path(script.dir, "plot_conservation_facet_motif_borders.R")

#################################
## Creating output directories ##
#################################

message("; Creating output directories.")

output.dir <- file.path(output.dir, cobind.title, "not_matched_direct_regions_analysis", family.name)
dir.create(output.dir, showWarnings = F, recursive = T)

###############
## Functions ##
###############

## 1. A function to run a written command only, if the output file does not exist
skip_if_exists <- function(file_path, command) {
  
  if (file.exists(file_path)) {
    message("; File ", file_path, " already exists. Skipping.")
  } else {
    message("; ", command)
    system(command)
  }
}

## 2. Opposite to %in% operator:

'%!in%' <- function(x,y)!('%in%'(x,y))

#############################
## Reading metadata tables ##
#############################

message("; Reading metadata tables.")

cobind_meta_tab <- data.table::fread(cobind.meta.path, header = TRUE, sep = "\t")
dhs_meta_tab    <- data.table::fread(dhs.meta.path, header = TRUE, sep = "\t")

############################################
## Finding and reading the spacings table ##
############################################

message("; Reading spacings table.")

spacings.tab.path <- file.path(cobind.input.dir, "results", "clustering_results", family.name, "spacings", "spacings_extended.txt")

## Reading spacings table:
tf_spacings_tab <- fread(spacings.tab.path, header = TRUE, sep = "\t")

###################################################
## Analysing COBIND regions directly in DHS data ##
###################################################

message("; Analysing for: ", family.name)

##------------------------------------##
## Finding the regions with cobinders ##
##------------------------------------##

## Listing regions with and without co-binders:
with.cobinders.regions.dir     <- file.path(cobind.input.dir, "results", "cobinder_regions", "BED", "full_regions", 
                                            "with_cobinder_case_motif_specific", family.name)
no.cobinder.regions.file.path  <- file.path(cobind.input.dir, "results", "cobinder_regions", "BED", "full_regions", 
                                            "no_cobinder", paste0("no_cobinder_full_regions_", family.name, ".bed"))

list.of.regions.with.cobinders <- list.files(with.cobinders.regions.dir,
                                             pattern = ".bed", full.names = TRUE)

##---------------------------------------------##
## Finding a list of not matching DHS datasets ##
##---------------------------------------------##

message("; Generating a list of not matching DHS datasets.")

## Finding matching DHS datasets:
list_of_unique_tf_datasets      <- unique(tf_spacings_tab$Dataset)
tf_filtered_cobind_meta         <- cobind_meta_tab[cobind_meta_tab$dataset %in% list_of_unique_tf_datasets]
list_of_unique_tf_biosample_IDs <- unique(tf_filtered_cobind_meta$encode_biosample_term_name)
nb_of_matching_biosample_IDs    <- length(list_of_unique_tf_biosample_IDs)

## Extracting DHS datasets that do not match:
tf_filtered_dhs_meta             <- dhs_meta_tab[dhs_meta_tab$encode_biosample_term_name %!in% list_of_unique_tf_biosample_IDs]
list_of_unique_dhs_datasets      <- unique(dhs_meta_tab$dhs_dataset)
nb_of_not_matching_biosample_IDs <- length(list_of_unique_dhs_datasets)

message("; There are ", nb_of_not_matching_biosample_IDs, " DHS datasets to analyse for ", family.name, ".")
        
nb_of_datasets_to_sample <- ifelse(nb_of_not_matching_biosample_IDs > nb_of_matching_biosample_IDs,
                                   yes = nb_of_matching_biosample_IDs,
                                   no = nb_of_not_matching_biosample_IDs)

message("; Sampling ", nb_of_datasets_to_sample, " biosamples randomly.")

tf_filtered_dhs_meta <- sample_n(tf_filtered_dhs_meta, nb_of_datasets_to_sample)

##-----------------------------------------------------##
## Iterating through individual files to find overlaps ##
##-----------------------------------------------------##

for (with.cobinder.regions.file.path in list.of.regions.with.cobinders) {
    
  ## Extracting subcluster ID, spacer and location:
  splitted_basename <- unlist(strsplit(basename(with.cobinder.regions.file.path), split = "_"))
  cobinder_id       <- splitted_basename[6]
  cobinder_location <- splitted_basename[7]
  cobinder_spacer   <- gsub(splitted_basename[8], pattern = ".bed", replacement = "")
    
  message("; Analysing for cobinder ", cobinder_id, " location ", cobinder_location, " spacer ", cobinder_spacer, ".")
    
  ## Creating output directory:
  case.output.dir <- file.path(output.dir,
                               paste("cobinder", cobinder_id, cobinder_location, cobinder_spacer, sep = "_"))
  dir.create(case.output.dir, showWarnings = FALSE, recursive = TRUE)
  
  ## Filtering the spacings table:
  cobinder_case_spacings_tab <- tf_spacings_tab %>%
    filter(Subcluster    == cobinder_id,
           Location      == cobinder_location,
           Spacer_length == cobinder_spacer)
  
  # list_of_unique_cobinder_case_datasets <- unique(cobinder_case_spacings_tab$Dataset)
  # ## Extracting the biosample IDs to match with DHS metadata:
  # cobinder_case_filtered_cobind_meta         <- cobind_meta_tab[cobind_meta_tab$dataset %in% list_of_unique_cobinder_case_datasets]
  # list_of_unique_cobinder_case_biosample_IDs <- unique(cobinder_case_filtered_cobind_meta$encode_biosample_term_name)
  # nb_of_matching_biosample_IDs <- length(list_of_unique_cobinder_case_biosample_IDs)
  # ## Filtering DHS metadata based on matching biosample IDs:
  # cobinder_case_filtered_dhs_meta <- dhs_meta_tab[dhs_meta_tab$encode_biosample_term_name %!in% list_of_unique_cobinder_case_biosample_IDs]
  # list_of_unique_dhs_datasets     <- unique(dhs_meta_tab$dhs_dataset)
  # message("; There are ", length(list_of_unique_dhs_datasets), " DHS datasets to analyse for ", family.name, ".")
  # message("; Sampling 5 biosamples randomly.")
  # cobinder_case_filtered_dhs_meta <- sample_n(cobinder_case_filtered_dhs_meta, 5)
  
  cobinder_case_filtered_dhs_meta <- tf_filtered_dhs_meta
    
  ## Initiating the data frame to output numbers of regions:
  cobinder_case_numbers_of_regions_with_cobinder <- vector("list", nrow(cobinder_case_filtered_dhs_meta))
  cobinder_case_numbers_of_regions_no_cobinder   <- vector("list", nrow(cobinder_case_filtered_dhs_meta))
  
  for (i in 1:nrow(cobinder_case_filtered_dhs_meta)) {
    
    dhs_entry    <- cobinder_case_filtered_dhs_meta[i, ]
    dataset_name <- dhs_entry$dhs_dataset
    
    message("; Analysing ", dataset_name, " DHS dataset.")
    
    ## Footprint data:
    path.to.dhs.footprint.bw  <- file.path(dhs.data.dir, dataset_name,
                                           "interval.all.fpr.bw")
    
    ## Output directory for cell-line:
    cell.type.case.output.dir <- file.path(case.output.dir,
                                           paste0("cobinder_comparison_", dataset_name))
    dir.create(cell.type.case.output.dir, showWarnings = FALSE, recursive = TRUE)
    
    ## Conservation-like standart analysis:
    conservation.like.with.cobinder.table.output <- file.path(cell.type.case.output.dir,
                                                              gsub(basename(with.cobinder.regions.file.path),
                                                                   pattern = ".bed", replacement = "_conservation.tsv"))
    conservation.like.no.cobinder.table.output   <- file.path(cell.type.case.output.dir,
                                                              gsub(basename(no.cobinder.regions.file.path),
                                                                   pattern = ".bed", replacement = "_conservation.tsv"))
    
    conservation.like.plot.output <- file.path(cell.type.case.output.dir,
                                               paste0("cobinder_comparison.pdf"))
    
    conservation.like.plot.cmd    <- paste0(get.conservation,
                                            " -i ", with.cobinder.regions.file.path, ",", no.cobinder.regions.file.path,
                                            " -c ", path.to.dhs.footprint.bw,
                                            " -w ", analysis.window,
                                            " -n ", chrom.sizes,
                                            " -o ", cell.type.case.output.dir,
                                            " -s ")
    
    skip_if_exists(conservation.like.with.cobinder.table.output, conservation.like.plot.cmd)
    
    ### Plotting with motif borders:
    conservation.plot.motif.borders.cmd <- paste0("Rscript ", plot.with.motif.borders,
                                                  " -c ", conservation.like.no.cobinder.table.output, ",", conservation.like.with.cobinder.table.output,
                                                  " -s ", path.to.dhs.footprint.bw,
                                                  " -f ", dirname(spacings.tab.path),
                                                  " -o ", conservation.like.plot.output,
                                                  " -t full ",
                                                  " -b ", cobinder_id,
                                                  " -l ", cobinder_location,
                                                  " -a ", cobinder_spacer)
    
    skip_if_exists(conservation.like.plot.output, conservation.plot.motif.borders.cmd)
      
    }
}

#####################
## Combining plots ##
#####################

message("; Combining resulting conservation plots.")

combined.pdf.output.path <- file.path(output.dir, paste0(family.name, "_combined_dhs_plots.pdf"))

combine.plots.cmd <- paste0("pdfunite ",
                            paste0(output.dir, "/*/*/cobinder_comparison.pdf"),
                            " ",
                            combined.pdf.output.path)

skip_if_exists(combined.pdf.output.path, combine.plots.cmd)

###################
## End of Script ##
###################