# Co-binder analysis in DNase I hypersensitivity data #

These commands instruct how to download the DNase I hypersensitivity (DHS) data, prepare it, how to prepare data with COBIND and analyse co-binder regions within DHS footprints from matching cell types.

DHS footprints are downloaded from [https://resources.altius.org/~jvierstra/projects/footprinting.2020/](https://resources.altius.org/~jvierstra/projects/footprinting.2020/).

Working directory should be the repository's main directory `cobind_manuscript`.

This analysis is performed using a specific Snakefile `Snakefile_dhs` (associated configuration file `config_files/config_dhs.yaml`) and consists of a few main parts:

1. Matching the cell types of DHS data to UniBind 2021 datasets from human cell types. For this we used DHS metadata downloaded from a [paper](https://www.nature.com/articles/s41586-020-2528-x) and ENCODE metadata downloaded from [ENCODE](https://www.encodeproject.org/). These two tables are necessary to match the cell types between DHS data and UniBind 2021 data. The two resources include ENCODE, which allowed us to find matches. The tables are included in this repository as `data/dhs_vierstra/dataset_info.csv` and `data/dhs_vierstra/ENCODE_dhs_chipseq_metadata.tsv`. All UniBind 2021 data and metadata should be downloaded from ZENODO repository (doi: 10.5281/zenodo.7681483).
2. Running COBIND on a subset of human TFBSs with DHS footprint-matching cell type information.
3. Finding in what cell types COBIND found at least one co-binder. Regions with co-binders in these datasets will be then investigated in DHS footprints.
4. Downloading DHS data for matching cell types.
5. Analysis of  co-binder regions in DHS data.

## 1. Matching cell types ##

First, we define the input files and data directories where data is or will be downloaded.

```bash
## Creating output directory:
output_directory="data/dhs_vierstra"
mkdir -p $output_directory

## Vierstra DHS metadata:
dhs_meta_path="data/dhs_vierstra/dataset_info.csv"

## ENCODE metadata:
encode_meta_path="data/dhs_vierstra/ENCODE_dhs_chipseq_metadata.tsv"

## UB21 HUMAN metadata:
ub_meta_path="data/ub_21/ub21_human_metadata.tsv"

ub_data_dir="/storage/scratch/ieva/ub_data/ub21/ub21_human_1000seqs"
ub_original_meta_path="/scratch/ieva/ub_data/ub21/ub21_human_metadata.tsv"

ub_for_dhs_dir="data/ub_21/ub21_human_for_dhs"
ub_meta_for_dhs_path="data/ub_21/ub21_human_for_dhs_metadata.tsv"

echo "; DHS metadata: $dhs_meta_path"
echo "; ENCODE metadata: $encode_meta_path"
echo "; UniBind metadata: $ub_meta_path"
```

Then we match the cell types between DHS and UniBind data and create subsets of data to be analysed with COBIND (TFBS data) or to be downloaded (DHS footprints). For this we also used JASPAR 2020 matrix metadata to map the datasets to the proper motif names. This table is also available in the repository as `data/JASPAR2020_motifs/jaspar_matrix_info_table.csv`.

```bash
echo "; Matching cell type information between DHS and UniBind data."

## Overlapping metadata:
Rscript bin/dhs_footprinting/prep/overlap_dhs_unibind_metadata.R \
-u $ub_meta_path \
-d $dhs_meta_path \
-e $encode_meta_path \
-o $output_directory ;

ub_meta_with_shared_biosamples="$output_directory/UB21_datasets_with_shared_biosamples.tsv"
dhs_meta_with_shared_biosamples_before_COBIND="$output_directory/DHS_datasets_with_shared_biosamples_before_COBIND.tsv"
dhs_meta_with_leftover_biosamples_before_COBIND="$output_directory/DHS_datasets_with_leftover_biosamples_before_COBIND.tsv"

echo "; UniBind metadata with shared samples: $ub_meta_with_shared_biosamples"
echo "; DHS metadata with shared samples before COBIND: $dhs_meta_with_shared_biosamples_before_COBIND"

## Selecting matching datasets:
jaspar_meta_path="data/JASPAR2020_motifs/jaspar_matrix_info_table.csv"

Rscript bin/dhs_footprinting/prep/select_dhs_matching_datasets.R \
-d $ub_data_dir \
-m $ub_original_meta_path \
-o $ub_for_dhs_dir \
-t $ub_meta_for_dhs_path \
-j $jaspar_matrix_info_table \
-l HS \
-s $ub_meta_with_shared_biosamples ;
```

## 2. Running COBIND on a subset of data ##

In this part we run the main `Snakefile` to dicover co-binders only in those human TFBS datasets that have matching cell type information in DHS data. For this analysis we used the threshold that was produced with commands described in `random_analysis.md`. If you reproduced the file with thresholds, it should be in this path: `COBIND_results/random/for_threshold/thresholding_gini_results.txt` or your can use already provided output file `run_analysis/thresholding_gini_results.txt`.

```bash
echo "; Running COBIND on matching cell type data."

## Path to a file with thresholds:
gini_thr_file="run_analysis/thresholding_gini_results.txt"

## Running COBIND:
title_query="^title.*"
gini_query="^GINI_THRESHOLD:.*"
tf_groups_query="^TF_FAMILIES.*"
data_query="^BEDS_folder:.*"

groups_array=( individual )

cat "$gini_thr_file" | sed 1d | grep "HUMAN" | grep "0.99" | while read species cutoff gini rdata ; do

  echo "; Analysis for: $species with $cutoff cutoff ( $gini )."

  ## Setting the assembly based on species:
  if [ "$species" == "HUMAN" ] ; then
    assembly="human_hg38"
  else
    echo "Non valid species added to the list."
  fi ;

  echo "; Assembly: $assembly"

  ## Converting species name from lowercase to uppercase:
  species_low=$(echo "$species" | tr '[:upper:]' '[:lower:]')
  echo "; Species: "$species_low""

  ## Creating config:
  config="config_files/config_""$assembly"".yaml"
  echo "; Configuration file: "$config""

  ## Gini:
  gini_thr="GINI_THRESHOLD: ""$gini"""
  echo "; Gini: "$gini""
  sed -i 's,'"$gini_query"','"$gini_thr"',' "$config"

  ## Data:
  data_used="BEDS_folder: data/ub_21/ub21_human_for_dhs"
  echo "; Used data: "${data_used}""
  sed -i 's,'"$data_query"','"$data_used"',' "$config"

  ## Running for individual TFs and families:
  for group in "${groups_array[@]}" ; do

    echo "; Grouping: "$group""

    ## Title:
    title="title: /storage/scratch/ieva/COBIND_results/""$group""/""$species""_for_DHS_""$cutoff"""
    echo "; "$title""
    sed -i 's,'"$title_query"','"$title"',' "$config"

    ## Grouping file:
    tf_group="TF_FAMILIES: data/tfs_groups/tf_""$group""_""$species_low"".txt"
    echo "; "$tf_group""
    sed -i 's,'"$tf_groups_query"','"$tf_group"',' "$config"

    cat $config

    ## Launching snakemake command:
    echo "snakemake --config analysis_id="$assembly"  --cores 60 -k"
    snakemake --config analysis_id="$assembly"  --cores 60 -k

    ## Copying results into processed:
    cp -R /storage/scratch/ieva/COBIND_results/""$group""/""$species""_for_DHS_""$cutoff"" COBIND_results/individual

  done ;
done ;
```

## 3. Finding matching cell types ##

We further select the matching cell types. This time these are cell types, in which datasets COBIND discovered at least one co-binder motif.

```bash
echo "; Filtering DHS data based on what COBIND found."

Rscript bin/dhs_footprinting/prep/filter_dhs_datasets_after_cobind.R \
-u $ub_meta_with_shared_biosamples \
-d $dhs_meta_with_shared_biosamples_before_COBIND \
-c COBIND_results/individual/HUMAN_for_DHS_0.99 \
-o $output_directory ;

dhs_meta_with_shared_biosamples_after_COBIND="$output_directory/DHS_datasets_with_shared_biosamples_after_COBIND.tsv"
echo "; DHS metadata with shared samples after COBIND: $dhs_meta_with_shared_biosamples_after_COBIND"
```

## 4. Downloading DHS footprint data ##

To save space, we do not download all DHS footprints, but only those representing matching cell types, in which TFBS data COBIND found a co-binder.

```bash
## Output directory:
data_output_directory="data/raw/DHS_footprinting_vierstra"

mkdir -p $data_output_directory

cat $dhs_meta_with_shared_biosamples_after_COBIND | sed 1d | cut -f 1 | while read sample_name ; do

    echo "; Downloading data for sample: $sample_name "

    ## Footprints BED files:

    echo "wget -r --no-parent -nH --cut-dirs=4 -P $data_output_directory -A "interval.all.fps.*.bed" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/" ;
    wget -r --no-parent -nH --cut-dirs=4 -P $data_output_directory -A "interval.all.fps.*.bed" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/

    ## BigWig file:

    echo "wget -r --no-parent -nH --cut-dirs=4 -P data/raw/DHS_footprinting_vierstra -A "interval.all.fpr.bw" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/"
    wget -r --no-parent -nH --cut-dirs=4 -P data/raw/DHS_footprinting_vierstra -A "interval.all.fpr.bw" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/

done ;
```

## 5. Analysis of co-binder regions in DHS data ##

Finally, we used the above data and results from COBIND run to investigate regions with co-binders within DHS footprints.

```bash
title_query="^title.*"

cobind_title_query="^COBIND_TITLE:.*"
cobind_dir_query="^COBIND_DIR.*"

cobind_meta_query="^COBIND_META.*"
dhs_meta_query="^DHS_META.*"
dhs_leftover_meta_query="^DHS_LEFTOVER_META.*"

## Config file:
config="config_files/config_dhs.yaml"
echo "; Configuration file: "$config""

## Title:
title="title: DHS_data_analysis"
echo "; "$title""
sed -i 's,'"$title_query"','"$title"',' "$config"

## COBIND results:
cobind_title="COBIND_TITLE: HUMAN_for_DHS_0.99"
echo "; "$cobind_title""
sed -i 's,'"$cobind_title_query"','"$cobind_title"',' "$config"

cobind_dir="COBIND_DIR: COBIND_results/individual/HUMAN_for_DHS_0.99"
echo "; "$cobind_dir""
sed -i 's,'"$cobind_dir_query"','"$cobind_dir"',' "$config"

## Metadata:
cobind_meta="COBIND_META: ""$ub_meta_with_shared_biosamples"""
echo "; "$cobind_meta""
sed -i 's,'"$cobind_meta_query"','"$cobind_meta"',' "$config"

dhs_meta="DHS_META: ""$dhs_meta_with_shared_biosamples_after_COBIND"""
echo "; "$dhs_meta""
sed -i 's,'"$dhs_meta_query"','"$dhs_meta"',' "$config"

dhs_leftover_meta="DHS_LEFTOVER_META: ""$dhs_meta_with_leftover_biosamples_before_COBIND"""
echo "; "$dhs_leftover_meta""
sed -i 's,'"$dhs_leftover_meta_query"','"$dhs_leftover_meta"',' "$config"

## Launching snakemake command:
echo "snakemake -s Snakefile_dhs --cores 10 -k"
snakemake -s Snakefile_dhs --cores 10 -k
```
