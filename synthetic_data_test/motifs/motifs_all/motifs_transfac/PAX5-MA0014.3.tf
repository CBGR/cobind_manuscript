AC  injected_PAX5-MA0014.3
XX
ID  injected_PAX5-MA0014.3
XX
DE  MA0014.3 PAX5; from JASPAR
P0       a     c     g     t
1      540   179   578   115
2      625   148   354   285
3       18   151  1231    12
4       18  1276    34    84
5      111    35  1243    23
6       94    88    30  1200
7       16    37  1355     4
8     1243    39    70    60
9       32  1305    57    18
10      69  1081   118   144
11     377   405   312   318
12     263   403   359   387
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
