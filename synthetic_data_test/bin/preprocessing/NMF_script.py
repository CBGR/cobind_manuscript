from Bio import SeqIO
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from math import *
from sklearn.decomposition import NMF
from scipy.special import xlogy
import seqlogo
import os
import sys
import random
from scipy import stats

from pathlib import Path

###Functions to run nonnegative matrix factorisation, motif filtering and trimming for the apply_NMF python scripts

#---------------------#
# Functions
#---------------------#
### 1. ### Converting the fasta sequences into a binary array.

## Reads a fasta .fa file, and outputs the sequences as a list of strings. Lower case letters/nucleotides are converted to upper cases.
def fasta2seq(fasta_path):
	print("; Processing: ", Path(fasta_path).stem)
	records=SeqIO.parse(fasta_path, "fasta")
	return([rec.seq.upper() for rec in records])

## Transforms a nucleotide character to the corresponding one hot encoded binary array of length 4.
def nuc2onehot(nuc):
	if nuc=='A':
		return([1,0,0,0])
	elif nuc=='C':
		return([0,1,0,0])
	elif nuc=='G':
		return([0,0,1,0])
	elif nuc=='T':
		return([0,0,0,1])
	else:
		return([1,1,1,1])

## Transforms a sequence (length n) in string format to a one hot encoded binary array (length 4n)
def seq2onehot(sequence):
	onehot=[]
	for nuc in sequence:
		onehot+=nuc2onehot(nuc)
	return(onehot)

## Transforms a list of sequences to the corresponding list of one hot encoded binary arrays
def seqlist2onehot(sequence_list):
	return([seq2onehot(seq) for seq in sequence_list])


### 2. ### Converting sequences into 0 to 5 encoding for the heatmaps (if they are requested).

### For the optional heatmaps three following functions.
## Transforms the sequences into the array with encoding 0,1,2,3,4,5. This is for heatmaps.
def nuc2int(nuc):
	if nuc=='A':
		return(0)
	elif nuc=='C':
		return(1)
	elif nuc=='G':
		return(2)
	elif nuc=='T':
		return(3)
	elif nuc =='-':
		return(5)
	else:
		return(4)

def seq2int(sequence):
	return([nuc2int(nuc) for nuc in sequence])

def seqlist2int(sequence_list):
	return([seq2int(seq) for seq in sequence_list])


### 3. ### Computing the median absolute deviation of a statistical distribution.

## MAD for distribution in general (we use this in motif filtering OLD version).
def median_absolute_dev(ic_distrib):
	cumul_distrib=np.cumsum(ic_distrib)
	median=np.where(cumul_distrib>0.5)[0][0]
	absolute_deviation=np.array([np.sum(ic_distrib[max(median-k,0):min(median+k,len(ic_distrib))]) for k in range(len(ic_distrib))])
	return(np.where(absolute_deviation>0.5)[0][0])


### 4. ### Computing the minimum median window size and window interval.

# ## Returns the size (!) k of the smallest window so that sum_[i,i+k[ (ic_distrib) >= 0.5 (half of the total IC).
# ## We use it for motif filtering.
# def min_median_window_size(ic_distrib):
# 	window_size_list=[]   #list of minimal k so that sum_[i,i+k[ (ic_distrib) >= 0.5
# 	window_sum_list=[]   #list of the corresponding values
# 	for i in range(len(ic_distrib)):
# 		cumul_sums=np.array([np.sum(ic_distrib[i:i+k]) for k in range(len(ic_distrib)-i+1)])
# 		above_median_indices=np.where(cumul_sums>0.5)[0]
# 		if len(above_median_indices)==0:
# 			break
# 		min_size=above_median_indices[0]
# 		window_size_list.append(min_size)
# 	return(np.min(window_size_list))


## Returns the smallest interval bounds (left - i, and right - j)(!) so that sum_[i,j[ (ic_distrib) >= 0.5 (half of the total IC).
## If several of such intervals exist, takes the one for which the sum is the biggest.
## We use this in the following function (extended_median_window_and_trim). This is for motif filtering.
def min_median_window_interval(ic_distrib):
	window_size_list=[]   #list of minimal k so that sum_[i,i+k[ (ic_distrib) >= 0.5
	window_sum_list=[]   #list of the corresponding values
	for i in range(len(ic_distrib)):
		cumul_sums=np.array([np.sum(ic_distrib[i:i+k]) for k in range(len(ic_distrib)-i+1)])
		above_median_indices=np.where(cumul_sums>0.5)[0]
		if len(above_median_indices)==0:
			break
		min_size=above_median_indices[0]
		window_size_list.append(min_size)
		window_sum_list.append(cumul_sums[min_size])
	total_min_size=np.min(window_size_list)
	min_size_indices=np.where(window_size_list==total_min_size)[0]
	min_window_sum_list=np.array(window_sum_list)[min_size_indices]
	best_reduced_index=np.argmax(min_window_sum_list)
	best_index=min_size_indices[best_reduced_index]
	return([best_index,best_index+total_min_size])


## Doubling the size of the median window interval to trim motifs.
## This is for trimming. To make sure we do not loose information during the trimming.
def extended_median_window_and_trim(ic,threshold):
	ic_distrib=ic/np.sum(ic)
	interval=min_median_window_interval(ic_distrib)
	size=interval[1]-interval[0]
	extension=ceil(size/2)+1
	reduced_ic=ic[max(interval[0]-extension,0):min(interval[1]+extension,len(ic))]
	if len(np.where(reduced_ic>threshold)[0])==0:
		left_trim=max(interval[0]-extension,0)
		right_trim=len(ic)-min(interval[1]+extension,len(ic))
	else:
		left_trim=max(interval[0]-extension,0)+np.min(np.where(reduced_ic>threshold)[0])
		right_trim=len(ic)-min(interval[1]+extension,len(ic))+(len(reduced_ic)-1-np.max(np.where(reduced_ic>threshold)[0]))
	return(left_trim,right_trim)

def extended_quantile_window_and_trim(ic,threshold):
	ic_distrib=ic/np.sum(ic)
	interval=min_median_window_interval(ic_distrib)
	size=interval[1]-interval[0]
	extension=ceil(size/2)+1
	reduced_ic=ic[max(interval[0]-extension,0):min(interval[1]+extension,len(ic))]
	if len(np.where(reduced_ic>threshold)[0])==0:
		left_trim=max(interval[0]-extension,0)
		right_trim=len(ic)-min(interval[1]+extension,len(ic))
	else:
		left_trim=max(interval[0]-extension,0)+np.min(np.where(reduced_ic>threshold)[0])
		right_trim=len(ic)-min(interval[1]+extension,len(ic))+(len(reduced_ic)-1-np.max(np.where(reduced_ic>threshold)[0]))
	return(left_trim,right_trim)


def gini_coef(ic):
	ic_distrib=ic/np.sum(ic)
	cumsum=np.cumsum(np.sort(ic_distrib))
	gini=1-2*np.sum(cumsum)/len(ic)
	return(gini)

## Creates a random set of fasta sequences. n_sequences is the number of sequences, and l_sequence is the length of the sequences.
def make_random_fasta(n_sequences,l_sequence):
	output=[''.join(random.choice(['A','C','G','T']) for _ in range(l_sequence)) for _ in range(n_sequences)]
	return(output)

## Adds the given motif on the fasta sequences, with chosen proportion and position.
# def add_pwm(fasta,motif,proportion,position):
# 	output = list(fasta)
# 	modified_indices = []

# 	for i in range(len(output)):
# 		if random.uniform(0, 1) < proportion:
# 			modified_indices.append(i)  # Record the index of the modified sequence
# 			str_list = list(output[i])
# 			for j in range(len(motif)):
# 				str_list[position + j] = random.choices(['A', 'C', 'G', 'T'], weights=motif[j], k=1)[0]
# 			output[i] = ''.join(str_list)

# 	return output, modified_indices

def add_pwm(fasta, motif, proportion, position, n_sequences):
    output = list(fasta)
    modified_indices = []
    # Calculate the exact number of sequences to modify
    num_to_modify = int(proportion * n_sequences)
    # Randomly select indices to modify
    modified_indices = random.sample(range(len(output)), num_to_modify)
    for i in modified_indices:
        str_list = list(output[i])
        for j in range(len(motif)):
            str_list[position + j] = random.choices(['A', 'C', 'G', 'T'], weights=motif[j], k=1)[0]
        output[i] = ''.join(str_list)

    return output, modified_indices


### 5. ### Applying NMF and trimming motifs.

## Run the NMF on the sequences with the given number of components and trimming threshold.
## A summary txt file with name "<experiment_name>_f<flank_size>_c<n_components>.txt" is always created in <score_folder>.
## If motifs are found, a folder <experiment_name>/<n_components>_components with motif information is created in <output_folder>.
### Find trim values for the found motif ppms using the extended minimal median window (smallest window containing half the information content), then doubling its size, and then applying threshold trimming.

def apply_NMF_trim(output_folder,score_folder,experiment_name,raw_sequences,n_components=6,trim_threshold=0.1,core_sequences=None,full_output=False,gini_threshold=0.5,kim_park_threshold=2):
	folder_path='{}/{}'.format(output_folder,experiment_name)

	seq_lengths=[len(seq) for seq in raw_sequences] # raw_sequences here are the ones in the fasta file.
	flank_size=max(set(seq_lengths), key = seq_lengths.count)
	valid_indices=np.where(np.array(seq_lengths)==flank_size)[0]
	sequences=[raw_sequences[i] for i in valid_indices] # sequences here means valid sequences (after possible filtering) - the ones that go into NMF.


	## Checking if there are at least two sequences in  the dataset. Even though we filter before, here it is better to have it like this for de-bugging.
	## If dataset is only one sequence, -10 will be written in the score file indicating an error (bad dataset).
	if len(sequences)<=0:
		# if not os.path.exists(score_folder):
		# 	os.makedirs(score_folder)
		# np.savetxt("{}/{}_f{}_c{}.txt".format(score_folder,experiment_name,flank_size,n_components),np.transpose(np.concatenate(([-2],[-2],[-2],[-2],[-2],[-2]),axis=0)),fmt='%f',header='total_sequences component_sequences cluster_sequences total_ic median_window_size gini_coefficient')

		return([-10 for c in range(n_components)])

	sequences_onehot=seqlist2onehot(sequences) # Converting flanks to one-hot binary array.

	if core_sequences is not None:
		core_sequences_onehot=np.array(seqlist2onehot(core_sequences)) # Converting core sequence to one-hot binary array.

	print("; Processing {} sequences.".format(len(sequences)))

	## Actual NMF computation with given parameters.
	print("; Applying NMF with {} components.".format(n_components))
	model=NMF(n_components=n_components, init='random', random_state=42, max_iter=10000, verbose=0, shuffle=True)  ### NMF parameters.
	#model=NMF(n_components=n_components, init='nndsvd', max_iter=500, verbose=0, shuffle=True)
	W = model.fit_transform(sequences_onehot) # Rows are the sequences and columns are the components.
	H = model.components_ # Columns are the one-hot encoded positions (for each position we have four columns) and rows are the components.

	## Normalization of the model so that for each component the maximum total value at a position is 1.
	# print("; Normalizing the model.")
	for i in range(len(H)):
		max_value=np.max(np.sum(np.reshape(H[i],(-1,4)),axis=1))
		H[i]/=max_value
		W[:,i]*=max_value

	## Finding the component that has the strongest weight for each sequence to form the clusters -> each sequence is assigned the best component.
	## The sequences are sorted according to the value of their best weight, which will be used to improve eventual vizualization of the clusters.
	# print("; Finding the best weight for each sequence.")
	best_weight_index=np.argpartition(-W,[0])[:,0]
	index_and_score=[[best_weight_index[i],-W[i][best_weight_index[i]]] for i in range(len(sequences))]

	# print("; Sorting sequences.")
	sort_indices=np.lexsort(np.transpose(index_and_score)[::-1]) # lexsort means sorting based on best weight - taking the first best weight, then the second and so on. Different layers of sorting.
	sequences_sorted=[sequences[i] for i in sort_indices] # Using indices to sort sequences.
	W_sorted=np.array([W[i] for i in sort_indices]) # Using indices to sort W matrix.
	sequences_onehot_sorted=[sequences_onehot[i] for i in sort_indices] # Using indices to sort onehot matrix.

	seq_ic_weights=np.array([1+1/np.log(len(H))*np.sum(xlogy(W_sorted[j]/np.sum(W_sorted[j]),W_sorted[j]/np.sum(W_sorted[j]))) for j in range(len(sequences))]) # IC for each sequence using the proportion of the different weight.
	## seq_ic_weights describes how exclusive the sequence is to its associated component.
	ic_weights_median=np.median(seq_ic_weights) # Median of above.
	ic_weights_mad=stats.median_absolute_deviation(seq_ic_weights) # MAD of above.

	W_median=np.median(W,axis=None) # Median value of all values in W matrix.

	#### The clusters are filtered to keep only the sequences with the strongest belonging to the corresponding component.
	# print("; Filtering sequences in each component.")
	component_indices=[]
	cluster_indices=[]
	for i in range(len(H)):
		### Version 1 of filtering that we used in the beginning.
		## Filtering sequences whose max weight is more than 50% of total weight
		#cluster_indices.append(np.where(2*W_sorted[:,i]>=np.sum(W_sorted,axis=1))[0])

		### Version 2 of filtering.
		## Kim-Park filtering, based on the paper.
		## Ref: 10.1093/bioinformatics/btm134
		## Take sequences with main component i, where main component weight is bigger than the median of all the values in W,
		## and the ic score is bigger than the median plus 3 (this number can be changed, vary between 1 (relaxed) and 3) times median absolute deviation of all the ic scores.
		component_indices.append(np.where(W_sorted[:,i]==np.max(W_sorted,axis=1))[0])
		cluster_indices.append(np.intersect1d(component_indices[i],np.intersect1d(np.where(seq_ic_weights>ic_weights_median+kim_park_threshold*ic_weights_mad)[0],np.where(np.max(W_sorted,axis=1)>W_median)[0])))



	#### We then use total ic, number of sequences and medians absolute deviation to select the clusters with a possible co-binding site.
	### Only those clusters will have their motifs and PFMs saved.
	if full_output:
		print("; Full output script.")

		# component_heatmap_folder_path='{}/{}_components/component_heatmaps'.format(folder_path,n_components)
		# if not os.path.exists(component_heatmap_folder_path):
		# 			os.makedirs(component_heatmap_folder_path)

		logos_folder_path='{}/{}_components/logos'.format(folder_path,n_components)
		if not os.path.exists(logos_folder_path):
					os.makedirs(logos_folder_path)

		pfms_folder_path='{}/{}_components/pfms'.format(folder_path,n_components)
		if not os.path.exists(pfms_folder_path):
					os.makedirs(pfms_folder_path)

		# if not os.path.exists('{}/{}_components/summary_heatmaps'.format(folder_path,n_components)):
		# 			os.makedirs('{}/{}_components/summary_heatmaps'.format(folder_path,n_components))
	else:
		# print("; Filtering found motifs and saving most significant ones.")
		logos_folder_path = '{}/{}_components'.format(folder_path,n_components)
		pfms_folder_path = '{}/{}_components'.format(folder_path,n_components)


	found_motif=False # Initialization.
	medians=[]
	gini_coefs=[]
	info_contents=[]
	n_sequences=[]
	component_sizes=[]
	for i in range(len(H)):

		# if full_output:
		# 	fig, ax = plt.subplots()
		# 	im = ax.imshow(np.transpose(H[i].reshape(-1,4)),cmap = 'hot')
		#
		# 	ax.set_yticks(np.arange(4))
		# 	ax.set_yticklabels(['A','C','G','T'],fontsize=5)
		# 	cbar = ax.figure.colorbar(im, ax=ax)
		# 	cbar.ax.set_ylabel('values', rotation=-90, va="bottom")
		#
		# 	plt.savefig('{}/component_{}.png'.format(component_heatmap_folder_path,i))

		component_sizes.append(len(component_indices[i]))
		n_sequences.append(len(cluster_indices[i]))

		# if (len(cluster_indices[i])<42 and not full_output) or len(cluster_indices[i])<1: # When there are less than 42 sequences in the cluster (filtered set of sequences associated to the component).
		if len(cluster_indices[i])<42 or len(cluster_indices[i])<1: # When there are less than 42 sequences in the cluster (filtered set of sequences associated to the component).
			print("; Cluster ", i, " does not have enough sequences: ", len(cluster_indices[i]))
		# if (len(cluster_indices[i])<42 and not full_output) or len(cluster_indices[i])<3: # When there are less than 42 sequences in the cluster (filtered set of sequences associated to the component).
			info_contents.append(-2) # If so, -2 will be appended as IC and -2 as median (a window size) in the summary file.
			medians.append(-2)
			gini_coefs.append(-2)

		else :
			### Version 1
			## Using sequences ic score as weight to build ppms (should be chosen according to the sequence filtering version above).
			#ppm_weights=seq_ic_weights
			### Version 2 - for KIM-PARK.
			## Using same weight for all sequences
			ppm_weights=np.ones_like(seq_ic_weights)

			## Ieva:
			weighted_counts=np.sum([ppm_weights[j]*np.array(sequences_onehot_sorted[j]) for j in cluster_indices[i]],axis=0).reshape(-1,4)
			weighted_counts_sum=np.sum(weighted_counts, axis=1)
			weighted_ppm=1/weighted_counts_sum[:, None]*weighted_counts

			## Tim:
			# total_weight=np.sum([ppm_weights[j] for j in cluster_indices[i]])
			# print(total_weight)
			# weighted_ppm=1/total_weight*np.sum([ppm_weights[j]*np.array(sequences_onehot_sorted[j]) for j in cluster_indices[i]],axis=0).reshape(-1,4)
			# print(weighted_ppm)

			weighted_ppm_object=seqlogo.Ppm(weighted_ppm)
			weighted_info_cont = weighted_ppm_object.ic # IC for each position.
			weighted_total_ic=np.sum(weighted_info_cont) # computing total IC (sum of IC for each position).

			info_contents.append(weighted_total_ic)


			# if weighted_total_ic<1 and not full_output:
			if weighted_total_ic<1:
				medians.append(-1) # -1 in the NMF summary file will be appended.
				gini_coefs.append(-1)
			else:
				weighted_ic_distrib=weighted_info_cont/weighted_total_ic # We see IC as a distribution, so we divide IC of position by the sum of all IC values.
				### OLD motif filtering option:
				## Computing a median window size using median absolute deviation (sensitive to asymetry)
				#median_window_size=2*median_absolute_dev(weighted_ic_distrib)
				mwi_left,mwi_right=min_median_window_interval(weighted_ic_distrib)
				### NEW motif filtering option:
				## Computing the minimal median window size
				median_window_size=mwi_right-mwi_left

				medians.append(median_window_size)

				### Computing Gini coefficient
				gini = gini_coef(weighted_info_cont)
				gini_coefs.append(gini)

				## Trimming the found motifs, using the window. This is copied from the code within the if, so I get trimmed ppm and know the size of the motif to be able to filter by that too.
				left_trim,right_trim=extended_median_window_and_trim(weighted_info_cont,trim_threshold)
				trimmed_weighted_ppm = weighted_ppm[left_trim:len(weighted_ppm)-right_trim]
				# print(len(trimmed_weighted_ppm))
				# print(gini)

				# if (2<=median_window_size<=6 and np.mean(weighted_info_cont[mwi_left:mwi_right])>0.5) or full_output: # Choosing those motifs where the mean of IC in the window (the window should be the size between 2 and 6) is more than 0.5 (can also be adjusted from 0.5 to 1.0 (1 being most stringent)).
				# if gini>gini_threshold) or full_output: #Choosing the motifs where the gini coefficient is bigger than the threshold
				if (len(trimmed_weighted_ppm)>3) and ((gini>gini_threshold) or full_output): #Choosing the motifs where the gini coefficient is bigger than the threshold

					print("; Exporting cluster ", i, " motif. Trimmed size: ", len(trimmed_weighted_ppm), ". Gini: ", gini)
					if not os.path.exists('{}/{}_components'.format(folder_path,n_components)):
						os.makedirs('{}/{}_components'.format(folder_path,n_components))
					# print(weighted_ppm_object)
					seqlogo.seqlogo(weighted_ppm_object, ic_scale = True, format = 'png',size='xlarge', filename="{}/logo_{}_med_{}_nseq_{}_ic_{}.png".format(logos_folder_path,i,median_window_size,len(cluster_indices[i]),weighted_total_ic))

					## This is ppm before the trim.
					#np.savetxt("{}/{}_components/ppm_{}.txt".format(folder_path,n_components,i),weighted_ppm,fmt='%f',header='>Component_{}'.format(i),comments='')
					## The following line was written by Ieva
					untrimmed_weighted_pfm = len(cluster_indices[i]) * weighted_ppm
					np.savetxt("{}/pfm_untrimmed_{}.txt".format(pfms_folder_path,i),untrimmed_weighted_pfm,fmt='%f',header='>Comp_{}'.format(i),comments='')

					## Indices, if there is a need to save them into a file.
					np.savetxt("{}/indices_{}.txt".format(pfms_folder_path,i),np.transpose([valid_indices[sort_indices[cluster_indices[i]]],ppm_weights[cluster_indices[i]]]),fmt='%f')

					## Info file
					# with open("{}/{}_components/info_{}.txt".format(folder_path,n_components,i),'w+') as info_file:
						# info_file.write("{}\t{}\t{}".format(len(sequences),len(component_indices[i]),len(cluster_indices[i])))

					## The following line need to be uncommented, if heatmaps of ordered and filtered heatmaps are desired.
					#found_motif=True

					# ## Trimming the found motifs, using the window.
					# left_trim,right_trim=extended_median_window_and_trim(weighted_info_cont,trim_threshold)
					# trimmed_weighted_ppm = weighted_ppm[left_trim:len(weighted_ppm)-right_trim]

					## Adding zero IC columns on the sides of the pfm to improve clustering
					blank_size=4
					left_blank_size=blank_size
					left_blank_ppm=0.25*np.ones((left_blank_size,4))
					right_blank_size=blank_size
					right_blank_ppm=0.25*np.ones((right_blank_size,4))

					trimmed_weighted_ppm=np.concatenate([left_blank_ppm,trimmed_weighted_ppm,right_blank_ppm],axis=0)
					left_trim-=left_blank_size
					right_trim-=right_blank_size

					#Converting to a pfm using either a fixed value or the number of sequences
					# trimmed_weighted_pfm = 1000 * trimmed_weighted_ppm
					trimmed_weighted_pfm = len(cluster_indices[i]) * trimmed_weighted_ppm

					np.savetxt("{}/cobinder_pfm_{}.txt".format(pfms_folder_path,i),trimmed_weighted_pfm ,fmt='%f',header='>Comp_{}_trimmed_{}_{}'.format(i,left_trim,right_trim),comments='')



					## Recovering the core motif the same way.
					if core_sequences is not None:
						core_indices=valid_indices[sort_indices[cluster_indices[i]]]
						core_weights=ppm_weights[cluster_indices[i]]

						## Ieva:
						weighted_core_counts=np.sum([core_weights[j]*core_sequences_onehot[int(core_indices[j])] for j in range(len(core_indices))],axis=0).reshape(-1,4)
						# print(core_sequences_onehot[int(core_indices[j])] for j in range(len(core_indices)))
						weighted_core_counts_sum=np.sum(weighted_core_counts, axis=1)
						weighted_core_ppm=1/weighted_core_counts_sum[:, None]*weighted_core_counts
						# print(weighted_core_ppm)

						# ## Tim:
						# weighted_core_ppm=1/np.sum(core_weights)*np.sum([core_weights[j]*core_sequences_onehot[int(core_indices[j])] for j in range(len(core_indices))],axis=0).reshape(-1,4)

						weighted_core_ppm_object=seqlogo.Ppm(weighted_core_ppm)
						seqlogo.seqlogo(weighted_core_ppm_object, ic_scale = True, format = 'png', size='xlarge', filename="{}/core_ppm_{}.png".format(logos_folder_path,i))
						# np.savetxt("{}/{}_components/core_ppm_{}.txt".format(folder_path,n_components,i),weighted_core_ppm,fmt='%f',header='>Component_{}'.format(i),comments='')

						weighted_core_info_cont = weighted_core_ppm_object.ic
						weighted_core_total_ic=np.sum(weighted_core_info_cont)

						left_core_trim,right_core_trim=extended_median_window_and_trim(weighted_core_info_cont,trim_threshold)
						trimmed_weighted_core_ppm = weighted_core_ppm[left_core_trim:len(weighted_core_ppm)-right_core_trim]

						# trimmed_weighted_core_pfm = 1000 * trimmed_weighted_core_ppm
						trimmed_weighted_core_pfm = len(cluster_indices[i]) * trimmed_weighted_core_ppm
						np.savetxt("{}/core_pfm_{}.txt".format(pfms_folder_path,i),trimmed_weighted_core_pfm ,fmt='%f',header='>Comp_{}_trimmed_{}_{}'.format(i,left_core_trim,right_core_trim),comments='')

						untrimmed_weighted_core_pfm = len(cluster_indices[i]) * weighted_core_ppm
						# np.savetxt("{}/untrimmed_core_pfm_{}.txt".format(pfms_folder_path,i),untrimmed_weighted_core_pfm ,fmt='%f',header='>Comp_{}'.format(i),comments='')

				else:
					print("; Motif of cluster ", i,  " is less that 3bp long or Gini coefficient is too low (", len(trimmed_weighted_ppm), ", ", gini, ")")

	## Saving the scores for each NMF run into the score folder.
	if not os.path.exists(score_folder):
		os.makedirs(score_folder)
	np.savetxt("{}/{}_f{}_c{}.txt".format(score_folder,experiment_name,flank_size,n_components),np.transpose(np.concatenate(([[len(sequences) for i in range(len(H))]],[component_sizes],[n_sequences],[info_contents],[medians],[gini_coefs]),axis=0)),fmt='%f',header='total_sequences component_sequences cluster_sequences total_ic median_window_size gini_coefficient')


	## This option outputs summary plots of the matrix factorization in case an interesting motif was found.
	## A specific line above needs to be uncommented in order this to work when full output is false.
	# if found_motif or full_output:
	# 	all_clusters=np.concatenate(cluster_indices)
	# 	sequences_filtered=[sequences_sorted[i] for i in all_clusters]
	# 	W_filtered=[W_sorted[i] for i in all_clusters]
	#
	# 	fig, ax = plt.subplots(1,2,figsize=(5,len(sequences)/100))
	# 	im = ax[0].imshow(seqlist2int(sequences_sorted),cmap = matplotlib.colors.ListedColormap(['green', 'blue','yellow', 'red']))
	#
	# 	cbar = ax[0].figure.colorbar(im, ax=ax[0])
	# 	cbar.ax.set_ylabel('Nucleotide', rotation=-90, va="bottom")
	# 	cbar.set_ticks((np.arange(4)+0.5)*3/4)
	# 	cbar.set_ticklabels(['A','C','G','T'])
	#
	# 	im2 = ax[1].imshow(W_sorted,cmap = 'hot')
	# 	cbar = ax[1].figure.colorbar(im2, ax=ax[1])
	# 	cbar.ax.set_ylabel('Weights', rotation=-90, va="bottom")
	# 	cbar.ax.tick_params(labelsize=5	)
	#
	# 	print("; Saving heatmaps for ordered and filtered sequences.")
	# 	plt.savefig("{}/{}_components/ordered_sequences.png".format(folder_path,n_components),dpi=100)
	#
	# 	fig, ax = plt.subplots(1,2,figsize=(5,len(all_clusters)/60))
	# 	im = ax[0].imshow(seqlist2int(sequences_filtered),cmap = matplotlib.colors.ListedColormap(['green', 'blue','yellow', 'red']))
	#
	# 	cbar = ax[0].figure.colorbar(im, ax=ax[0])
	# 	cbar.ax.set_ylabel('Nucleotide', rotation=-90, va="bottom")
	# 	cbar.set_ticks((np.arange(4)+0.5)*3/4)
	# 	cbar.set_ticklabels(['A','C','G','T'])
	#
	# 	im2 = ax[1].imshow(W_filtered,cmap = 'hot')
	# 	cbar = ax[1].figure.colorbar(im2, ax=ax[1])
	# 	cbar.ax.set_ylabel('Weights', rotation=-90, va="bottom")
	# 	cbar.ax.tick_params(labelsize=5	)
	#
	# 	plt.savefig("{}/{}_components/filtered_sequences.png".format(folder_path,n_components),dpi=300)
	# 	plt.close('all')
	return(medians)
