#!/bin/bash

####################
## Metadata paths ##
####################

## Changing directories and creating output directory:
cd /storage/mathelierarea/processed/ieva/projects/COBIND/pipeline_cobind

output_directory="data/dhs_vierstra"
mkdir -p $output_directory

## Vierstra DHS metadata:
dhs_meta_path="data/dhs_vierstra/dataset_info.csv"

## ENCODE metadata:
encode_meta_path="data/dhs_vierstra/ENCODE_dhs_chipseq_metadata.tsv"

## UB21 HUMAN metadata:
ub_meta_path="data/ub_21/ub21_human_metadata.tsv"

ub_data_dir="/storage/scratch/ieva/ub_data/ub21/ub21_human_1000seqs"
ub_original_meta_path="/scratch/ieva/ub_data/ub21/ub21_human_metadata.tsv"

ub_for_dhs_dir="data/ub_21/ub21_human_for_dhs"
ub_meta_for_dhs_path="data/ub_21/ub21_human_for_dhs_metadata.tsv"

## JASPAR metadata path:
jaspar_meta_path="data/JASPAR2020_motifs/jaspar_matrix_info_table.csv"


echo "; DHS metadata: $dhs_meta_path"
echo "; ENCODE metadata: $encode_meta_path"
echo "; UniBind metadata: $ub_meta_path"

##############################################################
## Match cell type information between DHS and UniBind data ##
##############################################################

echo "; Matching cell type information between DHS and UniBind data."

Rscript bin/dhs_footprinting/prep/overlap_dhs_unibind_metadata.R \
-u $ub_meta_path \
-d $dhs_meta_path \
-e $encode_meta_path \
-o $output_directory ;

ub_meta_with_shared_biosamples="$output_directory/UB21_datasets_with_shared_biosamples.tsv"
dhs_meta_with_shared_biosamples_before_COBIND="$output_directory/DHS_datasets_with_shared_biosamples_before_COBIND.tsv"

echo "; UniBind metadata with shared samples: $ub_meta_with_shared_biosamples"
echo "; DHS metadata with shared samples before COBIND: $dhs_meta_with_shared_biosamples_before_COBIND"

########################################################
## Subsetting UniBind data and running COBIND on them ##
########################################################

echo "; Running COBIND on matching cell type data."

Rscript bin/dhs_footprinting/prep/select_dhs_matching_datasets.R \
-d $ub_data_dir \
-m $ub_original_meta_path \
-o $ub_for_dhs_dir \
-t $ub_meta_for_dhs_path \
-j $jaspar_matrix_info_table \
-l HS \
-s $ub_meta_with_shared_biosamples ;

## Running COBIND:

title_query="^title.*"
gini_query="^GINI_THRESHOLD:.*"
tf_groups_query="^TF_FAMILIES.*"

groups_array=( individual )

cat COBIND_results/manuscript_random/for_threshold/threshold_gini_results.txt | sed 1d | grep "HUMAN" | grep "0.99" | while read species cutoff gini rdata ; do

  echo "; Analysis for: $species with $cutoff cutoff ( $gini )."

  ## Setting the assembly based on species:
  if [ "$species" == "HUMAN" ] ; then
    assembly="human_hg38"

  else
    echo "Non valid species added to the list."
  fi ;

  echo "; Assembly: $assembly"

  ## Converting species name from lowercase to uppercase:
  species_low=$(echo "$species" | tr '[:upper:]' '[:lower:]')
  echo "; Species: "$species_low""

  ## Creating config:
  config="config_files/config_""$assembly"".yaml"
  echo "; Configuration file: "$config""

  ## Gini:
  gini_thr="GINI_THRESHOLD: ""$gini"""
  echo "; Gini: "$gini""
  sed -i 's,'"$gini_query"','"$gini_thr"',' "$config"

  ## Running for individual TFs and families:
  for group in "${groups_array[@]}" ; do

    echo "; Grouping: "$group""

    ## Title:
    title="title: COBIND_results/""$group""/""$species""_for_DHS_""$cutoff"""
    echo "; "$title""
    sed -i 's,'"$title_query"','"$title"',' "$config"

    ## Grouping file:
    tf_group="TF_FAMILIES: data/tfs_groups/tf_""$group""_""$species_low"".txt"
    echo "; "$tf_group""
    sed -i 's,'"$tf_groups_query"','"$tf_group"',' "$config"

    # cat $config

    ## Launching snakemake command:
    echo "snakemake --config analysis_id="$assembly"  --cores 10 -k"
    # snakemake --config analysis_id="$assembly"  --cores 60 -k --unlock
    # snakemake --config analysis_id="$assembly"  --cores 60 -k --rerun-incomplete
    snakemake --config analysis_id="$assembly"  --cores 10 -k

  done ;

done ;

######################################################################
## Selecting only those cell types for which COBIND found something ##
######################################################################

echo "; Filtering DHS data based on what COBIND found."

Rscript bin/dhs_footprinting/filter_dhs_datasets_after_cobind.R \
-u $ub_meta_with_shared_biosamples \
-d $dhs_meta_with_shared_biosamples_before_COBIND \
-c COBIND_results/individual/HUMAN_for_DHS_0.99 \
-o $output_directory ;

dhs_meta_with_shared_biosamples_after_COBIND="$output_directory/DHS_datasets_with_shared_biosamples_after_COBIND.tsv"
echo "; DHS metadata with shared samples after COBIND: $dhs_meta_with_shared_biosamples_after_COBIND"

dhs_meta_with_shared_biosamples_leftovers="$output_directory/DHS_datasets_with_leftover_biosamples_before_COBIND.tsv"

########################################
## Downloading only the relevant data ##
########################################

data_output_directory="/storage/mathelierarea/raw/DHS_footprinting_vierstra"

mkdir -p $data_output_directory

cat $dhs_meta_with_shared_biosamples_after_COBIND | sed 1d | cut -f 1 | while read sample_name ; do

    echo "; Downloading data for sample: $sample_name "

    ## Footprints BED files:

    echo "wget -r --no-parent -nH --cut-dirs=4 -P $data_output_directory -A "interval.all.fps.*.bed" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/" ;
    wget -r --no-parent -nH --cut-dirs=4 -P $data_output_directory -A "interval.all.fps.*.bed" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/

    ## BigWig file:

    echo "wget -r --no-parent -nH --cut-dirs=4 -P /storage/mathelierarea/raw/DHS_footprinting_vierstra -A "interval.all.fpr.bw" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/"
    wget -r --no-parent -nH --cut-dirs=4 -P /storage/mathelierarea/raw/DHS_footprinting_vierstra -A "interval.all.fpr.bw" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/

    ## BAM file:

    echo "wget -r --no-parent -nH --cut-dirs=4 -P /storage/mathelierarea/raw/DHS_footprinting_vierstra -A "reads.ba*" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/"
    wget -r --no-parent -nH --cut-dirs=4 -P /storage/mathelierarea/raw/DHS_footprinting_vierstra -A "reads.ba*" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/

done ;

########################################
## Downloading only the relevant data ##
########################################

data_output_directory="/storage/mathelierarea/raw/DHS_footprinting_vierstra"

mkdir -p $data_output_directory

cat $dhs_meta_with_shared_biosamples_after_COBIND | sed 1d | cut -f 1 | while read sample_name ; do

    echo "; Downloading data for sample: $sample_name "

    ## Footprints BED files:

    echo "wget -r --no-parent -nH --cut-dirs=4 -P $data_output_directory -A "interval.all.fps.*.bed" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/" ;
    wget -r --no-parent -nH --cut-dirs=4 -P $data_output_directory -A "interval.all.fps.*.bed" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/

    ## BigWig file:

    echo "wget -r --no-parent -nH --cut-dirs=4 -P /storage/mathelierarea/raw/DHS_footprinting_vierstra -A "interval.all.fpr.bw" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/"
    wget -r --no-parent -nH --cut-dirs=4 -P /storage/mathelierarea/raw/DHS_footprinting_vierstra -A "interval.all.fpr.bw" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/

    ## BAM file:

    echo "wget -r --no-parent -nH --cut-dirs=4 -P /storage/mathelierarea/raw/DHS_footprinting_vierstra -A "reads.ba*" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/"
    wget -r --no-parent -nH --cut-dirs=4 -P /storage/mathelierarea/raw/DHS_footprinting_vierstra -A "reads.ba*" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/

done ;


###################################
## Downloading the leftover data ##
###################################

data_output_directory="/storage/mathelierarea/raw/DHS_footprinting_vierstra_not_matching_cell_types"

mkdir -p $data_output_directory

cat $dhs_meta_with_shared_biosamples_leftovers | sed 1d | cut -f 1 | while read sample_name ; do

    echo "; Downloading data for sample: $sample_name "

    ## BigWig file:

    echo "wget -r --no-parent -nH --cut-dirs=4 -P /storage/mathelierarea/raw/DHS_footprinting_vierstra -A "interval.all.fpr.bw" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/"
    wget -r --no-parent -nH --cut-dirs=4 -P /storage/mathelierarea/raw/DHS_footprinting_vierstra -A "interval.all.fpr.bw" https://resources.altius.org/~jvierstra/projects/footprinting.2020/per.dataset/$sample_name/


done ;


###################
## End of Script ##
###################
