---
title: "COBIND comparison with other tools, including MCOT"
output: 
  pdf_document:
    keep_md: true
header-includes:
- \usepackage{caption}
- \captionsetup[figure]{labelformat=empty}
---





![]( /storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/runtime_summary_for_review/max_runtime_summary_lineplot_proportion_across_motifs.pdf ) 
 ***Figure R1.***   **Run time comparison on simulated data.**   The figure reports the running time (y-axis) of the different tools (see legend) when applied to different numbers of sequences (columns) with distinct proportions of injected motif instances (see facets).

\newpage


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/ATF4-MA0833.2/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R2.*** **Comparisons between COBIND and other tools on simulated data.** We ran COBIND, MCOT, STREME, RSAT-dyads, RSAT-oligos, RSAT-positions, and SPAMO (subfacets) on simulated data containing different numbers of sequences (from 800 to 80,000, see facets) with inserted JASPAR MA0833.2 TF binding profile for ATF4 (see Methods for details). We considered (i) F1 scores and Matthew’s correlation coefficient (MCC) (in case multiple correct motifs are found, a motif with highest F1 score is visualized). (ii) the number of incorrect motifs discovered in each experiment. Good performance of a tool would be indicated by a discovered motif with high F1 score and high MCC and no incorrectly discovered motifs.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/CEBPB-MA0466.2/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R3.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0466.2 TF binding profile for CEBPB.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/CTCF-MA0139.1/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R4.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0139.1 TF binding profile for CTCF.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/Foxo1-MA0480.1/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R5.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0480.1 TF binding profile for Foxo1.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/GATA2-MA0036.3/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R6.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0036.3 TF binding profile for GATA2.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/HOXA1-MA1495.1/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R7.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA1495.1 TF binding profile for HOXA1.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/HOXB13-MA0901.2/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R8.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0901.2 TF binding profile for HOXB13.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/MEIS1-MA0498.2/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R9.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0498.2 TF binding profile for MEIS1.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/PAX5-MA0014.3/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R10.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0014.3 TF binding profile for PAX5.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/POU2F1-MA0785.1/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R11.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0785.1 TF binding profile for POU2F1.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/SOX2-MA0143.4/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R12.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0143.4 TF binding profile for SOX2.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/TAL1-MA0140.2/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R13.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0140.2 TF binding profile for TAL1.


![](/storage/mathelierarea/processed/ieva/projects/COBIND/cobind_manuscript/COBIND_results/synthetic_data_experiment_results/comparison_with_other_tools/TBX21-MA0690.1/comparison_with_others_for_review/injected_vs_all_discovered.pdf)
***Figure R14.*** **Comparisons between COBIND and other tools on simulated data.** Same as Figure R2, but with instances of the JASPAR MA0690.1 TF binding profile for TBX21.
