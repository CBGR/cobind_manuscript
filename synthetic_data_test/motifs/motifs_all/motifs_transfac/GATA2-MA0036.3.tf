AC  injected_GATA2-MA0036.3
XX
ID  injected_GATA2-MA0036.3
XX
DE  MA0036.3 GATA2; from JASPAR
P0       a     c     g     t
1     3303  2978  3201  4731
2     1986  4099  2966  5162
3      940 10607  1223  1443
4      444   259   150 13360
5      417    34    21 13741
6    14098    14    43    58
7      145    77    48 13943
8       96 13786   126   205
9     2373   492   367 10981
10    2912  3714  3656  3931
11    3278  3874  2216  4845
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
