import os
import sys
import glob
### Reads a table with only one motif and creates the files in the output folder to make the spacings table.

# Syntax : python cluster_one_motif <table_path> <output_folder>

#Output folder is the path of the output as given to the clustering script. It contains the prefix of the files in the clustering folder.

convert_matrix = os.path.join(os.getenv('COBIND_CLUSTERING'), "convert-matrix")


def create_output_files(table_path,output_folder):
	with open(table_path) as table_file:
		motif_line = table_file.readline().rstrip().split("\t")
	motif_path = motif_line[0]
	motif_name = motif_line[1]

	with open(motif_path) as motif_file:
		lines = motif_file.readlines()
		header = lines[0].rstrip()[1:]
		motif_length = len(lines) - 1

	os.makedirs(output_folder+"_tables",exist_ok = True)
	os.makedirs(output_folder+"_aligned_logos",exist_ok = True)


	with open(output_folder+"_tables/clusters.tab",'w') as clusters_table:
		clusters_table.write("cluster_1	{}_m1_{}".format(motif_name,header))
	with open(output_folder+"_tables/alignment_table.tab",'w') as alignment_table:
		alignment_table.write("#id	name	cluster strand	offset_up	offset_down	width	aligned_consensus	aligned_consensus_rc\n")
		alignment_table.write("{}_m1_{}	{}	cluster_1	D	0	0	{}	N	N".format(motif_name,header,header,motif_length))

	# os.system("convert-matrix -from cluster-buster -to tf -i {} -return counts > {}_aligned_logos/{}_m1_{}_aligned.tf".format(motif_path,output_folder,motif_name,header))
	# os.system("convert-matrix -from cluster-buster -to tf -i {} -return counts -rc > {}_aligned_logos/{}_m1_{}_aligned_rc.tf".format(motif_path,output_folder,motif_name,header))

	os.system("{} -from cluster-buster -to tf -i {} -return counts > {}_aligned_logos/{}_m1_{}_aligned.tf".format(convert_matrix, motif_path,output_folder,motif_name,header))
	os.system("{} -from cluster-buster -to tf -i {} -return counts -rc > {}_aligned_logos/{}_m1_{}_aligned_rc.tf".format(convert_matrix,motif_path,output_folder,motif_name,header))


if __name__=='__main__':
	arguments=sys.argv

	table_path=arguments[1]
	output_folder=arguments[2]

	create_output_files(table_path,output_folder)
