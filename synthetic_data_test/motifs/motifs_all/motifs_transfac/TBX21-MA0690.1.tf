AC  injected_TBX21-MA0690.1
XX
ID  injected_TBX21-MA0690.1
XX
DE  MA0690.1 TBX21; from JASPAR
P0       a     c     g     t
1     2322   263   937   872
2     4395    30   359   146
3      612   239  4395   398
4        2    17  4395    33
5        6   180    16  4395
6        1     0  4395     2
7      185   331    34  4395
8       80   126  4395   364
9     4395     0    38     0
10    4395   441   317  1287
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
