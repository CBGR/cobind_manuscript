AC  injected_CEBPB-MA0466.2
XX
ID  injected_CEBPB-MA0466.2
XX
DE  MA0466.2 CEBPB; from JASPAR
P0       a     c     g     t
1    10585  5007  6282   859
2       33    34     8 22732
3       17    14  2276 22732
4    10930    34 11803  2237
5      393 22732   221  2217
6     3521   555 22732   486
7     2527 22732     3  2135
8    22732   636     9     0
9    22732     9     0     9
10     155 10617   940 12116
XX
CC  program: jaspar
CC  matrix.nb: 1
XX
//
