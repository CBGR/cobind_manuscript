# COBIND analysis on UniBind 2022 data #

These commands instruct how to perform main analysis of UniBind 2021 data. The analysis consists of two main parts that must be run in a sequence:

A. Species-specific analysis
B. Interspecies analysis

Root output directory is the repository's directory.

Note that number of cores provided to Snakemake is 10.

## A. Species specific analysis ##

This is the main analysis, where `Snakefile` is used.

For this analysis you will need Gini coefficient thresholds that are species-specific. They can be generated following instruction in `random_analysis.md` (in this case the path to the file will be `COBIND_results/random/for_threshold/thresholding_gini_results.txt`) or using already provided output file `run_analysis/thresholding_gini_results.txt`. Be sure to provide a correct path bellow. We use 0.99 cutoff Gini threshold (0.95 is also available in the provided file).

```bash
## Path to a file with thresholds:
gini_thr_file="run_analysis/thresholding_gini_results.txt"

## Queries:
title_query="^title.*"
gini_query="^GINI_THRESHOLD:.*"
tf_groups_query="^TF_FAMILIES.*"

## Groups to itterate:
groups_array=( individual families )

## Reading the variables and running analysis:
cat "$gini_thr_file" | sed 1d | grep "0.99" | while read species cutoff gini rdata ; do

  echo "; Analysis for: $species with $cutoff cutoff ( $gini )."

  ## Setting the assembly based on species:
  if [ "$species" == "ARABIDOPSIS" ]; then
    assembly="arabidopsis_TAIR10"
  elif [ "$species" == "CELEGANS" ]; then
    assembly="celegans_WBcel235"
  elif [ "$species" == "DRERIO" ] ; then
    assembly="drerio_GRCz11"
  elif [ "$species" == "DROSOPHILA" ] ; then
    assembly="drosophila_dm6"
  elif [ "$species" == "HUMAN" ] ; then
    assembly="human_hg38"
  elif [ "$species" == "MOUSE" ] ; then
    assembly="mouse_mm10"
  elif [ "$species" == "RAT" ] ; then
    assembly="rat_Rnor_6.0"
  else
    echo "Non valid species added to the list."
  fi ;

  echo "; Assembly: $assembly" ;

  ## Converting species name from lowercase to uppercase:
  species_low=$(echo "$species" | tr '[:upper:]' '[:lower:]')
  echo "; Species: "$species_low""

  ## Creating config:
  config="config_files/config_""$assembly"".yaml"
  echo "; Configuration file: "$config""

  ## Gini:
  gini_thr="GINI_THRESHOLD: ""$gini"""
  echo "; Gini: "$gini""
  sed -i 's,'"$gini_query"','"$gini_thr"',' "$config"

  ## Running for individual TFs and families:
  for group in "${groups_array[@]}" ; do

    echo "; Grouping: "$group""

    ## Title:
    title="title: COBIND_results/""$group""/""$species""_""$cutoff"""
    echo "; "$title""
    sed -i 's,'"$title_query"','"$title"',' "$config"

    ## Grouping file:
    tf_group="TF_FAMILIES: data/tfs_groups/tf_""$group""_""$species_low"".txt"
    echo "; "$tf_group""
    sed -i 's,'"$tf_groups_query"','"$tf_group"',' "$config"

    ## Launching snakemake command:
    echo "snakemake --config analysis_id="$assembly"  --cores 10 -k"
    snakemake --config analysis_id="$assembly"  --cores 10 -k

  done ;
done ;
```

## B. Interspecies analysis ##

To run this analysis you must have the output of species-specific analysis in **A.**.

For this analysis a specific Snakefile `Snakefile_interspecies` is used.

```bash
## Queries:
interspecies_title_query="^title.*"
runs_ids_query="^RUNS_IDS.*"
analysis_titles_query="^ANALYSIS_TITLES.*"
base_dir="COBIND_results"

## Config:
config_interspecies="config_files/config_interspecies.yaml"
echo "; Configuration file: "$config_interspecies""

## Cutoffs to run analysis:
cutoffs_array=( 0.99 )

## Grouping to run analysis:
groups_array=( individual families )

for cutoff in "${cutoffs_array[@]}" ; do

  echo "; Gathering and summarizing for cutoff "$cutoff""

  for group in "${groups_array[@]}" ; do

    echo "; Analysing "$group" runs." 

    ## Title:
    interspecies_title="title: "$base_dir"/interspecies/""$group""_""$cutoff"""
    echo "; "$interspecies_title""
    sed -i 's,'"$interspecies_title_query"','"$interspecies_title"',' "$config_interspecies"

    ## Runs IDs:
    runs_ids="RUNS_IDS:\ [""$base_dir""/""$group""/ARABIDOPSIS_""$cutoff""\\,""$base_dir""/""$group""/CELEGANS_""$cutoff""\\,""$base_dir""/""$group""/DRERIO_""$cutoff""\\,""$base_dir""/""$group""/DROSOPHILA_""$cutoff""\\,""$base_dir""/""$group""/HUMAN_""$cutoff""\\,""$base_dir""/""$group""/MOUSE_""$cutoff""\\,""$base_dir""/""$group""/RAT_""$cutoff""]"
    echo "; Runs IDs: "$runs_ids""
    sed -i 's,'"$runs_ids_query"','"$runs_ids"',' "$config_interspecies"

    ## Analysis titles:
    analysis_titles="ANALYSIS_TITLES:\ ""$base_dir""/""$group""/ARABIDOPSIS_""$cutoff""\\,""$base_dir""/""$group""/CELEGANS_""$cutoff""\\,""$base_dir""/""$group""/DRERIO_""$cutoff""\\,""$base_dir""/""$group""/DROSOPHILA_""$cutoff""\\,""$base_dir""/""$group""/HUMAN_""$cutoff""\\,""$base_dir""/""$group""/MOUSE_""$cutoff""\\,""$base_dir""/""$group""/RAT_""$cutoff"""
    echo "; Analysis titles: "$analysis_titles""
    sed -i 's,'"$analysis_titles_query"','"$analysis_titles"',' "$config_interspecies"

    ## Running the pipeline:
    echo "snakemake -s Snakefile_interspecies --cores 10 -k"
    snakemake -s Snakefile_interspecies --cores 10 -k

  done ;
done ;
```
