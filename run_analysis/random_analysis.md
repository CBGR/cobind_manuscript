# Determining Gini threshold #

These commands instruct how to determine Gini coefficient threshold for each species. For this analysis a Snakefile `Snakefile_random_experiment` is used and its associated configuration files with extension `*_random.yaml`.

The most important output file in this analysis is `thresholding_gini_results.txt`, which contains all thresholds and is later used in the main analysis `unibind_analysis.md`. If you want to skip this step, The file is provided in the same directory as this, so you can provide this path when running commands in `unibind_analysis.md`.

Root output directory is the repository's directory.

```bash
species_array=( ARABIDOPSIS CELEGANS DRERIO DROSOPHILA HUMAN MOUSE RAT )

query="^title.*"
iterations_query="^RANDOM_ITERATIONS:.*"
data_query="^BEDS_folder:.*"
biasaway_query="^BIASAWAY_K.*"

for species in "${species_array[@]}"
do
  echo "; Analysis for: "$species""

  ## Setting the assembly based on species:
  if [ "$species" == "ARABIDOPSIS" ];
  then
    assembly="arabidopsis_TAIR10"
  elif [ "$species" == "CELEGANS" ];
  then
    assembly="celegans_WBcel235"
  elif [ "$species" == "DRERIO" ] ;
  then
    assembly="drerio_GRCz11"
  elif [ "$species" == "DROSOPHILA" ] ;
  then
    assembly="drosophila_dm6"
  elif [ "$species" == "HUMAN" ] ;
  then
    assembly="human_hg38"
  elif [ "$species" == "MOUSE" ] ;
  then
    assembly="mouse_mm10"
  elif [ "$species" == "RAT" ] ;
  then
    assembly="rat_Rnor_6.0"
  else
    echo "Non valid species added to the list."
  fi ;

  echo "; Assembly: $assembly" ;

  ## Config variable:
  config="config_files/config_""$assembly""_random.yaml"
  echo "; Configuration file: "$config""

  ## Title:
  title="title: COBIND_results/random/for_threshold/""$species""_random"
  echo "; "$title""
  sed -i 's,'"$query"','"$title"',' "$config"

  ## Changing the repeats:
  iterations="RANDOM_ITERATIONS: [r1]"
  echo "; "$iterations""
  sed -i ""s/"$iterations_query"/"$iterations"/"" "$config"
  
  ## Setting biasaway k value to 1:
  biasaway_thr="BIASAWAY_K: 1"
  echo "; "$biasaway_thr""
  sed -i 's,'"$biasaway_query"','"$biasaway_thr"',' "$config"

  ## Launching snakemake command:
  echo "snakemake -s Snakefile_random_experiment --config analysis_id="$assembly"  --cores 90"
  snakemake -s Snakefile_random_experiment --config analysis_id="$assembly"  --cores 90

done ;

## Summarizing Gini:
Rscript bin/main/gini_analysis/summarize_random_gini_thresholds.R -i COBIND_results/random/for_threshold -o COBIND_results/random/for_threshold

## Mixture model:
Rscript bin/main/gini_analysis/mixture_model.R -t COBIND_results/random/for_threshold/ARABIDOPSIS_random,COBIND_results/random/for_threshold/CELEGANS_random,COBIND_results/random/for_threshold/DRERIO_random,COBIND_results/random/for_threshold/DROSOPHILA_random,COBIND_results/random/for_threshold/HUMAN_random,COBIND_results/random/for_threshold/MOUSE_random,COBIND_results/random/for_threshold/RAT_random -o COBIND_results/random/for_threshold/mixture_model
```
