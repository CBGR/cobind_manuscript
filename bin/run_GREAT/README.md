## Run GREAT analysis

The script runs GREAT [1] analysis on genomic regions in a format of BED or narrowPeak. GREAT assigns the input regions to the nearby genes and reports various gene ontologies (GO). This tool uses rGREAT package [2]. All ontologies are returned as .tsv files and top 20 are plotted and exported as .pdf files (see output files).


[1] Cory Y McLean, Dave Bristor, Michael Hiller, Shoa L Clarke, Bruce T Schaar, Craig B Lowe, Aaron M Wenger, and Gill Bejerano. "GREAT improves functional interpretation of cis-regulatory regions". Nat. Biotechnol. 28(5):495-501, 2010. PMID 20436461
[2] Gu Z (2020). rGREAT: Client for GREAT Analysis. https://github.com/jokergoo/rGREAT, http://great.stanford.edu/public/html/.

### Software requirements:

- rGREAT R package
- ggplot2 R package
- tidyverse R package
- data.table R package
- optparse R package

### Arguments:

Description of all the arguments from the tool.

Mandatory arguments:

```
-p PEAK_FILE:     input BED or narrowPeak file with coordinates of interest.
-o OUTPUT_FOLDER: output directory to output analysis results (if not exist, this will be created by the script).
```

Optional arguments:

```
-g GENOME_VERSION: version of the genome. By default this is "hg38".
```

### Example

Provide at least one example of the tool.

```
Rscript R-scripts/run_GREAT_analysis.R -p regions_of_interest.bed -o GREAT_output -g hg38
``` 
