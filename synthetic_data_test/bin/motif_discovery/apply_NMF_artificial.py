import sys
from NMF_script import *

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

matplotlib.use('Agg')

### Run NMF on an artificial dataset made with the given motif and parameters.
### The motif file should be a tab delimited ppm table where each line corresponds to a position and the 4 colums contain the probability of each nucleotide at that position

### Syntax : python apply_NMF_artificial <motif_path> <n_sequences> <n_nucleotides> <motif_proportion> <motif_position> <output_folder> <score_folder> <experiment_name> <n_components> <trim_threshold>
## Real example: python python-scripts/apply_NMF_artificial.py motifs/archive/motif.txt 5000 32 0.01 5 Artificial_test Artificial_test test_5000_32_0.01_5 15 0

### A summary txt file with name "<experiment_name>_f<flank_size>_c<n_components>.txt" is always created in <score_folder>.
## Columns in the summary file are: number of sequences, total information content (IC of 32 nucleotides) and the window size.

### A folder <experiment_name>/<n_components>_components with motif information is created in <output_folder>.



#---------------------#
# Running the script.
#---------------------#

## Running the script with arguments.
if __name__=='__main__':
	arguments=sys.argv


	motif_path=arguments[1]
	n_sequences=int(arguments[2])
	n_nucleotides=int(arguments[3])
	motif_proportion=float(arguments[4])
	motif_position=int(arguments[5])

	output_folder=arguments[6]
	score_folder=arguments[7]
	experiment_name=arguments[8]
	n_components=int(arguments[9])
	trim_threshold=float(arguments[10])

	motif=np.loadtxt(motif_path)
	random_sequences = make_random_fasta(n_sequences,n_nucleotides)
	motif_sequences, modified_indices = add_pwm(random_sequences,motif,motif_proportion,motif_position)

	os.makedirs('{}/{}/{}_components'.format(output_folder,experiment_name,n_components),exist_ok = True)
	
	apply_NMF_trim(output_folder,score_folder,experiment_name,motif_sequences,n_components,trim_threshold,full_output=False)

	SeqIO.write([SeqRecord(Seq(motif_sequences[i]),str(i)) for i in range(len(motif_sequences))], '{}/{}/{}_components/sequences.fa'.format(output_folder,experiment_name,n_components), "fasta")

	indices_output_file_path = '{}/{}/{}_components/modified_indices.txt'.format(output_folder,experiment_name,n_components)
	with open(indices_output_file_path, 'w') as file:
		for index in modified_indices:
			file.write(str(index) + '\n')


## End of script.
